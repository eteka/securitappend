-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 03 mai 2020 à 21:22
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `secu`
--

-- --------------------------------------------------------

--
-- Structure de la table `biens`
--

DROP TABLE IF EXISTS `biens`;
CREATE TABLE IF NOT EXISTS `biens` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mode_acquisition_id` bigint(20) NOT NULL DEFAULT '0',
  `typebien_id` bigint(20) NOT NULL,
  `lieu_achat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marque_id` bigint(20) NOT NULL DEFAULT '0',
  `modele_id` bigint(20) NOT NULL DEFAULT '0',
  `visibilite` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PRIVATE',
  `vendeur` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_achat` date DEFAULT NULL,
  `imei1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imei2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_id` bigint(20) DEFAULT NULL,
  `devise_id` int(5) DEFAULT NULL,
  `prix_achat` bigint(20) DEFAULT '0',
  `infos` longtext COLLATE utf8mb4_unicode_ci,
  `user_id` bigint(20) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `photo` text COLLATE utf8mb4_unicode_ci,
  `photo_mini` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=122 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `biens`
--

INSERT INTO `biens` (`id`, `nom`, `mode_acquisition_id`, `typebien_id`, `lieu_achat`, `marque_id`, `modele_id`, `visibilite`, `vendeur`, `date_achat`, `imei1`, `imei2`, `media_id`, `devise_id`, `prix_achat`, `infos`, `user_id`, `deleted_at`, `created_at`, `updated_at`, `photo`, `photo_mini`) VALUES
(1, 'Moto Gros corps Haojue 150-A', 1, 0, 'Paris', 0, 0, 'Portable', '2015-02-15', '2015-02-15', '454454454', '5477787878', NULL, NULL, 680000, '', 3, NULL, '2020-03-31 01:20:08', '2020-03-31 01:20:08', '', NULL),
(2, 'Portable', 1, 0, 'Paris', 0, 0, 'PRIVE', '2015-02-15', '2015-02-15', '454454454', '5477787878', NULL, NULL, 80000, '', 3, NULL, '2020-03-31 01:22:05', '2020-03-31 01:22:05', '', NULL),
(3, 'Portable', 1, 24, 'Paris', 13, 16, 'PUBLIC', '2015-02-15', '2015-02-15', '454454454', '5477787878', NULL, 3, 15000, NULL, 3, NULL, '2020-03-31 01:28:47', '2020-04-27 15:56:51', '', NULL),
(4, 'HUawei Y9 Prim', 1, 0, 'dzkzdk', 0, 0, 'PRIVE', NULL, '1970-01-01', NULL, NULL, NULL, NULL, 150000, NULL, 3, NULL, '2020-03-31 08:37:41', '2020-03-31 08:37:41', '', NULL),
(5, 'Nokia 3045', 1, 0, 'Boutique Allo-Mobile de Porto-Novo', 0, 0, 'PRIVE', '2017-02-14', '2017-02-14', '545454455454', NULL, NULL, NULL, 25000, 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Est enim ad esse cumque delectus accusantium, consequuntur aspernatur unde perferendis, quas sit iste! Voluptatum, ea aspernatur odit officia autem consequuntur porro!Lorem ipsum dolor sit amet consectetur, adipisicing elit. Est enim ad esse cumque delectus accusantium, consequuntur aspernatur unde perferendis, quas sit iste! Voluptatum, ea aspernatur odit officia autem consequuntur porro!Lorem ipsum dolor sit amet consectetur, adipisicing elit. Est enim ad esse cumque delectus accusantium, consequuntur aspernatur unde perferendis, quas sit iste! Voluptatum, ea aspernatur odit officia autem consequuntur porro!\r\n', 3, NULL, '2020-03-31 15:38:28', '2020-03-31 15:38:28', '', NULL),
(6, 'Comment réussir', 1, 0, 'Parakou', 0, 0, 'PRIVE', '2020-03-31', '2020-03-31', '545454455454', '5477787878', NULL, NULL, 2500, 'Livre de culture personnel et d\'accomplissement de soir', 3, NULL, '2020-03-31 16:24:00', '2020-03-31 16:24:00', 'storage/cd1/medias/1585675440.jpg', NULL),
(7, 'Portable NOKIA', 8, 18, 'Parakou', 11, 9, 'PRIVE', 'Vendeur', '1989-04-01', '1245877777', NULL, 41, NULL, 150000, 'I have defined the following two Models:\r\n\r\nQuestion.php:\r\n\r\n<?php\r\n\r\nclass Question extends \\Eloquent\r\n{\r\n    protected $table = \'questions\';\r\n    protected $guarded = [\'id\'];\r\n\r\n    public function answers()\r\n\r\n    {\r\n\r\n        return $this->hasMany(\'Answer\');\r\n\r\n    }\r\n}\r\nAnswer.php:\r\n\r\n<?php\r\n\r\nclass Answer extends \\Eloquent\r\n{\r\n    protected $table = \'answers\';\r\n    protected $guarded = [\'id\'];\r\n\r\n    public function question()\r\n    {\r\n        return $this->belongsTo(\'Question\');\r\n    }\r\n}\r\nAnd I\'m trying to insert data to the Pivot table using this:\r\n\r\n$answer = Answer::creat', 3, NULL, '2020-03-31 16:28:47', '2020-04-17 00:01:12', 'storage/cd1/medias/1585675727.jpg', NULL),
(8, 'Smartephone', 1, 0, 'Parakou, Bénin', 0, 0, 'PRIVE', NULL, '1970-01-01', NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:35:43', '2020-04-04 20:54:26', '2020-04-16 17:35:43', 'storage/cd1/medias/1586037265.png', NULL),
(9, 'Divant super', 0, 0, 'Parakou', 0, 0, 'PRIVE', '2020-02-14', '2020-02-14', '545454455454', NULL, NULL, NULL, 0, 'Voilà un Bon achat', 3, '2020-04-16 17:33:22', '2020-04-04 20:59:03', '2020-04-16 17:33:22', '', NULL),
(10, 'Matériel information', 2, 8, 'Cotonou', 12, 4, 'PRIVE', '2020-02-01', '2020-02-01', '44555455', NULL, NULL, NULL, 80000, 'Mars', 3, NULL, '2020-04-07 12:27:47', '2020-04-10 14:32:11', NULL, NULL),
(11, 'Smartephone Huawei Y9', 1, 1, 'Parakou', 1, 4, 'PRIVE', NULL, NULL, '55556', '6556565s', NULL, NULL, 150000, 'Smartphone de qualité supérieure.', 3, NULL, '2020-04-07 22:48:56', '2020-04-09 21:00:08', NULL, NULL),
(12, 'z', 1, 0, 'zddzdz', 0, 0, 'PRIVE', NULL, '1970-01-01', NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:32:51', '2020-04-08 22:46:00', '2020-04-16 17:32:51', NULL, NULL),
(13, 's', 1, 0, 'ss', 0, 0, 'PRIVE', NULL, '1970-01-01', NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:33:10', '2020-04-08 22:46:35', '2020-04-16 17:33:10', NULL, NULL),
(14, 'Disque dur', 1, 9, 'Savè', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-08 22:46:54', '2020-04-09 21:38:37', NULL, NULL),
(15, 'zdzdzd', 1, 0, 'zdzzdz', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:33:05', '2020-04-08 23:08:25', '2020-04-16 17:33:05', NULL, NULL),
(16, 'zdlkdzlk', 3, 0, 'Paris', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:33:15', '2020-04-08 23:09:50', '2020-04-16 17:33:15', NULL, NULL),
(17, 'Appareil Photo Canon', 1, 1, 'Parakou', 5, 7, 'PRIVE', '2020-03-15', '2020-03-15', NULL, NULL, 1, NULL, 450000, 'Achat en ligne FNAC', 3, NULL, '2020-04-09 13:21:29', '2020-04-09 13:21:29', 'storage/cd1/medias/1586442088.jpg', 'storage/cd1/medias/thumbnail/1586442088.jpg'),
(18, 'TRAVEL CHARGER A5', 1, 1, '125', 11, 5, 'PRIVE', '2020-03-15', '2020-03-15', NULL, NULL, 2, NULL, 15000, 'Made in China', 3, NULL, '2020-04-09 13:35:31', '2020-04-09 21:35:52', NULL, NULL),
(19, 'kjjkjkkj', 1, 1, 'hjhjhj', 0, 1, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-09 22:28:02', '2020-04-09 22:44:04', 'storage/cd1/medias/1586475842.png', 'storage/cd1/medias/thumbnail/1586475842.png'),
(20, 'Bien 1', 1, 1, 'Parakou', 0, 0, 'PRIVE', NULL, NULL, '142545555555', NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 14:45:03', '2020-04-10 14:45:03', NULL, NULL),
(21, 'Téléphone Samsung S10', 1, 26, 'Paris', 13, 6, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, 'Cool', 3, NULL, '2020-04-10 15:03:47', '2020-04-19 23:04:45', 'storage/cd1/medias/1586534625.png', 'storage/cd1/medias/thumbnail/1586534625.png'),
(22, 'Armoire métalique', 2, 24, 'pppp', 14, 7, 'PRIVE', NULL, NULL, '522225', NULL, NULL, NULL, 50000, NULL, 3, NULL, '2020-04-10 15:21:28', '2020-04-19 23:17:29', 'storage/cd1/medias/1586535687.jpg', 'storage/cd1/medias/thumbnail/1586535687.jpg'),
(23, 'jkjkdzjkdzj', 3, 24, 'kdkjejkjke', 0, 0, 'PRIVE', NULL, NULL, ',dz,kdz', NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 15:21:59', '2020-04-10 15:21:59', 'storage/cd1/medias/1586535718.png', 'storage/cd1/medias/thumbnail/1586535718.png'),
(24, 'kjdzjkzdjk', 1, 24, 'dzkjdzjdzj', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 15:22:48', '2020-04-10 15:22:48', NULL, NULL),
(25, 'dzjkzdkj', 1, 30, 'kjdzkjzdkj', 0, 0, 'PRIVE', NULL, NULL, 'jzdjdzj', NULL, 10, NULL, 0, NULL, 3, NULL, '2020-04-10 15:23:09', '2020-04-10 15:23:09', 'storage/cd1/medias/1586535789.png', 'storage/cd1/medias/thumbnail/1586535789.png'),
(26, 'dzjkzkjd', 2, 24, 'zkkzjkjz', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, 11, NULL, 0, NULL, 3, NULL, '2020-04-10 15:31:45', '2020-04-10 15:31:45', 'storage/cd1/medias/1586536305.jpg', 'storage/cd1/medias/thumbnail/1586536305.jpg'),
(27, 'Save', 5, 24, 'kldkjlejd', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, 12, NULL, 0, NULL, 3, NULL, '2020-04-10 15:41:50', '2020-04-10 15:41:50', 'storage/cd1/medias/1586536909.jpg', 'storage/cd1/medias/thumbnail/1586536909.jpg'),
(28, 'jkeékjzkj', 1, 4, 'efjkefjk', 0, 0, 'PRIVE', NULL, NULL, 'zdklkdzl', NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 16:56:27', '2020-04-10 16:56:27', NULL, NULL),
(29, 'jkeékjzkj', 1, 4, 'efjkefjk', 0, 0, 'PRIVE', NULL, NULL, 'zdklkdzl', NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 16:56:50', '2020-04-10 16:56:50', NULL, NULL),
(30, 'jkeékjzkj', 1, 4, 'efjkefjk', 0, 0, 'PRIVE', NULL, NULL, 'zdklkdzl', NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 16:56:57', '2020-04-10 16:56:57', NULL, NULL),
(31, 'jkeékjzkj', 1, 4, 'efjkefjk', 0, 0, 'PRIVE', NULL, NULL, 'zdklkdzl', NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 16:58:42', '2020-04-10 16:58:42', NULL, NULL),
(32, 'jkeékjzkj', 1, 4, 'efjkefjk', 0, 0, 'PRIVE', NULL, NULL, 'zdklkdzl', NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 16:58:52', '2020-04-10 16:58:52', NULL, NULL),
(33, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 16:59:18', '2020-04-10 16:59:18', NULL, NULL),
(34, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 16:59:28', '2020-04-10 16:59:28', NULL, NULL),
(35, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 16:59:50', '2020-04-10 16:59:50', NULL, NULL),
(36, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:00:12', '2020-04-10 17:00:12', NULL, NULL),
(37, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:02:13', '2020-04-10 17:02:13', NULL, NULL),
(38, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:03:01', '2020-04-10 17:03:01', NULL, NULL),
(39, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:03:49', '2020-04-10 17:03:49', NULL, NULL),
(40, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:04:30', '2020-04-10 17:04:30', NULL, NULL),
(41, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:13:33', '2020-04-10 17:13:33', NULL, NULL),
(42, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:26:01', '2020-04-10 17:26:01', NULL, NULL),
(43, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:26:19', '2020-04-10 17:26:19', NULL, NULL),
(44, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:29:17', '2020-04-10 17:29:17', NULL, NULL),
(45, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:29:27', '2020-04-10 17:29:27', NULL, NULL),
(46, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:30:00', '2020-04-10 17:30:00', NULL, NULL),
(47, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:30:41', '2020-04-10 17:30:41', NULL, NULL),
(48, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:30:56', '2020-04-10 17:30:56', NULL, NULL),
(49, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:31:14', '2020-04-10 17:31:14', NULL, NULL),
(50, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:31:59', '2020-04-10 17:31:59', NULL, NULL),
(51, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:44:51', '2020-04-10 17:44:51', NULL, NULL),
(52, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:44:59', '2020-04-10 17:44:59', NULL, NULL),
(53, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:45:23', '2020-04-10 17:45:23', NULL, NULL),
(54, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:45:33', '2020-04-10 17:45:33', NULL, NULL),
(55, 'Suivant', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:45:52', '2020-04-19 18:18:16', NULL, NULL),
(56, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:45:57', '2020-04-10 17:45:57', NULL, NULL),
(57, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:46:22', '2020-04-10 17:46:22', NULL, NULL),
(58, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:46:28', '2020-04-10 17:46:28', NULL, NULL),
(59, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:48:08', '2020-04-10 17:48:08', NULL, NULL),
(60, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:48:28', '2020-04-10 17:48:28', NULL, NULL),
(61, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 17:48:39', '2020-04-10 17:48:39', NULL, NULL),
(62, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 18:40:17', '2020-04-10 18:40:17', NULL, NULL),
(63, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 18:41:01', '2020-04-10 18:41:01', NULL, NULL),
(64, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 18:41:27', '2020-04-10 18:41:27', NULL, NULL),
(65, 'YAMAHA 50', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 18:43:00', '2020-04-19 18:12:40', NULL, NULL),
(66, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 18:43:26', '2020-04-10 18:43:26', NULL, NULL),
(67, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 18:54:51', '2020-04-10 18:54:51', NULL, NULL),
(68, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 18:56:34', '2020-04-10 18:56:34', NULL, NULL),
(69, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 18:56:55', '2020-04-10 18:56:55', NULL, NULL),
(70, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 18:59:40', '2020-04-10 18:59:40', NULL, NULL),
(71, 'Récent', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 19:02:20', '2020-04-19 18:17:22', NULL, NULL),
(72, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 19:03:11', '2020-04-10 19:03:11', NULL, NULL),
(73, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 19:03:29', '2020-04-10 19:03:29', NULL, NULL),
(74, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 19:03:44', '2020-04-10 19:03:44', NULL, NULL),
(75, 'Vélo VTT', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, NULL, '2020-04-10 19:05:06', '2020-04-19 18:12:20', NULL, NULL),
(76, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-18 02:42:47', '2020-04-10 19:05:43', '2020-04-18 02:42:47', NULL, NULL),
(77, 'Hoofer Jack - Kawasaki - SONAEC - Dopel - Nokia - SOBEBRA', 8, 2, 'Londre', 17, 12, 'PRIVE', 'MArcial DAGBA - le Tontinier de la fois dernière qui font du bon travail au Bénin et ailleurs', '2018-02-01', '2214545454', NULL, 48, 3, 14000000, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\n      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 3, NULL, '2020-04-10 19:05:58', '2020-04-27 15:57:30', NULL, NULL),
(78, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-18 02:41:30', '2020-04-10 19:06:30', '2020-04-18 02:41:30', NULL, NULL),
(79, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-18 02:34:15', '2020-04-10 19:11:03', '2020-04-18 02:34:15', NULL, NULL),
(80, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-18 15:30:29', '2020-04-10 19:11:24', '2020-04-18 15:30:29', NULL, NULL),
(81, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-18 02:42:30', '2020-04-10 19:13:32', '2020-04-18 02:42:30', NULL, NULL),
(82, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-18 00:42:04', '2020-04-10 19:16:25', '2020-04-18 00:42:04', NULL, NULL),
(83, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-18 00:41:57', '2020-04-10 19:17:07', '2020-04-18 00:41:57', NULL, NULL),
(84, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-18 00:41:51', '2020-04-10 19:17:17', '2020-04-18 00:41:51', NULL, NULL),
(85, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-18 00:41:39', '2020-04-10 19:17:22', '2020-04-18 00:41:39', NULL, NULL),
(86, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:32:29', '2020-04-10 19:18:19', '2020-04-16 17:32:29', NULL, NULL),
(87, 'Sound Limk Mimi', 1, 4, 'New York', 15, 13, 'PRIVE', NULL, NULL, NULL, NULL, NULL, 3, 14, NULL, 3, NULL, '2020-04-10 19:19:19', '2020-04-23 23:10:34', NULL, NULL),
(88, 'Machine à laver', 1, 4, 'Dogbo', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, 40, NULL, 0, NULL, 3, NULL, '2020-04-10 19:20:22', '2020-04-17 23:52:59', NULL, NULL),
(89, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:31:58', '2020-04-10 19:21:36', '2020-04-16 17:31:58', NULL, NULL),
(90, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:31:54', '2020-04-10 19:22:48', '2020-04-16 17:31:54', NULL, NULL),
(91, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:31:49', '2020-04-10 19:24:03', '2020-04-16 17:31:49', NULL, NULL),
(92, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:31:45', '2020-04-10 19:28:11', '2020-04-16 17:31:45', NULL, NULL),
(93, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:31:40', '2020-04-10 19:28:33', '2020-04-16 17:31:40', NULL, NULL),
(94, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:31:36', '2020-04-10 19:28:46', '2020-04-16 17:31:36', NULL, NULL),
(95, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:31:33', '2020-04-10 19:28:52', '2020-04-16 17:31:33', NULL, NULL),
(96, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:31:16', '2020-04-10 19:31:27', '2020-04-16 17:31:16', NULL, NULL),
(97, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:31:12', '2020-04-10 19:37:01', '2020-04-16 17:31:12', NULL, NULL),
(98, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:31:09', '2020-04-10 19:37:08', '2020-04-16 17:31:09', NULL, NULL),
(99, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:31:04', '2020-04-10 19:37:19', '2020-04-16 17:31:04', NULL, NULL),
(100, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:31:00', '2020-04-10 19:37:35', '2020-04-16 17:31:00', NULL, NULL),
(101, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:30:56', '2020-04-10 19:39:21', '2020-04-16 17:30:56', NULL, NULL),
(102, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:30:52', '2020-04-10 19:39:29', '2020-04-16 17:30:52', NULL, NULL),
(103, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:30:48', '2020-04-10 19:40:36', '2020-04-16 17:30:48', NULL, NULL),
(104, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:30:44', '2020-04-10 19:40:50', '2020-04-16 17:30:44', NULL, NULL),
(105, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:30:41', '2020-04-10 19:41:14', '2020-04-16 17:30:41', NULL, NULL),
(106, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:31:28', '2020-04-10 19:42:21', '2020-04-16 17:31:28', NULL, NULL),
(107, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:30:31', '2020-04-10 19:46:45', '2020-04-16 17:30:31', NULL, NULL),
(108, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:30:37', '2020-04-10 19:47:07', '2020-04-16 17:30:37', NULL, NULL),
(109, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:30:19', '2020-04-10 19:47:39', '2020-04-16 17:30:19', NULL, NULL),
(110, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:30:13', '2020-04-10 19:51:54', '2020-04-16 17:30:13', NULL, NULL),
(111, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:29:06', '2020-04-10 19:52:10', '2020-04-16 17:29:06', NULL, NULL),
(112, 'kdzkjzd', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 3, '2020-04-16 17:28:59', '2020-04-10 19:52:55', '2020-04-16 17:28:59', NULL, NULL),
(113, 'Microphone', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, 31, NULL, 0, NULL, 3, '2020-04-16 17:31:24', '2020-04-10 19:53:55', '2020-04-16 17:31:24', NULL, NULL),
(114, 'Marteau', 1, 4, '5zde', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, 39, NULL, 0, NULL, 3, '2020-04-16 17:30:24', '2020-04-10 19:55:11', '2020-04-16 17:30:24', NULL, NULL),
(115, 'Iphone 5', 1, 27, 'Los Angeles', 15, 10, 'PRIVE', NULL, '2018-03-15', NULL, NULL, 35, NULL, 0, NULL, 3, NULL, '2020-04-10 19:57:34', '2020-04-17 14:53:08', NULL, NULL),
(116, 'Huawei Y9', 1, 4, 'Prakou', 1, 4, 'PRIVE', 'Clovis - Devant la gendarmerie de Parakou', '2020-01-15', '1542555556', NULL, 38, NULL, 150000, 'RAM 4\r\nMémoire Interne: 128 Go\r\nCaméra 16 Mpx\r\nTriple Caméra', 3, NULL, '2020-04-10 19:58:04', '2020-04-18 09:05:16', NULL, NULL),
(117, 'Moto deux roue Hauju 2018', 1, 4, 'Paris Saint Germain', 13, 6, 'PRIVE', NULL, NULL, '5656565656565', NULL, 37, NULL, 60000, 'Origine super idéal avec une superposition trop cool pour le vin de pal', 3, NULL, '2020-04-10 20:06:44', '2020-04-19 23:10:55', NULL, NULL),
(118, 'Disque dure HP 2To', 8, 4, 'Parakou', 18, 14, 'PRIVE', 'Brice WANONKOU', NULL, '55665565656565', 'deeffe', 43, 3, 150000, 'Amouse', 3, NULL, '2020-04-19 13:20:13', '2020-04-23 22:41:39', NULL, NULL),
(119, 'Machine à Laver', 1, 4, '4555554', 0, 0, 'PRIVE', NULL, NULL, '545454', 'mlklkl', NULL, 3, 360000, NULL, 3, NULL, '2020-04-23 22:21:30', '2020-04-23 22:42:48', NULL, NULL),
(120, 'Véhicule PORSCH Cayen', 5, 12, 'dzzdzdzd', 19, 15, 'PRIVE', NULL, NULL, '45556565zdlmzdl', NULL, 49, 3, 152000, NULL, 3, NULL, '2020-04-24 02:25:16', '2020-04-24 02:28:07', NULL, NULL),
(121, 'kjjjkj', 1, 30, 'jkjkjkjkjjkkjjk', 0, 0, 'PRIVE', NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 3, NULL, '2020-04-24 03:16:31', '2020-04-24 03:16:31', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `cours`
--

DROP TABLE IF EXISTS `cours`;
CREATE TABLE IF NOT EXISTS `cours` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `titre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enseignants` text COLLATE utf8mb4_unicode_ci,
  `assistants` text COLLATE utf8mb4_unicode_ci,
  `fichier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `annee_acad` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `faculte` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filiere` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `domaine` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mention` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grade` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ecu` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cours_id_index` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=146 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `cours`
--

INSERT INTO `cours` (`id`, `titre`, `enseignants`, `assistants`, `fichier`, `annee_acad`, `faculte`, `filiere`, `domaine`, `mention`, `grade`, `ecu`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Comptabilite generale 1', '', '', 'files/cours/FASEG/cours_up_comptabilite-generale-1.pdf', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:37', '2020-04-27 18:02:37'),
(2, 'Cours de demographie l1 faseg up 2020', '', '', 'files/cours/FASEG/cours_up_cours-de-demographie-l1-faseg-up-2020.pdf', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:37', '2020-04-27 18:02:37'),
(3, 'Cours de demographie l2 faseg up 2020', '', '', 'files/cours/FASEG/cours_up_cours-de-demographie-l2-faseg-up-2020.pdf', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:37', '2020-04-27 18:02:37'),
(4, 'Cours de management de la qualite et innovation', '', '', 'files/cours/FASEG/cours_up_cours-de-management-de-la-qualite-et-innovation.pdf', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:37', '2020-04-27 18:02:37'),
(5, 'Cours economie de letat l3 semestre 5 2019 2020 rev 1 13 04 2020', '', '', 'files/cours/FASEG/cours_up_cours-economie-de-letat-l3-semestre-5-2019-2020-rev-1-13-04-2020.pdf', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:37', '2020-04-27 18:02:37'),
(6, 'Cours fondements de leco inter l2 semestre 3 2019 2020', '', '', 'files/cours/FASEG/cours_up_cours-fondements-de-leco-inter-l2-semestre-3-2019-2020.pdf', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:37', '2020-04-27 18:02:37'),
(7, 'Cours fondements de leco inter l2 semestre 3 2019 20201', '', '', 'files/cours/FASEG/cours_up_cours-fondements-de-leco-inter-l2-semestre-3-2019-20201.pdf', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:37', '2020-04-27 18:02:37'),
(8, 'Cours gestion commerciale 19 20', '', '', 'files/cours/FASEG/cours_up_cours-gestion-commerciale-19-20.pdf', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:37', '2020-04-27 18:02:37'),
(9, 'Cours gestion commerciale 19 20', '', '', 'files/cours/FASEG/cours_up_cours-gestion-commerciale-19-20.ppt', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:37', '2020-04-27 18:02:37'),
(10, 'Cours methodologie licence 3 economie faseg up 2019 2020 rev 13 04 2020', '', '', 'files/cours/FASEG/cours_up_cours-methodologie-licence-3-economie-faseg-up-2019-2020-rev-13-04-2020.pdf', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:37', '2020-04-27 18:02:37'),
(11, 'Cours sur etude de cas l3 2020 toutes options1', '', '', 'files/cours/FASEG/cours_up_cours-sur-etude-de-cas-l3-2020-toutes-options1.pdf', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:37', '2020-04-27 18:02:37'),
(12, 'Cours theorie et pratique de lintegration semestre 5 2019 2020', '', '', 'files/cours/FASEG/cours_up_cours-theorie-et-pratique-de-lintegration-semestre-5-2019-2020.pdf', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:37', '2020-04-27 18:02:37'),
(13, 'Cours gestion commerciale 19 20', '', '', 'files/cours/FASEG/cours_up_cours_gestion_commerciale_19-20.ppt', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:37', '2020-04-27 18:02:37'),
(14, 'Comptabilite generale 1', '', '', 'files/cours/FASEG/cours_up_comptabilite-generale-1.pdf', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:38', '2020-04-27 18:02:38'),
(15, 'Cours de demographie l1 faseg up 2020', '', '', 'files/cours/FASEG/cours_up_cours-de-demographie-l1-faseg-up-2020.pdf', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:38', '2020-04-27 18:02:38'),
(16, 'Cours de demographie l2 faseg up 2020', '', '', 'files/cours/FASEG/cours_up_cours-de-demographie-l2-faseg-up-2020.pdf', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:38', '2020-04-27 18:02:38'),
(17, 'Cours de management de la qualite et innovation', '', '', 'files/cours/FASEG/cours_up_cours-de-management-de-la-qualite-et-innovation.pdf', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:38', '2020-04-27 18:02:38'),
(18, 'Cours economie de letat l3 semestre 5 2019 2020 rev 1 13 04 2020', '', '', 'files/cours/FASEG/cours_up_cours-economie-de-letat-l3-semestre-5-2019-2020-rev-1-13-04-2020.pdf', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:38', '2020-04-27 18:02:38'),
(19, 'Cours fondements de leco inter l2 semestre 3 2019 2020', '', '', 'files/cours/FASEG/cours_up_cours-fondements-de-leco-inter-l2-semestre-3-2019-2020.pdf', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:38', '2020-04-27 18:02:38'),
(20, 'Cours fondements de leco inter l2 semestre 3 2019 20201', '', '', 'files/cours/FASEG/cours_up_cours-fondements-de-leco-inter-l2-semestre-3-2019-20201.pdf', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:38', '2020-04-27 18:02:38'),
(21, 'Cours gestion commerciale 19 20', '', '', 'files/cours/FASEG/cours_up_cours-gestion-commerciale-19-20.pdf', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:38', '2020-04-27 18:02:38'),
(22, 'Cours gestion commerciale 19 20', '', '', 'files/cours/FASEG/cours_up_cours-gestion-commerciale-19-20.ppt', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:38', '2020-04-27 18:02:38'),
(23, 'Cours methodologie licence 3 economie faseg up 2019 2020 rev 13 04 2020', '', '', 'files/cours/FASEG/cours_up_cours-methodologie-licence-3-economie-faseg-up-2019-2020-rev-13-04-2020.pdf', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:38', '2020-04-27 18:02:38'),
(24, 'Cours sur etude de cas l3 2020 toutes options1', '', '', 'files/cours/FASEG/cours_up_cours-sur-etude-de-cas-l3-2020-toutes-options1.pdf', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:38', '2020-04-27 18:02:38'),
(25, 'Cours theorie et pratique de lintegration semestre 5 2019 2020', '', '', 'files/cours/FASEG/cours_up_cours-theorie-et-pratique-de-lintegration-semestre-5-2019-2020.pdf', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:38', '2020-04-27 18:02:38'),
(26, 'Cours gestion commerciale 19 20', '', '', 'files/cours/FASEG/cours_up_cours_gestion_commerciale_19-20.ppt', '2019-2020', '8', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:02:38', '2020-04-27 18:02:38'),
(27, 'L2 spri analyse quantitative adedodja', '', '', 'files/cours/FDSP/cours_up_l2-spri-analyse-quantitative-adedodja.pdf', '2019-2020', '5', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:04:47', '2020-04-27 18:04:47'),
(28, 'L2 spri droit inter public hountondji', '', '', 'files/cours/FDSP/cours_up_l2-spri-droit-inter-public-hountondji.pdf', '2019-2020', '5', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:04:47', '2020-04-27 18:04:47'),
(29, 'L2 spri into macroeconomie bah sabi', '', '', 'files/cours/FDSP/cours_up_l2-spri-into-macroeconomie-bah-sabi.pdf', '2019-2020', '5', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:04:47', '2020-04-27 18:04:47'),
(30, 'L3 spri decentralisation et developpement nanako', '', '', 'files/cours/FDSP/cours_up_l3-spri-decentralisation-et-developpement-nanako.pdf', '2019-2020', '5', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:04:47', '2020-04-27 18:04:47'),
(31, 'L3 spri droit enjeux problemes ahlinvi adjagbe', '', '', 'files/cours/FDSP/cours_up_l3-spri-droit-enjeux-problemes-ahlinvi-adjagbe.pdf', '2019-2020', '5', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:04:47', '2020-04-27 18:04:47'),
(32, 'L3 spri systeme politique americain kakpovi', '', '', 'files/cours/FDSP/cours_up_l3-spri-systeme-politique-americain-kakpovi.pdf', '2019-2020', '5', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:04:47', '2020-04-27 18:04:47'),
(33, 'Lic 2 droit public intro droit decentralisation nanako et gandonou', '', '', 'files/cours/FDSP/cours_up_lic-2-droit-public-intro-droit-decentralisation-nanako-et-gandonou.pdf', '2019-2020', '5', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:04:47', '2020-04-27 18:04:47'),
(34, 'Lic 3 droit patrimonial de la famille agossou clautaire', '', '', 'files/cours/FDSP/cours_up_lic-3-droit-patrimonial-de-la-famille-agossou-clautaire.pdf', '2019-2020', '5', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:04:47', '2020-04-27 18:04:47'),
(35, 'Lic 3 publicprive droit de la securite sociale adamou t yafradou', '', '', 'files/cours/FDSP/cours_up_lic-3-publicprive-droit-de-la-securite-sociale-adamou-t-yafradou.pdf', '2019-2020', '5', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:04:47', '2020-04-27 18:04:47'),
(36, 'Cours psychologie medicale historique', '', '', 'files/cours/IFSIO/cours_up_cours-psychologie-medicale-historique.pdf', '2019-2020', '10', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:07:56', '2020-04-27 18:07:56'),
(37, 'Cours psychologie medicale le malade et le milieu hospitalier', '', '', 'files/cours/IFSIO/cours_up_cours-psychologie-medicale-le-malade-et-le-milieu-hospitalier.pdf', '2019-2020', '10', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:07:56', '2020-04-27 18:07:56'),
(38, 'Cours psychologie medicale tognon l1', '', '', 'files/cours/IFSIO/cours_up_cours-psychologie-medicale-tognon-l1.pdf', '2019-2020', '10', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:07:56', '2020-04-27 18:07:56'),
(39, 'Cours soins infirmiers en pediatriesip', '', '', 'files/cours/IFSIO/cours_up_cours-soins-infirmiers-en-pediatriesip.docx', '2019-2020', '10', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:07:56', '2020-04-27 18:07:56'),
(40, 'Cours sur les brulures', '', '', 'files/cours/IFSIO/cours_up_cours-sur-les-brulures.pptx', '2019-2020', '10', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:07:56', '2020-04-27 18:07:56'),
(41, 'Extension continue', '', '', 'files/cours/IFSIO/cours_up_extension-continue.docx', '2019-2020', '10', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:07:56', '2020-04-27 18:07:56'),
(42, 'Fecondation', '', '', 'files/cours/IFSIO/cours_up_fecondation.pdf', '2019-2020', '10', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:07:56', '2020-04-27 18:07:56'),
(43, 'Ifsio1 internet', '', '', 'files/cours/IFSIO/cours_up_ifsio1-internet.docx', '2019-2020', '10', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:07:56', '2020-04-27 18:07:56'),
(44, 'Insulinotherapie ide2 ifsio', '', '', 'files/cours/IFSIO/cours_up_insulinotherapie-ide2-ifsio.pptx', '2019-2020', '10', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:07:56', '2020-04-27 18:07:56'),
(45, 'Les avortements', '', '', 'files/cours/IFSIO/cours_up_les-avortements.pdf', '2019-2020', '10', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:07:56', '2020-04-27 18:07:56'),
(46, 'Les infections locales en chirurgie', '', '', 'files/cours/IFSIO/cours_up_les-infections-locales-en-chirurgie.docx', '2019-2020', '10', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:07:56', '2020-04-27 18:07:56'),
(47, 'Methodologie de travail clinique', '', '', 'files/cours/IFSIO/cours_up_methodologie-de-travail-clinique.docx', '2019-2020', '10', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:07:56', '2020-04-27 18:07:56'),
(48, 'Mobile foetal', '', '', 'files/cours/IFSIO/cours_up_mobile-foetal.pdf', '2019-2020', '10', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:07:56', '2020-04-27 18:07:56'),
(49, 'Pf corrige', '', '', 'files/cours/IFSIO/cours_up_pf-corrige.docx', '2019-2020', '10', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:07:56', '2020-04-27 18:07:56'),
(50, 'Reste cours sim l1', '', '', 'files/cours/IFSIO/cours_up_reste-cours-sim-l1.docx', '2019-2020', '10', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:07:56', '2020-04-27 18:07:56'),
(51, 'Role de linfirmier dans la maladie de cushing', '', '', 'files/cours/IFSIO/cours_up_role-de-linfirmier-dans-la-maladie-de-cushing.docx', '2019-2020', '10', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:07:56', '2020-04-27 18:07:56'),
(52, 'Role de linfirmier dans le traitement dune hyperthyroidie', '', '', 'files/cours/IFSIO/cours_up_role-de-linfirmier-dans-le-traitement-dune-hyperthyroidie.docx', '2019-2020', '10', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:07:56', '2020-04-27 18:07:56'),
(53, 'Role de linfirmier dans linsulinotherapie', '', '', 'files/cours/IFSIO/cours_up_role-de-linfirmier-dans-linsulinotherapie.docx', '2019-2020', '10', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:07:56', '2020-04-27 18:07:56'),
(54, 'Role infirmier dans la maladie daddison', '', '', 'files/cours/IFSIO/cours_up_role-infirmier-dans-la-maladie-daddison.docx', '2019-2020', '10', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:07:56', '2020-04-27 18:07:56'),
(55, 'Sio technique de mise au seinnouveau document microsoft word', '', '', 'files/cours/IFSIO/cours_up_sio-technique-de-mise-au-seinnouveau-document-microsoft-word.docx', '2019-2020', '10', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:07:56', '2020-04-27 18:07:56'),
(56, 'Unite foetoplacentair1', '', '', 'files/cours/IFSIO/cours_up_unite-foetoplacentair1.pdf', '2019-2020', '10', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:07:56', '2020-04-27 18:07:56'),
(57, 'Uterus gravide1', '', '', 'files/cours/IFSIO/cours_up_uterus-gravide1.pdf', '2019-2020', '10', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:07:56', '2020-04-27 18:07:56'),
(58, 'Vitamines et metabolisme general aloukoutou', '', '', 'files/cours/IFSIO/cours_up_vitamines-et-metabolisme-general-aloukoutou.docx', '2019-2020', '10', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:07:56', '2020-04-27 18:07:56'),
(59, 'Vitamines et metabolisme general aloukoutou', '', '', 'files/cours/IFSIO/cours_up_vitamines-et-metabolisme-general-aloukoutou.rar', '2019-2020', '10', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:07:56', '2020-04-27 18:07:56'),
(60, 'Comportement du consommateur gc1', '', '', 'files/cours/IUT/cours_up_comportement-du-consommateur-gc1.pdf', '2019-2020', '1', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:11:14', '2020-04-27 18:11:14'),
(61, 'Connaissance de la banque iut up v2020', '', '', 'files/cours/IUT/cours_up_connaissance-de-la-banque-iut-up-v2020.pdf', '2019-2020', '1', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:11:14', '2020-04-27 18:11:14'),
(62, 'Cours detude de marche et prevision de la demande iut 2020', '', '', 'files/cours/IUT/cours_up_cours-detude-de-marche-et-prevision-de-la-demande-iut-2020.doc', '2019-2020', '1', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:11:14', '2020-04-27 18:11:14'),
(63, 'Cours economie bancaire', '', '', 'files/cours/IUT/cours_up_cours-economie-bancaire.pdf', '2019-2020', '1', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:11:14', '2020-04-27 18:11:14'),
(64, 'Dagba cplus plus iut', '', '', 'files/cours/IUT/cours_up_dagba_cplus_plus_iut.zip', '2019-2020', '1', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:11:14', '2020-04-27 18:11:14'),
(65, 'Dr gomina iut up gtl cours gestion des operations transit et douane', '', '', 'files/cours/IUT/cours_up_dr-gomina-iut-up-gtl-cours-gestion-des-operations-transit-et-douane.pdf', '2019-2020', '1', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:11:14', '2020-04-27 18:11:14'),
(66, 'Dr gomina up iut gtl cours gestion des operations import export', '', '', 'files/cours/IUT/cours_up_dr-gomina-up-iut-gtl-cours-gestion-des-operations-import-export.pdf', '2019-2020', '1', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:11:14', '2020-04-27 18:11:14'),
(67, 'Entreprise corrige et revue', '', '', 'files/cours/IUT/cours_up_entreprise-corrige-et-revue.pdf', '2019-2020', '1', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:11:14', '2020-04-27 18:11:14'),
(68, 'Gestion et organisation des services commerciaux gc1', '', '', 'files/cours/IUT/cours_up_gestion-et-organisation-des-services-commerciaux-gc1.pdf', '2019-2020', '1', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:11:14', '2020-04-27 18:11:14'),
(69, 'Get 4102 2020', '', '', 'files/cours/IUT/cours_up_get-4102-2020.pdf', '2019-2020', '1', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:11:14', '2020-04-27 18:11:14'),
(70, 'Ilm 4102 2020', '', '', 'files/cours/IUT/cours_up_ilm-4102-2020.pdf', '2019-2020', '1', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:11:14', '2020-04-27 18:11:14'),
(71, 'Int 4102 gtl1 2020', '', '', 'files/cours/IUT/cours_up_int-4102-gtl1-2020.pdf', '2019-2020', '1', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:11:14', '2020-04-27 18:11:14'),
(72, 'Lancement nouveaux produits avril 2020', '', '', 'files/cours/IUT/cours_up_lancement-nouveaux-produits-avril-2020.pptx', '2019-2020', '1', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:11:14', '2020-04-27 18:11:14'),
(73, 'Lot 4104 gtl2 2020', '', '', 'files/cours/IUT/cours_up_lot-4104-gtl2-2020.pdf', '2019-2020', '1', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:11:14', '2020-04-27 18:11:14'),
(74, 'Management des organisations 2020', '', '', 'files/cours/IUT/cours_up_management-des-organisations-2020.pdf', '2019-2020', '1', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:11:14', '2020-04-27 18:11:14'),
(75, 'Marches financiers cours licence2', '', '', 'files/cours/IUT/cours_up_marches-financiers-cours-licence2.docx', '2019-2020', '1', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:11:14', '2020-04-27 18:11:14'),
(76, 'Merchandising gc1', '', '', 'files/cours/IUT/cours_up_merchandising-gc1.pdf', '2019-2020', '1', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:11:14', '2020-04-27 18:11:14'),
(77, 'Module theories de organisations iut', '', '', 'files/cours/IUT/cours_up_module-theories-de-organisations-iut.pdf', '2019-2020', '1', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:11:14', '2020-04-27 18:11:14'),
(78, 'Null 3', '', '', 'files/cours/IUT/cours_up_null-3.pdf', '2019-2020', '1', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:11:14', '2020-04-27 18:11:14'),
(79, 'Null 4', '', '', 'files/cours/IUT/cours_up_null-4.pdf', '2019-2020', '1', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:11:14', '2020-04-27 18:11:14'),
(80, 'Th k dagba c ii 2020', '', '', 'files/cours/IUT/cours_up_th-k-dagba-c-ii-2020.zip', '2019-2020', '1', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-27 18:11:14', '2020-04-27 18:11:14'),
(81, '1 le trachome dr alfa bio prof assavedo 1', '', '', 'files/cours/FM/cours_up_1-le-trachome-dr-alfa-bio-prof-assavedo-1.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(82, '2 la lcet dr alfa bio correction prof assavedo', '', '', 'files/cours/FM/cours_up_2-la-lcet-dr-alfa-bio-correction-prof-assavedo.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(83, '3 cataracte dr alfa bio prof assavedo', '', '', 'files/cours/FM/cours_up_3-cataracte-dr-alfa-bio-prof-assavedo.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(84, 'Affections inflammtoires paupieres dcem3 2019 2020 dr assavedo', '', '', 'files/cours/FM/cours_up_affections-inflammtoires-paupieres-dcem3-2019-2020-dr-assavedo.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(85, 'Cat devant une epistaxis', '', '', 'files/cours/FM/cours_up_cat-devant-une-epistaxis.ppt', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(86, 'Complications des otites moyennes', '', '', 'files/cours/FM/cours_up_complications-des-otites-moyennes.docx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(87, 'Cours dophtalmologie dcem3 annee academique 2019 2020', '', '', 'files/cours/FM/cours_up_cours-dophtalmologie-dcem3-annee-academique-2019-2020.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(88, 'Diagnostic des dysphagies', '', '', 'files/cours/FM/cours_up_diagnostic-des-dysphagies.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(89, 'Diagnostic dune baisse dacuite visuelle bav', '', '', 'files/cours/FM/cours_up_diagnostic-dune-baisse-dacuite-visuelle-bav.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(90, 'Diagnostic dune exophtalmie mca assavedo', '', '', 'files/cours/FM/cours_up_diagnostic-dune-exophtalmie-mca-assavedo.ppt', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(91, 'Examen dun malade atteint de vertige', '', '', 'files/cours/FM/cours_up_examen-dun-malade-atteint-de-vertige.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(92, 'Examen dun malade sourd', '', '', 'files/cours/FM/cours_up_examen-dun-malade-sourd.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(93, 'Generalites sur les cancers des voies aerodigestives superieurs', '', '', 'files/cours/FM/cours_up_generalites-sur-les-cancers-des-voies-aerodigestives-superieurs.docx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(94, 'Introduction au cours dorl', '', '', 'files/cours/FM/cours_up_introduction-au-cours-dorl.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(95, 'La myopie dr assavedo dcem3 2016 2017 ppt', '', '', 'files/cours/FM/cours_up_la-myopie-dr-assavedo-dcem3-2016-2017-ppt.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(96, 'Les adenopathies cervicales chroniques med5', '', '', 'files/cours/FM/cours_up_les-adenopathies-cervicales-chroniques-med5.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(97, 'Les angines aigues 2017', '', '', 'files/cours/FM/cours_up_les-angines-aigues-2017.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(98, 'Les cellulites cervico faciales', '', '', 'files/cours/FM/cours_up_les-cellulites-cervico-faciales.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(99, 'Les complications des otites moyennes', '', '', 'files/cours/FM/cours_up_les-complications-des-otites-moyennes.docx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(100, 'Les conjonctivites', '', '', 'files/cours/FM/cours_up_les-conjonctivites.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(101, 'Les contusions oculaires dcem3 2019 2020', '', '', 'files/cours/FM/cours_up_les-contusions-oculaires-dcem3-2019-2020.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(102, 'Les dyspnees laryngees', '', '', 'files/cours/FM/cours_up_les-dyspnees-laryngees.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(103, 'Les glaucomes 1 dr amadou correction prag assavedo', '', '', 'files/cours/FM/cours_up_les-glaucomes-1-dr-amadou-correction-prag-assavedo.docx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(104, 'Les glaucomes congenitaux dr assavedo', '', '', 'files/cours/FM/cours_up_les-glaucomes-congenitaux-dr-assavedo.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(105, 'Les keratites', '', '', 'files/cours/FM/cours_up_les-keratites.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(106, 'Les otites externes 21 11 19 pdf', '', '', 'files/cours/FM/cours_up_les-otites-externes-21-11-19-pdf.pdf', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(107, 'Les otites externes 21 11 19', '', '', 'files/cours/FM/cours_up_les-otites-externes-21-11-19.doc', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(108, 'Les otites moyennes aigues', '', '', 'files/cours/FM/cours_up_les-otites-moyennes-aigues.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(109, 'Les otites moyennes chronique', '', '', 'files/cours/FM/cours_up_les-otites-moyennes-chronique.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(110, 'Les ulceres corneens', '', '', 'files/cours/FM/cours_up_les-ulceres-corneens.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(111, 'Les uveites', '', '', 'files/cours/FM/cours_up_les-uveites.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(112, 'Lonchocercoseouepilespsiedesrivieresdcem3powerpointfinal', '', '', 'files/cours/FM/cours_up_lonchocercoseouepilespsiedesrivieresdcem3powerpointfinal.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(113, 'Omc', '', '', 'files/cours/FM/cours_up_omc.docx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(114, 'Retine et affections generales', '', '', 'files/cours/FM/cours_up_retine-et-affections-generales.docx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(115, 'Rhinite aigue', '', '', 'files/cours/FM/cours_up_rhinite-aigue.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(116, 'Semiologie de la conjonctive', '', '', 'files/cours/FM/cours_up_semiologie-de-la-conjonctive.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(117, 'Semiologie de la cornee', '', '', 'files/cours/FM/cours_up_semiologie-de-la-cornee.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(118, 'Semiologie de liris et de la pupille', '', '', 'files/cours/FM/cours_up_semiologie-de-liris-et-de-la-pupille.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(119, 'Semiologie du cristallin', '', '', 'files/cours/FM/cours_up_semiologie-du-cristallin.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(120, 'Sinusites aigues', '', '', 'files/cours/FM/cours_up_sinusites-aigues.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(121, 'Vegetations adenoides', '', '', 'files/cours/FM/cours_up_vegetations-adenoides.pptx', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:51:02', '2020-04-28 00:51:02'),
(122, 'Cours  Biochimie numériques MCA GOMINA  FM UP', '', '', 'files/cours/FM/Cours  Biochimie numériques MCA GOMINA  FM UP.zip', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:57:55', '2020-04-28 00:57:55'),
(123, 'COURS ANATOMIE PATHOLOGIE 3e et 4e année MCA BRUN', '', '', 'files/cours/FM/COURS ANATOMIE PATHOLOGIE 3e et 4e année MCA BRUN.zip', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:57:55', '2020-04-28 00:57:55'),
(124, 'Cours d\'ophttalmo', '', '', 'files/cours/FM/Cours d\'ophttalmo.zip', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:57:55', '2020-04-28 00:57:55'),
(125, 'Cours d\'urologie', '', '', 'files/cours/FM/Cours d\'urologie.zip', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:57:55', '2020-04-28 00:57:55'),
(126, 'COURS DE CARDIOLOGIE', '', '', 'files/cours/FM/COURS DE CARDIOLOGIE.zip', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:57:55', '2020-04-28 00:57:55'),
(127, 'Cours de dermato', '', '', 'files/cours/FM/cours de dermato.zip', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:57:55', '2020-04-28 00:57:55'),
(128, 'Cours de Gynécologie', '', '', 'files/cours/FM/Cours de Gynécologie.zip', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:57:55', '2020-04-28 00:57:55'),
(129, 'Cours de neurologie', '', '', 'files/cours/FM/Cours de neurologie.zip', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:57:55', '2020-04-28 00:57:55'),
(130, 'COURS DE PEDIATRIE MEDECINE5 FM UP', '', '', 'files/cours/FM/COURS DE PEDIATRIE MEDECINE5_FM_UP.zip', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:57:55', '2020-04-28 00:57:55'),
(131, 'Cours de pneumologie', '', '', 'files/cours/FM/Cours de pneumologie.zip', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:57:55', '2020-04-28 00:57:55'),
(132, 'Cours de santé au travail', '', '', 'files/cours/FM/Cours de santé au travail.zip', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:57:55', '2020-04-28 00:57:55'),
(133, 'COURS ORL FM  UP 2020', '', '', 'files/cours/FM/COURS ORL FM -UP 2020.zip', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:57:55', '2020-04-28 00:57:55'),
(134, 'COURS UP COURS DE GYNECO OBSTETRIQUE', '', '', 'files/cours/FM/COURS_UP_COURS-DE-GYNECO-OBSTETRIQUE.zip', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:57:55', '2020-04-28 00:57:55'),
(135, 'Cours therapeutique neurologique', '', '', 'files/cours/FM/cours_up_cours-therapeutique-neurologique.pdf', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:57:55', '2020-04-28 00:57:55'),
(136, 'Syllabus pathologie cardio 2016', '', '', 'files/cours/FM/cours_up_syllabus-pathologie-cardio-2016.pdf', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:57:55', '2020-04-28 00:57:55'),
(137, 'Syllabus semio cardio 2016 opt', '', '', 'files/cours/FM/cours_up_syllabus-semio-cardio-2016-opt.pdf', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:57:55', '2020-04-28 00:57:55'),
(138, 'Syllabus thera cardio 2016', '', '', 'files/cours/FM/cours_up_syllabus-thera-cardio-2016.pdf', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:57:55', '2020-04-28 00:57:55'),
(139, 'Dr AHOUI Séraphin FM Cours 2020', '', '', 'files/cours/FM/Dr AHOUI Séraphin FM Cours 2020.zip', '2019-2020', '6', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-28 00:57:55', '2020-04-28 00:57:55'),
(140, 'Analyse2 par toyou robert', '', '', 'files/cours/ENSPD/cours_up_analyse2-par-toyou-robert.pdf', '2019-2020', '2', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-29 22:35:25', '2020-04-29 22:35:25'),
(141, 'Cours l1 microeconomie enspd 2019 2020 par nadege djossou', '', '', 'files/cours/ENSPD/cours_up_cours-l1-microeconomie-enspd-2019-2020-par-nadege-djossou.pdf', '2019-2020', '2', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-29 22:35:25', '2020-04-29 22:35:25'),
(142, 'L1 demo2032 notes de cours partie 1 par justin dansou', '', '', 'files/cours/ENSPD/cours_up_l1-demo2032-notes-de-cours-partie-1-par-justin-dansou.pdf', '2019-2020', '2', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-29 22:35:25', '2020-04-29 22:35:25'),
(143, 'L2 pdm0 3032 notes de cours par justin dansou', '', '', 'files/cours/ENSPD/cours_up_l2-pdm0-3032-notes-de-cours-par-justin-dansou.pdf', '2019-2020', '2', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-29 22:35:25', '2020-04-29 22:35:25'),
(144, 'Part1 cours series temporelles licence 2 par nadege djossou', '', '', 'files/cours/ENSPD/cours_up_part1-cours-series-temporelles-licence-2-par-nadege-djossou.pdf', '2019-2020', '2', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-29 22:35:25', '2020-04-29 22:35:25'),
(145, 'Variables aleatoires et lois classiques par francois koladjo', '', '', 'files/cours/ENSPD/cours_up_variables-aleatoires-et-lois-classiques-par-francois-koladjo.pdf', '2019-2020', '2', '', '', '', 'LICENCE', 'ECU', 1, '2020-04-29 22:35:25', '2020-04-29 22:35:25');

-- --------------------------------------------------------

--
-- Structure de la table `devises`
--

DROP TABLE IF EXISTS `devises`;
CREATE TABLE IF NOT EXISTS `devises` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `devise` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sigle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `user_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `devises_user_id_foreign` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `devises`
--

INSERT INTO `devises` (`id`, `created_at`, `updated_at`, `deleted_at`, `devise`, `sigle`, `description`, `user_id`) VALUES
(1, '2020-04-23 21:51:50', '2020-04-23 21:58:48', NULL, 'Dollar Américain', 'USD', NULL, 3),
(2, '2020-04-23 21:52:20', '2020-04-23 22:30:27', NULL, 'Franc CFA', 'FCFA', 'La monnaie de la France', 3),
(3, '2020-04-23 21:55:33', '2020-04-23 22:15:47', NULL, 'Euro', 'EUR', 'Monnaie de l\'Europe', 3);

-- --------------------------------------------------------

--
-- Structure de la table `etablissements`
--

DROP TABLE IF EXISTS `etablissements`;
CREATE TABLE IF NOT EXISTS `etablissements` (
  `id` int(10) UNSIGNED NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_cover` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titre` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sigle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `contenu` longtext COLLATE utf8mb4_unicode_ci,
  `view_count` int(11) NOT NULL,
  `etat` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `etablissements`
--

INSERT INTO `etablissements` (`id`, `logo`, `image_cover`, `titre`, `sigle`, `description`, `contenu`, `view_count`, `etat`, `created_at`, `updated_at`) VALUES
(1, '/uploads/etablissements/logo/26893.png', '/uploads/etablissements/image_cover/cover__250613.jpg', 'Institut Universitaire de Technologie', 'IUT', 'L’IUT est un établissement de formation professionnelle et de recherche de l’Université de Parakou.', 'I- DIFFÉRENTES FORMATIONS DE L’INSTITUT UNIVERSITAIRE DE TECHNOLOGIE\r\nL’IUT est un établissement de formation professionnelle et de recherche de l’Université de Parakou.\r\n\r\n1- Licence professionnelle\r\n\r\n1.1-    Licence cours du jour\r\n\r\nLa formation dure trois ans. Il existe cinq (5) filières :\r\n\r\n    Gestion des Entreprises (GE)\r\n    Gestion des Banques (GB)\r\n    Gestion Commerciale (GC)\r\n    Informatique de Gestion (IG)\r\n    Gestion des transports et logistique (GTL)\r\n\r\n1.2-  Licence cours du soir\r\n\r\n  La formation dure un an après deux ans de Brevet  de Techniciens Supérieurs (BTS). Les filières ouvertes sont :\r\n\r\n    Gestion des Entreprises\r\n    Gestion des Banques\r\n    Gestion Commerciale\r\n    Gestion des Ressources Humaines\r\n\r\n2- Masters professionnels\r\n\r\n    Master en Comptabilité, Contrôle et Audit\r\n    Master en Administration et Ingénierie des Ressources Humaines\r\n    Master en Gestion et Expertise commerciale\r\n    Master en Génie logiciel et Intégration d’Application\r\n    Master en Système d’Information et d’Aide à la Décision\r\n\r\nII- DÉBOUCHÉS POUR L’APPRENANT\r\n\r\nLes différents débouchés correspondant aux différentes formations disponibles au premier cycle à l’IUT se présentent comme suit:\r\n\r\n    GESTION DES ENTREPRISES\r\n\r\nCabinets comptables, services de comptabilité des entreprises, banque, assurances, administrations, petites, moyennes  et grandes entreprises, auto emploi.\r\n\r\n    GESTION DES BANQUES\r\n\r\nOrganismes financiers ou de gestion (établissement de crédits, entreprises commerciales de banque).\r\n\r\n    GESTION COMMERCIALE\r\n\r\nEntreprises commerciales ou services commerciaux de tout type d’entreprise, services de logistiques internationale, entreprise d’export, sociétés de transit et de manutention, banque, agence de communication, grandes entreprises de représentant commercial de chef de rayon dans le centre commercial (supermarché), d’attaché commercial de chef de produit, chargé des relations publiques,  assistant de communication interne diffusant des informations à l’intérieur de l’entreprise.  \r\n\r\n    INFORMATIQUE DE GESTION\r\n\r\nCentre informatique, banque, entreprise de prestation de service, éditeur de logiciels, grandes entreprises informatisées, poste d’analyse programmeur, chef de programme informatique, d’ingénieur logiciel, administrateur de réseau.\r\n\r\n    GESTION DES TRANSPORTS ET LOGISTIQUE\r\n\r\nBanque, Société d’assurance et administrations, petites et moyennes entreprises, grandes entreprises, entreprises de transport et de Gestion ferroviaire, service chargé de la logistique au niveau des entreprises, des administrations, poste d’agent comptable dans les compagnies aériennes, d’agent de fret dans les aéroports, de techniciens des études et l’exploitation de l’avion  civile.\r\n\r\n    GESTION DES RESSOURCES HUMAINES\r\n\r\nEntreprises, Assurance, Administrations,  Organisations, ONG\r\n\r\n3- SCOLARITE\r\n\r\n3.1- Voie de recrutement\r\n\r\nLes étudiants sont admis à l’IUT avec un baccalauréat (séries A, B, C, D, E, F, G2, G3) ou tout autre diplôme jugé équivalent. Sur étude de dossiers, l’Etat octroie des bourses d’études soit à 100% ou soit demi-bourses (l’étudiant donne une contribution de 100.000 f CFA par ans.', 0, 1, '2019-01-31 14:11:52', '2019-02-10 15:43:30'),
(2, '/uploads/etablissements/logo/77235.png', '/uploads/etablissements/image_cover/85598.jpg', 'École Nationale de Statistique, de Planification et de Démographie', 'ENSPD', 'L’École Nationale de Statistique, de Planification et de Démographie (ENSPD) est un Établissement Public d’Enseignement, de Formation professionnelle et de Recherche de l’Université de Parakou au Bénin. Créée en 2012, elle est dirigée par le professeur Mouftaou Amadou Sanni.', 'MISSION\r\n\r\nL’école en tant qu’établissement public d’enseignement a pour mission principale de former et de renforcer les capacités, à tous les niveaux du système de l’enseignement supérieur (Licence, Master, Doctorat et autres programmes spéciaux), des futurs diplômés, susceptibles de relever les défis du pays et de l’Afrique en matière de réalisation des enquêtes ou études sociodémographiques et économiques, d’analyse et de traitement des données, puis, de planification du développement socioéconomique.\r\n\r\nHISTORIQUE\r\n\r\nEn plus d’être un établissement de formation professionnelle et de recherche de l’Université de Parakou, l’ENSPD est également une école d’appui au développement. La création de l’école remonte en juin 2012 avec la mise en œuvre de la politique de la décentralisation universitaire du régime en place. En ces temps-là, au Bénin, tous les plans de développement conçus et mis en application dans les communes n’ont pas porté les fruits escomptés. La raison en est qu’ils ont été élaborés sans la participation des populations concernées et, surtout, en l’absence de ressources humaines compétentes et d’informations ou de données statistiques susceptibles de mieux arrimer les choix stratégiques aux problèmes réels de développement à régler. Il devient ainsi indispensable et urgent de disposer de ressources humaines compétentes et de données statistiques pertinentes et fiables en réponse aux exigences de l’environnement socioéconomique et de développement local adéquat. C’est à effet que sous le parrainage de l’ancien ministre d’État, Professeur François Adébayo Abiola, le gouvernement du Bénin créé par l\'arrêté ministériel du 07 juin 2012n 1, l’École Nationale de la Statistique, de la Planification et de la Démographie à l’Université de Parakou, notamment sur le site du Centre Universitaire de Tchaourou. En novembre 2016, l’École Nationale de Statistique, de Planification et de Démographie (ENSPD) est délocalisée de son site sis à Tchaourou vers le campus de l\'Université de Parakou, à la suite de l’actualisation de la carte universitaire du Bénin effectuée en 2016.\r\n\r\nLA RECHERCHE A L\'ENSPD\r\n\r\nDeux laboratoires de recherche ont été conçus et formellement créés par des arrêtés rectoraux : LaReSPD et ODeSPoL.\r\n    Le Laboratoire de Recherche en Science de la Population et du Développement (LaReSPD) par arrêté rectoral N°2335-2014/R-UP/VR-AARU du 09 décembre 2014. Les formations doctorales sont adossées à ce laboratoire2.\r\n    L’Observatoire Démographique et Statistique des Populations Locales (ODeSPoL) par arrêté rectoral N°2015-1889/R-UP/VR-AARU/SG/SA du 12 Août 2015. Il est la structure de l’ENSPD destinée à la mise en œuvre des opérations de collecte et de confection des bases de données. Ces données sont associées aux projets/programmes de recherches opérationnelles, fondamentales et doctorales du LaReSPD3.\r\n\r\n\r\nFORMATION\r\n\r\nLes formations proposées sont celles de Licence, Master et de Doctorat4 :\r\n\r\n    Licence : Le programme de Licence de l’ENSPD est un programme de spécialisation en trois ans, soit six semestres avec deux filières à savoir : Statistiques Appliquées et Planification et Suivi-Évaluation.\r\n    Master : Le programme de master à l’ENSPD s’exécute en deux ans avec deux options à savoir : Statistique appliquée et Démographie\r\n    Doctorat : La formation de doctorat vise à contribuer à la production des ressources humaines capables d’exercer un leadership intellectuel tant en matière de recherche scientifique, que d’innovation technologique et de formation universitaire.\r\n\r\nADMISSION\r\n\r\n-    LICENCE\r\n\r\n    Avoir le BAC (C ; D et E)\r\n    Etre reçu au concours organisé par la DEC ou être sélectionné par le MESRS comme à titre payant (montant fixé par le MESRS) ou à titre Sponsoring (425 000 F).\r\n\r\n-    MASTER\r\n\r\n    Etre titulaire d’une LICENCE Professionnelle ou d’une Maitrise en Sciences Economique et de Gestion ou tout autre diplôme équivalent\r\n    Etre sélectionné par l’IUT après étude de dossiers\r\n\r\n    Avoir les frais de formation s’élevant à 415 000 F pour les étudiants réguliers ou 450 000 pour les fonctionnaires\r\n\r\nPASSAGE EN ANNÉE SUPÉRIEURE\r\n\r\n Pour passer en année supérieure, l’étudiant doit valider 85% de Crédits:\r\n\r\nIl faut avoir validé 100% en LICENCE1, 100% en LICENCE 2 et 100% en LICENCE 3 pour être admissible ou pour être autorisé à soutenir son travail de fin de formation.\r\n\r\nFORMALITÉS ADMINISTRATIVES\r\n\r\nLes pièces à fournir pour retirer son relevé de notes ou son attestation de réussite\r\n\r\n-    Une demande manuscrite adressée au directeur de l’ENSPD\r\n-    Une photocopie simple de la carte d’étudiant\r\n-    Une Quittance de 5000F à prendre au niveau de la comptabilité (relevé de notes) ou de 2000 (attestation) ;\r\nLes pièces à fournir pour retirer son certificat d’inscription\r\n\r\n-    Une demande manuscrite adressée au directeur de l’ENSPD ou à l’enseignant désiré ;\r\n-    Une photocopie simple de la carte d’étudiant ;\r\n\r\nLes modalités de payement de frais d’inscription (bacheliers, anciens,  étrangers)\r\n\r\n-    Le paiement se fait en une seule tranche à la banque (EcoBanck) au numéro de compte intitulé UP inscription qui figure sur la fiche de préinscription.\r\n\r\nDémarrage et durée des inscriptions\r\n\r\n-    Le démarrage et la fin des  inscriptions sont fixés par un arrêté rectoral\r\n\r\nLes conditions à remplir pour suivre les cours (documents, uniforme + prix, période, équipements, …)\r\n\r\n-    Une fiche d’engagement à signer du parent ou tuteur et à légaliser au tribunal ;\r\n-    Acquisition du règlement pédagogique de l’école à 1000F au Secrétariat ;\r\n-    Achat d’uniforme ;\r\n-     Un ordinateur ;\r\n-    Un dictionnaire Français-Anglais ;\r\n-    Des cahiers de prise de notes pour les cours ;\r\n\r\nL\'accès à son résultat de fin d’année\r\n\r\n-    Les résultats sont affichés après délibération\r\n-    Les étudiants ont 72 heures après l’affichage des résultats pour déposer les requêtes de réclamation\r\n\r\nContact\r\n\r\nEmail: enspd_up@gmail.com', 0, 1, '2019-01-31 14:41:58', '2019-01-31 14:41:58'),
(3, '/uploads/etablissements/logo/64387.png', NULL, 'Ecoles Doctorale Des Sciences Agronomiques et de l\'Eau', 'EDSAE', 'Ecoles Doctorale Des Sciences Agronomiques et de l\'Eau', 'Ecoles Doctorale Des Sciences Agronomiques et de l\'Eau', 0, 1, '2019-02-02 19:42:36', '2019-02-02 19:42:36'),
(4, '/uploads/etablissements/logo/74631.PNG', NULL, 'Faculté des Lettres, Arts et Sciences Humaines', 'FLASH', 'Faculté des Lettres, Arts et Sciences Humaines', 'Faculté des Lettres, Arts et Sciences Humaines', 0, 1, '2019-02-02 19:43:21', '2019-02-02 19:43:21'),
(5, '/uploads/etablissements/logo/logo__58192.png', NULL, 'Faculté de Droit et de Science Politique', 'FDSP', 'Faculté de Droit et de Science Politique', 'Faculté de Droit et de Science Politique', 0, 1, '2019-02-02 19:46:20', '2019-02-02 19:46:20'),
(6, '/uploads/etablissements/logo/logo__63438.png', NULL, 'Faculté de Medécine', 'FM', 'Faculté de Medécine', NULL, 0, 1, '2019-02-02 19:53:56', '2019-02-02 19:53:56'),
(7, '/uploads/etablissements/logo/logo__34897.jpg', NULL, 'Faculté d\'Agronomie', 'FA', 'Faculté d\'Agronomie', 'Faculté d\'Agronomie', 0, 1, '2019-02-02 19:54:50', '2019-02-02 19:54:50'),
(8, '/uploads/etablissements/logo/logo__96711.jpg', NULL, 'Faculté des Sciences Économiques et de Gestion', 'FASEG', 'Faculté des Sciences Économiques et de Gestion', 'Faculté des Sciences Economiques et de Gestion', 0, 1, '2019-02-02 19:55:54', '2019-02-02 19:55:54'),
(9, '/uploads/etablissements/logo/logo__94371.png', NULL, 'Ecole Nationale des Techniciens en Santé publique et Surveillance Épidémiologique', 'ENATSE', 'Ecole Nationale des Techniciens en Santé publique et Surveillance Épidémiologique', 'Ecole Nationale des Techniciens en Santé publique et Surveillance Épidémiologique', 0, 1, '2019-02-02 20:02:31', '2019-02-02 20:02:31'),
(10, '/uploads/etablissements/logo/logo__80832.png', NULL, 'Institut de Formation en Soins Infirmiers et Obstétricaux', 'IFSIO', 'Institut de Formation en Soins Infirmiers et Obstétricaux', 'Institut de Formation en Soins Infirmiers et Obstétricaux', 0, 1, '2019-02-02 20:03:34', '2019-02-02 20:03:34');

-- --------------------------------------------------------

--
-- Structure de la table `marques`
--

DROP TABLE IF EXISTS `marques`;
CREATE TABLE IF NOT EXISTS `marques` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `nom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `user_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `marques_user_id_foreign` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `marques`
--

INSERT INTO `marques` (`id`, `created_at`, `updated_at`, `deleted_at`, `nom`, `description`, `user_id`) VALUES
(1, '2020-04-07 22:39:35', '2020-04-07 22:39:35', NULL, 'Huawei', '', 3),
(2, '2020-04-07 22:43:38', '2020-04-07 22:43:38', NULL, 'Huawei', '', 3),
(3, '2020-04-07 22:43:44', '2020-04-07 22:43:44', NULL, 'Huawei', '', 3),
(4, '2020-04-07 22:44:29', '2020-04-07 22:44:29', NULL, 'Huawei', '', 3),
(5, '2020-04-09 13:18:58', '2020-04-09 13:18:58', NULL, 'CANON', '', 3),
(6, '2020-04-09 13:19:26', '2020-04-09 13:19:26', NULL, 'CANON', '', 3),
(7, '2020-04-09 13:21:29', '2020-04-09 13:21:29', NULL, 'CANON', '', 3),
(8, '2020-04-09 13:27:10', '2020-04-09 13:27:10', NULL, 'TRAVEL CHARGER', '', 3),
(9, '2020-04-09 13:27:10', '2020-04-09 13:27:10', NULL, 'TRAVEL CHARGER', '', 3),
(10, '2020-04-09 13:27:24', '2020-04-09 13:27:24', NULL, 'TRAVEL CHARGER', '', 3),
(11, '2020-04-09 21:35:52', '2020-04-09 21:35:52', NULL, 'Nokia', '', 3),
(12, '2020-04-10 14:32:11', '2020-04-10 14:32:11', NULL, 'Mars', '', 3),
(13, '2020-04-10 15:03:47', '2020-04-10 15:03:47', NULL, 'Samsung', '', 3),
(14, '2020-04-10 15:21:15', '2020-04-10 15:21:15', NULL, 'Dopel', '', 3),
(15, '2020-04-17 14:52:49', '2020-04-17 14:52:49', NULL, 'Iphone', '', 3),
(16, '2020-04-19 12:43:31', '2020-04-19 12:43:31', NULL, 'Toyota', '', 3),
(17, '2020-04-19 12:47:29', '2020-04-19 12:47:29', NULL, 'Toyota de bon vieux temps à pendre en compte pour avancer dans la vie', '', 3),
(18, '2020-04-23 22:32:30', '2020-04-23 22:32:30', NULL, 'HP', '', 3),
(19, '2020-04-24 02:27:11', '2020-04-24 02:27:11', NULL, 'PORSCH', '', 3);

-- --------------------------------------------------------

--
-- Structure de la table `media`
--

DROP TABLE IF EXISTS `media`;
CREATE TABLE IF NOT EXISTS `media` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mini` text COLLATE utf8mb4_unicode_ci,
  `ref` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ext` text COLLATE utf8mb4_unicode_ci,
  `ref_id` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `media_user_id_foreign` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `media`
--

INSERT INTO `media` (`id`, `created_at`, `updated_at`, `deleted_at`, `title`, `url`, `type`, `mini`, `ref`, `ext`, `ref_id`, `user_id`) VALUES
(1, '2020-04-09 22:06:23', '2020-04-09 22:06:23', NULL, 'zdkjdzkj', 'storage/cd1/medias/1586475780.png', 'dzkjjkzd', 'kzdjjkejk', 'zjkjkef', NULL, '2', 42),
(2, '2020-04-09 22:43:02', '2020-04-09 22:43:02', NULL, 'kjjkjkkj', 'C:\\wamp65\\www\\2020\\securitapen\\public\\storage/cd1/medias/1586475780.png', 'IMAGE', NULL, '1586475782', 'png', NULL, 3),
(3, '2020-04-09 22:43:08', '2020-04-09 22:43:08', NULL, 'kjjkjkkj', 'C:\\wamp65\\www\\2020\\securitapen\\public\\storage/cd1/medias/1586475787.png', 'IMAGE', NULL, '1586475788', 'png', NULL, 3),
(4, '2020-04-09 22:44:04', '2020-04-09 22:44:04', NULL, 'kjjkjkkj', 'C:\\wamp65\\www\\2020\\securitapen\\public\\storage/cd1/medias/1586475842.png', 'IMAGE', NULL, '1586475844', 'png', NULL, 3),
(5, '2020-04-10 15:02:08', '2020-04-10 15:02:08', NULL, 'Bien2', 'storage/cd1/medias/1586534526.png', 'IMAGE', NULL, 'biens', 'png', NULL, 3),
(6, '2020-04-10 15:03:47', '2020-04-10 15:03:47', NULL, 'Téléphone Samsung S10', 'storage/cd1/medias/1586534625.png', 'IMAGE', NULL, 'biens', 'png', NULL, 3),
(7, '2020-04-10 15:21:15', '2020-04-10 15:21:15', NULL, 'Amihn', 'storage/cd1/medias/1586535675.jpg', 'IMAGE', NULL, 'biens', 'jpg', NULL, 3),
(8, '2020-04-10 15:21:28', '2020-04-10 15:21:28', NULL, 'Amihn', 'storage/cd1/medias/1586535687.jpg', 'IMAGE', NULL, 'biens', 'jpg', NULL, 3),
(9, '2020-04-10 15:21:59', '2020-04-10 15:21:59', NULL, 'jkjkdzjkdzj', 'storage/cd1/medias/1586535718.png', 'IMAGE', NULL, 'biens', 'png', NULL, 3),
(10, '2020-04-10 15:23:09', '2020-04-10 15:23:09', NULL, 'dzjkzdkj', 'storage/cd1/medias/1586535789.png', 'IMAGE', NULL, 'biens', 'png', NULL, 3),
(11, '2020-04-10 15:31:45', '2020-04-10 15:31:45', NULL, 'dzjkzkjd', 'storage/cd1/medias/1586536305.jpg', 'IMAGE', NULL, 'biens', 'jpg', NULL, 3),
(12, '2020-04-10 15:41:50', '2020-04-10 15:41:50', NULL, 'Save', 'storage/cd1/medias/1586536909.jpg', 'IMAGE', NULL, 'biens', 'jpg', NULL, 3),
(13, '2020-04-10 19:47:07', '2020-04-10 19:47:07', NULL, 'kdzkjzd', 'public/cd1/medias/6VoHKMD1RJg0VHm7LHIwbO8u5SiK0t_1586551627.jpg', 'IMAGE', 'public/cd1/medias/thumb/6VoHKMD1RJg0VHm7LHIwbO8u5SiK0t_1586551627.jpg', 'biens', 'jpg', NULL, 3),
(14, '2020-04-10 19:47:39', '2020-04-10 19:47:39', NULL, 'kdzkjzd', 'public/cd1/medias/znhwBSE2V1zf3TMRoXrQacZ3AJWaDr_1586551659.jpg', 'IMAGE', 'public/cd1/medias/thumb/znhwBSE2V1zf3TMRoXrQacZ3AJWaDr_1586551659.jpg', 'biens', 'jpg', NULL, 3),
(15, '2020-04-10 19:51:54', '2020-04-10 19:51:54', NULL, 'kdzkjzd', 'public/cd1/medias/6AtazyGWyN5bEmPOzNdsQBxUG1vvBS_1586551914.jpg', 'IMAGE', 'public/cd1/medias/thumb/6AtazyGWyN5bEmPOzNdsQBxUG1vvBS_1586551914.jpg', 'biens', 'jpg', NULL, 3),
(16, '2020-04-10 19:52:10', '2020-04-10 19:52:10', NULL, 'kdzkjzd', 'public/cd1/medias/TEb8jo8ML9szGKJBdsq4X9KD4uL6Lk_1586551930.jpg', 'IMAGE', 'public/cd1/medias/thumb/TEb8jo8ML9szGKJBdsq4X9KD4uL6Lk_1586551930.jpg', 'biens', 'jpg', NULL, 3),
(17, '2020-04-10 19:52:55', '2020-04-10 19:52:55', NULL, 'kdzkjzd', 'public/cd1/medias/Ef8MJOJll6NypbHQq3sX85mKWaFGaO_1586551975.jpg', 'IMAGE', 'public/cd1/medias/thumb/Ef8MJOJll6NypbHQq3sX85mKWaFGaO_1586551975.jpg', 'biens', 'jpg', NULL, 3),
(18, '2020-04-10 19:53:55', '2020-04-10 19:53:55', NULL, 'kdzkjzd', 'public/cd1/medias/8k8G4QKZtpiyAVWRsRq4wf9Jzx0780_1586552035.jpg', 'IMAGE', 'public/cd1/medias/thumb/8k8G4QKZtpiyAVWRsRq4wf9Jzx0780_1586552035.jpg', 'biens', 'jpg', NULL, 3),
(19, '2020-04-10 19:55:12', '2020-04-10 19:55:12', NULL, 'kdzkjzd', 'public/cd1/medias/6eVOAACzTAtxgyKsTiDprGlvyEkEJn_1586552111.jpg', 'IMAGE', 'public/cd1/medias/thumb/6eVOAACzTAtxgyKsTiDprGlvyEkEJn_1586552111.jpg', 'biens', 'jpg', NULL, 3),
(20, '2020-04-10 19:57:35', '2020-04-10 19:57:35', NULL, 'kdzkjzd', 'public/cd1/medias/gZkmp7NNMdnpqNXZEkVgXNEuzVELcD_1586552254.jpg', 'IMAGE', 'public/cd1/medias/thumb/gZkmp7NNMdnpqNXZEkVgXNEuzVELcD_1586552254.jpg', 'biens', 'jpg', NULL, 3),
(21, '2020-04-10 19:58:04', '2020-04-10 19:58:04', NULL, 'kdzkjzd', 'public/cd1/medias/inTdz7KI5ZUgIylF5Sdjq3qDIt095x_1586552284.jpg', 'IMAGE', 'public/cd1/medias/thumb/inTdz7KI5ZUgIylF5Sdjq3qDIt095x_1586552284.jpg', 'biens', 'jpg', NULL, 3),
(22, '2020-04-10 20:06:44', '2020-04-10 20:06:44', NULL, 'kdzkjzd', 'public/cd1/medias/KD7UBujvp2du1av33EwCBMlcdI1NYI_1586552804.jpg', 'IMAGE', 'public/cd1/medias/thumb/KD7UBujvp2du1av33EwCBMlcdI1NYI_1586552804.jpg', 'biens', 'jpg', NULL, 3),
(23, '2020-04-10 20:08:58', '2020-04-10 20:08:58', NULL, 'Machine B', 'public/cd1/medias/sdUa0bBecSPUpBOvMMpsz1B0amlwg9_1586552938.jpg', 'IMAGE', 'public/cd1/medias/thumb/sdUa0bBecSPUpBOvMMpsz1B0amlwg9_1586552938.jpg', 'biens', 'jpg', NULL, 3),
(24, '2020-04-10 21:56:24', '2020-04-10 21:56:24', NULL, 'Machine Bornage', 'cd1/medias/QYFcKhqlaFymOs6hkwH0sLtFV5U32w_1586559384.jpg', 'IMAGE', 'cd1/medias/thumb/QYFcKhqlaFymOs6hkwH0sLtFV5U32w_1586559384.jpg', 'biens', 'jpg', NULL, 3),
(25, '2020-04-10 21:58:25', '2020-04-10 21:58:25', NULL, 'Marteau', 'cd1/medias/T7EKjNniNquPamcvEPZYKRogUmgnP4_1586559505.png', 'IMAGE', 'cd1/medias/thumb/T7EKjNniNquPamcvEPZYKRogUmgnP4_1586559505.png', 'biens', 'png', NULL, 3),
(26, '2020-04-10 22:05:32', '2020-04-10 22:05:32', NULL, 'Marteau', 'public/cd1/medias/fmsSom4PtYOrmUlcb6idXrbXqOgfiH_1586559931.jpg', 'IMAGE', 'cd1/medias/thumb/fmsSom4PtYOrmUlcb6idXrbXqOgfiH_1586559931.jpg', 'biens', 'jpg', NULL, 3),
(27, '2020-04-10 22:06:37', '2020-04-10 22:06:37', NULL, 'Machine Bornage', 'public/cd1/medias/PQZQPfP9pClwHQoSq12TDAnCw5JXiA_1586559997.jpg', 'IMAGE', 'cd1/medias/thumb/PQZQPfP9pClwHQoSq12TDAnCw5JXiA_1586559997.jpg', 'biens', 'jpg', NULL, 3),
(28, '2020-04-10 22:06:51', '2020-04-10 22:06:51', NULL, 'kdzkjzd', 'public/cd1/medias/MWZFIfy2AAcZxnr5edyzOSLywAQOfb_1586560011.jpg', 'IMAGE', 'cd1/medias/thumb/MWZFIfy2AAcZxnr5edyzOSLywAQOfb_1586560011.jpg', 'biens', 'jpg', NULL, 3),
(29, '2020-04-10 22:07:11', '2020-04-10 22:07:11', NULL, 'kdzkjzd', 'public/cd1/medias/jy0AM85EH8X4LFzQFGPju3wz28ewEo_1586560031.png', 'IMAGE', 'cd1/medias/thumb/jy0AM85EH8X4LFzQFGPju3wz28ewEo_1586560031.png', 'biens', 'png', NULL, 3),
(30, '2020-04-10 22:07:30', '2020-04-10 22:07:30', NULL, 'kdzkjzd', 'public/cd1/medias/VuhNhFPG5wwLRXGF0emcsWzaACETEf_1586560050.jpg', 'IMAGE', 'cd1/medias/thumb/VuhNhFPG5wwLRXGF0emcsWzaACETEf_1586560050.jpg', 'biens', 'jpg', NULL, 3),
(31, '2020-04-10 22:08:06', '2020-04-10 22:08:06', NULL, 'Microphone', 'public/cd1/medias/XbbQw5EQUEoJ5OBqd9TolONm1z4cKm_1586560086.jpg', 'IMAGE', 'cd1/medias/thumb/XbbQw5EQUEoJ5OBqd9TolONm1z4cKm_1586560086.jpg', 'biens', 'jpg', NULL, 3),
(32, '2020-04-10 22:16:45', '2020-04-10 22:16:45', NULL, 'Machine Bornage', 'storage/cd1/medias/PsnwsMP3cuWBDW53pll6wcskjr794k_1586560605.png', 'IMAGE', 'storage/cd1/medias/thumb/PsnwsMP3cuWBDW53pll6wcskjr794k_1586560605.png', 'biens', 'png', NULL, 3),
(33, '2020-04-10 22:32:30', '2020-04-10 22:32:30', NULL, 'kdzkjzd', 'storage/public/cd1/medias/tEUayrrVC0RGlKRRYHoFvpKZycreIQ_1586561550.jpg', 'IMAGE', 'storage/cd1/medias/thumb/tEUayrrVC0RGlKRRYHoFvpKZycreIQ_1586561550.jpg', 'biens', 'jpg', NULL, 3),
(34, '2020-04-10 22:32:56', '2020-04-10 22:32:56', NULL, 'Marteau', 'storage/public/cd1/medias/SgN1PDZTgyXTNZIq0dtvyah6KujBu7_1586561576.jpg', 'IMAGE', 'storage/cd1/medias/thumb/SgN1PDZTgyXTNZIq0dtvyah6KujBu7_1586561576.jpg', 'biens', 'jpg', NULL, 3),
(35, '2020-04-10 22:33:30', '2020-04-10 22:33:30', NULL, 'kdzkjzd', 'storage/cd1/medias/9YHS6ozeWZCCgRXopOTgEPYKIIy8H0_1586561610.png', 'IMAGE', 'storage/cd1/medias/thumb/9YHS6ozeWZCCgRXopOTgEPYKIIy8H0_1586561610.png', 'biens', 'png', NULL, 3),
(36, '2020-04-16 09:49:42', '2020-04-16 09:49:42', NULL, 'Machine Bornage de longre pour désoucher mon champ de Mais et de Gombo', 'storage/cd1/medias', 'IMAGE', 'storage/cd1/medias/thumb', 'biens', 'jpg', '117', 3),
(37, '2020-04-16 09:53:51', '2020-04-16 09:53:51', NULL, 'Machine Bornage de longre pour désoucher mon champ de Mais et de Gombo', 'storage/cd1/medias/5x395xB9wgiIwvVjDjTYZrxGVUYNiJ_1587034431.jpg', 'IMAGE', 'storage/cd1/medias/thumb/5x395xB9wgiIwvVjDjTYZrxGVUYNiJ_1587034431.jpg', 'biens', 'jpg', '117', 3),
(38, '2020-04-16 09:54:12', '2020-04-16 09:54:12', NULL, 'kdzkjzd', 'storage/cd1/medias/yd9ZdISdzndfOmBlTrLQ2IV6ghukfS_1587034452.jpg', 'IMAGE', 'storage/cd1/medias/thumb/yd9ZdISdzndfOmBlTrLQ2IV6ghukfS_1587034452.jpg', 'biens', 'jpg', '116', 3),
(39, '2020-04-16 12:48:38', '2020-04-16 12:48:38', NULL, 'Marteau', 'storage/cd1/medias/kyUxFtZz5VkcwcTwdUjAPdnV8Zcv4a_1587044918.jpg', 'IMAGE', 'storage/cd1/medias/thumb/kyUxFtZz5VkcwcTwdUjAPdnV8Zcv4a_1587044918.jpg', 'biens', 'jpg', '114', 3),
(40, '2020-04-16 17:32:21', '2020-04-16 17:32:21', NULL, 'Brique de 15', 'storage/cd1/medias/i6mbm5H49sBfk4u68hqyvupuq19fLJ_1587061940.png', 'IMAGE', 'storage/cd1/medias/thumb/i6mbm5H49sBfk4u68hqyvupuq19fLJ_1587061940.png', 'biens', 'png', '88', 3),
(41, '2020-04-16 23:07:33', '2020-04-16 23:07:33', NULL, 'Portable NOKIA', 'storage/cd1/medias/8xyZq3m4M0vMCL4ETQbr8dORcCR9Fx_1587082051.jpg', 'IMAGE', 'storage/cd1/medias/thumb/8xyZq3m4M0vMCL4ETQbr8dORcCR9Fx_1587082051.jpg', 'biens', 'jpg', '7', 3),
(42, '2020-04-19 12:43:32', '2020-04-19 12:43:32', NULL, 'Hoofer Jack', 'storage/cd1/medias/GtQ0b3y5os1vAiazBbMsp9rKhsAUQL_1587303811.png', 'IMAGE', 'storage/cd1/medias/thumb/GtQ0b3y5os1vAiazBbMsp9rKhsAUQL_1587303811.png', 'biens', 'png', '77', 3),
(43, '2020-04-19 13:20:13', '2020-04-19 13:20:13', NULL, 'Disque dure HP', 'storage/cd1/medias/dOZUrDNWAjrBVnUkz4dIcAyclKw0qx_1587306013.png', 'IMAGE', 'storage/cd1/medias/thumb/dOZUrDNWAjrBVnUkz4dIcAyclKw0qx_1587306013.png', 'biens', 'png', '118', 3),
(44, '2020-04-19 17:01:34', '2020-04-19 17:01:34', NULL, 'Hoofer Jack - Kawasaki - SONAEC - Dopel - Nokia - SOBEBRA', 'storage/cd1/medias/Dfm3rKJtjPtESedJsf77on6Tz8E1Zp_1587319294.jpg', 'IMAGE', 'storage/cd1/medias/thumb/Dfm3rKJtjPtESedJsf77on6Tz8E1Zp_1587319294.jpg', 'biens', 'jpg', '77', 3),
(45, '2020-04-19 17:05:41', '2020-04-19 17:05:41', NULL, 'Hoofer Jack - Kawasaki - SONAEC - Dopel - Nokia - SOBEBRA', 'storage/cd1/medias/rrpl0eyZ4Z43n0ZBGai4vHSMfKM1tx_1587319541.jpg', 'IMAGE', 'storage/cd1/medias/thumb/rrpl0eyZ4Z43n0ZBGai4vHSMfKM1tx_1587319541.jpg', 'biens', 'jpg', '77', 3),
(46, '2020-04-19 17:05:54', '2020-04-19 17:05:54', NULL, 'Hoofer Jack - Kawasaki - SONAEC - Dopel - Nokia - SOBEBRA', 'storage/cd1/medias/0OogrW8iQsSjO582JIMC0AHkuGb0jf_1587319553.jpg', 'IMAGE', 'storage/cd1/medias/thumb/0OogrW8iQsSjO582JIMC0AHkuGb0jf_1587319553.jpg', 'biens', 'jpg', '77', 3),
(47, '2020-04-19 17:10:40', '2020-04-19 17:10:40', NULL, 'Hoofer Jack - Kawasaki - SONAEC - Dopel - Nokia - SOBEBRA', 'storage/cd1/medias/v3SMHWCNwIwv1Dze0rm1rF0jxxmQQQ_1587319839.jpg', 'IMAGE', 'storage/cd1/medias/thumb/v3SMHWCNwIwv1Dze0rm1rF0jxxmQQQ_1587319839.jpg', 'biens', 'jpg', '77', 3),
(48, '2020-04-19 17:11:14', '2020-04-19 17:11:14', NULL, 'Hoofer Jack - Kawasaki - SONAEC - Dopel - Nokia - SOBEBRA', 'storage/cd1/medias/wBF6wASA46NaG3VV7HcliGDCo88lI6_1587319874.jpg', 'IMAGE', 'storage/cd1/medias/thumb/wBF6wASA46NaG3VV7HcliGDCo88lI6_1587319874.jpg', 'biens', 'jpg', '77', 3),
(49, '2020-04-24 02:27:12', '2020-04-24 02:27:12', NULL, 'Véhicule PORSCH Cayen', 'storage/cd1/medias/XNhV1RR29idlTM3laP1XH7zOcRS169_1587698831.png', 'IMAGE', 'storage/cd1/medias/thumb/XNhV1RR29idlTM3laP1XH7zOcRS169_1587698831.png', 'biens', 'png', '120', 3);

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(36, '2014_10_12_000000_create_users_table', 1),
(37, '2014_10_12_100000_create_password_resets_table', 1),
(38, '2020_03_21_100810_create_objets_table', 1),
(39, '2020_03_23_213524_create_posts_table', 1),
(40, '2020_03_30_162857_create_biens_table', 1),
(41, '2020_04_01_155326_create_posts_table', 2),
(42, '2020_04_03_021820_create_type_transferts_table', 3),
(44, '2020_04_04_002300_create_mode_acquisitions_table', 4),
(45, '2020_04_04_003301_create_modeacquisitions_table', 5),
(46, '2020_04_04_182028_create_typebiens_table', 6),
(47, '2020_04_07_140136_create_marques_table', 7),
(48, '2020_04_07_140838_create_modeles_table', 8),
(49, '2020_04_09_225830_create_media_table', 9),
(51, '2020_04_18_142138_create_favoris_table', 10),
(53, '2020_04_23_224745_create_devises_table', 11);

-- --------------------------------------------------------

--
-- Structure de la table `modeles`
--

DROP TABLE IF EXISTS `modeles`;
CREATE TABLE IF NOT EXISTS `modeles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `nom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `marque_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `modeles_user_id_foreign` (`user_id`),
  KEY `modeles_marque_id_foreign` (`marque_id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `modeles`
--

INSERT INTO `modeles` (`id`, `created_at`, `updated_at`, `deleted_at`, `nom`, `description`, `marque_id`, `user_id`) VALUES
(1, '2020-04-09 13:32:21', '2020-04-09 13:32:21', NULL, 'RoHS', '', 8, 3),
(4, '2020-04-09 20:59:43', '2020-04-09 20:59:43', NULL, 'Y9', '', 1, 3),
(5, '2020-04-09 21:35:52', '2020-04-09 21:35:52', NULL, 'Fold', '', 11, 3),
(6, '2020-04-10 15:03:47', '2020-04-10 15:03:47', NULL, 'S10', '', 13, 3),
(7, '2020-04-10 15:21:15', '2020-04-10 15:21:15', NULL, 'A45', '', 14, 3),
(8, '2020-04-16 23:55:42', '2020-04-16 23:55:42', NULL, '110', '', 11, 3),
(9, '2020-04-16 23:57:52', '2020-04-16 23:57:52', NULL, 'AN 78', '', 11, 3),
(10, '2020-04-17 14:52:49', '2020-04-17 14:52:49', NULL, 'Iphone-5', '', 15, 3),
(11, '2020-04-19 12:43:31', '2020-04-19 12:43:31', NULL, '1254', '', 16, 3),
(12, '2020-04-19 12:47:29', '2020-04-19 12:47:29', NULL, 'Modele sonuom de Londre Paris Saint Germain de Super Game', '', 17, 3),
(13, '2020-04-19 13:15:36', '2020-04-19 13:15:36', NULL, 'Iphone-12', '', 15, 3),
(14, '2020-04-23 22:32:30', '2020-04-23 22:32:30', NULL, 'FRION', '', 18, 3),
(15, '2020-04-24 02:27:11', '2020-04-24 02:27:11', NULL, 'Cayen', '', 19, 3),
(16, '2020-04-27 15:56:51', '2020-04-27 15:56:51', NULL, 'S20', '', 13, 3);

-- --------------------------------------------------------

--
-- Structure de la table `mode_acquisitions`
--

DROP TABLE IF EXISTS `mode_acquisitions`;
CREATE TABLE IF NOT EXISTS `mode_acquisitions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `nom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `user_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mode_acquisitions_user_id_foreign` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `mode_acquisitions`
--

INSERT INTO `mode_acquisitions` (`id`, `created_at`, `updated_at`, `deleted_at`, `nom`, `description`, `user_id`) VALUES
(1, '2020-04-04 16:28:13', '2020-04-04 16:28:13', NULL, 'Achat', NULL, 3),
(2, '2020-04-04 16:33:55', '2020-04-04 16:36:31', NULL, 'Echange', 'Echange de bien contre un autre bien', 3),
(3, '2020-04-04 16:38:13', '2020-04-04 16:38:13', NULL, 'Donation', 'Don du bien par un proche ami ou tout autre personne', 3),
(4, '2020-04-04 16:41:01', '2020-04-04 16:41:01', NULL, 'Prescription acquisitive', NULL, 3),
(5, '2020-04-04 16:41:30', '2020-04-04 16:41:30', NULL, 'Accession', NULL, 3),
(6, '2020-04-04 16:41:45', '2020-04-04 16:41:45', NULL, 'Testament', NULL, 3),
(7, '2020-04-04 16:41:54', '2020-04-04 16:42:11', NULL, 'Succession', NULL, 3),
(8, '2020-04-04 17:16:58', '2020-04-04 19:57:51', NULL, 'Autres modes d\'acquisition', 'Autres modes d\'acquisition', 3);

-- --------------------------------------------------------

--
-- Structure de la table `objets`
--

DROP TABLE IF EXISTS `objets`;
CREATE TABLE IF NOT EXISTS `objets` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('etekawilfried@gmail.com', '$2y$10$oYrpIiSbh1mVnHANNnRS8uqeJp6EII9fffHQUVgnN4Mdop6csunwO', '2020-05-01 17:34:28');

-- --------------------------------------------------------

--
-- Structure de la table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `posts_user_id_foreign` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `posts`
--

INSERT INTO `posts` (`id`, `created_at`, `updated_at`, `title`, `content`, `category`, `user_id`) VALUES
(1, '2020-04-01 20:07:51', '2020-04-01 20:07:51', 'Dix outils', 'Dix outils pour créer votre CV gratuitement en ligneDix outils pour créer votre CV gratuitement en ligneDix outils pour créer votre CV gratuitement en ligneDix outils pour créer votre CV gratuitement en ligneDix outils pour créer votre CV gratuitement en ligneDix outils pour créer votre CV gratuitement en ligneDix outils pour créer votre CV gratuitement en ligne', 'technology', 3),
(12, '2020-04-02 17:06:22', '2020-04-02 17:06:35', 'Comprendre', 'Confirm deletion?\r\nOuvrir dans Google Traduction	\r\nCommentaires\r\nRésultat Web avec des liens annexes', 'technology', 1),
(13, '2020-04-02 17:39:38', '2020-04-02 17:39:38', 'cvcvcvVFVF', 'FBBFBFFB', 'technology', 2),
(14, '2020-04-02 17:42:07', '2020-04-02 17:42:07', 'SERIEU', '?JKJKJJKKKJ', 'technology', 2),
(15, '2020-04-02 17:43:19', '2020-04-02 17:43:19', 'Poteau', 'Poteau Poteau Poteau', 'technology', 2),
(16, '2020-04-02 17:43:33', '2020-04-02 17:43:33', 'Paris', 'Parsi', 'technology', 25),
(17, '2020-04-02 17:43:48', '2020-04-02 17:43:48', 'Pologne', 'dkjeidekjef efjkefjkef edjkefjkev zkjkjef', 'technology', 22),
(18, '2020-04-02 17:44:04', '2020-04-02 17:44:04', 'Pajam', 'lkikjd djuejk edjkefjkef efkjef efkjefjk efkjef', 'technology', 33),
(19, '2020-04-02 17:44:20', '2020-04-02 17:44:20', 'hjhdjefj', 'dnefjkrkjrgfenf fekjefjkf fekjfejkfe', 'technology', 36),
(20, '2020-04-02 17:44:43', '2020-04-02 17:44:43', 'Madouce', 'fekjjkef fejkef fejkjefk', 'technology', 52),
(21, '2020-04-02 17:45:06', '2020-04-02 17:45:06', 'Medecien', 'jkfjkefjkfe efkjfejkfe efkjfejkef', 'technology', 3),
(22, '2020-04-03 00:39:23', '2020-04-03 01:13:38', 'dzdzddéé', 'dzdzdz', 'technology', 1),
(23, '2020-04-03 01:01:03', '2020-04-03 01:01:03', 'ddfddf', 'eefeeffe', 'technology', 55),
(24, '2020-04-03 01:14:35', '2020-04-03 17:41:26', 'Salades', 'zdzdzzd', 'technology', 2),
(25, '2020-04-03 16:50:27', '2020-04-03 17:41:15', 'Depôtoire', 'dzdzjdzjdzjk', 'technology', 2),
(26, '2020-04-03 17:11:26', '2020-04-03 17:41:06', 'Marché', 'zdjjhdzhj', 'technology', 1225);

-- --------------------------------------------------------

--
-- Structure de la table `typebiens`
--

DROP TABLE IF EXISTS `typebiens`;
CREATE TABLE IF NOT EXISTS `typebiens` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `nom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `user_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `typebiens_user_id_foreign` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `typebiens`
--

INSERT INTO `typebiens` (`id`, `created_at`, `updated_at`, `deleted_at`, `nom`, `description`, `user_id`) VALUES
(1, '2020-04-04 18:03:25', '2020-04-04 18:03:25', NULL, 'Machines', NULL, 3),
(2, '2020-04-04 18:04:05', '2020-04-04 18:26:49', NULL, 'Véhicules, matériels roulants et Accessoires', NULL, 3),
(3, '2020-04-04 18:07:13', '2020-04-04 18:07:13', NULL, 'Electronique grand public', NULL, 3),
(4, '2020-04-04 18:07:35', '2020-04-04 18:08:21', NULL, 'Appareils électroménagers', NULL, 3),
(5, '2020-04-04 18:08:13', '2020-04-04 18:08:13', NULL, 'Vêtements', NULL, 3),
(6, '2020-04-04 18:08:57', '2020-04-04 18:08:57', NULL, 'Horloge, Bijouterie, lunetterie', NULL, 3),
(7, '2020-04-04 18:09:19', '2020-04-04 18:09:19', NULL, 'Lumières et éclairages', NULL, 3),
(8, '2020-04-04 18:09:38', '2020-04-04 18:09:38', NULL, 'Constructions et immobiliers', NULL, 3),
(9, '2020-04-04 18:10:06', '2020-04-04 18:10:06', NULL, 'Maisons et Jardins', NULL, 3),
(10, '2020-04-04 18:10:23', '2020-04-04 18:10:23', NULL, 'Meubles', NULL, 3),
(11, '2020-04-04 18:10:44', '2020-04-04 18:10:44', NULL, 'Textiles et produits en cuir', NULL, 3),
(12, '2020-04-04 18:10:58', '2020-04-04 18:10:58', NULL, 'Beauté et soins corporels', NULL, 3),
(13, '2020-04-04 18:11:29', '2020-04-04 18:11:29', NULL, 'Santé et Médecine', NULL, 3),
(14, '2020-04-04 18:11:56', '2020-04-04 18:11:56', NULL, 'Emballages et impressions', NULL, 3),
(15, '2020-04-04 18:12:41', '2020-04-04 18:12:41', NULL, 'Fournitures de bureau et scolaires', NULL, 3),
(16, '2020-04-04 18:13:01', '2020-04-04 18:13:01', NULL, 'Outils et matériels', NULL, 3),
(17, '2020-04-04 18:13:33', '2020-04-04 18:16:18', NULL, 'Sécurité et Protection', NULL, 3),
(18, '2020-04-04 18:16:46', '2020-04-04 18:16:46', NULL, 'Services', NULL, 3),
(19, '2020-04-04 18:17:24', '2020-04-04 18:17:24', NULL, 'Équipements électriques et fournitures', NULL, 3),
(20, '2020-04-04 18:18:10', '2020-04-04 18:18:10', NULL, 'Composants électroniques, accessoires et télécommunications', NULL, 3),
(21, '2020-04-04 18:18:39', '2020-04-04 18:18:39', NULL, 'Sport et Loisirs', NULL, 3),
(22, '2020-04-04 18:19:01', '2020-04-04 18:19:01', NULL, 'Jouets', NULL, 3),
(23, '2020-04-04 18:19:43', '2020-04-04 18:19:43', NULL, 'Objets artisanaux', NULL, 3),
(24, '2020-04-04 18:20:07', '2020-04-04 18:20:07', NULL, 'Bagages, Sacs et étuis', NULL, 3),
(25, '2020-04-04 18:20:35', '2020-04-04 18:20:35', NULL, 'Chaussures et Accessoires', NULL, 3),
(26, '2020-04-04 18:20:59', '2020-04-04 18:20:59', NULL, 'Produits chimiques', NULL, 3),
(27, '2020-04-04 18:21:25', '2020-04-04 18:21:25', NULL, 'Agriculture et Alimentation', NULL, 3),
(28, '2020-04-04 18:22:19', '2020-04-04 18:22:19', NULL, 'Boissons, emballages et accessoires', NULL, 3),
(29, '2020-04-04 18:23:12', '2020-04-04 18:23:12', NULL, 'Équipements de service', NULL, 3),
(30, '2020-04-04 19:58:20', '2020-04-04 19:58:31', NULL, 'Autres catégories', NULL, 3);

-- --------------------------------------------------------

--
-- Structure de la table `type_transferts`
--

DROP TABLE IF EXISTS `type_transferts`;
CREATE TABLE IF NOT EXISTS `type_transferts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `nom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `user_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type_transferts_user_id_foreign` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `type_transferts`
--

INSERT INTO `type_transferts` (`id`, `created_at`, `updated_at`, `deleted_at`, `nom`, `description`, `user_id`) VALUES
(1, '2020-04-03 01:26:22', '2020-04-03 01:26:22', NULL, 'Nom', 'ddjz', 4),
(2, '2020-04-03 01:26:39', '2020-04-03 01:26:39', NULL, 'lklklkkl', 'klklklklkl', 4);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `poste` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pseudo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_profil_id` bigint(20) NOT NULL,
  `photo_couverture_id` bigint(20) NOT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `etat` tinyint(1) NOT NULL DEFAULT '1',
  `type_compte_id` int(11) NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=110 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `poste`, `pseudo`, `email`, `photo_profil_id`, `photo_couverture_id`, `provider`, `provider_id`, `etat`, `type_compte_id`, `password`, `remember_token`, `email_verified_at`, `created_at`, `updated_at`) VALUES
(1, 'zkdkjdz', NULL, NULL, 'zdjkjkdz', 441, 44, 'klk', 'kkl\r\n', 1, 444, NULL, NULL, NULL, NULL, NULL),
(2, 'zkdkjdz', NULL, NULL, 'klkklkl', 441, 44, 'klk', 'kkl\r\n', 1, 444, NULL, NULL, NULL, NULL, NULL),
(3, 'ETEKA Wilfried ', 'Développeur Web Full Stack', NULL, 'etekawilfried@gmail.com', 0, 0, '0', '0', 1, 1, '$2y$10$nNKGEEM7dpKDOe0gh5GQ.ugHbAD5tmAsGbgx7uqgNHLGIvS7RMUXq', 'V9Bv9MveZGvGwLS4ed2HXPLOfr8WsEh6BfVQfwcCxjO3Qwj4EMHdaOLD8wXu', NULL, '2020-03-31 02:02:46', '2020-03-31 02:02:46'),
(4, 'Jadon Kohler V', NULL, NULL, 'zheller@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'H9PnrUW0OI', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(5, 'Brain Cassin', NULL, NULL, 'larson.shea@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '6ddGYYwjZF', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(6, 'Cathy Little', NULL, NULL, 'haag.donnie@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Pk4cUbTFMN', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(7, 'Neal Windler', NULL, NULL, 'filomena.auer@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'SNCiP0JZLp', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(8, 'Guy Cartwright', NULL, NULL, 'lempi70@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'TtzisRNnWe', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(9, 'Cecilia Terry V', NULL, NULL, 'bschumm@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'iApqMaJlU5', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(10, 'Mrs. Estella Stamm', NULL, NULL, 'vince.harber@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'anehizMFnY', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(11, 'Wilber Waelchi', NULL, NULL, 'cruickshank.josefina@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'wOC5ygjlc0', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(12, 'Nedra Bins', NULL, NULL, 'neal.stracke@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'FsZEwcDCQd', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(13, 'Johnathon Jaskolski IV', NULL, NULL, 'bosco.maiya@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'xHdeA82cno', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(14, 'Jazlyn Schroeder IV', NULL, NULL, 'jazlyn.shanahan@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'VsUp0RHN2U', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(15, 'Jada Lueilwitz', NULL, NULL, 'chand@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'l6klONMK0d', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(16, 'Ericka Rolfson', NULL, NULL, 'dee97@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'TlK9PK1QSv', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(17, 'Allene Huels', NULL, NULL, 'borer.eleanora@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '77eLXEjLCm', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(18, 'Mrs. Annabelle Lemke II', NULL, NULL, 'retha.huels@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2mAlkfjRsi', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(19, 'Kaden Stroman', NULL, NULL, 'sister07@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'JME3g9watu', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(20, 'Harry Eichmann', NULL, NULL, 'gleason.corbin@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'wg8VfhKcvf', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(21, 'Prof. Darius Lemke Jr.', NULL, NULL, 'nitzsche.lia@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'zWtgMTDl8o', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(22, 'Jeremy Kovacek', NULL, NULL, 'garfield.hyatt@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Yhduuy1Xfm', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(23, 'Sibyl Howe', NULL, NULL, 'jeffry.hoeger@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'z6nyxtCWwA', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(24, 'Krystina Torphy', NULL, NULL, 'clockman@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '6TpqzwSjvZ', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(25, 'Yolanda Ledner', NULL, NULL, 'bahringer.scot@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2h6vbqkq24', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(26, 'Dayne Witting II', NULL, NULL, 'orrin02@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'M1gFBrlAvI', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(27, 'Ricky Skiles II', NULL, NULL, 'victoria19@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'u1L0hdZy11', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(28, 'Sherwood Mayer', NULL, NULL, 'willis47@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'SF8SCYffbZ', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(29, 'Hulda Schumm', NULL, NULL, 'johnpaul15@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'mOdTdshsGT', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(30, 'Elsa D\'Amore', NULL, NULL, 'marlee99@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '9gvonUZvmS', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(31, 'Freida Jacobson', NULL, NULL, 'matilde95@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'dKVAgE0h9h', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(32, 'Dan Boyle', NULL, NULL, 'casey.jacobson@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'dxvOrYTkxG', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(33, 'Allan Bergstrom', NULL, NULL, 'charlotte.pfeffer@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '1bWjqINUlz', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(34, 'Miss Kristina Purdy IV', NULL, NULL, 'daugherty.clair@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'oI8Vpw3ZCB', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(35, 'Yolanda Johns', NULL, NULL, 'frieda06@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Y1GEUDQ0SG', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(36, 'Dr. Romaine Balistreri', NULL, NULL, 'larkin.elouise@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'rEmLoKQBcE', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(37, 'Miss Nikki Kautzer', NULL, NULL, 'wgleason@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'yWQhWKJh8j', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(38, 'Guy Ferry', NULL, NULL, 'helmer.dickinson@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'sGxgVTUdQc', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(39, 'Clifton Hill', NULL, NULL, 'dalton.tillman@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'PrTAPQDtdw', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(40, 'Maryam Zieme', NULL, NULL, 'edward31@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'wS7Gdbslyw', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(41, 'Mrs. Eliza Blanda', NULL, NULL, 'khill@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'zNli7gaEIN', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(42, 'Bud Glover', NULL, NULL, 'pfeffer.felix@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'CFOhARCHo1', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(43, 'Creola Abernathy', NULL, NULL, 'carroll75@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'AV2AjboBTX', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(44, 'Brant Hauck', NULL, NULL, 'whintz@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '0EhOohaW7d', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(45, 'Dr. Shaniya Jaskolski', NULL, NULL, 'friesen.deborah@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'tozjwnxSBL', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(46, 'Bradley Feeney', NULL, NULL, 'dfranecki@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'O3hyymoFCZ', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(47, 'Leonardo Russel', NULL, NULL, 'adela70@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '43pYEIB3TT', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(48, 'Kyler Conn', NULL, NULL, 'ulices79@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'HSG2YDZl6U', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(49, 'Reva Dicki V', NULL, NULL, 'kacie.balistreri@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ANQa8BFJye', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(50, 'Mr. Godfrey Kuhlman', NULL, NULL, 'bogisich.noemi@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'i59WciL03L', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(51, 'Mr. Columbus Satterfield', NULL, NULL, 'manley35@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'p4sYUPUEIo', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(52, 'Ashlee Moen', NULL, NULL, 'estevan75@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'T630lhbkcX', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(53, 'Ms. Zelda Volkman V', NULL, NULL, 'eloisa41@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'TYYlPldCG8', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(54, 'Jalyn Gibson', NULL, NULL, 'maurice.ritchie@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'YcpCZ1BBL4', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(55, 'Alan Gusikowski', NULL, NULL, 'bartell.devonte@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'SPY6Cv6Rl2', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(56, 'Cielo Farrell', NULL, NULL, 'brandyn55@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '7cxlWv7Bb2', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(57, 'Tatyana Rutherford', NULL, NULL, 'sswift@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'iunC1h1uSL', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(58, 'Carmella Terry', NULL, NULL, 'crona.london@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'edG0yOcAxu', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(59, 'Diego Howell', NULL, NULL, 'kozey.claudie@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'rd13tw6gPB', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(60, 'Ms. Ines Hagenes Sr.', NULL, NULL, 'jacobs.pat@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'iZikLTra4F', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(61, 'Hope Mitchell', NULL, NULL, 'hettinger.zelma@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'z6K4fOQkR2', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(62, 'Ms. Dixie Trantow', NULL, NULL, 'erling.jacobi@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'kZiogkef5Z', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(63, 'Mrs. Allison Berge', NULL, NULL, 'vward@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'jvLlvVoiwo', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(64, 'Nestor Turner', NULL, NULL, 'gvonrueden@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'y7uehrN6LG', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(65, 'Petra Rowe', NULL, NULL, 'haylee79@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'cmfK8n8DDH', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(66, 'Kaia Jacobson DDS', NULL, NULL, 'erutherford@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'yrAK9lqUv5', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(67, 'Ms. Brielle Cartwright', NULL, NULL, 'nsipes@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'P3AAxUPnlq', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(68, 'Letha Bailey', NULL, NULL, 'trycia.brown@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'v9ObKzxo9M', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(69, 'Johnny Rau', NULL, NULL, 'aubree.zieme@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'f8bAeVIcN2', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(70, 'Sherman Oberbrunner', NULL, NULL, 'grady.joaquin@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '7RwoCGNUyG', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(71, 'Javonte Goyette', NULL, NULL, 'becker.dolores@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'XWjTmQuayz', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(72, 'Rosa Hermiston PhD', NULL, NULL, 'weber.daron@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'LcOIFky9K7', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(73, 'Mr. Verner Franecki II', NULL, NULL, 'gwilliamson@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'lHu3p4up8M', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(74, 'Deon Rowe', NULL, NULL, 'lafayette70@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'pgcVj3E1hq', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(75, 'Mrs. Ashlee Volkman DVM', NULL, NULL, 'katrina.damore@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'RYNvFG2UfI', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(76, 'Kassandra Considine', NULL, NULL, 'qlittel@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'cfkLWQaAZA', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(77, 'Jeffry Weissnat', NULL, NULL, 'wilburn.bode@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'UFizJ2dT6d', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(78, 'Prof. Jerad Kemmer', NULL, NULL, 'adams.consuelo@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '19qk3rNfTc', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(79, 'Prof. Doyle Kunze', NULL, NULL, 'nbernhard@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'J5eenOpX7M', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(80, 'Cortez Ortiz', NULL, NULL, 'madge.anderson@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'UwVz5liMgO', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(81, 'Ward Treutel', NULL, NULL, 'murazik.darryl@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'pQtc9yzY25', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(82, 'Ezekiel Pfeffer', NULL, NULL, 'miller.danika@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'xkqGO44MOP', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(83, 'Dr. Deborah O\'Conner', NULL, NULL, 'hansen.ida@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '8Gr0GGjwx4', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(84, 'Prof. Cora Boyer IV', NULL, NULL, 'upton.abbie@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'fKXv6HWvgs', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(85, 'Dr. Rashad Murazik', NULL, NULL, 'rosendo.blick@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'SwAivvka9a', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(86, 'Hadley Bayer', NULL, NULL, 'roberta01@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '1LQ9oypTuK', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(87, 'Isidro Smitham', NULL, NULL, 'kward@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'PaynAQFj0t', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(88, 'Twila Olson', NULL, NULL, 'hamill.casper@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'tLrIgyRwEz', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(89, 'Dr. Hilbert Considine Jr.', NULL, NULL, 'katrine95@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'RlxWYak81g', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(90, 'Prof. Boyd O\'Conner DVM', NULL, NULL, 'langosh.catharine@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2WXeLbxeBM', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(91, 'Keaton Hyatt MD', NULL, NULL, 'hettinger.jeffry@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'iofxK7rMa2', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(92, 'Florian Murphy DVM', NULL, NULL, 'mariana33@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '7LW8sImc7f', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(93, 'Ms. Brianne Yundt IV', NULL, NULL, 'littel.hailie@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'sGLtsD8OyS', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(94, 'Jayde Douglas', NULL, NULL, 'stefanie.oconner@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'FXo19HgyMy', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(95, 'Mr. Hazel Johnson I', NULL, NULL, 'jefferey03@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'QEstYczZGi', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(96, 'Ms. Liliana Abernathy', NULL, NULL, 'else.zemlak@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'j4IBcWYy8K', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(97, 'Cordia Bernier', NULL, NULL, 'pswift@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'B9eG8i2VXK', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(98, 'Jamel Connelly Jr.', NULL, NULL, 'kuhn.jayme@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'd5Pbhmjuj8', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(99, 'Milford Reynolds', NULL, NULL, 'parker.althea@example.com', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Nw895TUIQN', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(100, 'Jaylen Hessel', NULL, NULL, 'wjerde@example.net', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'XaM4BNGkhO', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(101, 'Rod Rowe DVM', NULL, NULL, 'wolff.elena@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'qsQ1wwnt9l', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(102, 'Ms. Jody O\'Connell PhD', NULL, NULL, 'emilia79@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'EEuTYfy12l', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(103, 'Alexie Williamson Sr.', NULL, NULL, 'lempi.bashirian@example.org', 0, 0, '0', '0', 1, 1, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'XThAxBwei5', '2020-03-31 12:38:49', '2020-03-31 11:38:49', '2020-03-31 11:38:49'),
(104, 'Marcelo Doko', NULL, 'marcelo', 'marcelo@gmail.com', 0, 0, '0', '0', 1, 1, '$2y$10$s7J8QRdflB7JokwocsuDaOg1HQfEgRMinIsajBBm5Z4WSjr3rKtJG', NULL, NULL, '2020-04-20 23:18:51', '2020-04-20 23:18:51'),
(105, 'zddz', NULL, 'zddz', 'dzdzzetekawilfried@gmail.com', 0, 0, '0', '0', 1, 1, '$2y$10$VGim2hVVcfHjz3L0To/EPuBsAmyDMQFKh2uc4LTvnzIdivSWfG5Gy', NULL, NULL, '2020-04-20 23:44:20', '2020-04-20 23:44:20'),
(106, 'Eric Emmanuel HOUNKPONOU', NULL, 'eric', 'joseric@yahoo.fr', 0, 0, '0', '0', 1, 1, '$2y$10$EZe8hFNq4hpLiBM22kr4sud8XoVN6Nwd8Kd0djrG2xz5kP6l/2h8a', NULL, NULL, '2020-04-21 00:09:39', '2020-04-21 00:09:39'),
(107, 'zkddjzkdz dzdzdz', NULL, 'zkddjzkdz', 'srias@gmail.com', 0, 0, '0', '0', 1, 1, '$2y$10$937Tk8MRNAHmqH01vUgiKOSbH4SS10LmQBSycsSPcih63c2IAK4uG', NULL, NULL, '2020-04-21 23:48:56', '2020-04-21 23:48:56'),
(108, 'deed', NULL, 'deed', 'eedde@zzzddz', 0, 0, '0', '0', 1, 1, '$2y$10$807ZTmRxqkvco2iDP3hWW.QwD6DlCBt2Uqv65tLE8J90Jlw34zAdy', NULL, NULL, '2020-05-01 17:41:57', '2020-05-01 17:41:57'),
(109, 'zddz', NULL, 'zddz1', 'zdzd@dznzdnz.fr', 0, 0, '0', '0', 1, 1, '$2y$10$gMwBdF7z65MkjuTWj/fSr.wD3/T0dMy0JYwhjNvs8r6ox.TUZBtT2', NULL, NULL, '2020-05-03 14:43:30', '2020-05-03 14:43:30');

-- --------------------------------------------------------

--
-- Structure de la table `user_bien_favoris`
--

DROP TABLE IF EXISTS `user_bien_favoris`;
CREATE TABLE IF NOT EXISTS `user_bien_favoris` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `bien_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `ref` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`user_id`,`bien_id`),
  KEY `favoris_bien_id_foreign` (`bien_id`),
  KEY `user_id` (`user_id`,`bien_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user_bien_favoris`
--

INSERT INTO `user_bien_favoris` (`user_id`, `bien_id`, `created_at`, `updated_at`, `deleted_at`, `ref`) VALUES
(3, 56, '2020-04-23 22:51:08', '2020-04-23 22:51:08', NULL, NULL),
(3, 88, '2020-04-23 04:57:36', '2020-04-23 04:57:36', NULL, NULL),
(3, 75, '2020-04-23 21:31:01', '2020-04-23 21:31:01', NULL, NULL),
(3, 77, '2020-04-23 21:31:06', '2020-04-23 21:31:06', NULL, NULL),
(3, 74, '2020-04-23 21:31:10', '2020-04-23 21:31:10', NULL, NULL),
(3, 73, '2020-04-23 21:31:15', '2020-04-23 21:31:15', NULL, NULL),
(3, 119, '2020-04-23 22:41:45', '2020-04-23 22:41:45', NULL, NULL),
(3, 118, '2020-04-23 21:22:52', '2020-04-23 21:22:52', NULL, NULL),
(3, 68, '2020-04-23 22:45:25', '2020-04-23 22:45:25', NULL, NULL),
(3, 70, '2020-04-23 22:44:51', '2020-04-23 22:44:51', NULL, NULL),
(3, 67, '2020-04-23 22:45:12', '2020-04-23 22:45:12', NULL, NULL),
(3, 69, '2020-04-23 22:45:19', '2020-04-23 22:45:19', NULL, NULL),
(3, 65, '2020-04-23 22:45:06', '2020-04-23 22:45:06', NULL, NULL),
(3, 71, '2020-04-23 22:44:58', '2020-04-23 22:44:58', NULL, NULL),
(3, 72, '2020-04-23 22:44:47', '2020-04-23 22:44:47', NULL, NULL),
(3, 45, '2020-04-23 21:30:35', '2020-04-23 21:30:35', NULL, NULL),
(3, 115, '2020-04-23 21:30:57', '2020-04-23 21:30:57', NULL, NULL),
(3, 87, '2020-04-23 04:57:28', '2020-04-23 04:57:28', NULL, NULL),
(3, 21, '2020-04-19 23:04:30', '2020-04-19 23:04:30', NULL, NULL),
(3, 117, '2020-04-23 22:48:21', '2020-04-23 22:48:21', NULL, NULL),
(3, 60, '2020-04-23 22:51:16', '2020-04-23 22:51:16', NULL, NULL),
(3, 62, '2020-04-23 22:51:26', '2020-04-23 22:51:26', NULL, NULL),
(3, 61, '2020-04-23 22:51:32', '2020-04-23 22:51:32', NULL, NULL),
(3, 58, '2020-04-27 15:55:44', '2020-04-27 15:55:44', NULL, NULL),
(3, 121, '2020-04-27 15:55:57', '2020-04-27 15:55:57', NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
