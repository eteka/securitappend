<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersregistrationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_registration', function (Blueprint $table) {
            $table->bigIncrements('id');           
           
            $table->string('ip')->nullable();
            $table->string('origin')->nullable();
            $table->string('browser')->nullable();
            $table->string('url')->nullable();
            $table->string('protocole')->nullable();
            $table->string('http_accept')->nullable();
            $table->string('http_encoding')->nullable();
            $table->string('http_languages')->nullable();
            $table->string('app_lang')->nullable();
            $table->string('country')->nullable();
            $table->string('rcode1');
            $table->string('rcode2');
            //'user_id', 'ip', 'origin', 'browser', 'url', 'protocole', 'http_accept', 'http_encoding', 'http_languages', 'app_lang'
            $table->longText('infos')->nullable();
            $table->unsignedBigInteger('user_id')->unsigned(); 
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usersregistration');
    }
}
