<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBiensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('biens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nom');
            $table->biginteger('mode_acquisition_id')->default(0);
            $table->string('lieu_achat');            
            $table->biginteger('marque_id')->default(0);            
            $table->biginteger('modele_id')->default(0);
            $table->string('visibilite');
            $table->string('vendeur')->default(NULL);
            $table->date('date_achat')->default(NULL);
            $table->string('imei1')->default(NULL);
            $table->string('imei2')->default(NULL);
            $table->biginteger('prix_achat')->default(0);
            $table->longtext('infos');
            $table->biginteger('user_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('biens');
    }
}
