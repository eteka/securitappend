<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckValidateEmail
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      /*if(!Auth::user()){
        return redirect('login');
      }*/
        $user=Auth::user();
      if(!($user && $user->email_verified_at==NULL)){

           //->route('email_verified');

         return $next($request);
       }
       return redirect(route('email_verified'));

    }
}
