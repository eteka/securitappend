<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Modele;
use Illuminate\Http\Request;

class ModeleController extends Controller
{
    protected  $perPage = 10;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $order=strtolower($request->get('orderBy'))=="asc"?"asc":"desc";
        $perPage = $this->perPage;
        $total=Modele::select('id')->count();
        
        if (!empty($keyword)) {
            $modele = Modele::where('nom', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('marque_id', 'LIKE', "%$keyword%")
                ->orWhere('user_id', 'LIKE', "%$keyword%")
                ->orderBy("id",$order)->paginate($perPage);
        } else {
            $modele = Modele::orderBy("id",$order)->paginate($perPage);
        }

        return view('admin.modele.index', compact('modele','total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.modele.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'nom' => 'required|max:200'
		]);
        $requestData = $request->all();
        
        Modele::create($requestData);

        return redirect('admin/modele')->with('success', __("Sauvegarde effectuée !"));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $modele = Modele::findOrFail($id);

        return view('admin.modele.show', compact('modele'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $modele = Modele::findOrFail($id);

        return view('admin.modele.edit', compact('modele'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'nom' => 'required|max:200'
		]);
        $requestData = $request->all();
        
        $modele = Modele::findOrFail($id);
        $modele->update($requestData);

        return redirect('admin/modele')->with('info', __("Mis à jour effectuée"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Modele::destroy($id);

        return redirect('admin/modele')->with('danger',  __("Suppression effectuée"));
    }
}
