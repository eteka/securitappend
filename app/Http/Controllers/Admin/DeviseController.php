<?php

namespace App\Http\Controllers\Admin;


use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Devise;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class DeviseController extends Controller
{
    protected  $perPage = 10;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $order=strtolower($request->get('orderBy'))=="asc"?"asc":"desc";
        $perPage = $this->perPage;
        $total=Devise::select('id')->count();
        
        if (!empty($keyword)) {
            $devise = Devise::where('devise', 'LIKE', "%$keyword%")
                ->orWhere('Sigle', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orderBy("id",$order)->paginate($perPage);
        } else {
            $devise = Devise::orderBy("id",$order)->paginate($perPage);
        }

        return view('admin.devise.index', compact('devise','total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.devise.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'devise' => 'required|max:50',
			'sigle' => 'required|max:15'
		]);
        $requestData = $request->all();
        $requestData['user_id']=Auth::user()?Auth::user()->id:0;

        Devise::create($requestData);

        return redirect('admin/devise')->with('success', __("Sauvegarde effectuée !"));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $devise = Devise::findOrFail($id);

        return view('admin.devise.show', compact('devise'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $devise = Devise::findOrFail($id);

        return view('admin.devise.edit', compact('devise'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'devise' => 'required|max:50',
			'sigle' => 'required|max:15'
		]);
        $requestData = $request->all();
        $requestData['user_id']=Auth::user()?Auth::user()->id:0;

        
        $devise = Devise::findOrFail($id);
        $devise->update($requestData);

        return redirect('admin/devise')->with('info', __("Mis à jour effectuée"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Devise::destroy($id);

        return redirect('admin/devise')->with('danger',  __("Suppression effectuée"));
    }
}
