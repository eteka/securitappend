<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use App\Models\RegistrationSaver;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Request as URequest;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => ['required', 'string', 'min:3','max:100','string'],
            'prenom' => ['required', 'string', 'min:3','max:100','string'],
            'email' => ['required', 'string',
            'email',
            //'email:rfc,dns',
              'max:150', 'unique:users'],
              'password' => ['required',
               'min:6','max:30',
               'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/','confirmed'],
           // 'password' => ['required', 'string', 'min:8',],
          /*  'regex:/[a-z]/',      // must contain at least one lowercase letter
            'regex:/[A-Z]/',      // must contain at least one uppercase letter
            'regex:/[0-9]/',      // must contain at least one digit
            'regex:/[@$!%*#?&]  /'], // must contain a special character*/
            //'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%]).*$/', 'confirmed'],
            'sexe' => ['required', 'string', 'in:H,F']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $psdo=$this->generatePseudo($data['nom'],$data['prenom']);

        $u=User::create([
            'nom' => $data['nom'],
            'prenom' => $data['prenom'],
            'email' => $data['email'],
            'sexe' => $data['sexe'],
            'pseudo' => $psdo,
            'password' => Hash::make($data['password']),
        ]);
       
        return $u;
    }
    
    public function register(Request $request)
    {        
       
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));
        $a=$this->saveRUser($user,$request);
        
        $this->guard()->login($user);

        if ($response = $this->registered($request, $user)) {
            return $response;
        }

        return $request->wantsJson()
                    ? new Response('', 201)
                    : redirect($this->redirectPath());
       
        //return redirect($this->redirectTo);
    }

    public static function saveRUser(User $u,Request $request){
        $uid=$u->id;
        $code=rand(10,99)."-".rand(10,99)."-".rand(10,99);
        $ip=URequest::ip();
        $url=URequest::fullUrl();
        $browser=$request->header('User-Agent');
        $http_accept=$request->header('accept');
        $lang_accept=$request->header('accept-language');
        $encoding=$request->header('accept-encoding');
        $protocole=URequest::isSecure()?'https':'http';
        $lang=config('app.locale');
        
        $data=[
        'user_id'=>$uid,
        'rcode1'=>$code,
        'rcode2'=>bcrypt($uid), 
        'ip'=>$ip, 
        'origin'=>'web', 
        'browser'=>$browser, 
        'url'=>$url, 
        'protocole'=>$protocole, 
        'http_accept'=>$http_accept, 
        'http_encoding'=>$encoding, 
        'http_languages'=>$lang_accept, 
        'app_lang'=>$lang,
        'infos'=>''];
        
       return RegistrationSaver::create($data);
    }

    public function generatePseudo( $nom,$prenom )
    {
        $exp1=explode(' ',$nom);
        $exp2=explode(' ',$prenom);

        $p1=isset($exp1[0])?$exp1[0]:srand(10);
        $p2=isset($exp2[0])?$exp2[0]:srand(10);

        $p1=Str::slug($p1,'');
        $p2=Str::slug($p2,'');
        if(strlen($p1)>20){$p1=substr($p1,0,20);}
        $p=$pseudo=strtolower($p1.".".$p2);
        $p=$pseudo=substr($p,0,30);
        $t=1;$i=0;

        $compteur=0;
        do {

            if($i>0){ $pseudo=$p.$i;}
            $compteur= User::where('pseudo',$pseudo)->count();
            $i++;
        } while ($compteur >0);
        return ($pseudo);
    }
}
