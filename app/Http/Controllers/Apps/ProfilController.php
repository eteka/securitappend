<?php


namespace App\Http\Controllers\Apps;


use App\Http\Controllers\Controller;
use App\Mail\SendOnRegister;
use App\Models\Media;
use App\Models\Pays;
use App\Models\RegistrationSaver;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Image;
use PhpParser\Node\Stmt\TryCatch;

use function PHPSTORM_META\type;

class ProfilController  extends Controller
{

   
    protected $cd_profil_img="cd1/users/profils";
    protected $cd_profil_img_mini="cd1/users/profils/mini";
    protected $profil_mini_width=256;
    protected $profil_mini_height=256;
    /**
     * Create a new controller instance:Auth Middleware.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show Notification web page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function notification(Request $request)
    {

        return view('web.profil.notification');
    }
   
    /**
     * Show page to setting user profil.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit_profil (Request $request,$pseudo)
    {
        $user=User::where("pseudo",$pseudo)->firstOrFail();;
      
       
        if($user==null){
            abort(404);
        }
        return view('web.profil.edit_profil',compact('user'));
    }
    /**
     * Show page to setting user profil.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show_edit_password  (Request $request,$pseudo)
    {
        $user=User::where("pseudo",$pseudo)->firstOrFail();
        return view('web.profil.edit_password',compact('user'));
    }
    /**
     * show_edit_parametre : Show page to edit user profil informations.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show_edit_parametre   (Request $request,$pseudo)
    {
        $lang=config('app.locale');
        if($lang=='fr'){
            $pays=Pays::orderBy('nom_fr_fr')->pluck('nom_fr_fr','id')->prepend('Choisir votre pays...','');
        }else{
            $pays=Pays::orderBy('nom_en_gb')->pluck('nom_en_gb','id')->prepend('Choisir votre pays...','');
        }
        $user=User::where("pseudo",$pseudo)->firstOrFail();
        return view('web.profil.edit_parametre',compact('user','pays'));
    }
    /**
     * save_edit_password to change user password information.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function save_edit_password (Request $request,$pseudo)
    {
      
        $message_type="default";
        $message='';
        if(Auth::Check() && Auth::user()->pseudo==$pseudo)
        {
            $user=Auth::user();
            $request_data = $request->All();
            if (!(Hash::check($request->get('lastpassword'), Auth::user()->password))) {
                // The passwords matches
                return redirect()->back()->withErrors(["lastpassword"=>__("user.lastpaswword_incorrect")]);
            }
    
            if(strcmp($request->get('lastpassword'), $request->get('password')) == 0){
                //Current password and new password are same
               
                return redirect()->back()->withErrors(["password"=>__("user.samepaswword_incorrect")]);
            }
    
            $validatedData = $request->validate([
                'lastpassword' => 'required',
                'password' => 'min:6|max:30|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/|confirmed', 
               
            ]);
           /// $validator = $this->validateSavePassword($request_data);
           
             
            $message_type="success";
            $message=__('user.password_success');
            $user->password = Hash::make($request_data['password']);
            $user->save(); 
            Auth::logout();
            return redirect(route('login'))->with($message_type, $message);            
                    
        }
        else
        {
            return redirect()->to('/');
        }  
            

                        /*$this->validate($request, [
                            'lastpassword' => 'required|string|min:3|max:100|string',
                    'password' => 'min:6|max:30|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/|confirmed', 
                ]);*/
                
              
    }
    /**
     * save_profil_info to save profil information.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function save_profil_info (Request $request,$pseudo)
    {
        $user=Auth::user();
       $type=$request->input('type');
      // dd($type);
        $message_type="default";
        $message='';
        if($type=="compte" && $user->pseudo==$pseudo){ //Edition du compte

                $this->validate($request, [
                    'nom' => 'required|string|min:3|max:100|string',
                    'prenom' => 'required|string|min:3|max:100|string',                
                    'sexe' => 'required|string|in:H,F',
                    'poste' => 'min:0|max:200',
                    'profil_img' => "image|mimes:jpeg,png,jpg|max:2048"
                ]);

                
                //'email' => 'required|string|email|max:150|unique:users',    
                $requestData = $request->all();
                if($requestData['email']!=$user->email){
                    $this->validate($request, [
                        'email' => 'required|string|email|max:150|unique:users',
                    ]);
                    
                    $requestData = $request->all();
                    $user->email=$requestData['email'];
                }


                if($request->hasFile('profil_img')) {
                    if($request->file('profil_img')->isValid()){
                       
                        $image = $request->file('profil_img');
                        $fileName   = Str::random(30)."_".time(). '.' . $image->getClientOriginalExtension();
                        $ext=$image->getClientOriginalExtension();
        
                        //5- Sauvegarde  de l'original
                        $path = $image->storeAs("public/".$this->cd_profil_img,$fileName);
                        
                       //6- Sauvegarde  de la miniature
                        $img = Image::make($image->getRealPath());
                        $img->resize(256, 256, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                       
                        $path_mini=$this->cd_profil_img_mini."/".$fileName;
                        $img->stream();
                        $rs=Storage::disk('local')->put("public/".$path_mini, $img, 'public');
                       
                        
                        $npath=str_replace("public/",'',$path);
                        $media=[
                            'title' => 'Profil '.$user->fullname(),
                            'type' => "IMAGE",
                            'url' => "storage/".$npath,
                            'ref' => "users",
                            'ext' => $ext,
                            'user_id' => $user->id,
                            'ref_id' => "profil",
                            'mini' => "storage/".$path_mini,
                        ];
                        $media=Media::create($media);
                        $media_id=$media->id;
                        // 7- Mise a jour du Média
                        $user->photo_profil_id=$media_id;
                   
                    }else{
                        $message_type="warning";
                        $message=__("Erreur : Image nom valide");
                    }
                }
                
                $user->nom=$requestData['nom'];
                $user->prenom=$requestData['prenom'];
                $user->sexe=$requestData['sexe'];
                $user->poste=$requestData['poste'];
                $user->save();
                if($message==''){
                    $message_type="success";
                    $message=__('Mise à jour du compte effectuée avec succès!');
                }
                return redirect(route('edit_profil',$user->getPseudo()))->with($message_type, $message);
        
            
        }
        if($type=="profil" && $user->pseudo==$pseudo){
             //Edition du compte
           // dd($request);
            
                $this->validate($request, [
                   // 'profil_id' => 'string|min:3|max:100|string',
                    'ville' => 'string|min:3|max:100|string', 
                    'about' => "max:6000",
                    
                ]);

                
                //'email' => 'required|string|email|max:150|unique:users',    
                $requestData = $request->all();
                
                #GESTION DU PROFIL
                if(isset($requestData['profil_id']) && $requestData['profil_id']!=$user->pseudo  && $user->pseudo_editable){
                    $this->validate($request, [
                        'profil_id' => 'string|unique:users,pseudo',
                    ]);
                    
                    $user->pseudo=$requestData['profil_id'];
                    $user->pseudo_editable=0;
                }
                #GESTION DU PAYS
                if($requestData['pays']!=''){
                    $this->validate($request, [
                        'pays' => 'integer|exists:pays,id',
                    ]);                    
                    $user->pays_id=$requestData['pays'];
                }
                #TYPE DE PROFIL
                if($requestData['type_profil']!=''){                    
                   if($requestData['type_profil']=='1'){
                    $user->profil_state=TRUE;
                   }else{
                    $user->profil_state=FALSE;
                   }                    
                }
                if($requestData['site_web']!=''){                    
                    $this->validate($request, [
                        'site_web' => 'url|max:200',
                    ]);
                    $user->website=$requestData['site_web'];
                }
                $user->apropos=$requestData['about'];
                $user->ville=$requestData['ville'];
                $user->save();
                $message=__('Mise à jour du profil effectuée avec succès!');
                
                return redirect(route('show_edit_parametre',$user->getPseudo()))->with('success', $message);
            }
        return abort(404);       
        
    
    }
    /**
     * Show page to setting user profil.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function profil_settings(Request $request)
    {
        return view('web.profil.profil_settings');
    }
    /**
     * Show page to setting user profil.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function help_center(Request $request)
    {
        return view('web.help.center');
    }
    /**
     * Show page to setting laguage and hours of user.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function lang_settings(Request $request)
    {
        return view('web.profil.lang_settings');
    }
    /**
     * Show page to setting account security setting of user.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function account_security(Request $request)
    {
        return view('web.profil.account_security');
    }

    /**
     * Show page to account setting of bussiness account.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function account_business(Request $request)
    {
        return view('web.profil.account_business');
    }
    /**
     * function email_verified
     * Show page to enter user code sor account validation
     * @param NULL
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function email_verified()
    {
        $u=Auth::user();
        if($this->redirectIfValide($u)){return redirect('home');}
        return view('web.profil.email_verified');
    }
    public function email_validation(Request $request){
        
        
        $user=Auth::user();
        //if($this->redirectIfValide($user)){return redirect('home');}
        $this->validate($request, [
            'code1' => 'required|min:2|string',
            'code2' => 'required|min:2|string',
            'code3' => 'required|min:2|string'
         ]);
        
        $data=$request->all();
        $code =$data['code1']."-".$data['code2']."-".$data['code3'];
        $userR=RegistrationSaver::where('user_id',$user->id)->where('rcode1',$code)->first();//->where('etat',FALSE)
       
        /*$data=[
            'name'=>"DEGBE Oscar","message"=>"Je suis fier de vous"
          ];
          //mail("etekawilfried@gmail.com",'OInsc',"Vous êtes inscrit");
          #Mail::to('etekawilfried@gmail.com')->send(new SendOnRegister($data));*/
        
        if($userR){
            $user=Auth::user();
            $user->email_verified_at =time();
            $user->save();
            $userR->etat=1;
            $userR->save();
            $message_type='success';
            $message=__('user.account_valide');
           return redirect(route('account_verified'))->with($message_type,$message);
        }
        $errors=['code'=>"Code de validation non valide"];
        return back()->withErrors($errors);
    }
    public function account_verified(Request $request){
        $user=Auth::user();
        return view('web.profil.account_verified',compact('user'));
    }
    public function redirectIfValide(User $user){
        if($user->etat==1){           
          return  1;
        }
        return 0;
        
    }
}
