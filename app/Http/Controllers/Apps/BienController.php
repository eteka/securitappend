<?php

namespace App\Http\Controllers\Apps;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Str;
use App\Models\Bien;
use App\Models\Typebien;
use App\Models\Marque;
use App\Models\Media;
use App\Models\Devise;
use App\Models\Modele;
use App\Models\ModeAcquisition;
use App\User;
use Excel;
#use DataTables;
use Yajra\DataTables\DataTables;
use Auth;
use Image;
use Storage;

class BienController extends Controller
{
    protected $table="biens";
    public $perPage = 25;
    protected $cd_biens="cd1/medias";//thumbnail'
    protected $cd_biens_mini="cd1/medias/thumb";
    protected $mode_acquisition_table="mode_acquisitions";//mode_typebien_table
    protected $mode_typebien_table="typebiens";
    protected $mode_devise_table="devises";
    protected $mini_width=350;
    protected $mini_height=350;

    protected $width=3000;
    protected $height=2000;
     /**
     * Create a new controller instance:Auth Middleware.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function getUser($nb=10)
    {
        $users = User::select('id','name','email')->latest()->get();
        return Datatables::of( $users )->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        /*$keyword = $request->get('search');


        if (!empty($keyword)) {
            $posts = Bien::where('nom', 'LIKE', "%$keyword%")
                ->orWhere('lieu_achat', 'LIKE', "%$keyword%")
                ->orWhere('vendeur', 'LIKE', "%$keyword%")
                ->orWhere('infos', 'LIKE', "%$keyword%")
                ->latest()->paginate($this->perPage);
        } else {
            $posts = Post::latest()->paginate($this->perPage);
        }

        return view('admin.posts.index', compact('posts'));*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modes=$this->getModes();
        $categories=$this->getTypebiens();
        $devises=$this->getDevises();

        return view('apps.bien.create',compact('modes','categories',"devises"));
    }
/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   /* public function trash()
    {
        $modes=$this->getModes();
        $categories=$this->getTypebiens();
        $devises=$this->getDevises();
        //dd($categories);
        return view('apps.bien.create',compact('modes','categories',"devises"));
    }*/

    public function getModes()
    {
        return ModeAcquisition::orderBy('nom','asc')->pluck('nom','id')->prepend(__('Choisir un mode…'), '');
    }

    public function getTypebiens()
    {
        return Typebien::orderBy('nom','asc')->pluck('nom','id')->prepend(__('Choisir une catégorie…'), '');
    }
    public function getDevises()
    {
        return Devise::where('sigle','!=',null)->orderBy('sigle','asc')->pluck('sigle','id');//->prepend(__('Choisir une devise…'), '');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'nom_objet' => 'required|min:3|max:150',
            'visibilite' => 'required|in:PUBLIC,PRIVE',
            'mode' => 'required|max:150|integer|exists:'.$this->mode_acquisition_table.',id',
            'categorie' => 'required|integer|exists:'.$this->mode_typebien_table.',id',
            'lieu_objet' => 'required|max:180',
            'prix_object' => 'numeric|min:0|max:9999999999',
            'date_objet' => '',

            'vendeur' => 'max:180',
            'marque' => 'max:180',
            'modele' => 'max:180',
            'infos' => 'max:1800',
            'devise' => 'required|integer|exists:'.$this->mode_devise_table.',id',
            'photos' => "image|mimes:jpeg,png,jpg,gif|max:2048"
         ]);


        $requestData = $request->all();

        // 1- Save bien
        /*f
        * Traitement des données
        */
        $date_achat=$requestData['date_objet']==NULL?NULL:$requestData['date_objet'];
        $prix=$requestData['prix_objet']!=NULL?$requestData['prix_objet']:0;
        $data=[
            'nom'=>$requestData['nom_objet'],
            'mode_acquisition_id'=>$requestData['mode'],
            'lieu_achat'=>$requestData['lieu_objet'],
            'visibilite'=>$requestData['visibilite'],
            'vendeur'=>$requestData['vendeur'],
            'date_achat'=>$date_achat,
            'imei1'=>$requestData['imei1'],
            'imei2'=>$requestData['imei2'],
            'prix_achat'=>$prix,
            'typebien_id'=>$requestData['categorie'],
            'devise_id'=>$requestData['devise'],
            'infos'=>$requestData['infos'],
            'user_id'=>$this->getUserId(),
        ];

        //Sauvegarde
        $bien=Bien::create($data);

        // 2- Save and attach Marque
        $marque_id=0;
        if($requestData['marque']!=NULL){
            $SearchMarque=Marque::where('nom',  $requestData['marque'])->first();

            if($SearchMarque && $SearchMarque->count()>0){
                $marque_id=$SearchMarque->id;
            }else{
                $savemarque=Marque::create(['nom' => $requestData['marque'],'description'=>"",'user_id'=>$this->getUserId()]);
                $marque_id=$savemarque->id;
            }
            $bien->marque_id=$marque_id;
            $bien->save();
           // $bien->marque()->attach($marque_id);
        }
        // 3- Save and attach Mode
        $modele_id=0;
        if($requestData['modele']!=NULL){
            $SearchModele=Modele::where('nom',trim($requestData['modele']))->first();
            if($SearchModele && $SearchModele->count()>0){
                $modele_id=$SearchModele->id;
            }else{
                $save=Modele::create(['nom' => $requestData['modele'],'description'=>"",'marque_id'=>$marque_id,'user_id'=>$this->getUserId()]);
                $modele_id=$save->id;
            }
            $bien->modele_id=$modele_id;
            $bien->save();
            //$bien->modele()->attach($modele_id);
        }

        // 4- Save Original and Thumbnail Image
        $message="";
        if($request->hasFile('photos')) {
            if($request->file('photos')->isValid()){
                $image = $request->file('photos');
                $fileName   = Str::random(30)."_".time(). '.' . $image->getClientOriginalExtension();
                $ext=$image->getClientOriginalExtension();

                //5- Sauvegarde  de l'original
                $path = $image->resize($this->width, $this->mini_height, function ($constraint) {
                    $constraint->aspectRatio();
                })->storeAs("public/".$this->cd_biens,$fileName);

                //6- Sauvegarde  de la miniature
                $img = Image::make($image->getRealPath());
                $img->resize($this->mini_width, $this->mini_height, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $path_mini=$this->cd_biens_mini."/".$fileName;
                $img->stream();
                Storage::disk('local')->put("public/".$path_mini, $img, 'public');

                $b=new Bien();

                $tbl=$b->getTable();
                $npath=str_replace("public/",'',$path);
                $media=[
                    'title' => $requestData['nom_objet'],
                    'type' => "IMAGE",
                    'url' => "storage/".$npath,
                    'ref' => $tbl,
                    'ext' => $ext,
                    'user_id' => $this->getUserId(),
                    'ref_id' => $bien->id,
                    'mini' => "storage/".$path_mini,
                ];
                $media=Media::create($media);
                $media_id=$media->id;
                // 7- Mise a jour du Média
                $bien->media_id=$media_id;
                $bien->save();

            }else{
                $message=__("Erreur : Image nom valide");
            }
        }
        if($message==NULL)
        $message=__('Bien ajouté avec succès!');
        return redirect('/home')->with('flash_message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bien=Bien::findOrfail($id);
        //$nb_favoris=Auth::user()->biensFavoris()->count();
        $biens=null;
        if(Auth::user()){
        $biens=$bien->user->biens->where('id','!=',$id)->take(6);
        }
        return view('apps.bien.show', compact('bien','biens'));
    }
     /**
     * Display the article activity
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_activite($id)
    {
        $bien=Bien::findOrfail($id);
        return view('apps.bien.show_activites', compact('bien'));
    }

    /**
    * Display the article Mouvement
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function show_mouvement($id)
   {
       $bien=Bien::findOrfail($id);
       return view('apps.bien.show_mouvements', compact('bien'));
   }
    /**
    * Display the article Security
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show_securite($id)
    {
        $bien=Bien::findOrfail($id);
        return view('apps.bien.show_securite', compact('bien'));
    }
    /**
    * Display the article Statistique
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show_accessoire($id)
    {
        $bien=Bien::findOrfail($id);
        return view('apps.bien.show_accessoires', compact('bien'));
    }
    /**
    * Display the article Statistique
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show_statistique($id)
    {
        $bien=Bien::findOrfail($id);
        return view('apps.bien.show_statistiques', compact('bien'));
    }

    /*
    * @param Null
    * Retourner l'identifiant de l'utilisatuer connecté
    * @return $UserID
    */
    public function getUserId()
    {
        return Auth::user()?Auth::user()->id:0;;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_can_edit=TRUE;
        if($user_can_edit){
            $bien = Bien::findOrFail($id);
            //dd($bien);
            $bien->nom_objet=$bien->nom;
            $bien->mode=$bien->mode_acquisition_id;
            $bien->lieu_objet=$bien->lieu_achat;
            $bien->categorie=$bien->typebien_id;
            $bien->visibilite=$bien->visibilite;
            $bien->vendeur=$bien->vendeur;
            $bien->date_objet=$bien->date_achat=="1970-01-01"?NULL:$bien->date_achat;
            $bien->imei1=$bien->imei1;
            $bien->imei2=$bien->imei2;
            $bien->prix_objet=$bien->prix_achat;
            $bien->infos=$bien->infos;
            $bien->devise=$bien->devise_id;
            $bien->marque=$bien->marque?$bien->marque->nom:NULL;
            $bien->modele=$bien->modele?$bien->modele->nom:NULL;
            //'nom','mode_acquisition_id','lieu_achat','visibilite','vendeur',
            //'date_achat','imei1','imei2','prix_achat','infos','marque_id','modele_id','user_id','photo','photo_mini'
            $modes=$this->getModes();
            $categories=$this->getTypebiens();
            $devises=$this->getDevises();
            
            return view('apps.bien.edit', compact('bien','modes','categories','devises'));
        }else{
            return redirect()->back();
        }

        abort(404);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            
        $this->validate($request, [
            'nom_objet' => 'required|min:3|max:150',
            'visibilite' => 'required|in:PUBLIC,PRIVE',
            'mode' => 'required|max:150|integer|exists:'.$this->mode_acquisition_table.',id',
            'categorie' => 'required|integer|exists:'.$this->mode_typebien_table.',id',
            'devise' => 'required|integer|exists:'.$this->mode_devise_table.',id',
            'lieu_objet' => 'required|max:180',
            'prix_object' => 'numeric|min:0|max:9999999999',
            'date_objet' => '',

            'vendeur' => 'max:180',
            'marque' => 'max:180',
            'modele' => 'max:180',
            'infos' => 'max:1800',
            'photos' => "image|mimes:jpeg,png,jpg,gif|max:2048"
         ]);

        $requestData = $request->all();

        // 1- Save bien
        /*
        * Traitement des données
        */
       
        $date_achat=$requestData['date_objet']==NULL?NULL:$requestData['date_objet'];
        $prix=$requestData['prix_objet']!=NULL?$requestData['prix_objet']:0;
        $data=[
            'nom'=>$requestData['nom_objet'],
            'mode_acquisition_id'=>$requestData['mode'],
            'lieu_achat'=>$requestData['lieu_objet'],
            'visibilite'=>$requestData['visibilite'],
            'vendeur'=>$requestData['vendeur'],
            'date_achat'=>$date_achat,
            'imei1'=>$requestData['imei1'],
            'imei2'=>$requestData['imei2'],
            'prix_achat'=>$prix,
            'typebien_id'=>$requestData['categorie'],
            'devise_id'=>$requestData['devise'],
            'infos'=>$requestData['infos'],
        ];

        //mise à jour
        $bien = Bien::findOrFail($id);
        $bien->update($data);

        // 2- Save and attach Marque
        $marque_id=0;
        if($requestData['marque']!=NULL || ($bien->marque && $requestData['marque']!=$bien->marque->nom)){
            $SearchMarque=Marque::where('nom',  $requestData['marque'])->first();

            if($SearchMarque && $SearchMarque->count()>0){
                $marque_id=$SearchMarque->id;
            }else{
                $savemarque=Marque::create(['nom' => $requestData['marque'],'description'=>"",'user_id'=>$this->getUserId()]);
                $marque_id=$savemarque->id;
            }
           // dd($marque_id);
           $bien->marque_id=$marque_id;
           $bien->save();
            //$bien->marque()->attach($marque_id);

        }
        //dd($bien);
        // 3- Save and attach Mode
        $modele_id=0;
        if($requestData['modele']!=NULL || ($bien->modele && $requestData['modele']!=$bien->modele->nom)){
            $SearchModele=Modele::where('nom',trim($requestData['modele']))->first();

            if($SearchModele && $SearchModele->count()>0){
                $modele_id=$SearchModele->id;
            }else{
                $save=Modele::create(['nom' => $requestData['modele'],'description'=>"",'marque_id'=>$marque_id,'user_id'=>$this->getUserId()]);
                $modele_id=$save->id;
            }
            $bien->modele_id=$modele_id;
            $bien->save();
            //$bien->modele()->attach($modele_id);  dd('');
        }

        // 4- Save Original and Thumbnail Image
        $message="";
        if($request->hasFile('photos')) {
            if($request->file('photos')->isValid()){
                $image = $request->file('photos');
                $fileName   = Str::random(30)."_".time(). '.' . $image->getClientOriginalExtension();
                $ext=$image->getClientOriginalExtension();

                //5- Sauvegarde  de l'original
                $path = $image->storeAs("public/".$this->cd_biens,$fileName);
                //6- Sauvegarde  de la miniature
                $img = Image::make($image->getRealPath());
                $img->resize($this->mini_width, $this->mini_height, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $path_mini=$this->cd_biens_mini."/".$fileName;
                $img->stream();
                Storage::disk('local')->put("public/".$path_mini, $img, 'public');

                $b=new Bien();

                $tbl=$b->getTable();
                $npath=str_replace("public/",'',$path);
                $media=[
                    'title' => $requestData['nom_objet'],
                    'type' => "IMAGE",
                    'url' => "storage/".$npath,
                    'ref' => $tbl,
                    'ext' => $ext,
                    'user_id' => $this->getUserId(),
                    'ref_id' => $bien->id,
                    'mini' => "storage/".$path_mini,
                ];
                $media=Media::create($media);
                $media_id=$media->id;
                // 7- Mise a jour du Média
                $bien->media_id=$media_id;
                $bien->save();

            }else{
                $message=__("Erreur : Image nom valide");
            }
        }
        if($message==NULL){
            $message=__('Mise à jour effectuée avec succès!');
        }
        return redirect()->intended('home')->with('success', $message);
      //  return redirect()->to(url()->previous());//back()->with('success', $message);
        //return redirect()->route('show_bien',$bien->id)->with('success', $message);
        /*$this->validate($request, [
            'nom_objet' => 'required|min:3|max:250',
            'visibilite' => 'required|in:PUBLIC,PRIVE',
            'mode' => 'required|max:150|integer|exists:'.$this->mode_acquisition_table.',id',
            'categorie' => 'required|integer|exists:'.$this->mode_typebien_table.',id',
            'lieu_objet' => 'required|max:180',
            'prix_object' => 'numeric|min:0|max:9999999999',
            'date_objet' => '',

            'vendeur' => 'max:180',
            'marque' => 'max:180',
            'modele' => 'max:180',
            'infos' => 'max:1800',
            'photos' => "image|mimes:jpeg,png,jpg,gif|max:2048"
         ]);
        $requestData = $request->all();
        $bien = Bien::findOrFail($id);
        //print_r($requestData);
        //echo '<hr>';
        $userid=(Auth::user())?Auth::user()->id:0;


        $date_achat=$requestData['date_objet']==NULL?NULL:$requestData['date_objet'];
        $photo=$photo_mini=NULL;

        $img_save=FALSE;
        $ext="";
        if($request->hasFile('photos')) {

            $image = $request->file('photos');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $ext=$image->getClientOriginalExtension();

            $destinationPath = public_path($this->cd_biens);
            !is_dir($destinationPath)? @mkdir($destinationPath,0777,true):'';

            $destinationPath_mini = public_path($this->cd_biens_mini);
            !is_dir($destinationPath_mini)? @mkdir($destinationPath_mini,0777,true):'';

            $img = Image::make($image->getRealPath());

            $width =$this->width;// 3800; // Your max width
            $height =$this->height; // Your max height

            $img->height() > $img->width() ? $width=null : $height=null;


            $img->resize($this->mini_width, $this->mini_height, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath_mini.'/'.$input['imagename']);

            $img->save($destinationPath.'/'.$input['imagename']);

            $photo=$this->cd_biens."/".$input['imagename'];
            $photo_mini=$this->cd_biens_mini."/".$input['imagename'];
            $img_save=TRUE;
        }

        $media_id=NULL;
        if($img_save==TRUE)
        {
            $b=new Bien();
            $tbl=$b->getTable();
            $media2=[
                'title' => $requestData['nom_objet'],
                'url' => $this->cd_biens.'/'.$input['imagename'],
                'type' => "IMAGE",
                'ref' => $tbl,
                'ext' => $ext,
                'user_id' => $userid,
            ];
            //'title', 'url', 'type', 'mini', 'ref', 'ref_id', 'user_id'

            $media=Media::create($media2);
            $media_id=$media->id;

        }

        $data=[
            'nom'=>$requestData['nom_objet'],
            'mode_acquisition_id'=>$requestData['mode'],
            'lieu_achat'=>$requestData['lieu_objet'],
            'visibilite'=>$requestData['visibilite'],
            'vendeur'=>$requestData['date_objet'],
            'date_achat'=>$date_achat,
            'imei1'=>$requestData['imei1'],
            'imei2'=>$requestData['imei2'],
            'prix_achat'=>$requestData['prix_objet'],
            'typebien_id'=>$requestData['categorie'],
            'infos'=>$requestData['infos'],
            'photo'=>$photo,
            'photo_mini'=>$photo_mini,
            'media_id'=>$media_id,

        ];
        //dd($data);

        #Enrégistrement de ma marque
        $marque_id=0;

        if($requestData['marque']!=NULL){

            $SearchMarque=Marque::where('nom',  $requestData['marque'])->first();

            if($SearchMarque && $SearchMarque->count()>0){
                $marque_id=$SearchMarque->id;
            }else{
                $savemarque=Marque::create(['nom' => $requestData['marque'],'description'=>"",'user_id'=>$userid]);
                $marque_id=$savemarque->id;
            }
        }
        $data['marque_id']=$marque_id;

        #Enrégistrement du Modele
        $modele_id=0;
        if($requestData['modele']!=NULL){
            $SearchModele=Modele::where('nom',trim($requestData['modele']))->first();
            if($SearchModele && $SearchModele->count()>0){
                $modele_id=$SearchModele->id;
            }else{
                $save=Modele::create(['nom' => $requestData['modele'],'description'=>"",'marque_id'=>$marque_id,'user_id'=>$userid]);
                $modele_id=$save->id;
            }
        }
        $data['modele_id']=$modele_id;

        //dd($data);
        //return redirect()->back()->withInput();
        $bien->update($data);

        $msg=__('Mise à jour effectuée avec succès!');
        return redirect('/home')->with('success', $msg);*/


    }



            /**
             * Add bien to favorit.
             *
             * @param  int  $id, Request
             * @return \Illuminate\Http\Response
             */
            public function add_bien_favori(Request $request,$id)
            {
                $msg=NULL;
                $bien=Bien::findOrfail($id);
                if(sha1($id)==$request->token){
                    $tabs_bien_id=Auth::user()->biensFavoris()->select('bien_id')->get()->toArray();;
                    if(!in_array($bien->id,$tabs_bien_id)){
                        $bien->userFavoris()->sync(Auth::user()->id);
                        $msg=__("Ajouté aux favoris");
                    }

                    //dd($msg);
                }

                return redirect(route('dashboard'));//->with('success', $msg);
            }
             /**
             * Delete bien from favorit.
             *
             * @param  int  $id, Request
             * @return \Illuminate\Http\Response
             */
            public function delete_bien_favori(Request $request,$id)
            {
                $msg=NULL;
                $bien=Bien::findOrfail($id);
                if(sha1($id)==$request->token){
                    $b=Auth::user()->biensFavoris()->where('bien_id',$id)->first();//->get()->toArray();;
                    if($b){
                        $detach=Auth::user()->biensFavoris()->detach($b->id);
                        $msg=__("Retrait des favoris");

                    }

                }

                return redirect(route('home'))->with('success', $msg);
            }

            /**
              * Remove the specified resource from storage.
             *
             * @param  int  $id
             * @return \Illuminate\Http\Response
             */
            public function delete(Request $request,$id)
            {
            /*Modele::destroy($id);*/
            $this->validate($request, [
                'deleted' => 'required'
            ]);
            $crypt_id=$request->input('deleted');
            //dd($crypt_id."==".sha1($id));
            if($crypt_id==sha1($id)){
                $res=Bien::destroy($id);
                return redirect(route('home'))->with('danger',  __("Suppression effectuée"));
            }
        return redirect("/");//->back();
        //return redirect()->with('success', $msg);

    }
    public function bien_like($id,Request $request)
    {
        $bien=Bien::findOrfail($id);
        $crypt_id=$request->input('token');
        //dd($crypt_id."==".sha1($id));
        $user=Auth::user();
        if($crypt_id==sha1($id)){
            $uid=[$user->id];
            $t=$bien->likes()->get()->toArray();
            //dd($t); 
            if(!in_array($id,$t)){
            $res=$bien->likes()->attach($uid);;
            }
                       
        }
        return redirect("/");//->back();
    }
    public function bien_dislike ($id,Request $request)
    {
        $bien=Bien::findOrfail($id);
        $crypt_id=$request->input('token');
        //dd($crypt_id."==".sha1($id));
        $user=Auth::user();
        if($crypt_id==sha1($id)){
            $uid=[$user->id];
            $res=$bien->likes()->detach($uid);;
            //dd($res);            
        }
        return redirect('/');//->back();
    }
}
