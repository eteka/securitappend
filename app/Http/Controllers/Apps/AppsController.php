<?php

namespace App\Http\Controllers\Apps;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Models\Bien;
use App\Models\Typebien;
use App\User;
use Excel;
use Auth;


class AppsController extends Controller
{
  //Le nombre de bien sur la page d'accueil
  private $Bien_perPage=6;
  private $ajax_perPage=6;
  protected $nb_download_file_limit=30;
  /**
  * Create a new controller instance:Auth Middleware.
  *
  * @return void
  */
  public function __construct()
  {
    //dd(date('H:i:s'))
    $this->middleware('auth');
  }

  public function getDblimite()
  {
    return $this->nb_download_file_limit;
  }

  /**
  * Show the dashboard home page.
  *
  * @return \Illuminate\Contracts\Support\Renderable
  */
  public function index(Request $request)
  {
    $keyword = $request->get('search');

    $orderby=$request->get('orderby');

    $bien_exist=Auth::user()->biens();
    if(strtolower($orderby)=="desc"){
          $bien_exist=$bien_exist->oldest();
    }else{
        $bien_exist=$bien_exist->latest();
    }
     // dd($orderby);

    $count=$bien_exist?$bien_exist->count():NULL;

    $count_bien=$count." ".__('Bien');
    if($count>1){
      $count_bien=$count." ".__('Biens');
    }
    $biens=NULL;
    $count_bien=$count." ".__('Biens');
    if (!empty($keyword)) {
      $biens = Bien::where('nom', 'LIKE', "%$keyword%")
      ->orWhere('lieu_achat', 'LIKE', "%$keyword%")
      ->orWhere('vendeur', 'LIKE', "%$keyword%")
      ->orWhere('infos', 'LIKE', "%$keyword%")
      ->orderBY($orderby)->paginate($this->Bien_perPage);
    } else {
      if($request->ajax()){
        //sleep(2);
        $biens = $bien_exist?$bien_exist->paginate($this->ajax_perPage):NULL;
      }else{
        $biens =$bien_exist? $bien_exist->paginate($this->Bien_perPage):NULL;
      }

    }
    //dd($biens);

    //return json_encode($biens);
    // return view('apps.admin.datatable', compact('biens'));
    return view('apps.admin.dashboard', compact('biens','count_bien'));
  }
  /**
  * Show the profil  favoris page.
  *
  * @return \Illuminate\Contracts\Support\Renderable
  */
  public function profil_favoris($pseudo, Request $request)
  {
    $keyword = $request->get('search');
    $orderby=$request->get('orderby');
    $user=User::where('pseudo', $pseudo)->firstOrFail();
    
    $bien_exist=$user->biensFavoris();
    $count=$bien_exist->count();

    if(strtolower($orderby)=="desc"){
          $orderby="desc";
    }else{
          $orderby="asc";
    }

    $count_bien=$count." ".__('Bien');
    if($count>1){
      $count_bien=$count." ".__('Biens');
    }
    $count_bien=$count." ".__('Biens');
    $biens = $bien_exist->orderBy('user_bien_favoris.created_at',$orderby)->paginate($this->ajax_perPage);
    $biens=null;
    return view('web.profil.profil_favoris', compact('user','biens','count_bien'));
  }
  /**
  * Show the dashboard home page.
  *
  * @return \Illuminate\Contracts\Support\Renderable
  */
  public function bien_favoris(Request $request)
  {
    $keyword = $request->get('search');
    $orderby=$request->get('orderby');

    $bien_exist=Auth::user()->biensFavoris();
    $count=$bien_exist->count();

    if(strtolower($orderby)=="desc"){
          $orderby="desc";
    }else{
          $orderby="asc";
    }

    $count_bien=$count." ".__('Bien');
    if($count>1){
      $count_bien=$count." ".__('Biens');
    }
    $count_bien=$count." ".__('Biens');
    $biens = $bien_exist->orderBy('user_bien_favoris.created_at',$orderby)->paginate($this->ajax_perPage);
    return view('apps.admin.favoris', compact('biens','count_bien'));
  }
  /**
  * Show the dashboard home page.
  *
  * @return \Illuminate\Contracts\Support\Renderable
  */
  public function classement(Request $request)
  {

    $keyword = $request->get('search');
    $count_bien=__("Classement");
    #MODE  D'ACQUISITION DES BIENS
    $all_modes=['PRIVE','PUBLIC'];
    $b_prive=Auth::user()->biens()->where('visibilite','PRIVE')->count();
    $b_public=Auth::user()->biens()->where('visibilite','PUBLIC')->count();
    $modes=[
      ['nom'=>__('PRIVE'),'nb'=>$b_prive,'slug'=>"private"],
      ['nom'=>__('PUBLIC'),'nb'=>$b_public,'slug'=>"public"],
    ];
    #CATEGORIES DE  BIENS
    $categories=[];
    $lescategories=Typebien::orderBy('nom',"ASC")->get();
    $biens=Auth::user()->biens()->get();
    foreach ($lescategories as $c) {
      $nb=Auth::user()->biens()->where('typebien_id',$c->id)->count();
      if($nb>0)
      $categories[]=['nom'=>$c->nom,"id"=>$c->id,"nb"=>$nb];
    }
    //dd($categories);

    $biens = Auth::user()->biensFavoris()->paginate($this->ajax_perPage);
    return view('apps.admin.classement', compact('modes','categories','count_bien'));
  }

  /**
  * Classement by mode acquisition.
  *
  *@return \Illuminate\Contracts\Support\Renderable
  */
  public function classement_mode($slug,Request $request)
  {
    $biens=$data=NULL;
    $count_bien=__("Mode d'acquisition");
    $orderby=$request->get('orderby');

    if(strtolower($orderby)=='desc'){$orderby="desc";}else{$orderby="asc";}
    $s_count_bien="";
    $nb=0;
    if($slug=="private"){
      $data=Auth::user()->biens()->where('visibilite','PRIVE');
      $nb=$data->count();
      $s_count_bien=__("PRIVE");
        //dd($data->get());
    }
    elseif($slug=="public"){
      $data=Auth::user()->biens()->where('visibilite','PUBLIC');
      $nb=$data->count();
      $s_count_bien=__("PUBLIC");
    }
    if($data){
        $biens=$data->orderBy("created_at",$orderby)->paginate($this->Bien_perPage);
    }else{$biens=$data;}

    //if($biens==NULL){abort(404);}

    return view('apps.admin.classement_mode', compact('biens','nb','slug','count_bien','s_count_bien'));

  }

  /**
  * Classement by mode acquisition.
  *
  *@return \Illuminate\Contracts\Support\Renderable
  */
  public function classement_categorie($id,Request $request)
  {
    $count_bien=__("Catégorie");
    $orderby=$request->get('orderby');
    if(strtolower($orderby)=='desc'){$orderby="desc";}else{$orderby="asc";}

    $categorie=Typebien::findOrFail($id);
    $data=Auth::user()->biens()->where('typebien_id',$id);
    $nb=$data->count();
    $s_count_bien=__("PRIVE");
    $s_count_bien=$categorie->nom;
      if($data){
          $biens=$data->orderBy("created_at",$orderby)->paginate($this->Bien_perPage);
      }else{$biens=$data;}
    //if($biens==NULL){abort(404);}

    return view('apps.admin.classement_categorie', compact('biens','categorie','nb','count_bien','s_count_bien'));

  }

  /**
  * Export all user data in to Ecel format.
  *
  * @return \Illuminate\Contracts\Support\Renderable , XLSX, CSV
  */
  public function export_all($id,$n=50,$orderBy='ASC'){
    $nlimit=(int)$this->getDblimite();
    $n=(int)$n<=0?$nlimit:$n;
    $biens=Auth::user()->biens()->get();//->toArray();

    if($biens->count()>$nlimit){
      $biens=$biens=Bien::where('user_id',Auth::user()->id)->limit($nlimit)->get();

    }
    //dd($biens);
    if(!$biens->count()){return back();}
    Excel::create('ALL', function($excel) use ($biens) {

      // Set the title
      $excel->setTitle(__('Exportation de mes biens'));

      // Chain the setters
      $excel->setCreator('SecuritAppend')
      ->setCompany('Odacesoft');
      $excel->setDescription(__('Fichier de mes biens par SécuritAppend'));
      $entete=[
        0=>__("Numéro de série"),
        1=>__("Nom"),
        2=>__("Visibilité"),
        3=>__("Mode d'acquisition"),
        4=>__("Catégorie"),
        5=>__("Marque"),
        6=>__("Modèle"),
        7=>__("Vendeur"),
        8=>__("Lieu d'achat"),
        9=>__("Prix d'achat"),
        10=>__("Information"),
        11=>__("Image")
      ];

      $excel->sheet('All', function($sheet) use ($entete,$biens)
      {

            //$sheet->fromArray($b);
            //dd($biens->get());

            $l=1;
            $tab=["A1","B1","C1","D1","E1","F1","G1","H1","I1","J1","K1","L1"];
            $j=0;
            foreach ($tab as $column) {
              $v=isset($entete[$j])?$entete[$j]:'';

              $sheet->cell($column, function($cell) use ($v) {
                  $cell->setValue($v);
                  $cell->setFontWeight('bold');
                  $cell->setAlignment('center');
              });
          $j++;
        }
        #Set h
        $i=2;
        //$sheet->fromArray($biens,NULL, 'A2');

      foreach ($biens as  $b) {
          $mode=$b->mode?$b->mode->nom:"-";
          $type=$b->type?$b->type->nom:"-";
          $marque=$b->marque?$b->marque->nom:"-";
          $modele=$b->modele?$b->modele->nom:"-";
          $image=$b->media?$b->media->url:"-";
          $imei=$b->imei1;
          $imei=$b->imei2!=""?$imei.="-".$b->imei2:$imei;
          $imei=$imei!=""?"'".$imei."'":'-';
          $data=[
            $imei,
            $b->nom,
            $b->visibilite(), $mode,$type,
            $marque,$modele,$b->vendeur,
            $b->lieu_achat,$b->prix_achat,
            $b->infos,$image=="-"?$image:asset($image)
          ];

          $sheet->appendRow($i,$data);
          $i++;
        }
        $sheet->setAutoSize(true);
        $sheet->freezeFirstRow();
      });

      //})->download('csv');
    })->download('xlsx');
}
/**
* Export all user data Favoris in to Ecel format.
*
* @return \Illuminate\Contracts\Support\Renderable , XLSX, CSV
*/
public function export_favoris($id,$n=50,$orderBy='ASC'){
  $nlimit=(int)$this->getDblimite();
  $n=(int)$n<=0?$nlimit:$n;
  $biens=Auth::user()->biensFavoris()->get();//->toArray();

  if($biens->count()>$nlimit){
    $biens=$biens=Auth::user()->biensFavoris()->limit($nlimit)->get();

  }
  //dd($biens->count());
  if(!$biens->count()){return back();}
  Excel::create(__("Favoris"), function($excel) use ($biens) {

    // Set the title
    $excel->setTitle(__('Exportation de mes favoris'));

    // Chain the setters
    $excel->setCreator('SecuritAppend')
    ->setCompany('Odacesoft');
    $excel->setDescription(__('Fichier de mes biens favoris par SécuritAppend'));
    $entete=[
      0=>__("Numéro de série"),
      1=>__("Nom"),
      2=>__("Visibilité"),
      3=>__("Mode d'acquisition"),
      4=>__("Catégorie"),
      5=>__("Marque"),
      6=>__("Modèle"),
      7=>__("Vendeur"),
      8=>__("Lieu d'achat"),
      9=>__("Prix d'achat"),
      10=>__("Information"),
      11=>__("Image")
    ];

    $excel->sheet(__("Favoris"), function($sheet) use ($entete,$biens)
    {

          //$sheet->fromArray($b);
          //dd($biens->get());

          $l=1;
          $tab=["A1","B1","C1","D1","E1","F1","G1","H1","I1","J1","K1","L1"];
          $j=0;
          foreach ($tab as $column) {
            $v=isset($entete[$j])?$entete[$j]:'';

            $sheet->cell($column, function($cell) use ($v) {
                $cell->setValue($v);
                $cell->setFontWeight('bold');
                $cell->setAlignment('center');
            });
        $j++;
      }
      #Set h
      $i=2;
      //$sheet->fromArray($biens,NULL, 'A2');

    foreach ($biens as  $b) {
        $mode=$b->mode?$b->mode->nom:"-";
        $type=$b->type?$b->type->nom:"-";
        $marque=$b->marque?$b->marque->nom:"-";
        $modele=$b->modele?$b->modele->nom:"-";
        $image=$b->media?$b->media->url:"-";
        $imei=$b->imei1;
        $imei=$b->imei2!=""?$imei.="-".$b->imei2:$imei;
        $imei=$imei!=""?"'".$imei."'":'-';
        $data=[
          $imei,
          $b->nom,
          $b->visibilite(), $mode,$type,
          $marque,$modele,$b->vendeur,
          $b->lieu_achat,$b->prix_achat,
          $b->infos,$image=="-"?$image:asset($image)
        ];

        $sheet->appendRow($i,$data);
        $i++;
      }
      $sheet->setAutoSize(true);
      $sheet->freezeFirstRow();
    });

    //})->download('csv');
  })->download('xlsx');
}
    /**
     * Export all user data Favoris in to Ecel format.
     *
     * @return \Illuminate\Contracts\Support\Renderable , XLSX, CSV
     */
    public function export_mode($slug,$n=50,$orderBy='ASC'){

        $nlimit=(int)$this->getDblimite();
        $n=(int)$n<=0?$nlimit:$n;
        if(strtolower($slug)=="private"){
            $biens=Auth::user()->biens()->where('visibilite','PRIVE');//->toArray();
        }else{
            $biens=Auth::user()->biens()->where('visibilite','PUBLIC');//->toArray();
        }


        if($biens->count()>$nlimit){
            $biens=$biens->limit($nlimit);
        }
        $biens=$biens->get();

        if(!$biens->count()){return back();}
        Excel::create(__($slug), function($excel) use ($biens,$slug) {

            // Set the title
            $excel->setTitle(__('Exportation de mes biens'));

            // Chain the setters
            $excel->setCreator('SecuritAppend')
                ->setCompany('Odacesoft');
            $excel->setDescription(__('Fichier de mes biens par SécuritAppend'));
            $entete=[
                0=>__("Visibilité"),
                1=>__("Numéro de série"),
                2=>__("Nom"),
                3=>__("Mode d'acquisition"),
                4=>__("Catégorie"),
                5=>__("Marque"),
                6=>__("Modèle"),
                7=>__("Vendeur"),
                8=>__("Lieu d'achat"),
                9=>__("Prix d'achat"),
                10=>__("Information"),
                11=>__("Image")
            ];

            $excel->sheet(__($slug), function($sheet) use ($entete,$biens)
            {

                //$sheet->fromArray($b);
                //dd($biens->get());

                $l=1;
                $tab=["A1","B1","C1","D1","E1","F1","G1","H1","I1","J1","K1","L1"];
                $j=0;
                foreach ($tab as $column) {
                    $v=isset($entete[$j])?$entete[$j]:'';

                    $sheet->cell($column, function($cell) use ($v) {
                        $cell->setValue($v);
                        $cell->setFontWeight('bold');
                        $cell->setAlignment('center');
                    });
                    $j++;
                }
                #Set h
                $i=2;
                //$sheet->fromArray($biens,NULL, 'A2');

                foreach ($biens as  $b) {
                    $mode=$b->mode?$b->mode->nom:"-";
                    $type=$b->type?$b->type->nom:"-";
                    $marque=$b->marque?$b->marque->nom:"-";
                    $modele=$b->modele?$b->modele->nom:"-";
                    $image=$b->media?$b->media->url:"-";
                    $imei=$b->imei1;
                    $imei=$b->imei2!=""?$imei.="-".$b->imei2:$imei;
                    $imei=$imei!=""?"'".$imei."'":'-';
                    $image=$image=="-"?$image:asset($image);
                    $data=[
                        $b->visibilite(),
                        $imei,
                        $b->nom,
                        $mode,$type,
                        $marque,$modele,$b->vendeur,
                        $b->lieu_achat,$b->prix_achat,
                        $b->infos,$image
                    ];

                    $sheet->appendRow($i,$data);
                    $i++;
                }
                $sheet->setAutoSize(true);
                $sheet->freezeFirstRow();
            });

            //})->download('csv');
        })->download('xlsx');
    }
    /**
     * Export all user data Favoris in to Ecel format.
     *
     * @return \Illuminate\Contracts\Support\Renderable , XLSX, CSV
     */
    public function export_categorie($id,$n=50,$orderBy='ASC'){

        $nlimit=(int)$this->getDblimite();
        $type=Typebien::findOrfail($id);
        $n=(int)$n<=0?$nlimit:$n;

        $biens=Auth::user()->biens()->where('typebien_id',$type->id);//->toArray();
        $nom=$type->nom;


        if($biens->count()>$nlimit){
            $biens=$biens->limit($nlimit);
        }
        $biens=$biens->get();

        if(!$biens->count()){return back();}
        Excel::create(Str::slug($nom), function($excel) use ($biens,$nom) {

            // Set the title
            $excel->setTitle(__('Exportation de mes biens'));

            // Chain the setters
            $excel->setCreator('SecuritAppend')
                ->setCompany('Odacesoft');
            $excel->setDescription(__('Fichier de mes biens par SécuritAppend'));
            $entete=[
                0=>__("Visibilité"),
                1=>__("Numéro de série"),
                2=>__("Nom"),
                3=>__("Mode d'acquisition"),
                4=>__("Catégorie"),
                5=>__("Marque"),
                6=>__("Modèle"),
                7=>__("Vendeur"),
                8=>__("Lieu d'achat"),
                9=>__("Prix d'achat"),
                10=>__("Information"),
                11=>__("Image")
            ];

            $excel->sheet(__($nom), function($sheet) use ($entete,$biens)
            {

                //$sheet->fromArray($b);
                //dd($biens->get());

                $l=1;
                $tab=["A1","B1","C1","D1","E1","F1","G1","H1","I1","J1","K1","L1"];
                $j=0;
                foreach ($tab as $column) {
                    $v=isset($entete[$j])?$entete[$j]:'';

                    $sheet->cell($column, function($cell) use ($v) {
                        $cell->setValue($v);
                        $cell->setFontWeight('bold');
                        $cell->setAlignment('center');
                    });
                    $j++;
                }
                #Set h
                $i=2;
                //$sheet->fromArray($biens,NULL, 'A2');

                foreach ($biens as  $b) {
                    $mode=$b->mode?$b->mode->nom:"-";
                    $type=$b->type?$b->type->nom:"-";
                    $marque=$b->marque?$b->marque->nom:"-";
                    $modele=$b->modele?$b->modele->nom:"-";
                    $image=$b->media?$b->media->url:"-";
                    $imei=$b->imei1;
                    $imei=$b->imei2!=""?$imei.="-".$b->imei2:$imei;
                    $imei=$imei!=""?"'".$imei."'":'-';
                    $image=$image=="-"?$image:asset($image);
                    $data=[
                        $b->visibilite(),
                        $imei,
                        $b->nom,
                        $mode,$type,
                        $marque,$modele,$b->vendeur,
                        $b->lieu_achat,$b->prix_achat,
                        $b->infos,$image
                    ];

                    $sheet->appendRow($i,$data);
                    $i++;
                }
                $sheet->setAutoSize(true);
                $sheet->freezeFirstRow();
            });

            //})->download('csv');
        })->download('xlsx');
    }

public function export_bien($id){
  \Config::set('excel.csv.delimiter', ';');//DomPDF
  //\Config::set('excel.pdf.driver', 'DomPDF');
  $b=$bien=Bien::findOrfail($id);
  $tnom=explode(" ",$bien->nom);

  $nom=(isset($tnom[0]) && isset($tnom[1]))?$tnom[0]."-".$tnom[1]:isset($tnom[0])? $tnom[0]:"Data";
  $fname=substr(Str::slug($nom),0,30)."_".date('d_m_Y_h_i_s',time());
  if(!$bien->count()){return back();}
  Excel::create('export_'.$fname, function($excel) use ($b) {
    // Set the title
    $excel->setTitle($b->nom);

    // Chain the setters
    $excel->setCreator('SecuritAppen')
    ->setCompany('Odacesoft');
    //'nom','mode_acquisition_id','typebien_id','lieu_achat','visibilite','media_id','vendeur',
    //'date_achat','imei1','imei2','prix_achat','infos','marque_id','modele_id','user_id','photo','photo_mini'
    $user=$b->user?$b->user->name:"-";
    $mode=$b->mode?$b->mode->nom:"-";
    $type=$b->type?$b->type->nom:"-";
    $marque=$b->marque?$b->marque->nom:"-";
    $modele=$b->modele?$b->modele->nom:"-";
    $img=$b->media?$b->media->url:"-";
    $image=$img!=""?asset($img):'';
    $imei=$b->imei1;
    if($b->imei2!=""){$imei.="-".$b->imei2;}
    $imei="'".$imei."'";
    $b=[
      [__("Numéro de série"),$imei],
      [__("Nom"),$b->nom],
      [__("Visibilité"),$b->visibilite()],
      [__("Mode d'acquisition"),$mode],
      [__("Catégorie"),$type],
      [__("Marque"),$marque],
      [__("Modèle"),$modele],
      [__("Vendeur"),$b->vendeur],
      [__("Lieu d'achat"),$b->lieu_achat],
      [__("Prix d'achat"),$b->prix_achat],
      [__("Information"),$b->infos],
      [__("Image"),$image]
    ];
    // Call them separately
    $excel->setDescription('A demonstration to change the file properties');


    $excel->sheet(__("Mes Biens"), function($sheet) use ($b)
    {

      $sheet->fromArray($b);
      $i=1;
      //dd($b);
      $nb=count($b);
      $cpt=1;
      foreach ($b as $data) {
        if($cpt<$nb){
          $sheet->appendRow($i++,$data);
        }
        $cpt++;
      }
      // $sheet->data=[];
      $sheet->cell('A1:A20', function($cell)  {
        $cell->setFontWeight('bold');

      });
      $sheet->setAutoSize(true);

    });

    // dd($b);

  },'UTF-8')->export('xlsx');//,storage_path('storage/public/excels/exports'));


}

/**
* Show the application dashboard.
*
* @return \Illuminate\Contracts\Support\Renderable
*/
public function DetailsBien($fichier)
{
  return view('apps.admin.details');
}
/**
* Show the application dashboard.
*
* @return \Illuminate\Contracts\Support\Renderable
*/
public function NewBienForm()
{
  //dd('');
  return view('apps.bien.new');
}
/**
* Show the application dashboard.
*
* @return \Illuminate\Contracts\Support\Renderable
*/
public function NewBienSave()
{
  return view('apps.bien.new');
}

}
