<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\Contracts\Activity;

class Bien extends Model
{
  use SoftDeletes, LogsActivity;
       /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'biens';

    /**
    * The database primary key value.
    *
    * @var string
    */

    protected $primaryKey = 'id';
    /**
     * Configuration log plugins
     **/
    protected static $logName = 'Bien';
    protected static $logAttributes = ['nom','mode_acquisition_id','typebien_id','lieu_achat','visibilite','media_id','vendeur', 'date_achat','imei1','imei2','prix_achat','infos','marque_id','modele_id','user_id','devise_id','photo','photo_mini'];

    

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nom','mode_acquisition_id','typebien_id','lieu_achat','visibilite','media_id','vendeur', 'date_achat','imei1','imei2','prix_achat','infos','marque_id','modele_id','user_id','devise_id','photo','photo_mini'];
    
    
    public function getDescriptionForEvent(string $eventName): string
    {
        return "This model has been {$eventName}";
    }

    /*public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->description = __("activity.messages.{$eventName}");
    }*/

    public function user()
    {
      return $this->belongsTo("App\User",'user_id');
    }
     /**
     * Les biens favoris de l'utilisateur
     */
    public function userFavoris()
    {
        return $this->belongsToMany('App\Models\Bien',"user_bien_favoris",'bien_id',"user_id")->withTimestamps();
    }

    public function media()
    {
        return $this->belongsTo("App\Models\Media",'media_id');
    }
    public function devise()
    {
        return $this->belongsTo("App\Models\Devise",'devise_id');
    }
    public static function table()
    {
      return $this->table;
    }
    
    public function mode()
    {
      return $this->belongsTo("App\Models\ModeAcquisition",'mode_acquisition_id');
    }
    public function type()
    {
      return $this->belongsTo("App\Models\Typebien",'typebien_id');
    }
    
    public function marque()
    {
      return $this->belongsTo(\App\Models\Marque::class,'marque_id');
      //return $this->belongsTo("App\Models\Marque",'marque_id');
    }
    public function modele()
    {
      return $this->belongsTo("App\Models\Modele",'modele_id');
    }
    public function visibilite()
    {
      $r='-';
      if($this->visibilite=="PRIVE"){$r=__("Privée");}
      if($this->visibilite=="PUBLIC"){$r=__("Public");}
      return $r;
    }
    public function prix_achat()
    {      
      return $this->prix_achat>0?$this->prix_achat:'0';
    }
    public function nom()
    {      
      return strlen($this->nom)<65?$this->nom:substr($this->nom,0,62)."...";
    }
    public function imageMini()
    {
        return ($this->media()!=NULL) ? 'storage/assets/statics/app/default-property.jpg':$this->media;
    }
    public function image()
    {
      $img =($this->media && file_exists($this->media->url))? $this->media->url:'storage/assets/statics/app/default-property.jpg';
        return $img;
    }
    public function likes()
    {   
      return $this->belongsToMany(User::class, 'user_like_bien','bien_id','user_id')->withTimestamps();
    }
    public function mode_acquisition()
    {
      return $this->mode?$this->mode->nom:'';
    }
  }
