<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Spatie\Activitylog\Traits\LogsActivity;

class Devise extends Model
{
  use SoftDeletes, LogsActivity;
     
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'devises';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['devise', 'description',"sigle", 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
}
