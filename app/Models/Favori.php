<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Favori extends Model
{
    use SoftDeletes, LogsActivity;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'favoris';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'bien_id', 'ref'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function bien()
    {
        return $this->belongsTo('App\Models\Bien');
    }
    
}
