<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class LoginSaver extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'user_login';

  /**
  * The database primary key value.
  *
  * @var string
  */
  protected $primaryKey = 'id';

  /**
   * Attributes that should be mass-assignable.
   *
   * @var array
   */
  protected $fillable = ['user_id', 'ip', 'origin', 'browser', 'url', 'protocole', 'http_accept', 'http_encoding', 'http_languages'];
  
  public static  function saveUser(User $u){
    return "OK";
  }
}
