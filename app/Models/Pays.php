<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Pays extends Model
{
    use SoftDeletes, LogsActivity;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pays';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'alpha2','alpha3', 'nom_en_gb','nom_fr_fr',];

    public function users()
    {
      return $this->hasMany("App\Models\Bien");
    }  
    public function nom()
    {
      $lang=config('app.locale');
      if($lang=="fr"){
        return $this->nom_fr_fr;
      }
      return $this->nom_en_gb;
    }  
}
