<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Etablissement extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'etablissements';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['logo', 'image_cover', 'titre', 'sigle', 'description', 'contenu', 'etat'];
    
    public function filieres()
    {         
        return $this->hasMany('App\Models\Filiere');
    }
    
}
