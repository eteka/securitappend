<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Media extends Model
{
    use SoftDeletes, LogsActivity;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'media';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'url', 'type', 'mini', 'ref', 'ref_id','ext', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function biens()
    {
        return $this->belongsToMany('App\Models\Bien');
    }
    public function imageMini()
    {
        return $this->mini==''?'assets/statics/app/default-property.jpg':$this->mini;
    }
    
}
