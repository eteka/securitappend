<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Modele extends Model
{
    use SoftDeletes, LogsActivity;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'modeles';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nom', 'description', 'marque_id', 'user_id'];

    /**
     * Recupérer tous les biens ayant ce modèle.
     */
    public function biens()
    {
        return $this->hasMany('App\Models\Bien');
    }
     /**
     * Recupérer tous les marques de ce modèle.
     */
    public function marque()
    {
      return $this->belongsTo("App\Models\Marque",'marque_id');
    }

     /**
     * Recupérer tous les utilisateurs ayant des biens de ce modèle.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
}
