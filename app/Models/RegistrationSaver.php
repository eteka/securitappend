<?php

namespace App\Models;

use App\User;
use GuzzleHttp\Psr7\Request;
use Illuminate\Database\Eloquent\Model;

class RegistrationSaver extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users_registration';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','rcode1','rcode2', 'ip',
     'origin', 'browser', 'url', 'protocole', 'http_accept', 'http_encoding', 'http_languages', 'app_lang','infos','etat'];
    
    
}
