<?php

function changeDateFormate($date,$date_format){
    return \Carbon\Carbon::createFromFormat('Y-m-d', $date)->format($date_format);
}

function productImagePath($image_name)
{
    return public_path('images/products/'.$image_name);
}
function jsPlugin($plugin){
    $tabplugins=[
        'selectisize'=>['js'=>"assets/plugins/selectize/dist/js/standalone/selectize.min.js","css"=>"assets/plugins/selectize/css/selectize.default.css"],
        'lightbox'=>["js"=>"assets/plugins/lightbox/js/lightbox.min.js",'css'=>"assets/plugins/lightbox/css/lightbox.min.css"],
        'pace'=>["js"=>"assets/plugins/pace/pace.min.js",'css'=>"assets/plugins/pace/pace.css"],
        'bootstrap'=>["js"=>"assets/plugins/bootstrap/js/bootstrap.min.js"],
        'pjax'=>["js"=>"assets/plugins/pjax/js/pjax.js"],
        'linghbox'=>["js"=>"assets/plugins/bootstrap/js/bootstrap.min.js"],
        'jscroll'=>["js"=>"assets/plugins/jscroll/js/jquery.jscroll.min.js"],//sticky
        'sticky'=>["js"=>"assets/plugins/sticky/sticky.js"],///assets/plugins/jquery/jquery.min.js
        'jquery-1'=>["js"=>"assets/plugins/jquery/jquery.min.js"],
    ];
    $data="";
    if(isset($tabplugins[$plugin])){

        if(isset($tabplugins[$plugin]['js']) && $tabplugins[$plugin]['js']!=""){
            $url=asset($tabplugins[$plugin]['js']);
            $data.="<script src=".$url."  defer></script>";
        }
        if(isset($tabplugins[$plugin]['css']) && $tabplugins[$plugin]['css']!=""){
            $url=asset($tabplugins[$plugin]['css']);
            $data.="<link href=".$url." media='screen' rel='stylesheet' type='text/css'  data-turbolinks-track='reload'>";
        }

    }
    return $data;
}
function afficherPrix($montant,$devise="",$local="fr"){
    if(strtolower($local)=="en"){
        return $devise." ".$montant;
    }

    return $montant." ".$devise;

}