<?php

namespace App;

use App\Models\Bien;
use App\Models\Media;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Activitylog\Traits\LogsActivity;

class User extends Authenticatable
{
    use Notifiable, HasRoles, LogsActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected static $ignoreChangedAttributes = ['password'];
    protected $profil_default_f= "assets/statics/users/avatar-f.jpg";
    protected $profil_default_h= "assets/statics/users/avatar-h.jpg";

    protected static $logAttributes = [
        'nom','prenom','sexe','website', 'email','pseudo','ville','photo_profil_id', 'photo_couverture_id','pays_id','apropos',
        'poste', 'provider', 'provider_id', 'password','etat','type_compte_id','password','pseudo_editable','profil_state'
    ];


    protected $fillable = [
        'nom','prenom','sexe','website', 'email','pseudo','photo_profil_id', 'photo_couverture_id','poste', 'provider', 'provider_id', 'password','etat','type_compte_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','etat','type_compte_id'
    ];

     /**
    * The database default colomn.
    *
    * @var Array
    */
    protected $psdo;
    
    protected $attributes=[
        "photo_profil_id"=>0,
        "photo_couverture_id"=>0,
        "etat"=>1,
        "provider"=>'0',
        "provider_id"=>0,
        "type_compte_id"=>1,
      ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function getPseudo()
    {
       return $this->pseudo!=NULL?$this->pseudo:$this->id;
    }
    public function avatar()
    {
      $default=($this->sexe=='F') ?$this->profil_default_f:$this->profil_default_h;
      $avatar_img=$this->photoProfil();
      $avatar=$avatar_img?$avatar_img->mini:$default;
      
       return $avatar;
    }
    public function biens()
    {
      return $this->hasMany("App\Models\Bien",'user_id','id');
    }
    public function pays()
    {
      return $this->belongsTo("App\Models\Pays");
    }
    public function bien_liked()
    {   
      return $this->belongsToMany('App\Models\Bien', 'user_like_bien','user_id','bien_id');
    }
    public function has_liked(Bien $b)
    {
      return $this->bien_liked()->where('bien_id',$b->id)->count();
    }

    public function fullname()
    {
      return $this->nom.' '.$this->prenom;
    }
    public function profil()
    {
      return route('profil',$this->pseudo);
    }
     /**
     * Les biens favoris de l'utilisateur
     */
    public function biensFavoris()
    {
        return $this->belongsToMany('App\Models\Bien',"user_bien_favoris","user_id",'bien_id')->withTimestamps();
    }
    public function photoProfil()
    {
      //$userMedia=
      return Media::where('id',$this->photo_profil_id)->first();//->withTimestamps();
    }
}
