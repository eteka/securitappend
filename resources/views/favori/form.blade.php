<div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
    {!! Form::label('user_id', trans('favori.user_id'), ['class' => 'control-label']) !!}
    {!! Form::number('user_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('bien_id') ? 'has-error' : ''}}">
    {!! Form::label('bien_id', trans('favori.bien_id'), ['class' => 'control-label']) !!}
    {!! Form::number('bien_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('bien_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ref') ? 'has-error' : ''}}">
    {!! Form::label('ref', trans('favori.ref'), ['class' => 'control-label']) !!}
    {!! Form::textarea('ref', null, ('' == 'required') ? ['class' => 'form-control autosize', 'required' => 'required','rows'=>"4"] : ['class' => 'form-control autosize','rows'=>"4"]) !!}
    {!! $errors->first('ref', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? __('Mettre à jour') : __('Sauvegarder'), ['class' => 'btn btn-primary']) !!}
</div>
