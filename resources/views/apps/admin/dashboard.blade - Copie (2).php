@extends('layouts.app')

@section('footer')
        @include('layouts.partials.footer-light')
@endsection
@section('title')
{{__('Tableau de bord')}} - {{Auth::user()->fullname()}}
@endsection

@section('nav')
@include('layouts.partials.topnav-dark',['tabs'=>'home'])
@include('apps.includes.nav.notification')
@endsection


@push('script')
<script>
  function goTo(page, title, url) {
  if ("undefined" !== typeof history.pushState) {
    document.title = title;
    history.pushState({page: page}, title, url);
  } else {
    document.title = title;
    window.location.assign(url);
  }
}
//goTo("another page", "Page controler", '/terms');
</script>
{!!jsPlugin('jscroll')!!}
 <link href="{{asset('assets/plugins/enjoyhint/enjoyhint.css')}}" rel="stylesheet">
  <!--script src="{{asset('assets/plugins/enjoyhint/enjoyhint.min.js')}}"></script-->

<script type="text/javascript">

$(function() {

//initialize instance
//var enjoyhint_instance = new EnjoyHint({});

//simple config.
//Only one step - highlighting(with description) "New" button
//hide EnjoyHint after a click on the button.
/*var enjoyhint_script_steps = [
    {
        "next .navbar": 'Hello, I\'d like to tell you about EnjoyHint.<br> Click "Next" to proceed.'
    },
    {
        "click #main": 'Hello, I\'d like to tell you about EnjoyHint.<br> Click "Next" to proceed.'
    },*/
   /* {
        "next #left": "You can select different blocks. For example, let's select title.<br>" +
            "This event has a very simple code.<br>" +
            "{<br>" +
            "<text style='color: #00a6eb'>&nbsp &nbsp ' next #title '</text> : <text style='color: #2bff3c'>' Some description '</text> <br>" +
            "}<br>" +
            "<text style='color: #00ebe7'>next</text> - event (all events are described in the documentation)<br>" +
            "<text style='color: #00ebe7'>#title</text> - selector <br>" +
            "<text style='color: #00ebe7'>Some description</text> - Description for the block <br>"
    },
    {
        "click #right" : "For example, let's set a handler for a button click event.<br>" +
            "{<br>" +
            "<text style='color: #00a6eb'>&nbsp &nbsp ' click .btn-success '</text> : <text style='color: #2bff3c'>' Some description '</text> <br>" +
            "}<br>" +
            "Click the button and move on.",
        showSkip: false
    },
    {
        "next .container" : "You can highlight blocks by selecting them in a circle. <br>" +
            "<text style='color: #00ebe7'>shape</text> - you can define shape of highligting (rectangular||circle)<br>" +
            "{<br>" +
            "<text style='color: #00a6eb'>&nbsp &nbsp ' next #selector '</text> : <text style='color: #2bff3c'>' Some description ',</text> <br>" +
            "<text style='color: #00a6eb'>&nbsp &nbsp ' shape '</text> :  <text style='color: #2bff3c'>' circle '</text> <br>" +
            "}<br>" +
            "The circle radius seems to be too small.<br> Click 'Next' to fix it.",
        shape : 'circle'
    },
    {
        "next .row" : "At this step we fix radius<br>" +
            "<text style='color: #00ebe7'>radius</text> - sets the size of the circle radius<br>" +
            "{<br>" +
            "<text style='color: #00a6eb'>&nbsp &nbsp ' next #selector '</text> : <text style='color: #2bff3c'>' Some description ',</text> <br>" +
            "<text style='color: #00a6eb'>&nbsp &nbsp ' shape '</text> :  <text style='color: #2bff3c'>' circle '</text> <br>" +
            "<text style='color: #00a6eb'>&nbsp &nbsp ' radius '</text> : 80<br>" +
            "}<br>",
        shape : 'circle',
        radius: 80
    },
    {
        "next .nav" : "Sometimes you need to scroll the page either very slowly (as we've just done) or very fast.<br>" +
            "<text style='color: #00ebe7'>scrollAnimationSpeed</text> - sets the speed for the scroll page<br>" +
            "{<br>" +
            "<text style='color: #00a6eb'>&nbsp &nbsp ' next #selector '</text> : <text style='color: #2bff3c'>' Some description ',</text> <br>" +
            "<text style='color: #00a6eb'>&nbsp &nbsp ' scrollAnimationSpeed '</text> : 2500<br>" +
            "}<br>",
        scrollAnimationSpeed : 2500
    },
    {
        "key #main" : "You can attach handlers to keyboard events.<br>" +
            "<text style='color: #00ebe7'>keyCode</text> - key code for any 'key' event.<br>" +
            "{<br>" +
            "<text style='color: #00a6eb'>&nbsp &nbsp ' key #selector '</text> : <text style='color: #2bff3c'>' Some description ',</text> <br>" +
            "<text style='color: #00a6eb'>&nbsp &nbsp ' keyCode '</text> : 13<br>" +
            "}<br>" +
            "Enter some text and press 'Enter'",
        keyCode : 13
    }*/
/*
];

//set script config
enjoyhint_instance.set(enjoyhint_script_steps);

//run Enjoyhint script
enjoyhint_instance.run();
*/
  if(typeof $.fn.jscroll==="function") {
    $('.main-section').jscroll({
      autoTrigger: true,
      loadingFunction: function () {
        $('ul.paginations').hide();
      },
      loadingHtml: '<div id="cloader" class="cloader  sec_card"><div class="loader"></div></div>',
      padding: 0,
      debug: true,
      /*loadingFunction:function(){
        retur false;
      },*/
      nextSelector: '.pagination-wrapper .pagination li.active + li a',
      contentSelector: 'div.main-section #liste',
      
      callback: function () {
       // $('ul.pagination').remove();
        //$('ul.pagination').show();
        //$('.pagination-wrapper').addClass('padinnate-load');
        //alert('OK')
      }
    });
  }else{
    console.log("La function Jscroll n'est pas chargée")
  }
});

</script>
{!!jsPlugin('sticky')!!}
<script>
$(document).ready(function(){
  //$(".navbar-standard2").css('ZIndex',555555).sticky({topSpacing:0});
  $("#right,#profil-stick").sticky({topSpacing: 60});
});
</script>
@endpush
@section('content')
<style>


span.sec_icon>svg{
  height:22px;
}
.
.foot-list-inline {
  display:block;
}
.foot-list-inline .sec_icon>svg{
  fill:#686f76;
  height:15px;
  /*padding:0px 5px;*/
}
.foot-list-inline a{
  color:#686f76;
  text-decoration:none;
  margin-right:8px;

}

#new-elmt>.sec_icon>svg{
  fill:#ffffff;
  height:10px;
}

.gutter-condensed {
  margin-right: -8px;
  margin-left: -8px;
}
.list-style-none {
  list-style: none!important;
}
.top_home_param_bloc{
  padding: 10px 0
}
/* Style the form - display items horizontally */
.form-control-inline {
  display: flex;
  flex-flow: row wrap;
  align-items: center;
}
.form-control-xs{
  padding-bottom: 2px;
}
.pagination-wrapper{
  //margin-bottom: 15px;
}


.list-style-none{margin-bottom: 0px}
/*.padinnate-load{margin-bottom: 0px !important}
body.modal-open .supreme-container{
-webkit-filter: blur(1px);
-moz-filter: blur(1px);
-o-filter: blur(1px);
-ms-filter: blur(1px);
filter: blur(1px);
}
.modal-open .container-fluid, .modal-open  .container {
-webkit-filter: blur(5px) grayscale(90%);
}
.blur {
box-shadow: 0px 0px 20px 20px rgba(255,255,255,1);
text-shadow: 0px 0px 10px rgba(51, 51, 51, 0.9);
transform: scale(0.9);
opacity: 0.6;
}
.modal-open .container-fluid, .modal-open  .container {
-webkit-filter: blur(1px);
-moz-filter: blur(1px);
-o-filter: blur(1px);
-ms-filter: blur(1px);
filter: blur(1px);
}*/

</style>
<div class="main">
  <div class="container">
    <div class="row">
      <div class="col-md-2 hidden" id="left">
        <div id="profil-stick">
          @include ('apps.includes.nav.user-profil')
        </div>
      </div>
      <div class="col-md-8 col-md-offset-2">
        <!--div class="top_home_param_bloc  pad10">
          <div class="row">
            <div class="col-xs-12 text-center-xs col-sm-3 pad0_">
              @if(0 && isset($count_bien) && $count_bien!=NULL)
              <!--span class="text-muted bold"><i class="icon-bag"></i> {{$count_bien}}</span-->
              @endif
            </div>

            <div class="col-xs-12 text-center-xs mtop-xs-10 col-sm-9 text-right pr5">
              {!! Form::open(['route' => 'dashboard', 'class' => 'form-horizontal', 'method' => 'get']) !!}
              <div class="top-line-options">
                <span><a href="{{route('bien_favoris')}}" class="btn btn-md btn-default"><i class="icon-star"></i></a></span>
                <span><a href="{{route('classement')}}" title="{{__('Classement')}}" class="btn btn-md btn-default"><i class="icon-organization"></i></a></span>
                <span>
                  <a title="{{__('Tout exporter')}}" href="{{route('export_all_biens')}}" class="btn btn-md btn-default">
                   <svg class="svg-icon svg-icon-md rotate180" viewBox="0 0 20 20">
                      <path fill="none" d="M8.416,3.943l1.12-1.12v9.031c0,0.257,0.208,0.464,0.464,0.464c0.256,0,0.464-0.207,0.464-0.464V2.823l1.12,1.12c0.182,0.182,0.476,0.182,0.656,0c0.182-0.181,0.182-0.475,0-0.656l-1.744-1.745c-0.018-0.081-0.048-0.16-0.112-0.224C10.279,1.214,10.137,1.177,10,1.194c-0.137-0.017-0.279,0.02-0.384,0.125C9.551,1.384,9.518,1.465,9.499,1.548L7.76,3.288c-0.182,0.181-0.182,0.475,0,0.656C7.941,4.125,8.234,4.125,8.416,3.943z M15.569,6.286h-2.32v0.928h2.32c0.512,0,0.928,0.416,0.928,0.928v8.817c0,0.513-0.416,0.929-0.928,0.929H4.432c-0.513,0-0.928-0.416-0.928-0.929V8.142c0-0.513,0.416-0.928,0.928-0.928h2.32V6.286h-2.32c-1.025,0-1.856,0.831-1.856,1.856v8.817c0,1.025,0.832,1.856,1.856,1.856h11.138c1.024,0,1.855-0.831,1.855-1.856V8.142C17.425,7.117,16.594,6.286,15.569,6.286z"></path>
                    </svg>
                  </a>
                </span>

                {!! Form::select('orderby',["asc"=>__("Plus récent"),"desc"=>__("Plus ancien")], app('request')->input('orderby'),['class' => 'btn-default text-md btn l-inline form-control-md', 'required' => 'required',"onchange"=>"this.form.submit()"]) !!}
              </div>
              {!! Form::close() !!}
            </div>
          </div>
        </div-->
        <div class="main-section mtop15 mbottom30">
          <style>
            #add-cover a{text-decoration: none}
            #add-cover{
              height: 60px;
              display: none;
              /*background: #686f76;
              background: #737373;*/
              background: #244157;
              border-radius: 3px;
              margin-bottom: 15px;
            }
            .add-bien{
              border: 1px dashed #dedede;
              height: 40px;
            }
            #add_new_home {
              color: #f1f1f1;
              text-align: center;
              line-height: 20px;

            }
          </style>            
              @include('apps.includes.data.liste-biens',['biens'=>$biens,"icone"=>"icon-layers","msg"=>__('Aucun élément dans le classement')])
        </div>
        <div class="pagination-wrapper col-md-12 text-center mbottom30">
          {{$biens && $biens->links()?$biens->links():''}}
        </div>
</div>

<div   class="col-md-3 hidden  minh">
  <div id="right">
    <style>
    .lcad{
      box-shadow: 0 0 0 1px rgba(0,0,0,.15), 0 2px 3px rgba(0,0,0,.2);
      transition: box-shadow 83ms;
      background-color: #fff;
      border-radius: 2px;
      overflow: hidden;
      position: relative;
      height: 150px;
    }
    </style>
    <div  class="mtop15 text-sm text-muted mbottom10">
      <span class="icon-tag "></span> Des Boutiques à découvrir
    </div>
    <div class=" sec_cardbgwhiteshadow3noborderrond5">

      <div>
        <img class="img-responsive rond5" src="{{asset('storage/statics/pub/affiche-1.jpg')}}" alt="Publicité pate dentifrice">
      </div>
    </div>
    @include('layouts.partials.mini-footer')
  </div>
</div>
</div>
</div>
</div>

<!--------------------------------------------------->
<style>
  /*.background-dialog{
    position: absolute;
    background: rgba(0,0,0,.852);
    top:0;bottom: 0;left: 0;right: 0;
    z-index: 1000;
    overflow: auto;
  }
.dialog-container{
  width: 80%;
  height: 100%;
  background: rgba(0,0,0,.315);
  margin: auto;
  position: absolute
}
.dialog-left,.dialog-right{
  height: 500px;
 
}
body{
  //overflow: hidden;
}*/
</style>

@endsection
@push('dialog')

<div>
  <!--div class="background-dialog hidden">
    <div class="dialog-container_ container">
      <div class="dialog-row row">
        <div class="dialog-left col-md-8">
          Lorem ipsum dolor sit amet consectetur, adipisicing elit. In, earum, porro repellendus optio ipsum molestiae soluta vel vero esse ex corrupti delectus autem voluptate quo commodi possimus sed excepturi quis?
        </div>
        <div class="dialog-right col-md-4 bgwhite">
          <div class="card">
Lorem ipsum dolor sit amet consectetur adipisicing elit. Ducimus quo illo reiciendis libero aperiam vero voluptates nostrum fuga doloribus? Exercitationem magnam harum voluptatum consequuntur sunt nobis saepe ea, quisquam officiis.
          </div>
        </div>
      </div>
    </div>
  </div-->
</div>
@endpush
