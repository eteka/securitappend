@extends('layouts.app')
@section('title')
{{isset($count_bien)?$count_bien:__('Classement')}} -
@parent
@endsection
@section('nav')
@include('layouts.partials.topnav-dark',['tabs'=>"home"])
@include('apps.includes.nav.notification')
@endsection
@section('footer')

@endsection

@section('content')
<div class="main">
  <div class="container">
    <div class="row">
      <div class="col-md-2 hidden-xs" id="left">
        <div id="profil-stick">
          @include ('apps.includes.nav.user-profil')
        </div>
      </div>
      <div class="col-md-7 ">
        <div class="top_home_param_bloc ">
          <div class="row">
            <div class="col-xs-12 text-center-xs col-sm-4">
              @if(isset($count_bien) && $count_bien!=NULL)
              <span class="text-muted bold"><i class="icon-organization"></i> {{$count_bien}}</span>
              @endif
            </div>

            <div class="col-xs-12 col-sm-8 text-center-xs text-right">

              <div class="top-line-options mtop-xs-10">
                <span class="hidden-xs"><a title="{{__('Tableau de bord')}}" href="{{route('dashboard')}}" class="btn btn-md  btn-default"><i class="icon-home"></i></a></span>
                <span><a href="{{route('bien_favoris')}}" class="btn btn-md  btn-default">
                  <span class="title-svg-icon" title="{{__('Favoris')}}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"><path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"/></svg>
                  </span>
                </a></span>
                <span><a href="{{route('classement')}}" title="{{__('Classement')}}" class="btn btn-md bold "><i class="icon-organization"></i></a></span>

                <span><a title="{{__('Tout exporter')}}" href="{{route('export_favoris')}}" class="btn btn-md btn-default">
                  <svg class="svg-icon svg-icon-md rotate180" viewBox="0 0 20 20">
                      <path fill="none" d="M8.416,3.943l1.12-1.12v9.031c0,0.257,0.208,0.464,0.464,0.464c0.256,0,0.464-0.207,0.464-0.464V2.823l1.12,1.12c0.182,0.182,0.476,0.182,0.656,0c0.182-0.181,0.182-0.475,0-0.656l-1.744-1.745c-0.018-0.081-0.048-0.16-0.112-0.224C10.279,1.214,10.137,1.177,10,1.194c-0.137-0.017-0.279,0.02-0.384,0.125C9.551,1.384,9.518,1.465,9.499,1.548L7.76,3.288c-0.182,0.181-0.182,0.475,0,0.656C7.941,4.125,8.234,4.125,8.416,3.943z M15.569,6.286h-2.32v0.928h2.32c0.512,0,0.928,0.416,0.928,0.928v8.817c0,0.513-0.416,0.929-0.928,0.929H4.432c-0.513,0-0.928-0.416-0.928-0.929V8.142c0-0.513,0.416-0.928,0.928-0.928h2.32V6.286h-2.32c-1.025,0-1.856,0.831-1.856,1.856v8.817c0,1.025,0.832,1.856,1.856,1.856h11.138c1.024,0,1.855-0.831,1.855-1.856V8.142C17.425,7.117,16.594,6.286,15.569,6.286z"></path>
                    </svg>
                  </a></span>
              </div>
            </div>
          </div>
        </div>
        <div class="main-classement">

          <div class="list-classement ">
            <div class="line-bar ">
              <span class="text bgsilver  text-sm text-muted">  {{__("Mode d'acquisition")}}</span>
            </div>
          </div>
          <div class="mtop20 clearfix">
            <div class="row">
              @if(isset($modes))
              @foreach($modes as $m)
              <div class="col-sm-6">
                <div class="pad10 mbottom10 bgwhite rond3 borderd">
                  <span class="pad10 nodecoration">
                    <a  href="{{route('classement_mode',isset($m['slug'])?$m['slug']:'-')}}">    {{isset($m['nom'])?$m['nom']:'-'}} ({{isset($m['nb'])?$m['nb']:'0'}})</a>
                  </span>
                </div>
              </div>
              @endforeach
              @endif

            </div>
          </div>
          <div class="list-classement ">
            <div class="line-bar text-center">
              <span class="text bgsilver  text-sm text-muted">  {{__("Catégorie")}}</span>
            </div>
          </div>
          <div class="mtop20 clearfix">
            <div class="row">
              @if(isset($categories))
              @foreach($categories as $m)
              <div class="col-sm-6">
                <div class="pad10 bgwhite mbottom10 rond3 borderd">
                  <span class="pad10">
                    <a class="nodecoration" href='{{route("classement_categorie",isset($m["id"])?$m["id"]:0)}}'>    {{isset($m['nom'])?$m['nom']:'-'}} ({{isset($m['nb'])?$m['nb']:'0'}})</a>
                  </span>
                </div>
              </div>
              @endforeach
              @endif

            </div>
          </div>

        </div>
      </div>

      <div   class="col-md-3 hidden-xs  minh">
        <div id="right">
          <div  class="mtop15 text-sm text-muted mbottom10">
            <span class="icon-tag "></span> Des Boutiques à découvrir
          </div>
          <div class=" sec_cardbgwhiteshadow3noborderrond5">

            <div>
              <img class="img-responsive rond5" src="{{asset('storage/statics/pub/affiche-1.jpg')}}" alt="Publicité pate dentifrice">
            </div>
          </div>
          @include('layouts.partials.mini-footer')
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
