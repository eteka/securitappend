
@push('script')

{!!jsPlugin('selectisize')!!}
<script>
    $(function(){
        if ($ && typeof $.fn.selectize === 'function') {
            $('select.selectisize').selectize();
        }else{
            console.log("La fonction selesctisize n'existe pas ");
        }
    })
</script>
@endpush

                <div class="row">
                    <div class="col-sm-7">
                        <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                          <label for="nom_objet">{{__("Nom du l'objet")}} <span class="text-danger">*</span></label>
                          {!! Form::text('nom_objet', null,  ['class' => 'form-control', 'required' => 'required','autocomplete'=>"OFF","placeholder"=>__("Ordinateur portable")]) !!}

                          @if ($errors->any())
                          <small id="tf1Help" class="form-text text-muted">{{__("Veuillez indiquer un nom qui vous aidera à mieux identifier votre bien")}}</small>
                            @else
                            {!! $errors->first('nom_objet', '<p class="help-block">:message</p>') !!}
                            @endif
                    </div>
                    </div>
                    <div class="col-sm-5">
                            <div>
                                <div class="form-group {{ $errors->has('categorie') ? 'has-error' : ''}}">
                                    <label for="categorie"> {{__("Catégorie")}} <span class="text-danger">*</span></label>

                                    {!! Form::select('categorie', isset($categories)?$categories:[], null, ['class' => 'form-control selectisize', 'required' => 'required'] ) !!}
                                    {!! $errors->first('categorie', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                    </div>
                </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group {{ $errors->has('mode') ? 'has-error' : ''}}">
                                    <label for="mode">{{__("Mode d'acquisition")}} <span class="text-danger">*</span></label>

                                    {!! Form::select('mode', isset($modes)?$modes:[], null, ['class' => 'form-control selectisize', 'required' => 'required'] ) !!}
                                    {!! $errors->first('mode', '<p class="help-block">:message</p>') !!}
                                </div>
                                {!! $errors->first('mode', '<p class="help-block">:message</p>') !!}
                            </div>

                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group {{ $errors->has('lieu_objet') ? 'has-error' : ''}}">
                                    <label for="lieu_objet">{{__("Lieu d'acquisition")}}<span class="text-danger">*</span></label>
                                    {!! Form::text('lieu_objet', null,  ['class' => 'form-control', 'required' => 'required','autocomplete'=>"OFF","placeholder"=>__("Paris, France")]) !!}
                                 </div>
                            </div>
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group {{ $errors->has('photos') ? 'has-error' : ''}}">
                                    <label for="photo">{{__("Choisissir une photo")}} </label>
                                    <input type="file" id="photo" class="form-control" name="photos" accept='.jpeg,.jpg,.png,.gif' >

                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <div class="form-group ">
                                        <label for="visibilite_prive" >

                                          {{Form::radio('visibilite', 'PRIVE',"PRIVE",["required"=>"required","id"=>"visibilite_prive"])}}
                                          <i class="glyphicon glyphicon-lock"></i>
                                        {{__("Privé")}}
                                       <div>     <small class="text-muted text-xs">{{__('Il sera visibile uniquement par vous')}}</small></div>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12">
                                <div class="form-group m0">
                                        <label for="visibilite_public" >
                                        {{Form::radio('visibilite', 'PUBLIC',null,["required"=>"required","id"=>"visibilite_public"])}}
                                        <i class="glyphicon glyphicon-globe "></i>
                                        {{__("Public")}}
                                        <div>     <small class="text-muted text-xs">{{__('Il sera visibile pour le public')}}</small></div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group bbd  pad5">
                                <small class="text-muted">{{__("Paramètres avancés")}}</small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <div class="form-group {{ $errors->has("prix_objet") ? 'has-error' : ''}}">
                                    <label class="text-sm_" class="text-sm_" for="imei2">{{__("Prix d'achat")}} </label>
                                    {!! $errors->first('prix_objet', '<p class="help-block">:message</p>') !!}

                                    <div class="input-group  ">
                                        {!! Form::number('prix_objet', null,  ['class' => 'form-control','autocomplete'=>"OFF","placeholder"=>__("1350000")]) !!}



                                        <span class="input-group-addon sbtn-btn-info_ bge bold" id="basic-addon1">
                                    {!! Form::select('devise', isset($devises)?$devises:[], null, ['class' => ' bordere pad0 text-sm bge', 'required' => 'required'] ) !!}

                                    </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="form-group {{ $errors->has('imei1') ? 'has-error' : ''}}">
                                    <label class="text-sm_" for="imei1">{{__("Numéro de série 1")}} </label>
                                    {!! Form::text('imei1', null,  ['class' => 'form-control','autocomplete'=>"OFF","placeholder"=>__("12548754587")]) !!}

                                    @if ($errors->any())
                                    {!! $errors->first('imei1', '<p class="help-block">:message</p>') !!}
                                    @else
                                    <small id="Helpimei1" class="form-text text-sm text-muted">{{__("Code d'identification, IMEI, ISBN,...")}}</small>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="form-group {{ $errors->has('imei2') ? 'has-error' : ''}}">
                                    <label class="text-sm_" for="imei2">{{__("Numéro de série 2")}} </label>

                                    {!! Form::text('imei2', null,  ['class' => 'form-control','autocomplete'=>"OFF","placeholder"=>__("58745214587")]) !!}
                                    {!! $errors->first('imei2', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>

                            <!--div class="col-sm-12 col-md-4 col-lg-4">
                                <div class="form-group {{ $errors->has('prix_objet') ? 'has-error' : ''}}">
                                    <label for="prix_objet">{{__("Prix d'achat :")}}</label>
                                    <input type="number" class="form-control" id="prix_objet" value="{{old('prix_objet')==NULL?0:old('prix_objet')}}" value="0"  name="prix_objet">
                                </div>

                            </div-->

                            <!--div class="col-xs-12 col-sm-4">
                                <div class="form-group">
                                    <label for="imei3">{{__("IMEI 3")}} </label>
                                    <input type="text" class="form-control" id="imei3" value="{{old('imei3')}}" placeholder='{{__("55454656565")}}' name="imei3">
                                    <small id="tf1imei3" class="form-text text-muted">{{__("")}}</small>
                                </div>
                            </div-->
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-8 col-lg-8">
                                <div class="form-group {{ $errors->has('vendeur') ? 'has-error' : ''}}">
                                    <label class="text-sm_" for="vendeur">{{__("Réfrérences du vendeur (nom, tél,...)")}}</label>

                                    {!! Form::text('vendeur', null,  ['class' => 'form-control','autocomplete'=>"OFF","placeholder"=>__("Jean Moscou - Société ABC Sarl-IFU 1524888")]) !!}
                                    {!! $errors->first('vendeur', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-4">
                                <div class="form-group {{ $errors->has('date_objet') ? 'has-error' : ''}}">
                                    <label class="text-sm_" for="date_objet">{{__("Date d'acquisition")}}</label>

                                    {!! Form::date('date_objet', null,  ['class' => 'form-control', 'autocomplete'=>"OFF"]) !!}
                                    {!! $errors->first('date_objet', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group {{ $errors->has('marque') ? 'has-error' : ''}}">
                                    <label class="text-sm_" for="marque">{{__("La marque")}}</label>
                                    {!! Form::text('marque', null,  ['class' => 'form-control','autocomplete'=>"OFF","placeholder"=>__("LG, Samsung")]) !!}

                                    {!! $errors->first('marque', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group {{ $errors->has('modele') ? 'has-error' : ''}}">
                                    <label class="text-sm_" for="modele">{{__("Le Modèle")}} </label>
                                    {!! Form::text('modele', null,  ['class' => 'form-control','autocomplete'=>"OFF","placeholder"=>__("MT6,Y7,S30")]) !!}

                                    {!! $errors->first('modele', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>


                        </div>


                        <!--div class="row">
                           <div class="col-sm-12 col-md-12 col-lg-12">
                               <div class="form-group">
                                   <label for="photo_objet">{{__("Ajouter une photo")}}</label>
                                   <input type="file" class="form-control" name="photo_objet" id="photo_objet" >
                               </div>
                           </div>

                       </div>
                       <div class="row">
                           <div class="col-sm-12 col-md-12 col-lg-12">
                               <div class="form-group">
                                   <label for="document_objet">{{__("Document d'acquisition")}}</label>
                                   <input type="file" class="form-control" name="document_objet" id="document_objet" >
                               </div>
                           </div>

                       </div-->

                        <div class="form-group">
                          <label for="infos">{{__("Autres informations")}}</label>
                         {!! Form::textarea('infos', null,  ['class' => 'form-control autosize','autocomplete'=>"OFF",'rows'=>"4","placeholder"=>__("Origine, numérros de marquages, particularités, N° IMEI pour les téléphones et autres,...")]) !!}

                        </div>
                        <div class="form-group">
                            {!! Form::submit($formMode === 'edit' ? __('Mettre à jour') : __('Sauvegarder'), ['class' => 'btn btn-xs-block btn-primary']) !!}
                        </div>
