@extends('layouts.app')


    
@endsection
@section('content')
<style>
    #nav-slime-bar{
        //height:40px;
        
        background:#ffffff;
        border-bottom:1px solid #dddddd;
    }
    .nav-slime{
        padding:0 10px;
        margin:0;
        ms-flex-wrap: nowrap;
    flex-wrap: nowrap;
    white-space: nowrap;
    -webkit-overflow-scrolling: touch;
    }
    .nav-slime>li a,.nav-slime .nav-slime-link a{
        text-decoration:none;
        color:#686f76
    }
    .nav-slime>li.active,.nav-slime .nav-slime-link.active,
    .nav-slime>li:hover,.nav-slime .nav-slime-link:hover{
        border-bottom:2px solid #10CF7F;
    }
    
    .nav-slime>li,.nav-slime .nav-slime-link{
        display: inline-block;
        list-style-type:none;
        padding: 1rem;
    border-width: 0 0 3px;
    border: 1px solid transparent;
    border-top-left-radius: .25rem;
    border-top-right-radius: .25rem;
    border-bottom:2px solid #ffffff;
    margin-top:-2px;
    }
    .nav-slime .nav-slime-link.active {
    color: #28313b;
    background-color: transparent;
    border-color: #346cb0;
}
span.sec_icon>svg{
    height:22px;
}
.menu-profil>li{
    padding:5px 0;
    white-space:nowrap;
}
.sec_card{
    background-color: #fff;
    border: none;
    border-radius: .25rem;
    -webkit-box-shadow: 0 0 0 1px rgba(61,70,79,.05), 0 1px 3px 0 rgba(61,70,79,.15);
    box-shadow: 0 0 0 1px rgba(61,70,79,.05), 0 1px 3px 0 rgba(61,70,79,.15);
    
    margin-top:15px;
    margin-bottom:10px;
}
.foot-list-inline {
    display:block;
}
.foot-list-inline .sec_icon>svg{
    fill:#686f76;
    height:15px;
    padding:0px 5px;
}
.foot-list-inline a{
    color:#686f76;
    text-decoration:none;
    margin-left:8px;
    
}

#new-elmt>.sec_icon>svg{
    fill:#ffffff;
    height:10px;
}
</style>

<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <div class="profil_left">
                @include ('apps.includes.nav.user-profil')
                </div>
            </div>
            <div class="col-md-8">
                <div class="row">
                   <div class="col-xs-12 col-sm-12">
                   {{ Form::open(array('url' => 'new')) }}
                   {{ Form::component('bsText', 'components.form.text', ['name', 'value' => null, 'attributes' => []])}}
                   <div class="Subhead mtop15  sec_card ">
                       <div class="sec_bg-info">
                       <div class="card-body">
                            <h3 class="Subhead-heading ">{{__("Ajouter un nouvel objet")}}</h3>
                            
                            <p class="Subhead-description text-sm text-muted">
                            {{__("Un object, c'est tout bien que vous venez d'acquérir ou que vous avez acheté dans le passé et qui est en votre nom. 
                            Avez-vous des difficultés ?")}} , <a href="#">{{__("touvez de l'aide")}}.</a>
                            </p>
                        </div>
                        </div>
                            <div class="card-body">
                            <fieldset>
                        <!--legend>Base style</legend-->
                        <!-- .form-group -->
                        <div>
                        @if (count($errors) > 0)
                        <div class = "alert alert-danger">
                            <ul class="list-unstyled">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        </div>
                        <div class="form-group">
                          <label for="nom_objet">{{__("Nom du l'objet")}} <span class="text-danger">*</span></label>
                          <input type="text" class="form-control" id="nom_objet" require placeholder='{{__("Ordinateur portable")}}' name="nom_objet">
                          <small id="tf1Help" class="form-text text-muted">{{__("Veuillez indiquer un nom qui vous aidera à mieux identifier votre bien")}}</small>
                        </div>
                        <div class="row">
                           
                            <div class="col-sm-12 col-md-4 col-lg-4">
                                <div class="form-group {{ $errors->has('mode') ? 'has-error' : ''}}">
                                    <label for="mode">{{__("Mode d'acquisition")}} <span class="text-danger">*</span></label>
                                    
                                    {!! Form::select('mode', isset($modes)?$modes:[], true), null, ('' == 'required') ? ['class' => 'form-control selectisize', 'required' => 'required'] : ['class' => 'form-control selectisize']) !!}
                                    {!! $errors->first('mode', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>                            
                       
                            <div class="col-sm-12 col-md-8 col-lg-8">
                                <div class="form-group">
                                    <label for="param_objet">{{__("Vendeur")}}</label>
                                    <input type="text" name="param_objet" id="param_objet" class="form-control form-control-type-">
                                </div>
                            </div>
                        </div>
                        <div class="row">                            
                            <div class="col-sm-12 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label for="lieu_objet">{{__("Lieu d'acquisition :")}}<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="lieu_objet" placeholder='{{__("Paris, France")}}' name="lieu_objet">
                                </div>
                            </div>
                            
                            <div class="col-sm-12 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label for="date_objet">{{__("Date d'acquisition :")}}</label>
                                    <input type="date" class="form-control" id="date_objet" require  name="date_objet">
                                </div>
                            </div>
                            
                            <div class="col-sm-12 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label for="prix_objet">{{__("Prix d'achat :")}}</label>
                                    <input type="number" class="form-control" id="prix_objet" value="0"  name="prix_objet">
                                    <!--small id="prix_objet" class="form-text text-muted">{{__("0 : un don ou d'une héritage")}}</small-->
                       
                                </div>
                            </div> 
                        </div>
                        
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <div class="form-group">
                                    <label for="nom_objet">{{__("IMEI 1")}} </label>
                                    <input type="text" class="form-control" id="nom_objet" require placeholder='{{__("21125454545454")}}' name="nom_objet">
                                    <small id="tf1Help" class="form-text text-muted">{{__("Numéro d'identification de l'object")}}</small>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="form-group">
                                    <label for="nom_objet">{{__("IMEI 2")}} </label>
                                    <input type="text" class="form-control" id="nom_objet" require placeholder='{{__("55454656565")}}' name="nom_objet">
                                    <small id="tf1Help" class="form-text text-muted">{{__("")}}</small>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="form-group">
                                    <label for="nom_objet">{{__("IMEI 3")}} </label>
                                    <input type="text" class="form-control" id="nom_objet" require placeholder='{{__("55454656565")}}' name="nom_objet">
                                    <small id="tf1Help" class="form-text text-muted">{{__("")}}</small>
                                </div>
                            </div>
                        </div>
                        <!--div class="row">                           
                           <div class="col-sm-12 col-md-12 col-lg-12">
                               <div class="form-group">
                                   <label for="photo_objet">{{__("Ajouter une photo")}}</label>
                                   <input type="file" class="form-control" name="photo_objet" id="photo_objet" >
                               </div>
                           </div> 
                           
                       </div>
                       <div class="row">                           
                           <div class="col-sm-12 col-md-12 col-lg-12">
                               <div class="form-group">
                                   <label for="document_objet">{{__("Document d'acquisition")}}</label>
                                   <input type="file" class="form-control" name="document_objet" id="document_objet" >
                               </div>
                           </div> 
                           
                       </div-->
                        
                        <div class="form-group">
                          <label for="tf5">{{__("Autres informations utiles")}}</label>
                          <textarea class="form-control" id="tf5" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                          <div class="row">
                              <div class="col-md-4"></div>
                              <div class="col-md-8 text-right">
                                    <input type="submit" name="submit-bien" value="{{__('Sauvegarder')}}" class="btn btn-primary">
                              </div>
                          </div>
                        </div>
                        <!-- /.form-group -->
                      </fieldset> 
                            
                            </div>
                        </div>

                        
                    </div>
                   </div>
                    {{ Form::close() }}
                   <div class="clear50"></div>
                  
                </div>
            </div>
        </div>
    </div>
</div>
<div class="top-nav-tabs hidden">
    <div class="container ">
        <div class="row">            
            <div class="col-sm-12">
        
                <ol class="breadcrumb">
                <li><a href="#">ETEKA Wilfried</a></li>
                    <li><a href="#">Library</a></li>
                    <li class="active">Data</li> 
                </ol>
            </div>
            <div class="col-md-12">
                <div class=" nav-tabs-cover hidden">
                <ul class="nav nav-tabs sec_nav_tabs" id="sec_nav_tabs">
                    <li role="presentation" class="active">
                    <a href="#">
                    <span class="sec-tabs-icon">
                    <svg class="svg-icon" viewBox="0 0 20 20">
							<path d="M14.467,1.771H5.533c-0.258,0-0.47,0.211-0.47,0.47v15.516c0,0.414,0.504,0.634,0.802,0.331L10,13.955l4.136,4.133c0.241,0.241,0.802,0.169,0.802-0.331V2.241C14.938,1.982,14.726,1.771,14.467,1.771 M13.997,16.621l-3.665-3.662c-0.186-0.186-0.479-0.186-0.664,0l-3.666,3.662V2.711h7.994V16.621z"></path>
						</svg>
                    </span>
                    <span class="tabs-info">Élément </span></a></li>
                    <li role="presentation">
                    <a href="#">
                    <span class="sec-tabs-icon">
                    
                        <svg class="svg-icon" viewBox="0 0 20 20">
							<path d="M10.25,2.375c-4.212,0-7.625,3.413-7.625,7.625s3.413,7.625,7.625,7.625s7.625-3.413,7.625-7.625S14.462,2.375,10.25,2.375M10.651,16.811v-0.403c0-0.221-0.181-0.401-0.401-0.401s-0.401,0.181-0.401,0.401v0.403c-3.443-0.201-6.208-2.966-6.409-6.409h0.404c0.22,0,0.401-0.181,0.401-0.401S4.063,9.599,3.843,9.599H3.439C3.64,6.155,6.405,3.391,9.849,3.19v0.403c0,0.22,0.181,0.401,0.401,0.401s0.401-0.181,0.401-0.401V3.19c3.443,0.201,6.208,2.965,6.409,6.409h-0.404c-0.22,0-0.4,0.181-0.4,0.401s0.181,0.401,0.4,0.401h0.404C16.859,13.845,14.095,16.609,10.651,16.811 M12.662,12.412c-0.156,0.156-0.409,0.159-0.568,0l-2.127-2.129C9.986,10.302,9.849,10.192,9.849,10V5.184c0-0.221,0.181-0.401,0.401-0.401s0.401,0.181,0.401,0.401v4.651l2.011,2.008C12.818,12.001,12.818,12.256,12.662,12.412"></path>
						</svg>
                    </span>
                    <span class="tabs-info">Activités</span></a></li>
                    <li role="presentation"><a href="#">
                    <span class="sec-tabs-icon">
                    <svg class="svg-icon" viewBox="0 0 20 20">
							<path d="M12.319,5.792L8.836,2.328C8.589,2.08,8.269,2.295,8.269,2.573v1.534C8.115,4.091,7.937,4.084,7.783,4.084c-2.592,0-4.7,2.097-4.7,4.676c0,1.749,0.968,3.337,2.528,4.146c0.352,0.194,0.651-0.257,0.424-0.529c-0.415-0.492-0.643-1.118-0.643-1.762c0-1.514,1.261-2.747,2.787-2.747c0.029,0,0.06,0,0.09,0.002v1.632c0,0.335,0.378,0.435,0.568,0.245l3.483-3.464C12.455,6.147,12.455,5.928,12.319,5.792 M8.938,8.67V7.554c0-0.411-0.528-0.377-0.781-0.377c-1.906,0-3.457,1.542-3.457,3.438c0,0.271,0.033,0.542,0.097,0.805C4.149,10.7,3.775,9.762,3.775,8.76c0-2.197,1.798-3.985,4.008-3.985c0.251,0,0.501,0.023,0.744,0.069c0.212,0.039,0.412-0.124,0.412-0.34v-1.1l2.646,2.633L8.938,8.67z M14.389,7.107c-0.34-0.18-0.662,0.244-0.424,0.529c0.416,0.493,0.644,1.118,0.644,1.762c0,1.515-1.272,2.747-2.798,2.747c-0.029,0-0.061,0-0.089-0.002v-1.631c0-0.354-0.382-0.419-0.558-0.246l-3.482,3.465c-0.136,0.136-0.136,0.355,0,0.49l3.482,3.465c0.189,0.186,0.568,0.096,0.568-0.245v-1.533c0.153,0.016,0.331,0.022,0.484,0.022c2.592,0,4.7-2.098,4.7-4.677C16.917,9.506,15.948,7.917,14.389,7.107 M12.217,15.238c-0.251,0-0.501-0.022-0.743-0.069c-0.212-0.039-0.411,0.125-0.411,0.341v1.101l-2.646-2.634l2.646-2.633v1.116c0,0.174,0.126,0.318,0.295,0.343c0.158,0.024,0.318,0.034,0.486,0.034c1.905,0,3.456-1.542,3.456-3.438c0-0.271-0.032-0.541-0.097-0.804c0.648,0.719,1.022,1.659,1.022,2.66C16.226,13.451,14.428,15.238,12.217,15.238"></path>
						</svg>
                    </span>
                    <span class="tabs-info">Mouvements</span> </a></li>
                    <li role="presentation"><a href="#">
                    <span class="sec-tabs-icon">
                    <svg class="svg-icon" viewBox="0 0 20 20">
							<path d="M17.638,6.181h-3.844C13.581,4.273,11.963,2.786,10,2.786c-1.962,0-3.581,1.487-3.793,3.395H2.362c-0.233,0-0.424,0.191-0.424,0.424v10.184c0,0.232,0.191,0.424,0.424,0.424h15.276c0.234,0,0.425-0.191,0.425-0.424V6.605C18.062,6.372,17.872,6.181,17.638,6.181 M13.395,9.151c0.234,0,0.425,0.191,0.425,0.424S13.629,10,13.395,10c-0.232,0-0.424-0.191-0.424-0.424S13.162,9.151,13.395,9.151 M10,3.635c1.493,0,2.729,1.109,2.936,2.546H7.064C7.271,4.744,8.506,3.635,10,3.635 M6.605,9.151c0.233,0,0.424,0.191,0.424,0.424S6.838,10,6.605,10c-0.233,0-0.424-0.191-0.424-0.424S6.372,9.151,6.605,9.151 M17.214,16.365H2.786V7.029h3.395v1.347C5.687,8.552,5.332,9.021,5.332,9.575c0,0.703,0.571,1.273,1.273,1.273c0.702,0,1.273-0.57,1.273-1.273c0-0.554-0.354-1.023-0.849-1.199V7.029h5.941v1.347c-0.495,0.176-0.849,0.645-0.849,1.199c0,0.703,0.57,1.273,1.272,1.273s1.273-0.57,1.273-1.273c0-0.554-0.354-1.023-0.849-1.199V7.029h3.395V16.365z"></path>
						</svg>
                    </span>
                    <span class="tabs-info">Accessoires</span> </a></li>
                    <li role="presentation"><a href="#">
                    <span class="sec-tabs-icon">
                    <svg class="svg-icon" viewBox="0 0 20 20">
							<path d="M17.283,5.549h-5.26V4.335c0-0.222-0.183-0.404-0.404-0.404H8.381c-0.222,0-0.404,0.182-0.404,0.404v1.214h-5.26c-0.223,0-0.405,0.182-0.405,0.405v9.71c0,0.223,0.182,0.405,0.405,0.405h14.566c0.223,0,0.404-0.183,0.404-0.405v-9.71C17.688,5.731,17.506,5.549,17.283,5.549 M8.786,4.74h2.428v0.809H8.786V4.74z M16.879,15.26H3.122v-4.046h5.665v1.201c0,0.223,0.182,0.404,0.405,0.404h1.618c0.222,0,0.405-0.182,0.405-0.404v-1.201h5.665V15.26z M9.595,9.583h0.81v2.428h-0.81V9.583zM16.879,10.405h-5.665V9.19c0-0.222-0.183-0.405-0.405-0.405H9.191c-0.223,0-0.405,0.183-0.405,0.405v1.215H3.122V6.358h13.757V10.405z"></path>
						</svg>
                    </span>
                    <span class="tabs-info">Projets</span> </a></li>
                    <li role="presentation"><a href="#">
                    <span class="sec-tabs-icon">
                        <svg class="svg-icon" viewBox="0 0 20 20">
							<path d="M12.546,4.6h-5.2C4.398,4.6,2,7.022,2,10c0,2.978,2.398,5.4,5.346,5.4h5.2C15.552,15.4,18,12.978,18,10C18,7.022,15.552,4.6,12.546,4.6 M12.546,14.6h-5.2C4.838,14.6,2.8,12.536,2.8,10s2.038-4.6,4.546-4.6h5.2c2.522,0,4.654,2.106,4.654,4.6S15.068,14.6,12.546,14.6 M12.562,6.2C10.488,6.2,8.8,7.904,8.8,10c0,2.096,1.688,3.8,3.763,3.8c2.115,0,3.838-1.706,3.838-3.8C16.4,7.904,14.678,6.2,12.562,6.2 M12.562,13C10.93,13,9.6,11.654,9.6,10c0-1.654,1.33-3,2.962-3C14.21,7,15.6,8.374,15.6,10S14.208,13,12.562,13"></path>
						</svg>
                    </span>
                    <span class="tabs-info">Sécurité</span> </a></li>
                    <li role="presentation"><a href="#">
                    <span class="sec-tabs-icon">
                    <svg class="svg-icon" viewBox="0 0 20 20">
							<path d="M11.709,7.438H8.292c-0.234,0-0.427,0.192-0.427,0.427v8.542c0,0.234,0.192,0.427,0.427,0.427h3.417c0.233,0,0.426-0.192,0.426-0.427V7.865C12.135,7.63,11.942,7.438,11.709,7.438 M11.282,15.979H8.719V8.292h2.563V15.979zM6.156,11.709H2.74c-0.235,0-0.427,0.191-0.427,0.426v4.271c0,0.234,0.192,0.427,0.427,0.427h3.417c0.235,0,0.427-0.192,0.427-0.427v-4.271C6.583,11.9,6.391,11.709,6.156,11.709 M5.729,15.979H3.167v-3.416h2.562V15.979zM17.261,3.167h-3.417c-0.235,0-0.427,0.192-0.427,0.427v12.812c0,0.234,0.191,0.427,0.427,0.427h3.417c0.234,0,0.427-0.192,0.427-0.427V3.594C17.688,3.359,17.495,3.167,17.261,3.167 M16.833,15.979h-2.562V4.021h2.562V15.979z"></path>
						</svg>
                    </span>
                    <span class="tabs-info">Statistiques</span> </a></li>
                    
                </ul>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
