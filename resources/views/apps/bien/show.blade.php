@extends('layouts.app')
@section('title')
{{$bien->nom." - "}}
@parent
@endsection

<!------NAV MENU---->
@section('nav')
@include('layouts.partials.topnav-dark')
@endsection
<!------FIN NAV MENU---->
<!------PLUGINS---->
@push('script')
{!!jsPlugin('lightbox')!!}
@endpush
<!------FIN PLUGINS---->

@section('footer')
@include('layouts.partials.footer-light')
@endsection

@section('content')
<style>
.show-img-container img{
  margin: auto;
  display:block;
  display: flex;
  justify-content: center;
  
  vertical-align: middle;
  max-width: 100%;
  max-height: 100%;;background: #bec2c7;
}
.showldiv{
    border-right: 1px solid #eee;
}
.show-img-container{
 /* background: #bec2c7; */
  overflow: hidden;
  /*
  background: #f7f8f8;*/
margin-top:15px;
}

.b-show-title{
font-weight: bold;
font-size: 18px;
line-height: 18px;
}
.b-show-title-cover{
font-size: 14px;
line-height: 1.2;
z-index: 2;
border-radius: 0 0 4px 4px;
bottom: 0;
width: auto;
right: 0px;
left:15px;
}
.show-bien-cover{
  border: 1px solid #e8f0f6;
  border-radius: 3px;
  background: #ffffff;
  margin-top  : 10px;
}
.b-show-onfo-header{
  padding: 10px 15px;
  border-bottom: 1px solid #dddddd;
}
.u-name{
  font-size: 14px;
}
</style>
<div class="">
  <div class="container">

    <div class="row">
      <div class="col-md-1hidden" id="show-bloc-left">
       
      </div>
      <div class="col-md-8 col-md-offset-2 mbottom30">
        <div class="show-bien-cover">
        <div class="row">          
          <div class="col-sm-12" >
            <span class="plus_menu_cover" >
              <div class="dropdown">
                <a data-toggle="dropdown"  class="plus_menu" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                  <i class="icon-options"></i></a>
                  <div class="dropdown-menu dropdown-menu-right">
                    <ul class="sec-nav">
                      <!--li class="sec-nav__item">
                      <a class="sec-nav__link" href="{{route('show_bien',$bien->id)}}">
                      <i class="icon icon-eye"></i>
                      <span class="sec-nav__link-text">Visualiser</span>
                    </a>
                  </li-->
                  <li class="sec-nav__item">
                    <a class="sec-nav__link" href="{{route('edit_bien',$bien->id)}}">
                      <i class="icon-note"></i>
                      <span class="sec-nav__link-text">Modifier</span>
                    </a>
                  </li>
                  @if($bien->userFavoris && $bien->userFavoris->count())
  
                  <li class="sec-nav__item">
                    <a class="sec-nav__link" href="{{route('delete_bien_favori',$bien->id."?token=".sha1($bien->id))}}">
                      <i class="icon icon-star"></i>
                      <span class="sec-nav__link-text">{{__('Retirer de mes favoris')}}</span>
                    </a>
                  </li>
                  @else
                  <li class="sec-nav__item">
                    <a class="sec-nav__link" href="{{route('add_bien_favori',$bien->id."?token=".sha1($bien->id))}}">
                      <i class="icon icon-star"></i>
                      <span class="sec-nav__link-text">{{__('Ajouter aux favoris')}}</span>
                    </a>
                  </li>
  
                  @endif
                  <li class="sec-nav__item" id="trash{{$bien->id}}">
                    <a class="sec-nav__link" type="button" data-toggle="modal" data-target="#SecDeleteModal{{$bien->id}}" data-whatever="@mdo" href="#trash{{$bien->id}}">
  
                      <i class="icon-trash"></i>
                      <span class="sec-nav__link-text">Supprimer</span>
                    </a>
                  </li>
                  <li class="sec-nav__item">
                    <a class="sec-nav__link" href="{{route('export_bien',$bien->id)}}">
                      <i class="icon-anc"></i>
                      <span class="sec-nav__link-text">Exporter</span>
                    </a>
                  </li>
                </ul>
  
              </div>
            </div>
          </span>
            <div class="pad15">
              <div id="user-head-card"  class="borderbottomc">   
                    <span class="avatar-xs-cover">
                      <a title="{{$bien->user->fullname()}}" class="pull-left"  href="{{$bien->user->profil()}}"><img alt="{{$bien->user->fullname()}}" src="{{asset($bien->user->avatar())}}"  class="avatar avatar-xs photo">
                      </a>
                    </span>

                    <div class="u-name">
                    <a title="{{$bien->user->fullname()}}"   href="{{$bien->user->profil()}}">{{$bien->user->fullname()}}</a>
                    </div>

                    <div class="mini-date-card">
                    @if($bien->visibilite=="PRIVE")
                    <i class="icon-lock"></i>                  
                    @else
                    <i class="icon-eye"></i>
                    @endif
                    
                    {{$bien->created_at->diffForHumans()}}
                    </div>
                    
                </div>
              </div>
                <div class="pad0_15">
                  <div class="b-show-title-cover">
                    <h4 class="b-show-title">{{$bien->nom}}</h4>
                  </div>
            </div>
            
            <div class="show-img-container">
              @if($bien->media && file_exists($bien->media->url))
              @php
              $media=$bien->media;
              $code1=($bien->imei1!="")?$bien->imei1:NULL;
              $code=($bien->imei2!="")?$code1." - ".$bien->imei2:$code1;
              @endphp
              <div class="media-leftspull-right" >
                <a href="{{asset($bien->image())}}" title="{{$bien->title}}"  data-lightbox="bien-{{$bien->id}}" class="media-circles"><img class="img-responsive"   src="{{asset($bien->image())}}" alt="{{$bien->title}}">  </a>
              </div>
              @if($code!="")
              <div class="bgsilver border_sec bgsilver rond5  pad5_10 mtop5  text-center">
                {{__('Numéro de série')}}: <b>{{$code}}</b>
              </div>
              @endif
              @endif
              
            </div>
          </div>
          <div class="col-sm-12 col-md-12  rdiv">
            <div  class="b-show-infos">
              <div class="card-header bordertope">
                {{__('Détails')}}
              </div>
              <ol class="rows text-sm d-flex flex-wrap list-unstyled rond3" id="showlistrow">
                <li  >
                  <span class="sec_show_line_subtitle">
                      {{__("Propriétaire")}}
                    </span>
                    <span class="sec_show_line_desc  ">
                      <span class="">{{$bien->user->fullname()}} </span>
                    </span>
                </li>
                <li  >
                    
                    <span class="sec_show_line_subtitle">
                      {{__("Visibilité")}}
                    </span>
                    <span class="sec_show_line_desc  ">
                      <span class="text-uppercase">{{$bien->visibilite()}} </span>
                    </span>
                </li>
                <li >
                    <span class="sec_show_line_subtitle">
                      {{_("Mode d'acquisition")}}
                    </span>
                    <span class="sec_show_line_desc">
                      {{$bien->mode?$bien->mode->nom:'-'}}
                    </span>
                </li>
                @if($bien->lieu_achat!=NULL)
                <li  >
                  <span class="sec_show_line_subtitle">
                      {{__("Lieu d'acquisition")}}
                    </span>
                    <span class="sec_show_line_desc">
                      {{$bien->lieu_achat?$bien->lieu_achat:__('Non defini')}}
                    </span>
                </li>
                @endif
                <li  >
                  <div >
                    
                    <span class="sec_show_line_subtitle">
                      {{__("Prix d'acquisition")}}
                    </span>
                    <span class="sec_show_line_desc">
                      {{$bien->prix_achat?$bien->prix_achat:__('0')}}
                    </span>
                </li>
                <li  >
                    <span class="sec_show_line_subtitle">
                      {{__('Catégorie')}}
                    </span>
                    <span class="sec_show_line_desc">
                      {{$bien->type?$bien->type->nom:'-'}}
                    </span>
                </li>
                <li  >
                    <span class="sec_show_line_subtitle">
                      {{__("Date d'acquisition")}}
                    </span>
                    <span class="sec_show_line_desc">
                      @php
                      \Carbon\Carbon::setLocale('en');
                      @endphp
                      {{isset($bien->date_achat)  && $bien->date_achat!="1970-01-01"?\Carbon\Carbon::parse($bien->date_achat)->format('d-m-Y'):__('-')}}
                    </span>
                </li>
                @if($bien->vendeur)
                <li  >
                    <span class="sec_show_line_subtitle">
                      {{__("Vendeur")}}
                    </span>
                    <span class="sec_show_line_desc">
                      {{$bien->vendeur?$bien->vendeur:__('Non defini')}}
                    </span>
                </li>
                @endif
                <li  >
                    <span class="sec_show_line_subtitle">
                      {{__("Marque")}}
                    </span>
                    <span class="sec_show_line_desc">
                      {{$bien->marque?$bien->marque->nom:__('Non defini')}}
                    </span>
                </li>
                @if($bien->modele)
                <li  >
                    <span class="sec_show_line_subtitle">
                      {{__("Modèle")}}
                    </span>
                    <span class="sec_show_line_desc">
                      {{$bien->modele?$bien->modele->nom:__('Non defini')}}
                    </span>
                </li>
                @endif
                @if($bien->infos)
                <li class="col-xs-12 col-sm-12 pad5"  >
                    <span class="sec_show_line_subtitle">
                      {{__("Informations")}}
                    </span>
                    <span class="sec_show_line_desc">
                      {!!$bien->infos?$bien->infos:__('Non defini')!!}
                    </span>
                </li>
                @endif
      
              </ol>
            </div>
          </div>
        </div>
        </div>
      

    </div>
    

  </div>
  <div class="row">
    <div class="com-md-10 col-lg-8 col-md-offset-2">
      <div class="card mbottom30">
        <div class="card-header">
          Recommandation  
        </div>
      </div>
          @if(isset($biens))
          @include('apps.includes.data.liste-biens',['biens'=>$biens,"icone"=>"icon-layers","msg"=>__('Aucun élément dans le classement')])
          @endif
       
     
    </div>
  </div>
</div>
</div>
</div>

@endsection
@push('dialog')
    <div class="modal modal-danger fade" id="SecDeleteModal{{$bien->id}}" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
                  <div class="modal-dialog modal-info" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title bold" id="gridSystemModalLabel">
                        {{__("Supression d'élément")}} </h4>
                      </div>
                      <div class="modal-body">
                        <div class="row">
                          <div class="col-md-12">
                            <span class="text-info"></span>
                            <form class="delete" action="{{route('delete_bien',$bien->id)}}" method="POST">
                              <input type="hidden" name="_method" value="DELETE">
                              {{ csrf_field() }}
                              <input type="hidden" name="deleted" id="deleted" value="{{sha1($bien->id)}}">


                              <div class="pad10 bordererond5">

                                <h4 class="bold">{{__("Voulez-vous supprimer ce élément : ")}}{{$bien->nom}} ?</h4>
                                <ul class="list-inline-point text-muted text-sm">
                                  <li>

                                    <small class=""><i class="icon-user"></i>  <a href=""> {{$bien->user?$bien->user->name:"-"}} </a> &nbsp;&nbsp;</small>
                                    <li>
                                      <small class=""><i class="icon-calendar"></i> {{__('Ajouté,')}} {{$bien->created_at->diffForHumans()}}</small>
                                    </li>
                                    @if($bien->created_at!=$bien->updated_at)
                                    <li>
                                      <small class=""><i class="icon-pencil"></i> {{__('Dernière mise à jour,')}} {{$bien->updated_at->diffForHumans()}}</small>
                                    </li>
                                    @endif
                                  </ul>
                                </div>
                                <div class="text-muted text-sm mtop5">
                                  {{__("Si vous remarquez que ce bien contient des erreurs, vous pouvez cliquez sur le bouton modifier")}}
                                 
                                </div>

                              </div>
                            </div>

                          </div>
                          <div class="modal-footer">
                            
                            
                            <button type="button" class="btn btn-link btn-lg " data-dismiss="modal"><i class="icon-close"></i> {{__("Fermer")}}</button>
                            <a href="{{route('edit_bien',$bien->id)}}" class="btn btn-block bordertope btn-link btn-lg"><i class="icon-note"></i>  {{__("Modifier")}}</a>
                            <button type="submit" class="btn btn-link bordertope text-danger btn-lg rond0033"> <i class="icon-trash"></i> {{__("Supprimer")}}</button>

                          </form>
                        </div>
                      </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                  </div><!-- /.modal -->
                </div>
    @endpush  
