@extends('layouts.app')
@section('title',__('Nouveau bien'))
@section('nav')
        @include('layouts.partials.topnav-dark')
@endsection
@section('footer')
        @include('layouts.partials.footer-light')
@endsection

@section('content')
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <div class="profil_left">
                
                </div>
            </div>
                <div class="col-md-8">
                <div class="row">
                   <div class="col-xs-12 col-sm-12">
                   <!--form method="POST" action="{{ url('/admin/posts') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data"-->
                        
                   {{ Form::open(array('route' => 'create_bien','method'=>"POST",'files'=>TRUE,'class="form-horizontal"')) }}
                    {{csrf_field()}}
                   {{ Form::component('bsText', 'components.form.text', ['name', 'value' => null, 'attributes' => []])}}
                   <div class="Subhead mtop15  sec_card ">
                       <div class="sec_bg-info">
                            <div class="card-body">
                                <h3 class="Subhead-heading ">{{__("Nouveau bien")}}</h3>
                                
                                <p class="Subhead-description text-sm text-muted">
                                {{__("Il s'agit de tout bien, qu'il soit matériel physique ou non que vous venez d'acquérir ou que vous avez acheté dans le passé et qui est en votre nom. 
                                Avez-vous des difficultés ?")}} , <a href="#">{{__("touvez de l'aide")}}.</a>
                                </p>
                            </div>
                        </div>
                            <div class="card-body">
                            <fieldset>
                        <!--legend>Base style</legend-->
                        <!-- .form-group -->
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        @include ('apps.bien.form', ['formMode' => __('Sauvegarder')])

                        <!--div class="form-group">
                          <div class="row">
                              <div class="col-md-4"></div>
                              <div class="col-md-8 text-right">
                                  <input type="submit" name="submit-bien" value="{{__('Sauvegarder')}}" class="btn btn-primary">
                              </div>
                          </div>
                        </div-->
                        <!-- /.form-group -->
                      </fieldset> 
                            
                            </div>
                        </div>

                        
                    </div>
                   </div>
                    {{ Form::close() }}
                   <div class="clear50"></div>
                  
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
