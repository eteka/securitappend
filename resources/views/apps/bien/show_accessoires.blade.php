@extends('layouts.app')
@section('title')
   {{__('Accessoires')}} - {{$bien->nom." - "}}
    @parent
@endsection
@section('body')
    <body id="app-layout" class="bgwhite">
@show
@section('nav')
 @include('layouts.partials.simple-nav-dark')
 @include('layouts.partials.show_bien_topnav',['section'=>"accessoire"])
@endsection
@section('footer')
        @include('layouts.partials.footer-light')
@endsection

@section('content')

@endsection