<section class="bgwhite space-sm pb-4">
    <div class="container">
        <div class="row justify-content-between align-items-center">
            <div class=" col-md-8   col-xs-6">
            <h1 >
                @if(isset($section))
                {{$section}}
                @endif
            </h1>
            </div>
            <!--end of col-->
            <div class=" col-md-4 col-xs-6 text-right">
            </div>
            <!--end of col-->
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
<section class="flush-with-above pad0">
    <div class="bgwhite" id="form-compte" >
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav nav-tabs" id="user-profil" role="tablist">
                        <li class="nav-item  {{isset($tab) && $tab=="general"?'active':''}}">
                        <a class="nav-link"  href="{{route('edit_profil',$user->getPseudo())}}">
                                {{__('user.general')}}
                            </a>
                        </li>
                        <li class="nav-item {{isset($tab) && $tab=="parametre"?'active':''}}">
                            <a class="nav-link" id="parametre-tab"  href="{{route('show_edit_parametre',$user->getPseudo())}}" >
                                {{__('user.parametre')}}
                            </a>
                        </li>
                        <li class="nav-item {{isset($tab) && $tab=="password"?'active':''}}">
                        <a class="nav-link"  href="{{route('show_edit_password',$user->getPseudo())}}">{{__('user.password')}}</a>
                        </li>
                        <!--li class="nav-item">
                            <a class="nav-link" id="notifications-tab" href="{{route('show_edit_notification',$user->profil())}}">Notifications</a>
                        </li-->
                    </ul>
                </div>
                <!--end of col-->
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </div>
</section>  