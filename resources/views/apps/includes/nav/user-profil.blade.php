<style> 
  #menu-r-profil>li{
    display: block;
  }
#menu-r-profil .profil_span_info,#menu-r-profil .sec_icon{
  line-height:15px;
  overflow: hidden;
  max-width: 100%;
}
.profil_span_link a{
  color: #f3f3f3;
  padding: 2px;
  background: #2c7ec3;
}
.profil_span_link a:hover{
  color: #ffffff;
  text-decoration: none;
}
.bloc_menu-r-menu{
  margin-top: 15px;
}
.bloc-r-section
{
  text-transform: uppercase;
  color: #888888;
  font-size: 10px;
  font-weight: bold;
}
.menu-r{
  padding: 0;
  margin:0;
}
.menu-r li{
  list-style-type: none;
  
  margin-top: -1px;
  color: #565854;
  margin-top: 2px
}
.menu-r li a{
  text-decoration: none;
  display: block;
  padding: 5px 5px ;
  color: #505676;border-radius: 3px;
}
.menu-r li a:hover{
  background: #ffffff;
  background: rgba(255,255,255,.8);
  
}
.menu-r li a.active{
  background: #ffffff;
  background: rgba(255,255,255,.8);
    border-left: 3px solid #232425;
}
</style>
@if(isset($user) && $user)
<div class="mtop10">
  <!--div class="text-center">
    <a  tabindex="-1" href="#" id="preview" class="avatar mauto avatar-lg avatar-brd img-circle"><img alt="" src="{{asset(Auth::user()->avatar())}}"  class="" class="img-responsive">
    </a>
  </div-->
</div>
<div>
  <h5><a href="{{route('profil',$user->id)}}" class="bold">{{$user->name}}</a></h5>
  <ul id="menu-r-profil" class="list-unstyled menu-profil text-sm">
    
    <li title='{{__("Adresse du profil")}}'>
      <div class="row"> 
        <div class="col-xs-2 col-sm-1">
          <span class="icon-link pad5"></span>
        </div>
        <div class="col-xs-9 col-sm-10"> 
          
          <div class="profil_span_info btn  pad0" > <a class="" href="{{$user->profil()?url($user->profil()):url('')}}"><span class="">{{$user->pseudo?"@".$user->pseudo:$user->id}} </a></span>
        </div>
        </div>
      </div>
      
    </li>
    @if($user->poste)
    <li title='{{__("Ocupation actuelle")}}'>
      <div class="row"> 
        <div class="col-xs-2 col-sm-1">
          <span class="icon-tag pad5"></span>
        </div>
        <div class="col-xs-9 col-sm-10"> 
          
<span class="profil_span_info" > {{$user->poste}}</span>
        </div>
      </div>
      
    </li>
    @endif
     @if($user->localisation)
    <li title='{{__("Localisation")}}'>
      <div class="row"> 
        <div class="col-xs-2 col-sm-1">
          <span class="sec_icon_ icon-location-pin pad5"></span>
        </div>
        <div class="col-xs-9 col-sm-10"> 
<span class="profil_span_info" > {{$user->localisation}}</span>
        </div>
      </div>
      
    </li>
    @endif
     @if($user->website)
    <li title='{{__("Site Web")}}'>
      <div class="row"> 
        <div class="col-xs-2 col-sm-1">
          <span class="sec_icon_ icon-globe pad5">
          </span>
        </div>
        <div class="col-xs-9 col-sm-10"> 
<span class="profil_span_info" > <a target="_blanck" href="{{route('redirect_url',['url'=>$user->website])}}">{{$user->website}}</a></span>
        </div>
      </div>
      
    </li>
    @endif
    
  </ul>
</div>
@endif
<div class="bloc_menu-r-menu"> 
  <h5 class="bloc-r-section">{{__("Menu de navigation")}}</h5>
<div class="menu-r-cover"> 
    <ul class="menu-r">
      <li><a class="active" href="#"><span class="icon-bag"> </span> {{__("Biens")}}</a></li>
      <li><a href="#"><span class="icon-camera"></span> {{__("Photos")}}</a></li>
    </ul>
</div>
</div>