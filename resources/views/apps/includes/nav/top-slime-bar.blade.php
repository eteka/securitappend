
<div id="nav-slime-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-6 col-xs-12">
            <ul class="nav-slime">
                <li class="nav-slime {{isset($tabs) && $tabs=="home"?'active':''}}"><a href="{{route('feed')}}"><i class="icon-feed"></i></a> </li>
            <li class="nav-slime {{isset($tabs) && $tabs=="dashboard"?'active':''}}"><a href="{{route('dashboard')}}">{{__('Tableau de bord')}}</a> </li>
            <li class="nav-slime"><a href="">Favoris</a> </li>
            <!--li class="nav-slime {{isset($tabs) && $tabs=="stat"?'active':''}}"><a href="">{{__('Statistiques')}} </a> </li>
                    <li class="nav-slime"><a href="">Activités</a> </li>
                    <li class="nav-slime"><a href="">Favoris</a> </li>
                    <li class="nav-slime"><a href="">Discussions</a> </li>
                    <li class="nav-slime"><a href="">Statistiques</a> </li-->
                </ul>
            </div>
            <div class="col-sm-6  col-xs-12 col-md-3 text-right">
                <ul class="nav-slime hidden-xs nav-slime-link">
                    <li class="nav-slime"><a href="">{{__('Aide')}} </a> </li>
                    <li class="nav-slime"><a href="">{{__('Paramètres')}} </a> </li>
                </ul>
                <!--a class="btn btn-success bold btn-sm mtop5" id="new-elmt" href="{{route('create_bien')}}">

                <span class="icon-plus"></span>  Nouveau
                </a-->
            </div>
        </div>
    </div>

</div>

<div>

</div>
<!--div class="container">
    <div class="row">
        <div class="col-md-12 hidden-xs">
        <ul class="breadcrumb">
            <li><a href="#">Bien</a></li>
            <li><a href="#">Moto</a></li>
            <li>Edition</li>
        </ul>
        </div>
    </div>
</div-->
<style>
    #nav-slime-bar{
        max-height:44px;
        background:#ffffff;
        border-bottom:1px solid #dddddd;
        margin-top: 54px;
    }
    .nav-slime{
        padding:0 ;
        margin:0;
        ms-flex-wrap: nowrap;
    flex-wrap: nowrap;
    white-space: nowrap;
    -webkit-overflow-scrolling: touch;
    }
    .nav-slime>li a,.nav-slime .nav-slime-link a{
        text-decoration:none;
        color:#686f76
    }
    .nav-slime>li.active,.nav-slime .nav-slime-link.active{
        border-bottom:2px solid #10CF7F;
        font-weight: 600;
    }

    .nav-slime>li,.nav-slime .nav-slime-link{
        display: inline-block;
        list-style-type:none;
        padding:10px 5px;
    border-width: 0 0 3px;
    border: 1px solid transparent;
    border-top-left-radius: .25rem;
    border-top-right-radius: .25rem;
    margin-top:-2px;
    }
    .nav-slime.nav-slime-link li>a{
        color: #4160a3
    }
    .nav-slime .nav-slime-link.active {
    color: #28313b;
    background-color: transparent;
    border-color: #346cb0;
}
/*
span.sec_icon>svg{
    height:22px;
}*/
/*
.foot-list-inline {
    display:block;
}
.foot-list-inline .sec_icon>svg{
    fill:#686f76;
    height:15px;
    padding:0px 5px;
}
.foot-list-inline a{
    color:#686f76;
    text-decoration:none;
    margin-left:8px;

}

#new-elmt>.sec_icon>svg{
    fill:#ffffff;
    height:10px;
}*/
</style>
