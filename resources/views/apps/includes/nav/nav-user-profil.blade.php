@if($user)
<style> 
    #head-profil-cover{
        /*height: 280px !important;*/
        background: #ffffff;
        border: 0px solid #dfe5ea;
        border:0px solid #e5eff5;
        margin-top:10px;
        width: 680px;
        border-radius:3px;
        margin-bottom: 10px;
        }
    #banner{
        height: 150px;
    }
    #top-profil-header{
        height: 150px;
        border-bottom: 0px solid  #dfe5ea;
        background: url("{{asset('assets/statics/app/pages/profil_bg_default.JPG')}}") center center #dfe5ea;
        border-radius: 3px 3px 0 0;
        
    }
    @media screen and (max-width: 600px){
        #top-profil-header{
            height: 90px;
        }
    }
    #user_profil_img{
        height: 100px;
    border-radius: 100px;
    position: absolute;
    left: 40px;
    margin-top: 75px;
    border: 3px solid #fff;
    }
    #profil_user_name{
        margin-top: 5px;
        margin-bottom: 0px;
        margin-left: 0px;
        font-weight: bold;
    }
    .ml130{margin-left: 130px}
    #bottom-profil-header{
        border:1px solid #e5eff5;
        height: 135px;
        border-radius: 0 0 3px 3px;
        padding: 0 10px 5px;
    }
    #user_stats>li{
        padding: 2px 10px;
        margin-top: 5px;
        border-right: 1px solid #e5eff5;
        height: 40px;
    }
    #user_stats{
       border-top: 1px solid #e5eff5;
    }
.d-inline-block {
display: inline-block !important;
}
.dropdown .dropdown-item {
    display: block;
    padding: 2px 10px;
}
    .menu-profil-home{
        margin-bottom: 10px;
        /*border-bottom: 2px solid #ffffff;
        padding:5px 15px;
        background: #ffffff;
        border-radius: 3px ;
        box-shadow: -1px 0px 3px #dedede;
        border-top: 1px solid #e5eff5;*/
    }
    .menu-tabs_explorer{
        padding: 0;
        margin:0;
    }
    .menu-tabs_explorer li>a{
        color: inherit;
        text-decoration: none;
        padding: 0 2px
    }
</style>
<section class="bgwhite ">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xs-12 col-md-12">
                <div class="media row">
                    <div class="text-center col-xs-4 col-md-2 col-md-offset-2 col-lg-2">
                      <a  tabindex="-1" href="#" id="preview" class="avatar mauto avatar-lg avatar-xlg"><img alt="" src="{{asset(Auth::user()->avatar())}}"  class="" class="img-responsive">
                      </a>
                    </div>
                    <div class="media-body_ col-xs-8 col-md-8">
                        <div class="mb-3">
                            <h1 class="h2 mbottom10">{{$user->fullname()}}</h1>
                            @if($user->pays || $user->ville)
                            <span class="text-muted text-sm"><i class="icon-location-pin"></i>
                              @endif
                              {{$user->pays?$user->pays->nom().',':''}} {{$user->ville?$user->ville:''}}{!!($user->ville || $user->pays)?"&nbsp;&nbsp;| &nbsp;&nbsp;":''!!}</span>
                            <span class="text-sm text-muted">
                              @if($user->website)
                                  <span class="sec_icon_ icon-globe ">
                                  </span>
                        <span class="profil_span_info" {{__("Site Web")}} > <a target="_blanck" href="{{route('redirect_url',['url'=>$user->website])}}">{{$user->website}}</a></span>
                                
                            @endif
                            &nbsp;&nbsp;| &nbsp;&nbsp;<span class="text-sm text-muted"><i class="icon-calendar"> </i><span class="text-muted" class="" href="{{url('/')}}"> {{__("Inscrit")}} {{$user->created_at->diffForHumans()}}</span>
                                                    
                          </span>
                          </div>
                        <p>
                            {{$user->poste}}
                        </p>
                        <div >
                            <button class="btn btn-outline-primary nobg " data-toggle="button"><i class="icon-user-follow"></i> {{__("S'abonner")}}</button>
                            <button class="btn btn-outline-primary nobg">
                              <svg class="svg-icon" viewBox="0 0 20 20">
                                <path d="M14.999,8.543c0,0.229-0.188,0.417-0.416,0.417H5.417C5.187,8.959,5,8.772,5,8.543s0.188-0.417,0.417-0.417h9.167C14.812,8.126,14.999,8.314,14.999,8.543 M12.037,10.213H5.417C5.187,10.213,5,10.4,5,10.63c0,0.229,0.188,0.416,0.417,0.416h6.621c0.229,0,0.416-0.188,0.416-0.416C12.453,10.4,12.266,10.213,12.037,10.213 M14.583,6.046H5.417C5.187,6.046,5,6.233,5,6.463c0,0.229,0.188,0.417,0.417,0.417h9.167c0.229,0,0.416-0.188,0.416-0.417C14.999,6.233,14.812,6.046,14.583,6.046 M17.916,3.542v10c0,0.229-0.188,0.417-0.417,0.417H9.373l-2.829,2.796c-0.117,0.116-0.71,0.297-0.71-0.296v-2.5H2.5c-0.229,0-0.417-0.188-0.417-0.417v-10c0-0.229,0.188-0.417,0.417-0.417h15C17.729,3.126,17.916,3.313,17.916,3.542 M17.083,3.959H2.917v9.167H6.25c0.229,0,0.417,0.187,0.417,0.416v1.919l2.242-2.215c0.079-0.077,0.184-0.12,0.294-0.12h7.881V3.959z"></path>
                              </svg>
                            </button>
                            <button class="btn btn-outline-primary nobg">
                              <svg class="svg-icon" viewBox="0 0 20 20">
                                <path d="M14.613,10c0,0.23-0.188,0.419-0.419,0.419H10.42v3.774c0,0.23-0.189,0.42-0.42,0.42s-0.419-0.189-0.419-0.42v-3.774H5.806c-0.23,0-0.419-0.189-0.419-0.419s0.189-0.419,0.419-0.419h3.775V5.806c0-0.23,0.189-0.419,0.419-0.419s0.42,0.189,0.42,0.419v3.775h3.774C14.425,9.581,14.613,9.77,14.613,10 M17.969,10c0,4.401-3.567,7.969-7.969,7.969c-4.402,0-7.969-3.567-7.969-7.969c0-4.402,3.567-7.969,7.969-7.969C14.401,2.031,17.969,5.598,17.969,10 M17.13,10c0-3.932-3.198-7.13-7.13-7.13S2.87,6.068,2.87,10c0,3.933,3.198,7.13,7.13,7.13S17.13,13.933,17.13,10"></path>
                              </svg>
                            </button>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!--end of col-->
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>  
<style>
  .sec_tabs_cover {
    border-bottom:0px solid #dedede;
  }
  .sec_tabs {
      border-width: 0px;
  }

.sec_tabs .nav-item .nav-link {
  border-width: 0px;
}
.sec_tabs .nav-item .nav-link:hover {
  border-width: 0px;
  color: #4582EC;
}
.sec_tabs .nav-item .nav-link {
  padding:10px 15px;
  transition: color .35s ease, border-color .35s ease;
  border-bottom: 3px solid rgba(255, 255, 255, 0);
      border-bottom-color: rgba(255, 255, 255, 0);
      color: inherit
}
.sec_tabs .nav-item.active {
  margin-bottom: 0px
}
.sec_tabs .nav-item.active .nav-link/*, .sec_tabs .nav-item.show .nav-link*/ {
  color: #4582EC;
  border-left-width: 0px;
  border-top-width: 0px;
  border-right-width: 0px;
  margin-bottom: -2px !important;
  border-bottom: 2px solid #4582EC !important;
}
</style>
<section class="flush-with-above space-0 nav-tabs-cover">
  <div class="bgwhite sec_tabs_cover">
      <div class="container">
          <div class="row">
              <div class="col-md-8 col-md-offset-2">
                  <ul class="nav nav-tabs sec_tabs"  role="tablist">
                      <li class="nav-item {{(isset($tabs)&& $tabs=='profil')?"active":''}}" >
                      <a class="nav-link " title=" {{__('Profil')}}"  href="{{route('profil',$user->getPseudo())}}" ><i class="icon-user"></i> {{__('Profil')}}</a>
                      </li>
                      <li class="nav-item {{(isset($tabs)&& $tabs=='favoris')?"active":''}}">
                          <a class="nav-link" title=" {{__('Profil')}}"  href="{{route('profil_favoris',$user->getPseudo())}}" ><i class="icon-star"></i> {{__('Favoris')}}</a>
                      </li>
                      <li class="nav-item {{(isset($tabs)&& $tabs=='collections')?"active":''}}">
                          <a class="nav-link" title=" {{__('Profil')}}" href="#" role="tab" aria-controls="followers" aria-selected="false"><i class="icon-layers"></i> {{__('Collections')}}</a>
                      </li>
                      <li class="nav-item {{(isset($tabs)&& $tabs=='following')?"active":''}}">
                          <a class="nav-link" title=" {{__('Profil')}}" href="#" ><i class="icon-user-following"></i> {{__('Abonnés')}}</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link {{(isset($tabs)&& $tabs=='follow')?"active":''}}" href="#" ><i class="icon-user-follow"></i> {{__('Abonnements')}}</a>
                      </li>
                  </ul>
              </div>
              <!--end of col-->
          </div>
          <!--end of row-->
      </div>
      <!--end of container-->
  </div>
</section>
@endif