<div class="rows mbottom30" id="listes">
    <style>
      .mc{
        //position: absolute;
        background: #eeeeee;
        overflow: hidden;
        max-width: 100%;
        margin:auto;
      }
      .fh_{
        font-size:14px;
        font-weight: 100;
        color: #888888 
      }    
      .cell {
       overflow: hidden;
    height: 210px;
    -webkit-border-radius: 8px;
    -moz-border-radius: 8px;
    border-radius: 8px;
    margin: 0 6px;
        margin-left: 6px;
    overflow: hidden;
   // background-color: #f1f2f4;
   //width:215px;
  }
  .cell-cover{
  height: 210px;
  overflow: hidden;
  /*background: #244157;*/
  }
  
  .cell-title{
  font-size: 14px;
  padding: 24px 16px 12px 16px;
  line-height: 1.2;
  font-weight: bold;
  text-shadow: 1px 1px 2px rgba(0,0,0,0.3);
  color: #fff;
  -webkit-border-radius: 6px;
  -moz-border-radius: 6px;
  border-radius: 6px;
  background: -moz-linear-gradient(top,rgba(44,66,80,0) 0,#2c4250 100%);
  background: -webkit-linear-gradient(top,rgba(44,66,80,0) 0,#2c4250 100%);
  background: linear-gradient(to bottom,rgba(44,66,80,0) 0,#2c4250 100%);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#002c4250',endColorstr='#2c4250',GradientType=0);
  width: 100%;
  z-index: 2;
  position: absolute;
  bottom: 0;
    width: auto;
    right: 6px;
    left:6px;
  }
  .cell-b-title{
  font-weight: bold;
  font-size: 18px;
  line-height: 18px;
  }
  .cell-sm-title{
  font-weight: 400;
  color: #ffffff;
  font-size: 13px;
  width: 100px;
  text-overflow: ellipsis;
  }
  .cell-title a{
  color: #ffffff;
  text-decoration: none;
  }
  .avatar-xs-cover{
  float: left;
  }
  .avatar-xs a{text-decoration: none;}
  .avatar-xs{
  height: 30px;
  width: 30px;
  margin-right: 3px;
  border-radius: 50%;  
  }
  .badge-white{
  background: #fff7f7;
  color: #484848;
  text-shadow: none;
  }
  #user-foot-card{
  width: 100%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  }
  .mini-date-card{
  font-size: 10px;
  font-weight: 400 !important;
  margin-top: 0px;
  float: left;
  }
  @media screen and (max-width: 600px) {
  .cell-title{
    left:21px;
    right:21px;
  }
  }
  .plus_menu{
    position: absolute;
  margin-top: 5px;
  margin-bottom: 0px;
  right: 5px;
  cursor: pointer;
  color: #000;
  padding: 0 10px;
  background: #fff;
  height: 15px;
  line-height: 12px;
  border-radius: 10px;
  }
  .plus_menu_cover{
    overflow: visible; position: relative; width: 80px;
  }
  .plus_menu_cover .dropdown-menu{
    right: 15px;
  top: 17px;
  }
  /*ul#user-menu::before,*/
  .dropdown-menu.dropdown-menu-right::before {
      content: '';
      display: block;
      position: absolute;
      bottom: -7px;
      margin-left: 30%;
      z-index: 5000;
      margin-left: calc(40% + 30px);
      border: 10px solid transparent;
      border-bottom-color: #fff;
      top: 0;
      height: 20px;
      margin-top: -20px;
      right: 8px;
  }
  .plus_menu_cover .dropdown-menu::before {
    
  }
  .like-top a{ text-decoration: none;}
  .like-top{
    position: absolute;
    top: 5px;
    left: 15px;;
  }
   </style>
      @if(isset($biens) && $biens!=NULL && $biens->count()>0)
      @foreach($biens as $bien)
      <div class="col-xs-12  col-sm-6 col-md-4 col-lg-4 pad0  mbottom10 ">
          <div class="cell sec_card">
            <span class="plus_menu_cover" >
              <div class="dropdown">
                <a data-toggle="dropdown"  class="plus_menu" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                  <i class="icon-options"></i></a>
                  <div class="dropdown-menu dropdown-menu-right">
                    <ul class="sec-nav">
                      <!--li class="sec-nav__item">
                      <a class="sec-nav__link" href="{{route('show_bien',$bien->id)}}">
                      <i class="icon icon-eye"></i>
                      <span class="sec-nav__link-text">Visualiser</span>
                    </a>
                  </li-->
                  <li class="sec-nav__item">
                    <a class="sec-nav__link" href="{{route('edit_bien',$bien->id)}}">
                      <i class="icon-note"></i>
                      <span class="sec-nav__link-text">Modifier</span>
                    </a>
                  </li>
                  @if($bien->userFavoris && $bien->userFavoris->count())
  
                  <li class="sec-nav__item">
                    <a class="sec-nav__link" href="{{route('delete_bien_favori',$bien->id."?token=".sha1($bien->id))}}">
                      <i class="icon icon-star"></i>
                      <span class="sec-nav__link-text">{{__('Retirer de mes favoris')}}</span>
                    </a>
                  </li>
                  @else
                  <li class="sec-nav__item">
                    <a class="sec-nav__link" href="{{route('add_bien_favori',$bien->id."?token=".sha1($bien->id))}}">
                      <i class="icon icon-star"></i>
                      <span class="sec-nav__link-text">{{__('Ajouter aux favoris')}}</span>
                    </a>
                  </li>
  
                  @endif
                  <li class="sec-nav__item" id="trash{{$bien->id}}">
                    <a class="sec-nav__link" type="button" data-toggle="modal" data-target="#SecDeleteModal{{$bien->id}}" data-whatever="@mdo" href="#trash{{$bien->id}}">
  
                      <i class="icon-trash"></i>
                      <span class="sec-nav__link-text">Supprimer</span>
                    </a>
                  </li>
                  <li class="sec-nav__item">
                    <a class="sec-nav__link" href="{{route('export_bien',$bien->id)}}">
                      <i class="icon-anc"></i>
                      <span class="sec-nav__link-text">Exporter</span>
                    </a>
                  </li>
                </ul>
  
              </div>
            </div>
          </span>
            <div class="cell-cover ">
              <div class="like-top">
                <a href=""><i class="icon-heart"></i></a>
              </div>
            @if($bien->media && file_exists($bien->media->mini))
            @php
            $media=$bien->media;
            @endphp
            <a href="{{route('show_bien',$bien->id)}}" ><img  class="media-circle_  mc img-responsive" width="auto" src="{{$media->imageMini()}}" alt="{{$media->title}}">  </a>
           @else
           <a href="{{route('show_bien',$bien->id)}}" ><img  class="media-circle_  mc img-responsive" width="auto" src="{{$bien->imageMini()}}" alt="{{$bien->nom()}}">  </a>
          
            @endif
          
          <div class="cell-title">
           <a href="{{route('show_bien',$bien->id)}}">
             
            <h4 title="{{$bien->nom}}" class="cell-b-title">
              @if(Auth::user()->biensFavoris() && Auth::user()->biensFavoris()->where("bien_id",$bien->id)->count())
              <span class="title-svg-icon start-yellow" title="{{__('Ajouté aux favoris')}}">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"><path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"/></svg>
              </span>
              @endif
              {{$bien->nom()}}</h4>
            </a> 
            <ul class="list-inline text-xs mbottom0">
              <li><i class="icon-bubble"></i> 552 &nbsp;</li>
              <li><i class="icon-heart"></i> 12552 &nbsp;</li>
              <li><i class="icon-credit-card"></i> 12$ &nbsp;</li>
            </ul>
            <div id="user-foot-card" class="pull-left mbottom5 text-sm mtop5">
                    <span class="avatar-xs-cover">
                      <a title="{{$bien->user?$bien->user->fullname():''}}" class="pull-left"  href="{{$bien->user->profil()}}"><img alt="{{$bien->user->fullname()}}" src="{{asset($bien->user->avatar())}}"  class="avatar avatar-xs photo">
                      </a>
                    </span>
                    <a title="{{$bien->user?$bien->user->fullname():''}}"   href="{{$bien->user->profil()}}">{{$bien->user?$bien->user->fullname():''}}</a>
                    <div class="mini-date-card">
                    @if($bien->visibilite=="PRIVE")
                    <i class="icon-lock"></i>                  
                    @else
                    <i class="icon-eye"></i>
                    @endif
                    
                    {{$bien->created_at->diffForHumans()}}
                    </div>
            </div>
          </div>
          
        </div>
        </div>
      </div>    
      @endforeach
  
  
    @else
    <div class="col-sm-12col-md-12col-md-offset-text-center item">
      <div class="card sec_card text-center ">
        <div class="card-body ">
          <div class="pad30"> <i class="{{isset($icone)?$icone:"icon-layers"}} huge1 display-5"></i></div>
          <div class="mb20 pad10">{{isset($msg)?$msg:__("La boite magic de vos biens est vide")}}</div>
          <a href="{{route('create_bien')}}" class="btn btn-default btn-sm btn-blocks bold text-un mtop10"><i class="icon-plus "></i> {{__('Ajouter un nouveau')}}</a>
        </div>
      </div>
  
  
    </div>
    @endif
  </div>
  
  
  