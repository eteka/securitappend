<div class="row" id="listes">
 
  <style>
   #listes{
      padding: 0 10px
    }
    .mc{
      //position: absolute;
      background: #eeeeee;
      overflow: hidden;
      max-width: 100%;
      margin:auto;
      min-height:auto;
    }
    .fh_{
      font-size:14px;
      font-weight: 100;
      color: #888888 
    }    
    .cell {
     overflow: hidden;
  height: 200px;
  -webkit-border-radius: 8px;
  -moz-border-radius: 8px;
  border-radius: 8px;
  margin: 0 6px;
      margin-left: 6px;
  overflow: hidden;
  border:1px solid #ddd;
 // background-color: #f1f2f4;
 //width:215px;
}
.cell-cover{
height: 200px;
overflow: hidden;
background: #eee;
/*background: #244157;*/
}

.cell-title{
font-size: 14px;
padding: 24px 16px 12px 16px;
line-height: 1.2;
font-weight: bold;
text-shadow: 1px 1px 2px rgba(0,0,0,0.3);
color: #fff;
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 6px;
background: -moz-linear-gradient(top,rgba(44,66,80,0) 0,#2c4250 100%);
background: -webkit-linear-gradient(top,rgba(44,66,80,0) 0,#2c4250 100%);
background: linear-gradient(to bottom,rgba(44,66,80,0) 0,#2c4250 100%);
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#002c4250',endColorstr='#2c4250',GradientType=0);
width: 100%;
z-index: 2;
position: absolute;
bottom: 0;
  width: auto;
  right: 6px;
  left:6px;
}
.cell-b-title{
font-weight: bold;
font-size: 18px;
line-height: 18px;
}
.cell-sm-title{
font-weight: 400;
color: #ffffff;
font-size: 13px;
width: 100px;
text-overflow: ellipsis;
}
.cell-title a{
color: #ffffff;
text-decoration: none;
}
.avatar-xs-cover{
float: left;
}
.avatar-xs a{text-decoration: none;}
.avatar-xs{
height: 30px;
width: 30px;
margin-right: 3px;
border-radius: 50%;  
}
.badge-white{
background: #fff7f7;
color: #484848;
text-shadow: none;
}
#user-foot-card{
width: 100%;
white-space: nowrap;
margin-top: 10px;
overflow: hidden;
text-overflow: ellipsis;
}
.mini-date-card{
font-size: 10px;
font-weight: 400 !important;
margin-top: 0px;
float: left;
}
@media screen and (max-width: 600px) {
.cell-title{
  left:21px;
  right:21px;
}
}
.plus_menu{
  position: absolute;
margin-top: 5px;
margin-bottom: 0px;
right: 5px;
cursor: pointer;
color: #000;
padding: 0 10px;
background: #fff;
height: 15px;
line-height: 12px;
border-radius: 10px;
}
.plus_menu_cover{
  overflow: visible; position: relative; width: 80px;
}
.plus_menu_cover .dropdown-menu{
  right: 15px;
top: 17px;
}
/*ul#user-menu::before,*/
.dropdown-menu.dropdown-menu-right::before {
    content: '';
    display: block;
    position: absolute;
    bottom: -7px;
    margin-left: 30%;
    z-index: 5000;
    margin-left: calc(40% + 30px);
    border: 10px solid transparent;
    border-bottom-color: #fff;
    top: 0;
    height: 20px;
    margin-top: -20px;
    right: 8px;
}
.plus_menu_cover .dropdown-menu::before {
  
}
.like-top.active{ background: #10CF7F;}
.like-top a i{ line-height: 21px;}
.like-top a{ text-decoration: none;}
.like-top:hover{ background: #10CF7F;}
.like-top a i{ text-decoration: none;color: #ffffff;font-weight: 600}
.like-top{
  position: absolute;
top: 5px;
left: 15px;
padding: 5px;
background: rgba(0,0,0,.5);
border-radius: 50%;
height: 30px;
width: 30px;
text-align: center;
font-size: 15px;
}
.sec_card,.cell,.cell-cover,.cell-title{
  border-radius: 3px !important;
}
/*
.cell-title{
  display: none;
}
.cell:hover .cell-title{
  display: block;
}*/
 </style>
    @if(isset($biens) && $biens!=NULL && $biens->count()>0)
    @foreach($biens as $bien)
    @if($bien->user)
    <div class="col-xs-12  col-sm-6 col-md-4 col-lg-4 pad0  mbottom15 ">
        <div class="cell sec_cards">
          <span class="plus_menu_cover" >
            <div class="dropdown">
              <a data-toggle="dropdown"  class="plus_menu" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                <i class="icon-options"></i></a>
                <div class="dropdown-menu dropdown-menu-right">
                  <ul class="sec-nav">
                    <!--li class="sec-nav__item">
                    <a class="sec-nav__link" href="{{route('show_bien',$bien->id)}}">
                    <i class="icon icon-eye"></i>
                    <span class="sec-nav__link-text">Visualiser</span>
                  </a>
                </li-->
                <li class="sec-nav__item">
                  <a class="sec-nav__link" href="{{route('edit_bien',$bien->id)}}">
                    <i class="icon-note"></i>
                    <span class="sec-nav__link-text">Modifier</span>
                  </a>
                </li>
                @if($bien->userFavoris && $bien->userFavoris->count())

                <li class="sec-nav__item">
                  <a class="sec-nav__link" href="{{route('delete_bien_favori',$bien->id."?token=".sha1($bien->id))}}">
                    <i class="icon icon-star"></i>
                    <span class="sec-nav__link-text">{{__('Retirer de mes favoris')}}</span>
                  </a>
                </li>
                @else
                <li class="sec-nav__item">
                  <a class="sec-nav__link" href="{{route('add_bien_favori',$bien->id."?token=".sha1($bien->id))}}">
                    <i class="icon icon-star"></i>
                    <span class="sec-nav__link-text">{{__('Ajouter aux favoris')}}</span>
                  </a>
                </li>

                @endif
                <li class="sec-nav__item" id="trash{{$bien->id}}">
                  <a class="sec-nav__link" type="button" data-toggle="modal" data-target="#SecDeleteModal{{$bien->id}}" data-whatever="@mdo" href="#trash{{$bien->id}}">

                    <i class="icon-trash"></i>
                    <span class="sec-nav__link-text">Supprimer</span>
                  </a>
                </li>
                <li class="sec-nav__item">
                  <a class="sec-nav__link" href="{{route('export_bien',$bien->id)}}">
                    <i class="icon-doc"></i>
                    <span class="sec-nav__link-text">Exporter</span>
                  </a>
                </li>
              </ul>

            </div>
          </div>
        </span>
          <div class="cell-cover " >
            @if(Auth::user()->has_liked($bien))
            <div class="like-top active" title="{{__("Je n'aime plus")}}">
              <a  href="{{route('dislike',$bien->id."?token=".sha1($bien->id))}}"><i class="icon-heart"></i></a>
            </div>
            @else            
            <div class="like-top" title="{{__("J'aime")}}">
              <a  href="{{route('like',$bien->id."?token=".sha1($bien->id))}}"><i class="icon-heart"></i></a>
            </div>
            @endif
            
          @if($bien->media && file_exists($bien->media->mini))
          @php
          $media=$bien->media;
          @endphp
          <a href="{{route('show_bien',$bien->id)}}" class="show_product" ><img  class="media media-img  mc img-responsive" width="auto" data-turbolinks-track="reload" data-img-src="{{$bien->imageMini()}}"  src="{{asset($media->imageMini())}}" alt="{{$media->title}}">  </a>
         @else
         <a href="{{route('show_bien',$bien->id)}}" class="show_product" ><img  class="media media-img  mc img-responsive" width="auto" data-turbolinks-track="reload" data-img-src="{{$bien->imageMini()}}" src="{{asset($bien->imageMini())}}" alt="{{$bien->nom()}}">  </a>
        
          @endif
        
        <div class="cell-title">
         <a href="{{route('show_bien',$bien->id)}}" class="show_product">
           
          <h4 title="{{$bien->nom}}" class="cell-b-title">
            @if(Auth::user()->biensFavoris() && Auth::user()->biensFavoris()->where("bien_id",$bien->id)->count())
            <span class="title-svg-icon start-yellow" title="{{__('Ajouté aux favoris')}}">
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"><path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"/></svg>
            </span>
            @endif
            {{$bien->nom()}}</h4>
          </a> 
          <ul class="list-inline text-xs mbottom0">
          <li><i class="icon-directions"></i> {{$bien->mode_acquisition()}}</li>
          @if($bien->prix_achat)
          <li><i class="icon-credit-card"></i> {{$bien->prix_achat()}} &nbsp;</li>
          @endif
            <li><i class="icon-heart"></i> {{$bien->likes()->count()}} &nbsp;</li>
            
          </ul>
          <div id="user-foot-card" class="pull-left  text-sm ">
                  <span class="avatar-xs-cover">
                    <a title="{{$bien->user?$bien->user->fullname():''}}" class="pull-left"  href="{{$bien->user?$bien->user->profil():''}}"><img alt="{{$bien->user?$bien->user->fullname():''}}" src="{{asset($bien->user->avatar())}}"  class="avatar avatar-xs photo">
                    </a>
                  </span>
                  <a title="{{$bien->user?$bien->user->fullname():''}}"   href="{{$bien->user?$bien->user->profil():''}}">{{$bien->user?$bien->user->fullname():''}}</a>
                  <div class="mini-date-card">
                  @if($bien->visibilite=="PRIVE")
                  <i class="icon-lock"></i>                  
                  @else
                  <i class="icon-eye"></i>
                  @endif
                  
                  {{$bien->created_at->diffForHumans()}}
                  </div>
          </div>
        </div>
        
      </div>
      </div>
    </div>  
    @push('dialog')
    <div class="modal modal-danger fade" id="SecDeleteModal{{$bien->id}}" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
                  <div class="modal-dialog modal-info" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title bold" id="gridSystemModalLabel">
                        {{__("Supression d'élément")}} </h4>
                      </div>
                      <div class="modal-body">
                        <div class="row">
                          <div class="col-md-12">
                            <span class="text-info"></span>
                            <form class="delete" action="{{route('delete_bien',$bien->id)}}" method="POST">
                              <input type="hidden" name="_method" value="DELETE">
                              {{ csrf_field() }}
                              <input type="hidden" name="deleted" id="deleted" value="{{sha1($bien->id)}}">


                              <div class="pad10 bordererond5">

                                <h4 class="bold">{{__("Voulez-vous supprimer ce élément : ")}}{{$bien->nom}} ?</h4>
                                <ul class="list-inline-point text-muted text-sm">
                                  <li>

                                    <small class=""><i class="icon-user"></i>  <a href=""> {{$bien->user?$bien->user->name:"-"}} </a> &nbsp;&nbsp;</small>
                                    <li>
                                      <small class=""><i class="icon-calendar"></i> {{__('Ajouté,')}} {{$bien->created_at->diffForHumans()}}</small>
                                    </li>
                                    @if($bien->created_at!=$bien->updated_at)
                                    <li>
                                      <small class=""><i class="icon-pencil"></i> {{__('Dernière mise à jour,')}} {{$bien->updated_at->diffForHumans()}}</small>
                                    </li>
                                    @endif
                                  </ul>
                                </div>
                                <div class="text-muted text-sm mtop5">
                                  {{__("Si vous remarquez que ce bien contient des erreurs, vous pouvez cliquez sur le bouton modifier")}}
                                 
                                </div>

                              </div>
                            </div>

                          </div>
                          <div class="modal-footer">
                            
                            
                            <button type="button" class="btn btn-link btn-lg " data-dismiss="modal"><i class="icon-close"></i> {{__("Fermer")}}</button>
                            <a href="{{route('edit_bien',$bien->id)}}" class="btn btn-block bordertope btn-link btn-lg"><i class="icon-note"></i>  {{__("Modifier")}}</a>
                            <button type="submit" class="btn btn-link bordertope text-danger btn-lg rond0033"> <i class="icon-trash"></i> {{__("Supprimer")}}</button>

                          </form>
                        </div>
                      </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                  </div><!-- /.modal -->
                </div>
    @endpush 
    @endif 
    @endforeach


  @else
  <div class=" col-sm-12">
    <div class="card sec_card text-center ">
      <div class="card-body ">
        <div class="pad30"> <i class="{{isset($icone)?$icone:"icon-layers"}} huge1 display-5"></i></div>
        <div class="mb20 pad10">{{isset($msg)?$msg:__("Aucun élément disponible !")}}</div>
        <a href="{{route('create_bien')}}" class="btn btn-default btn-sm btn-blocks bold text-un mtop10"><i class="icon-plus "></i> {{__('Ajouter un élément')}}</a>
      </div>
    </div>


  </div>
  @endif
</div>
@push('script')
<script>
  /*$(document).on('turbolinks:load', function () {
    $("#listes .media-img").each(function() {
        var attrImage = $(this).attr('data-img-src');
        alert(attrImage);
        if(attrImageBG !== undefined) {
            $(this).attr('src', "OK");
            $(this).hide();
            
        }
    });
  })*/
  </script>
@endpush



