<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

#Authentification
//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});

//Route cache:
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});
Route::get('/view-cache', function() {
     $exitCode = Artisan::call('view:cache');
    return '<h1>Clear View cache</h1>';
});


Auth::routes();

#Auth::routes();
// Authentication routes...
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');//ok
Route::post('/login', 'Auth\LoginController@login')->name('postlogin');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');//ok
#Route::get('/confirm/{token}', 'Auth\LoginController@getConfirm')->name('confirm');
 
// Resend routes...
#Route::get('auth/resend', 'Auth\LoginController@getResend')->name('resend');
 
// Registration routes...
Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');//ok
Route::post('/register', 'Auth\RegisterController@register')->name('postlogin');//ok
 
// Password reset link request routes...
Route::get('password/email', 'Auth\ForgotPasswordController@showLinkReque
stForm')->name('password_email');//ok password forgotten 
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLink')->name('postpassword_email');//ok forgotpassword
 
// Password reset routes...
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password_reset');//ok
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('postpassword_resset');//ok

Route::get('test/{slug}',function($slug){
  if($slug=="db"){
    try {
      $db=DB::connection()->getPdo();
      echo "Connecion Réussie";
    } catch (\Exception $e) {
      die("Could not connect to the database.  Please check your configuration. error:" . $e->getMessage() );
    }
  }
  return "<hr>TEST :".$slug;
});
Route::name('language')->get('lang/{lang}', 'LangueController@ChangerLang');
Route::get('login/google', 'Auth\LoginController@redirectToProvider');
Route::get('login/google/callback', 'Auth\LoginController@handleProviderCallback');//user_profil

Route::get('login/google/callback', 'Auth\LoginController@handleProviderCallback')->name('callback');//user_profil

Route::get('/', 'Apps\PageController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/@{id}', 'HomeController@index')->name('user_sprofil');
Route::get('login/{provider}', 'SocialController@redirect');
Route::get('login/{provider}/callback','SocialController@Callback');

#Apps

Route::get('/dashboard', 'Apps\AppsController@index')->name('dashboard');
Route::get('/app/{fichier}', 'Apps\AppsController@DetailsBien')->name('DetailsBien');
#Bien de chaque utilisateur
#API
Route::get('/api/v1/getUser', 'Apps\BienController@getUser')->name('apiv1GetUser');
Route::get('/help', 'Apps\PageController@help')->name('help_apps');


Route::get('/new', 'Apps\BienController@create')->name('create_bien');
Route::post('/new', 'Apps\BienController@store')->name('store_bien');
Route::get('/bien/edit/{id}', 'Apps\BienController@edit')->name('edit_bien');
Route::get('/bien/{id}', 'Apps\BienController@show')->name('show_bien');
Route::get('/bien/trash/{id}', 'Apps\BienController@trash')->name('trash_bien');
Route::delete('/bien/delete/{id}', 'Apps\BienController@delete')->name('delete_bien');
Route::patch('/bien/edit/{id}', 'Apps\BienController@update')->name('update_bien');
Route::get('/bien/favori/{id}', 'Apps\BienController@add_bien_favori')->name('add_bien_favori');//delete_bien_favori
Route::get('/bien/unfavori/{id}', 'Apps\BienController@delete_bien_favori')->name('delete_bien_favori');

Route::get("/favoris", 'Apps\AppsController@bien_favoris')->name('bien_favoris');
Route::get("/classement", 'Apps\AppsController@classement')->name('classement');
Route::get("/export/favoris", 'Apps\AppsController@export_favoris')->name('export_favoris');
Route::get("/export/bien/all", 'Apps\AppsController@export_all')->name('export_all_biens');
Route::get('/export/bien/{id}', 'Apps\AppsController@export_bien')->name('export_bien');//
Route::get('/export/bien/mode/{slug}', 'Apps\AppsController@export_mode')->name('export_mode');//
Route::get('/export/bien/categorie/{id}', 'Apps\AppsController@export_categorie')->name('export_categorie');//

Route::get('/notifications', 'Apps\ProfilController@notification')->name('notification');
Route::get('/profil/@{pseudo}', 'Apps\ProfilController@profil')->name('profil');
Route::get('/profil/@{pseudo}/edition', 'Apps\ProfilController@edit_profil')->name('edit_profil');
Route::get('/account/settings', 'Apps\ProfilController@profil_settings')->name('profil_settings');
Route::get('/help/center', 'Apps\ProfilController@help_center')->name('help_center');
Route::get('/language/settings', 'Apps\ProfilController@lang_settings')->name('lang_settings');
Route::get('/account/security', 'Apps\ProfilController@account_security')->name('account_security');
Route::get('/account/business', 'Apps\ProfilController@account_business')->name('account_business');
Route::get('/redirect_url', 'Apps\PageController@redirect_url')->name('redirect_url');

Route::get('/bien/classement/mode/{slug}', 'Apps\AppsController@classement_mode')->name('classement_mode');//classement_mode
Route::get('/bien/classement/categorie/{id}', 'Apps\AppsController@classement_categorie')->name('classement_categorie');//classement_mode

Route::get('/about', 'Apps\PageController@about')->name('about');
Route::get('/terms', 'Apps\PageController@condition')->name('condition');
Route::get('/privacy', 'Apps\PageController@reglement')->name('reglement');
Route::get('/cookies', 'Apps\PageController@cookies')->name('cookies');
Route::get('/soumission', 'Apps\PageController@soumission')->name('soumission');

#Route::update('/new/{id}', 'Apps\BienController@update')->name('update_bien');
##########################################################################
#    VISUALISATION D'UN BIEN
##########################################################################
Route::get('/bien/{id}/activity', 'Apps\BienController@show_activite')->name('show_activite_bien');
Route::get('/bien/{id}/movement', 'Apps\BienController@show_mouvement')->name('show_mouvement_bien');
Route::get('/bien/{id}/security', 'Apps\BienController@show_securite')->name('show_securite_bien');
Route::get('/bien/{id}/accessory', 'Apps\BienController@show_accessoire')->name('show_accessoire_bien');
Route::get('/bien/{id}/statistique', 'Apps\BienController@show_statistique')->name('show_statistique_bien');



Route::get('/test',function(){
  return view('web.test2');
});
Route::resource('admin/posts', 'Admin\\PostsController');
Route::resource('admin/type-transfert', 'Admin\\TypeTransfertController');
Route::resource('admin/posts', 'Admin\\PostsController');
Route::resource('admin/modeacquisition', 'Admin\\ModeacquisitionController');
Route::resource('admin/typebien', 'Admin\\TypebienController');
Route::resource('admin/marque', 'Admin\\MarqueController');
Route::resource('admin/marque', 'Admin\\MarqueController');
Route::resource('admin/modele', 'Admin\\ModeleController');
Route::resource('admin/media', 'Admin\\MediaController');
Route::resource('favori', 'FavoriController');
Route::resource('admin/devise', 'Admin\\DeviseController');

Route::get('nom', 'Apps\PageController@nom');

Route::get('rcours/{slug}', 'Apps\PageController@rcours');
Route::get('icours/{slug}', 'Apps\PageController@icours');