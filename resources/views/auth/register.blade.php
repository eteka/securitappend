@extends("layouts.app")

@section("title")
{{__("Inscription")}} - @parent
@endsection
@section('body')
<body id="app-layout" class="bgsilver">
@show
@section("content")


  @section("nav")
  @endsection

  @section("footer")
  </body>
  </html>
  @endsection
  <style>
    .maincover{
    position: relative;
    display: flex;
    min-height: 100vh;

    }
    .mainpart1{
        display: flex;
    justify-content: flex-end;
    width: 50%;
    padding: 0;
    background: #000;
    background: url('{{asset("./assets/statics/app/pages/home_story.jpg")}}') no-repeat 50% 40% #244157;
      background-size: cover;
    }
    .mainpart1_backdrop{
      background: rgba(0, 0, 0, 0.35);
    }
    .mainpart2{
        flex-grow: 1;
    display: flex;
    justify-content: flex-start;
    padding: 0;
    background: #fff;
    box-shadow: 0 0 5px rgba(0,0,0,.01), -5px 0 30px -8px rgba(0,0,0,.04);
    }

    .register-logo-right{
        margin:15px 0px 10px;

    }
    .hspecial-infos-register h2{
      font-size: 28px;
    }
    .rinfo-links a{
      margin-left:  0px;
      color: #eeeeee;
    }
      .rinfo-links a:hover{color: white;background-color:}
    .register-logo-right img{height:40px;}
    .register-card-body{
        padding:0 35px;
    }
    .sec_icon svg{
        height:20px;
        color:#ffffff;
    }
    .sbtn-success{
        color: #fff;
        background-color: #10CF7F;
        border-color: #34d18f;
        padding:12px;
    }
    .sbtn-success:hover{
        background:#2ac584;
       // opacity:.7
    }
      /************************************************************/
    .hspecial-info-register{
        padding: 40px 10%;
    }
      .hspecial-info-register>h1{
          font-size: 30vh;
          font-weight: bolder;

      }
    .img-register-cover{
        padding: 15px 15%;
        margin: auto;
        min-height: 400px;
    }
    .hspecial-infos-register{
        margin: 0 7%;
        color: #505676;
        color: #f7f7f7;
    }
    .hspecial-infos-register h1{

    }
    .img-register-cover img{
        margin: auto;
        max-height: 450px;
    }


/***************************************************************************/
@supports (-webkit-appearance: none) or (-moz-appearance: none) {
     input[type='checkbox'], input[type='radio'] {
         --active: #275efe;
         --active-inner: #fff;
         --focus: 2px rgba(39, 94, 254, .3);
         --border: #bbc1e1;
         --border-hover: #275efe;
         --background: #fff;
         --disabled: #f6f8ff;
         --disabled-inner: #e1e6f9;
         -webkit-appearance: none;
         -moz-appearance: none;
         height: 21px;
         outline: none;
         display: inline-block;
         vertical-align: top;
         position: relative;
         margin: 0 5px 0;
         cursor: pointer;
         border: 1px solid var(--bc, var(--border));
         background: var(--b, var(--background));
         transition: background 0.3s, border-color 0.3s, box-shadow 0.2s;
    }
     input[type='checkbox']:after, input[type='radio']:after {
         content: '';
         display: block;
         left: 0;
         top: 0;
         position: absolute;
         transition: transform var(--d-t, 0.3s) var(--d-t-e, ease), opacity var(--d-o, 0.2s);
    }
     input[type='checkbox']:checked, input[type='radio']:checked {
         --b: var(--active);
         --bc: var(--active);
         --d-o: 0.3s;
         --d-t: 0.6s;
         --d-t-e: cubic-bezier(0.2, 0.85, 0.32, 1.2);
    }
     input[type='checkbox']:disabled, input[type='radio']:disabled {
         --b: var(--disabled);
         cursor: not-allowed;
         opacity: 0.9;
    }
     input[type='checkbox']:disabled:checked, input[type='radio']:disabled:checked {
         --b: var(--disabled-inner);
         --bc: var(--border);
    }
     input[type='checkbox']:disabled + label, input[type='radio']:disabled + label {
         cursor: not-allowed;
    }
     input[type='checkbox']:hover:not(:checked):not(:disabled), input[type='radio']:hover:not(:checked):not(:disabled) {
         --bc: var(--border-hover);
    }
     input[type='checkbox']:focus, input[type='radio']:focus {
         box-shadow: 0 0 0 var(--focus);
    }
     input[type='checkbox']:not(.switch), input[type='radio']:not(.switch) {
         width: 21px;
    }
     input[type='checkbox']:not(.switch):after, input[type='radio']:not(.switch):after {
         opacity: var(--o, 0);
    }
     input[type='checkbox']:not(.switch):checked, input[type='radio']:not(.switch):checked {
         --o: 1;
    }
     input[type='checkbox'] + label, input[type='radio'] + label {
         font-size: 14px;
         line-height: 21px;
         display: inline-block;
         vertical-align: top;
         cursor: pointer;
         margin-left: 4px;
    }
     input[type='checkbox']:not(.switch) {
         border-radius: 7px;
    }
     input[type='checkbox']:not(.switch):after {
         width: 5px;
         height: 9px;
         border: 2px solid var(--active-inner);
         border-top: 0;
         border-left: 0;
         left: 7px;
         top: 4px;
         transform: rotate(var(--r, 20deg));
    }
     input[type='checkbox']:not(.switch):checked {
         --r: 43deg;
    }
     input[type='checkbox'].switch {
         width: 38px;
         border-radius: 11px;
    }
     input[type='checkbox'].switch:after {
         left: 2px;
         top: 2px;
         border-radius: 50%;
         width: 15px;
         height: 15px;
         background: var(--ab, var(--border));
         transform: translateX(var(--x, 0));
    }
     input[type='checkbox'].switch:checked {
         --ab: var(--active-inner);
         --x: 17px;
    }
     input[type='checkbox'].switch:disabled:not(:checked):after {
         opacity: 0.6;
    }
     input[type='radio'] {
         border-radius: 50%;
    }
     input[type='radio']:after {
         width: 19px;
         height: 19px;
         border-radius: 50%;
         background: var(--active-inner);
         opacity: 0;
         transform: scale(var(--s, 0.7));
    }
     input[type='radio']:checked {
         --s: 0.5;
    }
}
 ul {
     margin: 12px;
     padding: 0;
     list-style: none;
     width: 100%;
     max-width: 320px;
}
 ul li {
     margin: 16px 0;
     position: relative;
}
 html {
     box-sizing: border-box;
}
 * {
     box-sizing: inherit;
}
 *:before, *:after {
     box-sizing: inherit;
}
/*
 body {
     min-height: 100vh;
     font-family: 'Inter', Arial, sans-serif;
     color: #8a91b4;
     display: flex;
     justify-content: center;
     align-items: center;
     background: #f6f8ff;
}*/
 @media (max-width: 800px) {
     body {
         flex-direction: column;
    }
}

/***************************************************************************/
  </style>

<div class="containers">
    <div class="rows maincover">
    <div class="col-md-6 mainpart1 hidden-sms hidden-xs">
        <div class="container mainpart1_backdrop ">
            <div class="row">
                <div class="col-md-12">
                <!--h2 class="huge1 bold"><span class="sec_color1">Sécurit</span><span class="sec_color0">Apen</span></h2-->

                </div>
                <div class="col-md-12">
                    <div class="img-register-cover">
                        <!--img src="{{asset("assets/statics/app/register-home-guard.png")}}" height="450px" class="img-responsive" alt="" !-->
                    </div>
                    <div class="hspecial-infos-register">

                    <h1 class="bold">{{__("Commencez")}}
                    {{__("le voyage de la sécurisation de vos biens !")}}</h1>
                    <div class="text-md bold">
                        <p class="text-sm">{!!__("auth.register-info")!!}<p>
                          <div class="rinfo-links">
                            <a class="btn btn-md borderc m0" href="{{url("about")}}">{{__("En savoir plus")}} <i class="icon-arrow-right"></i></a>
                        </div>
                    </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

        <div class="col-md-6 mainpart2">
            <div class=" container__">
                <div class="row__">
                    <div class="col-xs-12  col-md-10 col-lg-8">
                    <div id="register_card">
                        <div class="rows ">

                            <div class="row">
                                <div class="col-md-12 text-center ">
                                <div class="register-logo-right ">

                                    <a class="" href="{{ url("/") }}" >
                                        <img class="profile-imgs" src="{{asset("assets/app/logo.png")}}"  alt="SecuritApen">
                                    </a>

                                </div>
                                </div>
                            </div>
                        </div>
                        <div class=" text-center mbottom20 page-header">
                          <h2 class="pad0  m0">{{__("Inscrivez-vous !")}}"</h2>
                          <small class="text-muted text-xs">{{__("Parceque vos biens méritent d'être protégés")}}</small>
                        </div>
                        <div class="register-card-body">
                            <form method="POST" action="{{ route("register") }}">
                                @csrf

                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <div><label for="nom">{{ __("Nom") }}</label></div>
                                        <div class="sec_formicon sec-input-icon">
                                        <svg version="1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 0C5.383 0 0 5.383 0 12c0 3.18 1.232 6.177 3.47 8.438A11.913 11.913 0 0 0 12 24c3.234 0 6.268-1.269 8.542-3.574A11.924 11.924 0 0 0 24 12c0-6.617-5.383-12-12-12zM4.655 20.17c1.029-.563 2.533-1.111 4.002-1.644.514-.186 1.027-.372 1.52-.559a.5.5 0 0 0 .323-.468V15a.511.511 0 0 0-.335-.472C10.118 14.511 9 14.072 9 12a.48.48 0 0 0-.482-.494c-.047-.032-.138-.218-.138-.506s.091-.474.12-.5A.5.5 0 0 0 9 10c0-.125-.037-.256-.118-.544-.147-.523-.537-1.913-.194-2.366.038-.051.245-.194.714-.1.26.055.518-.11.583-.369.163-.649 1.326-1.121 2.765-1.121s2.602.472 2.765 1.121c.229.917-.178 2.188-.373 2.799-.098.305-.142.443-.142.58a.48.48 0 0 0 .482.494c.046.032.137.218.137.506s-.091.474-.12.5a.5.5 0 0 0-.5.5c0 2.072-1.118 2.511-1.158 2.526A.5.5 0 0 0 13.5 15v2.5a.5.5 0 0 0 .324.468c.543.204 1.086.399 1.614.591 1.51.546 2.943 1.067 3.919 1.599A10.901 10.901 0 0 1 12 23a10.908 10.908 0 0 1-7.345-2.83zm15.453-.739c-1.047-.625-2.583-1.181-4.33-1.812l-1.278-.466v-1.848c.501-.309 1.384-1.108 1.49-2.936.386-.226.63-.727.63-1.37 0-.579-.198-1.043-.52-1.294.243-.757.681-2.144.385-3.327C16.138 4.992 14.256 4.5 12.75 4.5c-1.342 0-2.982.391-3.569 1.456-.707-.042-1.095.273-1.29.53-.635.838-.216 2.368.02 3.211-.329.249-.531.718-.531 1.303 0 .643.244 1.144.63 1.37.106 1.828.989 2.627 1.49 2.936v1.848l-1.184.433c-1.624.589-3.299 1.196-4.413 1.858A10.928 10.928 0 0 1 1 12C1 5.935 5.935 1 12 1s11 4.935 11 11c0 2.774-1.024 5.397-2.892 7.431z"></path></svg>
                                        <input id="nom" autocomplete="OFF" type="text" placeholder="{{__("Nom de famille")}}" class="form-control form-control-type-1  @error("nom") is-invalid @enderror" name="nom" value="{{ old("nom") }}" required  autofocus>

                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div>
                                            <label for="name">{{ __("Prénom") }}</label></div>
                                        <div class="sec_formicon sec-input-icon">
                                            <svg version="1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 0C5.383 0 0 5.383 0 12c0 3.18 1.232 6.177 3.47 8.438A11.913 11.913 0 0 0 12 24c3.234 0 6.268-1.269 8.542-3.574A11.924 11.924 0 0 0 24 12c0-6.617-5.383-12-12-12zM4.655 20.17c1.029-.563 2.533-1.111 4.002-1.644.514-.186 1.027-.372 1.52-.559a.5.5 0 0 0 .323-.468V15a.511.511 0 0 0-.335-.472C10.118 14.511 9 14.072 9 12a.48.48 0 0 0-.482-.494c-.047-.032-.138-.218-.138-.506s.091-.474.12-.5A.5.5 0 0 0 9 10c0-.125-.037-.256-.118-.544-.147-.523-.537-1.913-.194-2.366.038-.051.245-.194.714-.1.26.055.518-.11.583-.369.163-.649 1.326-1.121 2.765-1.121s2.602.472 2.765 1.121c.229.917-.178 2.188-.373 2.799-.098.305-.142.443-.142.58a.48.48 0 0 0 .482.494c.046.032.137.218.137.506s-.091.474-.12.5a.5.5 0 0 0-.5.5c0 2.072-1.118 2.511-1.158 2.526A.5.5 0 0 0 13.5 15v2.5a.5.5 0 0 0 .324.468c.543.204 1.086.399 1.614.591 1.51.546 2.943 1.067 3.919 1.599A10.901 10.901 0 0 1 12 23a10.908 10.908 0 0 1-7.345-2.83zm15.453-.739c-1.047-.625-2.583-1.181-4.33-1.812l-1.278-.466v-1.848c.501-.309 1.384-1.108 1.49-2.936.386-.226.63-.727.63-1.37 0-.579-.198-1.043-.52-1.294.243-.757.681-2.144.385-3.327C16.138 4.992 14.256 4.5 12.75 4.5c-1.342 0-2.982.391-3.569 1.456-.707-.042-1.095.273-1.29.53-.635.838-.216 2.368.02 3.211-.329.249-.531.718-.531 1.303 0 .643.244 1.144.63 1.37.106 1.828.989 2.627 1.49 2.936v1.848l-1.184.433c-1.624.589-3.299 1.196-4.413 1.858A10.928 10.928 0 0 1 1 12C1 5.935 5.935 1 12 1s11 4.935 11 11c0 2.774-1.024 5.397-2.892 7.431z"></path></svg>
                                            <input id="name" autocomplete="OFF" type="text" placeholder="{{__("Votre prénom")}}" class="form-control form-control-type-1  @error("prenom") is-invalid @enderror" name="prenom" value="{{ old("prenom") }}" required autofocus>

                                        </div>
                                        </div>
                                        <div class="">
                                       <div class="col-sm-12">
                                         @error("nom")
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                       @error("prenom")
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                       </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <div><label for="email" >{{ __("E-Mail Address") }}</label></div>
                                        <div class="sec_formicon sec-input-icon">
                                        <svg version="1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                        <path d="M21 4H3c-1.103 0-2 .897-2 2v11c0 1.103.897 2 2 2h18c1.103 0 2-.897 2-2V6c0-1.103-.897-2-2-2zm0 1l.159.032L12 12.36 2.841 5.032 3 5h18zm1 12c0 .551-.449 1-1 1H3c-.551 0-1-.449-1-1V6c0-.11.03-.21.063-.309l9.625 7.7a.504.504 0 0 0 .624 0l9.625-7.7A.966.966 0 0 1 22 6v11z"></path>
                                        </svg>
                                        <input autocomplete="off" id="email"  placeholder="{{__("Courier électronique")}}" type="email" class="form-control form-control-type-1 @error("email") is-invalid @enderror" name="email" value="{{ old("email") }}" required autocomplete="email" class="form-control form-control-type-1 {{ $errors->has("email") ? " is-invalid" : "" }}" autofocus>
                                        </div>
                                        @error("email")
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">

                                    <div class="col-md-6">
                                      <div class="">
                                      <label for="password"  class=" col-form-label text-md-right">{{ __("Password") }}</label>
                                      </div>
                                        <div class="sec_formicon sec-input-icon sec-input-icon-lock">
                                            <svg version="1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                                <path d="M12 14c-.552 0-1 .449-1 1 0 .365.207.672.5.846V18.5a.5.5 0 0 0 1 0v-2.654A.987.987 0 0 0 13 15c0-.551-.448-1-1-1zm8.5-5H18V6c0-3.309-2.691-6-6-6S6 2.691 6 6v3H3.5a.5.5 0 0 0-.5.5v14a.5.5 0 0 0 .5.5h17a.5.5 0 0 0 .5-.5v-14a.5.5 0 0 0-.5-.5zM7 6c0-2.757 2.243-5 5-5s5 2.243 5 5v3H7V6zm13 17H4V10h16v13z"></path>
                                            </svg>
                                            <input autocomplete="OFF" type="password" class="form-control form-control-type-1 {{ $errors->has("password") ? " is-invalid" : "" }}" id="lpassword" name="password" title="{{__("Mot de passe")}}" placeholder="{{__("Mot de passe")}}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="mtop-xs-10">
                                      <label for="password" class=" col-form-label text-md-right">{{ __("Confirmation") }}</label>
                                      </div>
                                        <div class="sec_formicon sec-input-icon sec-input-icon-lock">
                                            <svg version="1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                                <path d="M12 14c-.552 0-1 .449-1 1 0 .365.207.672.5.846V18.5a.5.5 0 0 0 1 0v-2.654A.987.987 0 0 0 13 15c0-.551-.448-1-1-1zm8.5-5H18V6c0-3.309-2.691-6-6-6S6 2.691 6 6v3H3.5a.5.5 0 0 0-.5.5v14a.5.5 0 0 0 .5.5h17a.5.5 0 0 0 .5-.5v-14a.5.5 0 0 0-.5-.5zM7 6c0-2.757 2.243-5 5-5s5 2.243 5 5v3H7V6zm13 17H4V10h16v13z"></path>
                                            </svg>
                                            <input type="password" id="password-confirm" class="form-control form-control-type-1 form-control @error("password") is-invalid @enderror" id="lpassword" name="password_confirmation" title="{{__("Répéter le mot de passe")}}" placeholder="{{__("Confirmation")}}" required>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        @error("password")
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong><br>
                                                <div class="text-xs bold">{{__('auth.password-info')}}</div>
                                            </span>
                                        @else
                                        <div class="text-xs text-muted">{{__('auth.password-info')}}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12 ">
                                        <label for="genre" class="genre">{{__("Sexe")}}</label>
                                    </div>
                                    <div class="col-md-12 mbottom15">
                                        <label for="sexe1">   {{Form::radio('sexe','H',null,['required'=>"required","id"=>"sexe1"])}} {{__("Masculin")}}</label>
                                        
                                        <label for="sexe2">   {{Form::radio('sexe','F',null,['required'=>"required","id"=>"sexe2"])}} {{__("Féminin")}}</label>
    
                                    </div>
                                    <div class="col-md-12">
                                        @error("sexe")
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                </div>
                                <div class="form-group row mb-5 mt10">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-success sbtn-success btn-block btn-md btn-icon text-white">
                                            <span class="sec_icon">
                                                <!--svg class="svg-icon" viewBox="0 0 20 20">
							<path d="M12.522,10.4l-3.559,3.562c-0.172,0.173-0.451,0.176-0.625,0c-0.173-0.173-0.173-0.451,0-0.624l3.248-3.25L8.161,6.662c-0.173-0.173-0.173-0.452,0-0.624c0.172-0.175,0.451-0.175,0.624,0l3.738,3.736C12.695,9.947,12.695,10.228,12.522,10.4 M18.406,10c0,4.644-3.764,8.406-8.406,8.406c-4.644,0-8.406-3.763-8.406-8.406S5.356,1.594,10,1.594C14.643,1.594,18.406,5.356,18.406,10M17.521,10c0-4.148-3.374-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.147,3.374,7.521,7.521,7.521C14.147,17.521,17.521,14.147,17.521,10"></path>
						</svg--></span>
                                         {{ __("Créer mon compte") }}
                                        </button>
                                    </div>
                                </div>
                                <div class="form-group row text-center text-sm">
                                <div class="col-md-12">
                                    {!!__('auth.conditions',['url1'=>route("condition"),'url2'=>route("reglement")])!!}
                                <hr class="mtop10">
                                {{__("Si vous avez déjà un compte,")}}  <a class="bold" href="{{url("login")}}">{{__("Connectez-vous !")}}</a>
                                <div class="text-center-xs text-center ">
                                    <ul class="list-inline pad5_0  m0 mauto    position-absolute_  text-xs">
                                        <li><a href="{{url("lang/fr")}}?{{http_build_query(["c_url"=>url()->full()])}}">{{__("FR")}}</a></li>
                                        <li><a href="{{url("lang/en")}}?{{http_build_query(["c_url"=>url()->full()])}}">{{__("EN")}}</a></li>
                                    </ul>
                                </div>

                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
