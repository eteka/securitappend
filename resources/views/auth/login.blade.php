@extends('layouts.app')

@section('title')
{{__('Connexion')}} -
@parent
@endsection

@section('content')
  @section('body')
  <body class="bgsilver">
  @endsection

  @section('nav')
  @endsection

  @section('footer')
  </body>
  </html>
  @endsection
  <style>


.btn-primary{
  background: #2196f3
}

@media only screen and (max-width: 767px) {
  #lpassword,#btlogin {
    border-radius:5px !important;
    border:1px solid #ccd6db;
  }
  #btlogin{
  background:#244157;
  /*border-color:#244157;*/
  color:#FFFFFF;
  /*background: #2196f3;
    border: 1px solid #2196f4;*/
}
}
@media only screen and (min-width: 768px) {
      #lpassword{
      border-radius:5px 0 0 5px !important;
      border-right-width:0px;
      }
      #btlogin{
      border-radius:0 5px 5px 0;
      /*background: #2196f3;
        border: 1px solid #2196f4;*/
    }
  
}

  </style>

      <meta name="google-signin-scope" content="profile email">
          <meta name="google-signin-client_id" content="448987361359-q83b6gs3csv118gksgt5d8019l6vdril.apps.googleusercontent.com">
          <script src="https://apis.google.com/js/platform.js" async defer></script>
      <div class=" minh">
       
      <div class="container ">
          <div class="row justify-content-center login-cover">
              <div class="col-md-12 text-center">
              <a class="nav-logo-middle login-margin" href="{{ url('/') }}" >
                              <img src="{{ asset('assets/app/logo.png')}}"  alt="Logo">
              </a>
              </div>
              <div class="col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 mtop20  ">

                  <div class="login-card border_sec mtop-xs-10">
                    <div class=" text-center mtop0 pad10_0 mbottom10 page-header">
                      <h2 class="pad0  m0">{{__("Connectez-vous !")}}</h2>
                      <small class="text-muted text-xs">{{__("Parceque vos biens méritent d'être protégés")}}</small>
                    </div>
                   <div class="account-wall">

                      @if ($errors->has('email'))
                      <div class="alert alert-danger help-block_ text-danger text-center">
                      <strong>{{ $errors->first('email') }}</strong>
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                      </div>
                      @endif
                        <!--img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
                            alt=""-->
                        <form class="form-signin" role="form" method="POST" action="{{ route('login') }}">
                                    {{ csrf_field() }}
                         <div class="form-group pad0 ">
                         <div class="row">
                            <div class="col-sm-12 pad0 ">
                              <div class="sec_formicon sec-input-icon">
                              <svg version="1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
<path d="M21 4H3c-1.103 0-2 .897-2 2v11c0 1.103.897 2 2 2h18c1.103 0 2-.897 2-2V6c0-1.103-.897-2-2-2zm0 1l.159.032L12 12.36 2.841 5.032 3 5h18zm1 12c0 .551-.449 1-1 1H3c-.551 0-1-.449-1-1V6c0-.11.03-.21.063-.309l9.625 7.7a.504.504 0 0 0 .624 0l9.625-7.7A.966.966 0 0 1 22 6v11z"></path>
</svg>
                                <input type="text" @if($errors->has('email')) data-placement="left" data-toggle="popover" data-trigger="focus" title="Dismissible popover" data-content="And here's some amazing content. It's very engaging. Right?" @endif class="form-control form-control-type-1 {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{Old('email')}}" autocomplete='OFF' placeholder="{{__('Adresse email')}}" title="{{__('Adresse email ou pseudo')}}" name="email" required autofocus>

                              </div>
                            </div>
                            </div>
                         </div>
                         <div class="form-group">
                         <div class="row">
                          <div class="col-sm-7 pad0">
                          <div class="sec_formicon sec-input-icon sec-input-icon-lock">
                          <svg version="1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
<path d="M12 14c-.552 0-1 .449-1 1 0 .365.207.672.5.846V18.5a.5.5 0 0 0 1 0v-2.654A.987.987 0 0 0 13 15c0-.551-.448-1-1-1zm8.5-5H18V6c0-3.309-2.691-6-6-6S6 2.691 6 6v3H3.5a.5.5 0 0 0-.5.5v14a.5.5 0 0 0 .5.5h17a.5.5 0 0 0 .5-.5v-14a.5.5 0 0 0-.5-.5zM7 6c0-2.757 2.243-5 5-5s5 2.243 5 5v3H7V6zm13 17H4V10h16v13z"></path>
</svg>
                          <input type="password" class="form-control form-control-type-1 {{ $errors->has('password') ? ' is-invalid' : '' }}" id="lpassword" name="password" title="{{__('Mot de passe')}}" placeholder="{{__('Mot de passe')}}" required>
                        </div>
                          </div>
                          <div class="col-sm-5 pad0">
                          <button class="btn pad10 btn-primary   btn-block" id="btlogin" type="submit">
                              {{__("Connexion")}}</button>
                          </div>
                         </div>
                           </div>

                       <div class="row text-sm">
                       <div class="col-sm-6 pad0 text-center-xs">
                        <label>
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Rester connecté') }}
                        </label>
                       </div>
                       <div class="col-sm-6 pad0 text-center-xs mtop5">
                         <a class="" href="{{ route('password.request') }}">
                                    {{ __('Mot de passe oublié ?') }}
                          </a>
                       </div>
                       </div>


                </form>
            </div>
            <div class="lregister" >
            <div class=" text-center">
              <a href="{{url('register')}}" class="text-center btn btn-success new-account">{{__('Nouveau, créer un compte')}} </a>
                <ul class="list-inline mtop10 text-xs">
                    <li><a href="{{url('lang/fr')}}?{{http_build_query(['c_url'=>url()->full()])}}">{{__('FR')}}</a></li>
                    <li><a href="{{url('lang/en')}}?{{http_build_query(['c_url'=>url()->full()])}}">{{__('EN')}}</a></li>
                </ul>
            </div>
          </div>

              <div id="social_login" class="hidden">

                  <hr>
                      <h5 class="text-center bold">Ou se connecter avec:</h5>

                      <div class="card-body">
                        <div class="row">
                              <div class="col-md-6 mt30 mtop10">
                              <a class="btn google d-flex btn-default btn-block btn-auth " id="oauth-login-google_oauth2" rel="nofollow" ><img alt="Google" title="Sign in with Google"  src="https://assets.gitlab-static.net/assets/auth_buttons/google_64-9ab7462cd2115e11f80171018d8c39bd493fc375e83202fbb6d37a487ad01908.png">
                              <span> Google</span>
                              </a>
                            </div>
                          <div class="col-md-6 mtop10">
                              <a  scope="public_profile,email"
                                onlogin="checkLoginState();" class="btn facebook no-border d-flex btn-default btn-block btn-auth " id="oauth-login-google_oauth2" rel="nofollow" >
                                  <i class="fa fa-facebook fa-fw"></i>

                                  <span> Facebook</span>
                              </a>
                              <!-- fb:login-button
                                scope="public_profile,email"
                                onlogin="checkLoginState();">
                              </fb:login-button-->
                              <script>
                              window.fbAsyncInit = function() {
                                FB.init({
                                  appId            : '231458358024679',
                                  autoLogAppEvents : true,
                                  xfbml            : true,
                                  version          : 'v6.0'
                                });
                              };
                            </script>

                            <script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>
                            </div>
                          </div>
                          <div class="row">

                            <div class="col-md-6 mtop10">
                              <a class="btn no-border twitter d-flex btn-default btn-block btn-auth " id="oauth-login-google_oauth2" rel="nofollow" >
                              <i class="fa fa-twitter fa-fw"></i>
                              <span> Twitter</span>
                              </a>
                            </div>
                            <div class="col-md-6 mtop10">
                              <a class="btn linkedin no-border d-flex btn-default btn-block btn-auth " id="oauth-login-google_oauth2" rel="nofollow" >
                              <i class="fa fa-linkedin fa-fw"></i>
                                <span> LinkedIn</span>
                              </a>
                            </div>
                          </div>
                          <div class="row">

                            <div class="col-md-6 mtop10">
                              <a class="btn github d-flex btn-default btn-block btn-auth " id="oauth-login-google_oauth2" rel="nofollow" >
                              <i class="fa fa-github fa-fw"></i>
                                <span> GitHub</span>
                              </a>
                            </div>
                            <div class="col-md-6 mtop10">
                              <a class="btn no-border pinterest d-flex btn-default btn-block btn-auth " id="oauth-login-google_oauth2" rel="nofollow" >
                              <i class="fa fa-pinterest fa-fw"></i>
                              <span> Pinterest</span>
                              </a>
                            </div>

                        </div>
                        <div class="row">
                      <div class="col-md-12 mtop10 ">
                      <hr>
            </div>

                        </div>
                    </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
          @include('layouts.partials.footer-light')
  </div>
@endsection
