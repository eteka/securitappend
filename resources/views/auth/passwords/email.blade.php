@extends('layouts.app')
@section('title')
    {{__('auth.reinitialisation_header')}} -
@parent
@endsection

@section('footer_')
    @include('layouts.partials.footer-light')
@endsection
@section('content')
<div class="container minh">
    <div class="row justify-content-center mtop50">
        <div class="col-md-6 col-md-offset-3">
            <div class="card sec_card border_secs mtop50">
                

                <div class="card-body">
                    <b>{{__("auth.reinitialisation_header")}}</b>
                <p class="text-xs text-muted ">
                    {!!__("auth.reinitialisation_description",['email'=>''])!!}
                  </p>
                <hr>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-5 col-form-label text-md-right">{{ __('Adresse email ou pseudo') }}:</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mbottom30">
                            <div class="col-md-7  col-xs-12 col-md-offset-4 text-right">
                                <button type="submit" class="btn btn-xs-block btn-success">
                                    {{ __('Envoyer le lien de réinitialisation') }}
                                </button>
                            </div>
                        </div>

                    </form>
                        <hr>
                        <div class="row">
                            <div class="col-md-3">
                                <ul class="list-inline mtop10 text-xs-center text-center-xs text-xs">
                                    <li><a href="{{url('lang/fr')}}?{{http_build_query(['c_url'=>url()->full()])}}">{{__('FR')}}</a></li>
                                    <li><a href="{{url('lang/en')}}?{{http_build_query(['c_url'=>url()->full()])}}">{{__('EN')}}</a></li>
                                </ul>
                            </div>
                            <div class="col-md-9 col-xs-12 text-xs-center text-center-xs">
                                <ul class="list-inline pad10_0 text-xs">
                                    <li>&raquo; <a class="" href="{{route('login')}}">{{__('Se connecter')}}</a> </li>
                                    <li>ou   &raquo;   <a   href="{{route('register')}}">{{__('Créer un compte')}}</a></li>
                                </ul>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
