@extends('layouts.app')
@section('title',__('Edition  Models\ModeAcquisition '))
@section('nav')
        @include('layouts.partials.topnav-dark')
@endsection
@section('footer')
        @include('layouts.partials.footer-light')
@endsection
@section('content')

 <div class="container minh">
        <div class="row">
            @include('admin.sidebar')
            <div class="col-md-8">
                 <div class="row">
                    <div class="col-md-12">
                        <div class="sub_main_menu">
                         
                        @section('main_message') 
                        <div class="card-header">Models\ModeAcquisition {{ $models\modeacquisition->id }}</div>
                        @endsection
                        </div>
                   </div>                    
                </div>
                <div class="card m-card">
                   
                    <div class="card-body">

                        <a href="{{ url('/admin/models\-mode-acquisition') }}" title="Back"><button class="btn btn-link btn-sm"><i class="fa icon-arrow-left" aria-hidden="true"></i> {{__('Retour')}}</button></a>

                        <a href="{{ url('/admin/models\-mode-acquisition/' . $models\modeacquisition->id . '/edit') }}" title="{{__('Modifier')}}"><button class="btn btn-link btn-sm"><i class="fa fa icon-note -square-o" aria-hidden="true"></i> {{__('Modifier')}}</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/models\modeacquisition', $models\modeacquisition->id],
                            'style' => 'display:inline'
                        ]) !!}
                             <button type="submit" class="btn btn-link   text-danger" title="{{__('Supprimer cet élément')}}" onclick="return confirm('{{__("Confirmer la suppression ?")}}')"><i class="icon-trash" aria-hidden="true"></i> {{__('Supprimer')}}</button>
                                                   
                        {!! Form::close() !!}
                    </div>
                    <div>
                        <div class="table-responsive">
                            <table class="table table-borderless table-hover">
                                <tbody>
                                    <tr><th> {{ trans('models\modeacquisition.nom') }} </th><td> {{ $models\modeacquisition->nom }} </td></tr><tr><th> {{ trans('models\modeacquisition.description') }} </th><td> {{ $models\modeacquisition->description }} </td></tr><tr><th> {{ trans('models\modeacquisition.user_id') }} </th><td> {{ $models\modeacquisition->user_id }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
