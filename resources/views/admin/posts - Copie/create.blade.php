@extends('layouts.app')
@section('title',__('Edition  Post '))
@section('nav')
        @include('layouts.partials.topnav-dark')
@endsection
@section('footer')
        @include('layouts.partials.footer-light')
@endsection
@section('content')
@include ('apps.includes.nav.top-slime-bar')
    <div class="container minh" >
        <div class="row">
           <div class="col-md-2"></div>            
            <div class="col-md-8">
                <div class="card">
                    <div class="sec_bg-info">
                            <div class="card-body">
                                <h3 class="Subhead-heading ">{{__("Nouveau ")}} Post</h3>
                                
                                <p class="Subhead-description text-sm text-muted">
                                {{__("Post.create_message")}} <a href="{{route("help_apps")}}"><i class="icon-info" aria-hidden="true"></i></a>
                                </p>
                            </div>
                        </div>
                    <div class="card-body">
                        <a href="{{ url('/admin/posts') }}" title="Back"><button class="btn btn-link btn-sm"><i class="fafa- icon-arrow-left" aria-hidden="true"></i> Back</button></a>
                        
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/posts', 'class' => 'form-horizontal', 'files' => true]) !!}

                        @include ('admin.posts.form', ['formMode' => 'create'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
