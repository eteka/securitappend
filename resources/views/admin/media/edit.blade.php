@extends('layouts.app')
@section('title',__('Edition  Medium'))
@section('nav')
        @include('layouts.partials.topnav-dark')
@endsection
@section('footer')
        @include('layouts.partials.footer-light')
@endsection
@section('content')
   <div class="container minh">
        <div class="row">
            @include('admin.sidebar')
            <div class="col-md-8">
                 <div class="row">
                    <div class="col-md-12">
                        <div class="sub_main_menu">
                        @section('main_message')
                        <div class="card-header">Edit  Medium #{{ $medium->id }}</div>
                        @endsection
                        </div>
                   </div>                    
                </div>
                <div class="card">
                    <div class="sec_bg-info">
                        <div class="card-body">
                            <h3 class="Subhead-heading ">{{__("Edition")}} Medium</h3>
                            
                            <p class="Subhead-description text-sm text-muted">
                            {{__("Medium.edit_message")}} <a href="{{route("help_apps")}}"><i class="icon-info" aria-hidden="true"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="card-body">
                        <a href="{{ url('/admin/media') }}" title="Back"><button class="btn btn-link btn-sm"><i class="fa fa- icon-arrow-left" aria-hidden="true"></i>{{_('Retour')}} </button></a>
                        

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($medium, [
                            'method' => 'PATCH',
                            'url' => ['/admin/media', $medium->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('admin.media.form', ['formMode' => 'edit'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
