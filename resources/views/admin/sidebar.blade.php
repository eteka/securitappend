<style>
   
</style>
<div class="col-md-2 col-sm-3 pr0 mtop10">
    @include("apps.includes.nav.mini-profil")
    <div class="slime_menu">
        

        <div class="">
            <ul class="nav slime_nav"  role="tablist">
                <li role="presentation">
                    <a href="{{ url('/admin') }}">
                    <i class="icon icon-home"></i>
                      {{__('Tableau de board')}}
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ route('modeacquisition.index') }}">
                    <i class="icon icon-peoples"></i>
                    {{__("Mode d'acquisition")}}
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ route('typebien.index') }}">
                    <i class="icon icon-peoples"></i>
                    {{__("Type de bien")}}
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ route('devise.index') }}">
                        <i class="icon icon-peoples"></i>
                        {{__("Devise ")}}
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/users') }}">
                    <i class="icon icon-people"></i>
                    {{__('Les utilisateurs')}}
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/biens') }}">
                    <i class="icon icon-bag"></i>
                    {{__('Biens')}}
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/typebien') }}">
                    <i class="icon icon-handbag"></i>
                    {{__('Type de bien')}}
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/transfert') }}">
                    <i class="icon icon-shuffle"></i>
                    {{__('Transferts')}}
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/blog/article') }}">
                    <i class="icon icon-bulb"></i>
                    {{__('Le Blog')}}
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/Blog/categorie') }}">
                    <i class="icon icon-grid"></i>
                    {{__("Types d'article")}}
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/admin') }}">
                    <i class="icon icon-bubbles"></i>
                    {{__('Forum')}}
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/traductions') }}">
                    <i class="icon icon-flag"></i>
                    {{__('Traductions')}}
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/admin') }}">
                    <i class="icon icon-equalizer"></i>
                    {{__('Paramètres')}}
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>

