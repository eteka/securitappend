@extends('layouts.app')
@section('title',__('Edition  TypeTransfert '))
@section('nav')
        @include('layouts.partials.topnav-dark')
@endsection
@section('footer')
        @include('layouts.partials.footer-light')
@endsection
@section('content')
@include ('apps.includes.nav.top-slime-bar')
 <div class="container minh">
        <div class="row">
            @include('admin.sidebar')
            <div class="col-md-8">
                 <div class="row">
                    <div class="col-md-12">
                        <div class="sub_main_menu">
                        @section('main_message') 
                        <div class="card-header">TypeTransfert {{ $typetransfert->id }}</div>
                        @endsection
                        </div>
                   </div>                    
                </div>
                <div class="card m-card">
                   
                    <div class="card-body">

                        <a href="{{ url('/admin/type-transfert') }}" title="Back"><button class="btn btn-link btn-sm"><i class="fa icon-arrow-left" aria-hidden="true"></i> Back</button></a>

                        <a href="{{ url('/admin/type-transfert/' . $typetransfert->id . '/edit') }}" title="Edit TypeTransfert"><button class="btn btn-link btn-sm"><i class="fa fa icon-note -square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/typetransfert', $typetransfert->id],
                            'style' => 'display:inline'
                        ]) !!}
                             <button type="submit" class="btn btn-link   text-danger" title="{{__('Supprimer cet élément')}}" onclick="return confirm('{{__("Confirmer la suppression ?")}}')"><i class="icon-trash" aria-hidden="true"></i> Delete</button>
                                                   
                        {!! Form::close() !!}
                    </div>
                    <div>
                        <div class="table-responsive">
                            <table class="table table-borderless table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $typetransfert->id }}</td>
                                    </tr>
                                    <tr><th> {{ trans('typetransfert.nom') }} </th><td> {{ $typetransfert->nom }} </td></tr><tr><th> {{ trans('typetransfert.description') }} </th><td> {{ $typetransfert->description }} </td></tr><tr><th> {{ trans('typetransfert.user_id') }} </th><td> {{ $typetransfert->user_id }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
