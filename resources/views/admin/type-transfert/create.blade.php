@extends('layouts.app')
@section('title',__('Edition  TypeTransfert '))
@section('nav')
        @include('layouts.partials.topnav-dark')
@endsection
@section('footer')
        @include('layouts.partials.footer-light')
@endsection
@section('content')
@include ('apps.includes.nav.top-slime-bar')
 <div class="container minh">
        <div class="row">
            @include('admin.sidebar')
            <div class="col-md-8">
                 <div class="row">
                    <div class="col-md-12">
                        <div class="sub_main_menu">
                        @section('main_message')
                        @endsection
                        </div>
                   </div>                    
                </div>
                <div class="card">
                    <div class="sec_bg-info">
                        <div class="card-body">
                            <h3 class="Subhead-heading ">{{__("Nouveau ")}} TypeTransfert</h3>
                            
                            <p class="Subhead-description text-sm text-muted">
                            {{__("TypeTransfert.create_message")}} <a href="{{route("help_apps")}}"><i class="icon-info" aria-hidden="true"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="card-body">
                        <a href="{{ url('/admin/type-transfert') }}" title="{{__('Retour')}}"><button class="btn btn-link btn-sm"><i class="fafa- icon-arrow-left" aria-hidden="true"></i> {{__('Retour')}}</button></a>
                        
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/type-transfert', 'class' => 'form-horizontal', 'files' => true]) !!}

                        @include ('admin.type-transfert.form', ['formMode' => __('Sauvegarder')])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
