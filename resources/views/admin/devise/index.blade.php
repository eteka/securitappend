@extends('layouts.app')
@section('title',__('Edition  Devise'))
@section('nav')
        @include('layouts.partials.topnav-dark')
@endsection
@section('footer')
        @include('layouts.partials.footer-light')
@endsection
@section('content')
    <div class="container minh">
        <div class="row">
            @include('admin.sidebar')
            <div class="col-md-8 col-sm-9">
                @section('notification')                
                    @include ('apps.includes.nav.notification-1')        
                @show
                 <div class="row">
                    <div class="col-md-12">
                        <div class="sub_main_menu">
                            <h4 class="model_name"><a href="{{ url('/admin/devise') }}"> Devise</a></h4>
                            <span class="separateur-v"></span>
                           
                           <div class="core_searchgroup">
                                <span>{{$devise->count()}}<small><sub>{{isset($total) && $total>0?"/".$total:''}}</sub></small>   {{__("élément")}}{{__($devise->count()>1?'ter_s':'')}} </span>
                                {!! Form::open(['method' => 'GET', 'url' => '/admin/devise', 'class' => 'form_searchgroup', 'role' => 'search'])  !!}
                        
                           
                                <div class="inner-search">
                                    <input type="text" class="form-control" name="search"  value="{{ request('search') }}" class="form-control" placeholder="{{__('Rechercher...')}}" id="generalSearch">
                                    <span class="search_icon">
                                        <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="svg-icon">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24"></rect>
                                            <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                            <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"></path>
                                        </g>
                                    </svg>                                    <!--<i class="flaticon2-search-1"></i>-->
                                </span>
                                {!! Form::close() !!}
                            </span>
                        </div>
                        </div>
                        
                    
                    <div class="right_main_menu">
                    <a href="{{ url('/admin/devise/create') }}" title='{{__("Nouveau")}}' class="btn btn-success btn-sm bold"><i class="sec_icon icon-plus"></i> {{__('Nouveau')}} </a>
                    <div class="dropdown dropdown-inline" data-toggle="sec-tooltip-"  data-placement="left">
                        <a href="#" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="icon-arrow-down"></i>                         
                        </a>
                        <div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">
                            <!--begin::Nav-->
                            <ul class="sec-nav">
                               <li class="text-info"><b>{{__("Trier par...")}}</b></li>
                                <li class="sec-nav__item">
                                    <a href="{{url()->current()}}?orderBy=desc" class="sec-nav__link">
                                        <i class="icon-arrow-down-circle"></i>
                                        <span class="sec-nav__link-text">{{__("Plus récent")}}</span>
                                    </a>
                                </li>
                                
                                <li class="sec-nav__item">
                                    <a href="{{url()->current()}}?orderBy=asc" class="sec-nav__link">
                                        <i class="icon-arrow-up-circle"></i>
                                        <span class="sec-nav__link-text">{{__("Plus ancien")}}</span>
                                    </a>
                                </li>                        
                                
                                <li class="sec-nav__ separator"></li>
                                <li class="sec-nav__foot">
                                    <a class="bold" href="{{ url('/admin/devise/export/') }}">{{("Exporter")}}</a>                                    
                                </li>
                            </ul>
                            <!--end::Nav-->
                        </div>
                        </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    <div>
                </div>
                 @if($devise->count()>0)
                <div class="card m-card maincard ">

                        <div class="table-responsive">
                            <table class="table table-borderless  table-hover">
                                <thead>
                                    <tr>
                                        <th width="50px"><div class="pad0_15 hcheck">#</div></th><th>{{ trans('devise.devise') }}</th><th>{{ trans('devise.sigle') }}</th><th>{{ trans('devise.description') }}</th><th>{{__('Actions')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $i=1;?>
                               
                                @foreach($devise as $item)
                                    <tr class="text-sm">
                                        <!--td><div class="pad0_15">{{ $i++ }}<div></td-->
                                        <td>
                                        <div class="pad0_15 pcheck ucheck"><input type="checkbox" name="p" value="{{$item->id}}"></div>
                                        </td>
                                        <td>{{ $item->devise }}</td><td>{{ $item->sigle }}</td><td>{{ $item->description }}</td>
                                        <td nowrap="nowrap">
                                        <div class="dropdown dropdown-inline" data-toggle="sec-tooltip-"  data-placement="left">
                                        <a href="#" class="btn btn-icon btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="icon-options"></i>                          <!--<i class="flaticon2-plus"></i>-->
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">
                                            <!--begin::Nav-->
                                            <ul class="sec-nav">
                                                <li class="sec-nav__item">
                                                <a href="{{ url('/admin/devise/' . $item->id) }}" title="{{__('Visualiser')}}">
                                                        <i class="icon-eye"></i>                                                        
                                                        <span class="sec-nav__link-text">{{__('Visualiser')}}</span>
                                                    </a>
                                                </li>
                                                <li class="sec-nav__item">
                                                <a href="{{ url('/admin/devise/' . $item->id . '/edit') }}" title="{{__('Modifier')}}">
                                                        <i class="icon-note"></i>                                                        
                                                        <span class="sec-nav__link-text">{{__('Modifier')}}</span>
                                                    </a>
                                                </li>
                                                <li class="sec-nav__item">
                                                <a href="{{ url('/admin/devise/' . $item->id . '/export') }}" title="{{__('Exporter')}}">
                                                        <i class="icon-action-redo"></i>                                                        
                                                        <span class="sec-nav__link-text">{{__('Exporter')}}</span>
                                                    </a>
                                                </li>
                                                <li class="sec-nav__item"> 
                                                    {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/devise', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                <button type="submit" class="btn btn-link bold d-block text-danger" title="{{__('Supprimer cet élément')}}" onclick="return confirm('{{__("Confirmer la suppression ?")}}')"><i class="icon-trash" aria-hidden="true"></i> Delete</button>
                                                    {!! Form::close() !!}
                                                </li> 
                                            </ul>
                                            <!--end::Nav-->
                                        </div>
                                        </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $devise->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                    @else
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="card m-card mtop15">
                                <div class="card-body text-center">
                                    <div class="mtop40"><span class="icon-layers text-muted icon-huge2"></span></div>
                                    <div class="text-sm msg-card-info mtop10 text-muted">{{__('Aucun élément ajouté pour le moment')}}</div>
                                    <div class="div-btn-card mtop15 mbottom40">
                                        <a class="btn  btn-md btn-card btn-brand btn-brand-info bold" href='{{ url('/admin/devise/create') }}'>
                                         {{__('Créer nouveau')}}</a>
                                    </div>
                               </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>

                @include('apps.includes.nav.rightbar')
            </div>
            
        </div>
    </div>
@endsection
