@extends('layouts.app')

@section('title')
    {{__('Mes Notifications')}}
    {{Auth::user()?" - ".Auth::user()->fullname():''}}

@endsection

@section('nav')
    @include('layouts.partials.simple-nav-dark',['menu'=>"notification"])
    @include('apps.includes.nav.notification')
@endsection
@section('footer')

@endsection

@section('content')
    <style>
        .b-left-notif{
            border-right: 1px solid #dddddd;
        }
        .list-card{
            margin: 0px;
            padding: 0px;
            box-shadow: 0 2px 3px #dedede;
        }
        .list-card .list-card-item a{
            text-decoration: none;
        }
        .list-card .list-card-item{
            padding: 10px 20px;
            background-color: #fff;
            border: 1px solid #d8dee2;
            margin-top: -1px;
            min-height:60px;

            list-style-type: none;
        }
        .list-card .list-card-item:last-child{
            border-radius: 0 0 3px 3px ;
        }
        .list-card .list-card-header {
            padding: 0 10px;
            border: 1px solid #d8dee2;
            /*background: #e0e3ef;*/
            background: #fbfbfd;
            padding: 10px 15px;
            margin: 10px 0 0 0;
            border-radius: 3px 3px 0 0;
            list-style-type: none;
        }
        .pbody{
            margin-top: 54px;
        }
        .in-nofif{
            //font-size: 12px;
        }
    </style>
    <div class="pbody">
        <div class="container">
            <div class="row">
                <div class="col-md-3  minh b-left-notif ">
                    <div>
                        <ul class="list-group mtop10">
                            <li class="list-group-item"><i class="icon-magnifier"></i> {{__("Boite de réception")}}</li>
                            <li class="list-group-item"><i class="icon-plane"></i> {{__("Enrégisté")}}</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 hidden-xs" id="left">

                    <div class="main-section ">
                        <ul class="list-card">
                            <h5 class="list-card-header">{{__("Notifications")}}</h5>
                        @for($i=0;$i<10;$i++)
                            <li class="list-card-item ">
                                <a href="#">
                                    <div class="in-nofif text-sm">
                                       <div class="noti-user bold"> <i class="icon-info"></i> Auteur du message</div>
                                      <div class="text-muted "> lorem "jkjd dzjdzjhdzjdz

                                           prestodb/presto #14434

                                           Estimate bytecode size for methods based on Bytecode tree

                                      </div>
                                    </div>
                                </a>
                            </li>
                        @endfor
                    </ul>
                    </div>
                </div>

                <div   class="col-md-3 hidden-xs  minh">
                    <div id="right">

                        <div  class="mtop15 text-sm text-muted mbottom10">
                            <span class="icon-tag "></span> Des Boutiques à découvrir
                        </div>
                        <div class=" sec_cardbgwhiteshadow3noborderrond5">

                            <div>
                                <img class="img-responsive rond5" src="{{asset('storage/statics/pub/affiche-1.jpg')}}" alt="Publicité pate dentifrice">
                            </div>
                        </div>
                        @include('layouts.partials.mini-footer')
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
