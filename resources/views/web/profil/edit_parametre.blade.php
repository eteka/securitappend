@extends('layouts.app')

@section('title')
{{trans('user.profil_parametre')}} -
    {{Auth::user()->fullname()}}

@endsection

@section('nav')
    @include('layouts.partials.simple-nav-dark',['menu'=>"notification"])
    @include('apps.includes.nav.notification')
@endsection
@section('footer')

@endsection

@section('content')

    <div class="main">
       @include('apps.includes.nav.profil_edit',['section'=>trans('user.profil_parametre'),'tab'=>'parametre'])
       
        <div class="tab-panefade showactive" id="general" role="tabpanel" aria-labelledby="general-tab">
            <div class="container">
               
               <div class="row" id="form-profil">
                   <div class="col-md-12">
                       <br>
 <div class="row" >
                    
    <!--end of col-->
    <div class="col-sm-12 col-md-8 order-md-1 ">
        
        <form  method="POST" action="{{ route('save_profil_info',$user->getPseudo()) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
           @csrf
           <input type="hidden" name="type" value="profil">
            <div class="card mbottom50">
                <div class="card-body">
                  <div class=" ">
                <div class="row">
               <div class="col-md-12">
                <h4 class="page-header">{{trans('user.profil_parametre_edit')}}</h4>
                   @php
                   $state_link=$user->pseudo_editable?'':'disabled';                              
                   $bg=$user->pseudo_editable?'bgsilver':'bgwhite';                              
                   @endphp
                <div class="form-group {{ $errors->has('profil_id') ? 'has-error' : ''}}">
                    {!! Form::label('bien_id', trans('user.porfil_id'), ['class' => 'control-label']) !!}
                    <div class="input-group">
                    <span class="input-group-addon {{$bg}}" id="basic-addon2">{{url('/')}}/@</span>
                    {!! Form::text('profil_id', $user->pseudo,  ['class' => 'form-control rond0_3_3_0 bgsilverbga bold text-bold', 'required' => 'required',$state_link]) !!}
                   
                    </div>
                   
                {!! $errors->first('profil_id', '<p class="help-block">:message</p>') !!}
                <small class="text-muted text-xs">
                    {{__("user.profil_id_info")}}
                </small>
                </div> 
               </div>
               
            
                <div class="form-group col-md-6">
                    <div class="">
                        <div><label for="email" >{{ __("user.pays") }}</label></div>
                        <div class="sec_formicon sec-input-icon">
                            <svg class="svg-icon" viewBox="0 0 20 20">
                                <path d="M10,1.375c-3.17,0-5.75,2.548-5.75,5.682c0,6.685,5.259,11.276,5.483,11.469c0.152,0.132,0.382,0.132,0.534,0c0.224-0.193,5.481-4.784,5.483-11.469C15.75,3.923,13.171,1.375,10,1.375 M10,17.653c-1.064-1.024-4.929-5.127-4.929-10.596c0-2.68,2.212-4.861,4.929-4.861s4.929,2.181,4.929,4.861C14.927,12.518,11.063,16.627,10,17.653 M10,3.839c-1.815,0-3.286,1.47-3.286,3.286s1.47,3.286,3.286,3.286s3.286-1.47,3.286-3.286S11.815,3.839,10,3.839 M10,9.589c-1.359,0-2.464-1.105-2.464-2.464S8.641,4.661,10,4.661s2.464,1.105,2.464,2.464S11.359,9.589,10,9.589"></path>
                            </svg>
                            
                            {!! Form::select('pays', isset($pays)?$pays:[''=>__('Pays')],$user->pays?$user->pays->id:'', ['class' => 'form-control form-control-type-1'] ) !!}

                           </div>
                        @error("pays")
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <div class="">
                        <div><label for="ville" >{{__('user.ville')}}</label></div>
                        <div class="sec_formicon sec-input-icon">
                            <svg class="svg-icon" viewBox="0 0 20 20">
                                <path d="M18.092,5.137l-3.977-1.466h-0.006c0.084,0.042-0.123-0.08-0.283,0H13.82L10,5.079L6.178,3.671H6.172c0.076,0.038-0.114-0.076-0.285,0H5.884L1.908,5.137c-0.151,0.062-0.25,0.207-0.25,0.369v10.451c0,0.691,0.879,0.244,0.545,0.369l3.829-1.406l3.821,1.406c0.186,0.062,0.385-0.029,0.294,0l3.822-1.406l3.83,1.406c0.26,0.1,0.543-0.08,0.543-0.369V5.506C18.342,5.344,18.242,5.199,18.092,5.137 M5.633,14.221l-3.181,1.15V5.776l3.181-1.15V14.221z M9.602,15.371l-3.173-1.15V4.626l3.173,1.15V15.371z M13.57,14.221l-3.173,1.15V5.776l3.173-1.15V14.221z M17.547,15.371l-3.182-1.15V4.626l3.182,1.15V15.371z"></path>
                            </svg>
                            {!! Form::text('ville', $user->ville,  ['class' => 'form-control form-control-type-1','autocomplete'=>'OFF','placeholder'=>__('user.ville_name')]) !!}
                   
                         </div>
                        @error("ville")
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <div><label for="site_web" >{{ __("user.site_web") }}</label></div>
                        <div class="sec_formicon sec-input-icon">
                            <svg class="svg-icon" viewBox="0 0 20 20">
                                <path d="M16.469,8.924l-2.414,2.413c-0.156,0.156-0.408,0.156-0.564,0c-0.156-0.155-0.156-0.408,0-0.563l2.414-2.414c1.175-1.175,1.175-3.087,0-4.262c-0.57-0.569-1.326-0.883-2.132-0.883s-1.562,0.313-2.132,0.883L9.227,6.511c-1.175,1.175-1.175,3.087,0,4.263c0.288,0.288,0.624,0.511,0.997,0.662c0.204,0.083,0.303,0.315,0.22,0.52c-0.171,0.422-0.643,0.17-0.52,0.22c-0.473-0.191-0.898-0.474-1.262-0.838c-1.487-1.485-1.487-3.904,0-5.391l2.414-2.413c0.72-0.72,1.678-1.117,2.696-1.117s1.976,0.396,2.696,1.117C17.955,5.02,17.955,7.438,16.469,8.924 M10.076,7.825c-0.205-0.083-0.437,0.016-0.52,0.22c-0.083,0.205,0.016,0.437,0.22,0.52c0.374,0.151,0.709,0.374,0.997,0.662c1.176,1.176,1.176,3.088,0,4.263l-2.414,2.413c-0.569,0.569-1.326,0.883-2.131,0.883s-1.562-0.313-2.132-0.883c-1.175-1.175-1.175-3.087,0-4.262L6.51,9.227c0.156-0.155,0.156-0.408,0-0.564c-0.156-0.156-0.408-0.156-0.564,0l-2.414,2.414c-1.487,1.485-1.487,3.904,0,5.391c0.72,0.72,1.678,1.116,2.696,1.116s1.976-0.396,2.696-1.116l2.414-2.413c1.487-1.486,1.487-3.905,0-5.392C10.974,8.298,10.55,8.017,10.076,7.825"></path>
                            </svg>
                            {!! Form::text('site_web', $user->website,  ['class' => 'form-control form-control-type-1','autocomplete'=>'OFF','placeholder'=>__("http://mywebsite.com")]) !!}
                   
                        </div>
                        @error("site_web")
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group row">
                        <label for="about">{{__('user.apropos')}}</label>
                        {!! Form::textarea('about', $user->apropos,  ['class' =>'form-control form-control-lg','rows'=>'4','autocomplete'=>'OFF','id'=>'about','placeholder'=>__('user.apropos_info')]) !!}
                   
                    @error("about")
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group row">
                        <div class="custom-control custom-checkbox custom-checkbox-switch">
                            <input type="checkbox" class="custom-control-input" value="1" checked name="type_profil" id="public">
                        <label class="custom-control-label" for="public">{{__('user.profil_public')}}</label>
                       <div class="text-sm bold">
                            <small class="text-muted">{!!__('user.profil_public_info',['url'=>route('condition')])!!}</small>
                        </div>
                        
                        </div>
                    </div>
                </div>
               
               
                
                <div class="form-group row">
                    <hr>
                    <div class="col-md-12">
                        {!! Form::submit(__('Sauvegarder'), ['class' => 'btn btn-info bold']) !!}
                    </div>
                </div>
        </div> 
                </div>
            </div>
            
        </form>
    </div>
    </div>
    <div class="col-sm-12 col-md-4 order-md-2">
        <div class="alert alert-info text-small" role="alert">
            <i class="icon-user"></i>
            <span>
                {!!__('user.profil_edit_info')!!} 
            </span>
        <div><a class="bold" href="{{$user->profil()}}">{{{__('user.show_profil')}}} </a></div>
        </div>
    </div>
    <!--end of col-->
</div>
                   </div>
               </div>
               
                <!--end of row-->
            </div>
            <!--end of container-->
        </div>
    </div>
@endsection
