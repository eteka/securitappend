@extends('layouts.app')

@section('title')
{{trans('user.password_edit')}} -
    {{Auth::user()->fullname()}}

@endsection

@section('nav')
    @include('layouts.partials.simple-nav-dark',['menu'=>"notification"])
    @include('apps.includes.nav.notification')
@endsection
@section('footer')

@endsection

@section('content')

    <div class="main">
        @include('apps.includes.nav.profil_edit',['section'=>trans('user.password_edit'),'tab'=>'password'])
        <div class="tab-panefade showactive" id="general" role="tabpanel" aria-labelledby="general-tab">
            <div class="container">
                <!--end of row-->
                <div class="row">
                   
                    <!--end of col-->
                    <div class="col-xs-12 col-md-8 order-md-1">
                        <div class="card sec_card mtop20">

                            <div class="card-body ">
                            <h4 class="page-header">{{trans('user.password_edit')}}</h4>
                                <form method="POST" action="{{ route('save_edit_password',$user->getPseudo()) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                           
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12">
                                            @if (count($errors) > 0)
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12 ">
                                            {!! Form::label('lastpassword', trans('user.lastpassword'), ['class' => 'control-label']) !!}
                                            <div class="sec_formicon sec-input-icon">
                                                <svg class="svg-icon" viewBox="0 0 20 20">
                                                    <path d="M17.308,7.564h-1.993c0-2.929-2.385-5.314-5.314-5.314S4.686,4.635,4.686,7.564H2.693c-0.244,0-0.443,0.2-0.443,0.443v9.3c0,0.243,0.199,0.442,0.443,0.442h14.615c0.243,0,0.442-0.199,0.442-0.442v-9.3C17.75,7.764,17.551,7.564,17.308,7.564 M10,3.136c2.442,0,4.43,1.986,4.43,4.428H5.571C5.571,5.122,7.558,3.136,10,3.136 M16.865,16.864H3.136V8.45h13.729V16.864z M10,10.664c-0.854,0-1.55,0.696-1.55,1.551c0,0.699,0.467,1.292,1.107,1.485v0.95c0,0.243,0.2,0.442,0.443,0.442s0.443-0.199,0.443-0.442V13.7c0.64-0.193,1.106-0.786,1.106-1.485C11.55,11.36,10.854,10.664,10,10.664 M10,12.878c-0.366,0-0.664-0.298-0.664-0.663c0-0.366,0.298-0.665,0.664-0.665c0.365,0,0.664,0.299,0.664,0.665C10.664,12.58,10.365,12.878,10,12.878"></path>
                                                </svg>
                                                 {!! Form::password('lastpassword', ['class' => 'form-control form-control form-control-type-1 ', 'required' => 'required','autocomplete'=>'OFF','placeholder'=>trans('user.lastpassword')] ) !!}
                                            
                                        </div>
                                        <div>
                                            @error("lastpassword")
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>                                               
                                            </span>
                                            @enderror
                                        </div>
                                        </div>
                                        <div class="col-md-12">
                                            {!! Form::label('password', trans('user.password'), ['class' => 'control-label']) !!}
                                            <div class="sec_formicon sec-input-icon">
                                                <svg class="svg-icon" viewBox="0 0 20 20">
                                                    <path d="M17.308,7.564h-1.993c0-2.929-2.385-5.314-5.314-5.314S4.686,4.635,4.686,7.564H2.693c-0.244,0-0.443,0.2-0.443,0.443v9.3c0,0.243,0.199,0.442,0.443,0.442h14.615c0.243,0,0.442-0.199,0.442-0.442v-9.3C17.75,7.764,17.551,7.564,17.308,7.564 M10,3.136c2.442,0,4.43,1.986,4.43,4.428H5.571C5.571,5.122,7.558,3.136,10,3.136 M16.865,16.864H3.136V8.45h13.729V16.864z M10,10.664c-0.854,0-1.55,0.696-1.55,1.551c0,0.699,0.467,1.292,1.107,1.485v0.95c0,0.243,0.2,0.442,0.443,0.442s0.443-0.199,0.443-0.442V13.7c0.64-0.193,1.106-0.786,1.106-1.485C11.55,11.36,10.854,10.664,10,10.664 M10,12.878c-0.366,0-0.664-0.298-0.664-0.663c0-0.366,0.298-0.665,0.664-0.665c0.365,0,0.664,0.299,0.664,0.665C10.664,12.58,10.365,12.878,10,12.878"></path>
                                                </svg>
                                                 {!! Form::password('password', ['class' => 'form-control form-control form-control-type-1 ', 'required' => 'required','autocomplete'=>'OFF','placeholder'=>trans('user.newpassword')] ) !!}
                                            
                                            </div>
                                            </div>
                                            <div class="">
                                           <div class="col-sm-12">
                                             @error("password")
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}
                                                        
                                                    </strong>
                                                   
                                                </span><hr class="m0">
                                                <div class="alert alert-danger m0"><i class="icon-info"></i> {{__('auth.password-info')}}</div>
                                                @else
                                                <div class="text-xs text-muted">{{__('auth.password-info')}}</div>
                                            @enderror
                                            
                                           
                                           </div>
                                        </div>
                                    </div>
        
                                    
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            {!! Form::label('password_confirmation', trans('user.password_confirmation'), ['class' => 'control-label']) !!}
                                            <div class="sec_formicon sec-input-icon">
                                                <svg class="svg-icon" viewBox="0 0 20 20">
                                                    <path d="M17.308,7.564h-1.993c0-2.929-2.385-5.314-5.314-5.314S4.686,4.635,4.686,7.564H2.693c-0.244,0-0.443,0.2-0.443,0.443v9.3c0,0.243,0.199,0.442,0.443,0.442h14.615c0.243,0,0.442-0.199,0.442-0.442v-9.3C17.75,7.764,17.551,7.564,17.308,7.564 M10,3.136c2.442,0,4.43,1.986,4.43,4.428H5.571C5.571,5.122,7.558,3.136,10,3.136 M16.865,16.864H3.136V8.45h13.729V16.864z M10,10.664c-0.854,0-1.55,0.696-1.55,1.551c0,0.699,0.467,1.292,1.107,1.485v0.95c0,0.243,0.2,0.442,0.443,0.442s0.443-0.199,0.443-0.442V13.7c0.64-0.193,1.106-0.786,1.106-1.485C11.55,11.36,10.854,10.664,10,10.664 M10,12.878c-0.366,0-0.664-0.298-0.664-0.663c0-0.366,0.298-0.665,0.664-0.665c0.365,0,0.664,0.299,0.664,0.665C10.664,12.58,10.365,12.878,10,12.878"></path>
                                                </svg>
                                            {!! Form::password('password_confirmation',['class' => 'form-control  form-control-type-1 ', 'required' => 'required','autocomplete'=>'OFF','placeholder'=>trans('user.password_confirmation')] ) !!}
                                            
                                        </div>
                                            @error("password_confirmation")
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            <div class=" text-xs text-muted mtop10">
                                                <span>
                                                    {{__('user.newpassword_info')}}
                                                       
                                                </span>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                    <div class="form-group row">
                                        <hr>
                                        <div class="col-md-12">
                                            {!! Form::submit(__('Changer mot mot de passe'), ['class' => 'btn btn-info']) !!}
                                        </div>
                                    </div>
                                </form>

                        
                            </div>

                        </div>
                        
                        
                        
                    </div>
                    <div class="col-xs-12 col-md-4 order-md-2">
                        <div class="alert alert-info text-small mtop50" role="alert">
                            <i class="icon-info"></i>
                            <span>
                                {{__('user.newpassword_info')}}
                                   
                            </span>
                        </div>
                    </div>
                    <!--end of col-->
                </div>
               <div class="row" id="form-profil">
                   <div class="col-md-12">
                       <hr>
                   </div>
               </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </div>
    </div>

@endsection