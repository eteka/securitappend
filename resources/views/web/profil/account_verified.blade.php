@extends('layouts.app')
@section('title')
    {{__('Vérification de mon compte') }} -
@parent
@endsection

@section('nav')
    @include('layouts.partials.simple-nav-dark',['menu'=>"notification"])
    @include('apps.includes.nav.notification')
@endsection
@section('footer')
    @include('layouts.partials.footer-light',['bg'=>''])
@endsection
@section('content')
<style>
    .form-input-code{
        border: 1px solid #dddddd;
        font-size: 35px;
        width: 60px;
        padding: 5px 10px;
    }
    .l-tiret{
        font-size: 35px;
        padding: 5px 10px
    }
</style>
<div class="container minh hmin">
    <div class="row justify-content-center mtop50 mbottom50">
        <div class="col-md-6 col-md-offset-3">
            <div class="card sec_card border_secs ">
              
                <div class="card-body ">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{route('email_validation')}}">
                        @csrf

                        <div class="form-group row">
                          <div class="col-md-12 text-center mtop20 ">
                          <img src="{{url('assets/statics/app/user/account_verified.png')}}" height="200px" alt="{{__('user.account_valide')}}">
                         
                        <h1 class="mtop20">{{__('user.valide_account_header')}}</h1>
                           <div class="text-xs text-muted">
                               {!!__('user.valide_account_info')!!}
                           </div>
                        </div>
                        </div>

                        <div class="form-group row mbottom30 text-center">
                            <div class="col-md-12  col-xs-12 ">
                                <hr>
                                <a href="{{url('/home')}}" class="btn btn-lg bold btn-default">
                                    {{ __('Mon tableau de bord') }} 
                                </a> &nbsp;&nbsp;&nbsp;
                            <a href="{{route('edit_profil',$user->getPseudo())}}"  class="btn btn-md bold btn-xs-block btn-lg btn-success">
                                    {{ __('Editer mon prodil') }} <i class="icon-arrow-right"></i>
                                </a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
