@extends('layouts.app')

@section('title')    
    {{$user->fullname()." "}} {{__("sur ")}} {{config('app.name')}}
@endsection
@section('nav')
    @include('layouts.partials.simple-nav-dark',['menu'=>"notification"])
    @include('apps.includes.nav.notification')
@endsection
@section('footer')
@include('layouts.partials.footer-light')
@endsection

@section('content')

<div class="main ">
 @include('apps.includes.nav.nav-user-profil',['tabs'=>"profil"])
  <div class="container">
    <div class="row">
      
      <div class="col-md-8 col-md-offset-2">
        <div  class="cardsec_card hidden" id="head-profil-cover">
            <div id="top-profil-header" >
                <!--img id="user_profil_img" src="{{asset('storage/statics/users/image-profile.jpg')}}" alt="ETEKA Wilfried"-->
            </div>
        <div id="bottom-profil-header" class="">
           <div class="row">
               <div class="col-md-7">
                    <h4 class="mtop50 bold " id="profil_user_name"><a href="">{{$user->fullname()}}</a></h4>
            <div class="user_statut" ><span class="text-sm sec_color1 bold">
                <i class="icon-badge"></i> {{$user->badge?$user->badge->nom:__('Aucun badge')}}</span></div>
            <div class="text-sm  btn-block text-muted mbottom5 mtop5">   
            <ul class="list-inline">
                
                 <li title="{{$user->created_at}}">
                    <i class="icon-calendar"> </i><span class="text-muted" class="" href="{{url('/')}}"> {{__("Inscrit")}} {{$user->created_at->diffForHumans()}}</span>
                </li>
                <li class="hidden">
                    <i class="icon-badge" title='{{__("Les badges décernés")}}'> </i><a class="text-muted" class="" href="{{url('/')}}">      Aucun</a>
                </li>
            </ul>
            </div>
               </div>
               <div class="col-md-5 text-right">   
                <!--ul class="list-inline mtop20">
                    <li>
                        <a href="" class="btn btn-success"> <i class="icon-like"> </i> S'abonner </a>
                    </li>
                     <li>
                        <a href="" class="btn btn-default"> <i class="icon-envelope"> </i>  Message </a>
                    </li>
                </ul-->
               </div>
           </div>
            <div class="user_stats_cover hidden-xs ">   
                        <ul class="list-inline text-sm" id="user_stats">
                            <li>
                                <p class="bold m0 item-header"> <i class="icon-bag"></i>   {{__('Biens')}}</p>  
                                <span class="text-muted">555</span>
                            </li>
                            <li>
                                <p class="bold m0 item-header"> <i class="icon-grid"></i>   {{__('Catégories')}}</p>  
                                <span class="text-muted">5</span>
                            </li>
                            <li>
                                <p class="bold m0 item-header"> <i class="icon-target"></i>   {{__('Marques')}}</p>  
                                <span class="text-muted">555</span>
                            </li>
                            <li>
                                <p class="bold m0 item-header"> <i class="icon-equalizer"></i>   {{__('Modèles')}}</p>  
                                <span class="text-muted">8</span>
                            </li>
                        </ul>
            </div>
            
        </div>

    </div>
  
    
        <div class="top_home_param_bloc_ mtop15 ">
          <div class="row">
            <div class="col-xs-12 text-center-xs col-sm-3">
                
            </div>
            <div class="col-xs-12 hidden text-center-xs mtop-xs-10 col-sm-9 text-right">
              {!! Form::open(['route' => 'dashboard', 'class' => 'form-horizontal', 'method' => 'get']) !!}
              <div class="top-line-options hidden">
                <span><a href="{{route('bien_favoris')}}" class="btn btn-md btn-default"><i class="icon-star"></i></a></span>
                <span><a href="{{route('classement')}}" title="{{__('Classement')}}" class="btn btn-md btn-default"><i class="icon-organization"></i></a></span>
                <span>
                  <a title="{{__('Tout exporter')}}" href="{{route('export_all_biens')}}" class="btn btn-md btn-default">
                   <svg class="svg-icon svg-icon-md rotate180" viewBox="0 0 20 20">
                      <path fill="none" d="M8.416,3.943l1.12-1.12v9.031c0,0.257,0.208,0.464,0.464,0.464c0.256,0,0.464-0.207,0.464-0.464V2.823l1.12,1.12c0.182,0.182,0.476,0.182,0.656,0c0.182-0.181,0.182-0.475,0-0.656l-1.744-1.745c-0.018-0.081-0.048-0.16-0.112-0.224C10.279,1.214,10.137,1.177,10,1.194c-0.137-0.017-0.279,0.02-0.384,0.125C9.551,1.384,9.518,1.465,9.499,1.548L7.76,3.288c-0.182,0.181-0.182,0.475,0,0.656C7.941,4.125,8.234,4.125,8.416,3.943z M15.569,6.286h-2.32v0.928h2.32c0.512,0,0.928,0.416,0.928,0.928v8.817c0,0.513-0.416,0.929-0.928,0.929H4.432c-0.513,0-0.928-0.416-0.928-0.929V8.142c0-0.513,0.416-0.928,0.928-0.928h2.32V6.286h-2.32c-1.025,0-1.856,0.831-1.856,1.856v8.817c0,1.025,0.832,1.856,1.856,1.856h11.138c1.024,0,1.855-0.831,1.855-1.856V8.142C17.425,7.117,16.594,6.286,15.569,6.286z"></path>
                    </svg>
                  </a>
                </span>

                {!! Form::select('orderby',["asc"=>__("Plus récent"),"desc"=>__("Plus ancien")], app('request')->input('orderby'),['class' => 'btn-default text-md btn l-inline form-control-md', 'required' => 'required',"onchange"=>"this.form.submit()"]) !!}
              </div>
              {!! Form::close() !!}
            </div>
          </div>
        </div>
        <div class="main-section minh">
            <div class="card mbottom15">
                <div class="card-header">
                <h4 class="bold">{{__('A propos')}}</h4>
                </div>
                <div class="card-body">
                {{$user->apropos}}
                </div>
            </div>
          @if(isset($biens))
          @include('apps.includes.data.liste-biens',['biens'=>$biens,"icone"=>"icon-layers","msg"=>__('Aucun élément dans le classement')])
          @endif
        </div>
</div>

</div>

</div>
</div>

@endsection
