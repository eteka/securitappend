@extends('layouts.app')

@section('title')
{{trans('user.profil_parametre')}} -
    {{Auth::user()->fullname()}}

@endsection

@section('nav')
    @include('layouts.partials.simple-nav-dark',['menu'=>"notification"])
    @include('apps.includes.nav.notification')
@endsection
@section('footer')

@endsection

@section('content')
    <div class="main">
       @include('apps.includes.nav.profil_edit',['section'=>trans('user.profil_edit'),'tab'=>'general'])
        <div class="tab-panefade showactive" id="general" role="tabpanel" aria-labelledby="general-tab">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="mtop20">
                        <div class="row"> 
                            <form method="POST" action="{{ route('save_profil_info',$user->getPseudo()) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="type" value="compte">
                            <div class="col-md-2 col-lg-2 col-sm-3 col-xs-4">
                            <div id="photo-profil">
                                <span >
                                @if(Auth::user()->photoProfil())
                                
                                <a  tabindex="-1" href="#" id="preview" class="avatar avatar-lg avatar-brd img-circle"><img alt="" src="{{asset(Auth::user()->avatar())}}"  class="" class="img-responsive">
                                </a>
                               
                             @else
                             @php
                             $avatar=(Auth::user()->sexe=='F') ? "assets/statics/users/avatar-f.jpg":'assets/statics/users/avatar-h.jpg';  
                            @endphp
                             <a  tabindex="-1" href="#" id="preview" class="avatar avatar-lg avatar-brd img-circle"><img alt="" src="{{asset($avatar)}}"  class="" class="img-responsive">
                             </a>
                            @endif
                        </span>
                         </div>
                            </div>
                            <div class="col-md-10 col-lg-10 col-sm-9 col-xs-8">
                               
                                <div class="mtop30">
                                   
                                       
                                        <label for="upload" class=" custom-file-upload btn btn-default">
                                            <i class="icon-camera"></i> <input onchange="handleFiles(files)" id="upload"  onchange="handleFiles(files)" name="profil_img" type="file" accept=".png,.jpg,.jpeg" />
                                            {{__('user.select_image')}}
                                        </label>
                                        <div>
                                            <small class="text-muted ">{{__('user.image_info')}}</small>
                                        </div>
                                        @error("profil_img")
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <!--end of col-->
                </div>
                <!--end of row-->
                
                <div class="row mbottom10 mtop20">
                    <div class="col-md-12 ">
                        
                        
                    </div>
                    <!--end of col-->
                </div>
                <!--end of row-->
                <div class="row">
                   
                    <!--end of col-->
                    <div class="col-xs-12 col-md-8 order-md-1">
                        <div class="card sec_card mbottom50">
                            <div class="card-body">
                                <div class="mbottom20">
                                    <h4 class="page-header">{{trans('user.profil_edition')}}</h4>  
                                </div>
                                
                                    @csrf
                                    
                                    <div class="form-group row">
                                        <div class="col-md-6 ">
                                            {!! Form::label('nom', trans('user.nom'), ['class' => 'control-label']) !!}
                                            <div class="sec_formicon sec-input-icon">
                                            <svg version="1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 0C5.383 0 0 5.383 0 12c0 3.18 1.232 6.177 3.47 8.438A11.913 11.913 0 0 0 12 24c3.234 0 6.268-1.269 8.542-3.574A11.924 11.924 0 0 0 24 12c0-6.617-5.383-12-12-12zM4.655 20.17c1.029-.563 2.533-1.111 4.002-1.644.514-.186 1.027-.372 1.52-.559a.5.5 0 0 0 .323-.468V15a.511.511 0 0 0-.335-.472C10.118 14.511 9 14.072 9 12a.48.48 0 0 0-.482-.494c-.047-.032-.138-.218-.138-.506s.091-.474.12-.5A.5.5 0 0 0 9 10c0-.125-.037-.256-.118-.544-.147-.523-.537-1.913-.194-2.366.038-.051.245-.194.714-.1.26.055.518-.11.583-.369.163-.649 1.326-1.121 2.765-1.121s2.602.472 2.765 1.121c.229.917-.178 2.188-.373 2.799-.098.305-.142.443-.142.58a.48.48 0 0 0 .482.494c.046.032.137.218.137.506s-.091.474-.12.5a.5.5 0 0 0-.5.5c0 2.072-1.118 2.511-1.158 2.526A.5.5 0 0 0 13.5 15v2.5a.5.5 0 0 0 .324.468c.543.204 1.086.399 1.614.591 1.51.546 2.943 1.067 3.919 1.599A10.901 10.901 0 0 1 12 23a10.908 10.908 0 0 1-7.345-2.83zm15.453-.739c-1.047-.625-2.583-1.181-4.33-1.812l-1.278-.466v-1.848c.501-.309 1.384-1.108 1.49-2.936.386-.226.63-.727.63-1.37 0-.579-.198-1.043-.52-1.294.243-.757.681-2.144.385-3.327C16.138 4.992 14.256 4.5 12.75 4.5c-1.342 0-2.982.391-3.569 1.456-.707-.042-1.095.273-1.29.53-.635.838-.216 2.368.02 3.211-.329.249-.531.718-.531 1.303 0 .643.244 1.144.63 1.37.106 1.828.989 2.627 1.49 2.936v1.848l-1.184.433c-1.624.589-3.299 1.196-4.413 1.858A10.928 10.928 0 0 1 1 12C1 5.935 5.935 1 12 1s11 4.935 11 11c0 2.774-1.024 5.397-2.892 7.431z"></path></svg>
                                            {!! Form::text('nom', $user->nom,['class' => 'form-control form-control form-control-type-1 ', 'required' => 'required','autocomplete'=>'OFF','placeholder'=>trans('user.name')] ) !!}
                                            
                                        </div>
                                        <!--div class="form-group {{ $errors->has('bien_id') ? 'has-error' : ''}}">
                                            {!! Form::label('bien_id', trans('favori.bien_id'), ['class' => 'control-label']) !!}
                                            {!! Form::number('bien_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                            {!! $errors->first('bien_id', '<p class="help-block">:message</p>') !!}
                                        </div-->
                                        </div>
                                        <div class="col-md-6">
                                            {!! Form::label('prenom', trans('user.prenom'), ['class' => 'control-label']) !!}
                                            <div class="sec_formicon sec-input-icon">
                                                <svg version="1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 0C5.383 0 0 5.383 0 12c0 3.18 1.232 6.177 3.47 8.438A11.913 11.913 0 0 0 12 24c3.234 0 6.268-1.269 8.542-3.574A11.924 11.924 0 0 0 24 12c0-6.617-5.383-12-12-12zM4.655 20.17c1.029-.563 2.533-1.111 4.002-1.644.514-.186 1.027-.372 1.52-.559a.5.5 0 0 0 .323-.468V15a.511.511 0 0 0-.335-.472C10.118 14.511 9 14.072 9 12a.48.48 0 0 0-.482-.494c-.047-.032-.138-.218-.138-.506s.091-.474.12-.5A.5.5 0 0 0 9 10c0-.125-.037-.256-.118-.544-.147-.523-.537-1.913-.194-2.366.038-.051.245-.194.714-.1.26.055.518-.11.583-.369.163-.649 1.326-1.121 2.765-1.121s2.602.472 2.765 1.121c.229.917-.178 2.188-.373 2.799-.098.305-.142.443-.142.58a.48.48 0 0 0 .482.494c.046.032.137.218.137.506s-.091.474-.12.5a.5.5 0 0 0-.5.5c0 2.072-1.118 2.511-1.158 2.526A.5.5 0 0 0 13.5 15v2.5a.5.5 0 0 0 .324.468c.543.204 1.086.399 1.614.591 1.51.546 2.943 1.067 3.919 1.599A10.901 10.901 0 0 1 12 23a10.908 10.908 0 0 1-7.345-2.83zm15.453-.739c-1.047-.625-2.583-1.181-4.33-1.812l-1.278-.466v-1.848c.501-.309 1.384-1.108 1.49-2.936.386-.226.63-.727.63-1.37 0-.579-.198-1.043-.52-1.294.243-.757.681-2.144.385-3.327C16.138 4.992 14.256 4.5 12.75 4.5c-1.342 0-2.982.391-3.569 1.456-.707-.042-1.095.273-1.29.53-.635.838-.216 2.368.02 3.211-.329.249-.531.718-.531 1.303 0 .643.244 1.144.63 1.37.106 1.828.989 2.627 1.49 2.936v1.848l-1.184.433c-1.624.589-3.299 1.196-4.413 1.858A10.928 10.928 0 0 1 1 12C1 5.935 5.935 1 12 1s11 4.935 11 11c0 2.774-1.024 5.397-2.892 7.431z"></path></svg>
                                                {!! Form::text('prenom', $user->prenom,['class' => 'form-control form-control form-control-type-1 ', 'required' => 'required','autocomplete'=>'OFF','placeholder'=>trans('user.prenom')] ) !!}
                                            
                                            </div>
                                            </div>
                                            <div class="">
                                           <div class="col-sm-12">
                                             @error("nom")
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                           @error("prenom")
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                           </div>
                                        </div>
                                    </div>
        
                                    
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            {!! Form::label('poste', trans('user.poste'), ['class' => 'control-label']) !!}
                                            <div class="sec_formicon sec-input-icon">
                                            <svg class="svg-icon" viewBox="0 0 20 20">
                                                <path d="M17.283,5.549h-5.26V4.335c0-0.222-0.183-0.404-0.404-0.404H8.381c-0.222,0-0.404,0.182-0.404,0.404v1.214h-5.26c-0.223,0-0.405,0.182-0.405,0.405v9.71c0,0.223,0.182,0.405,0.405,0.405h14.566c0.223,0,0.404-0.183,0.404-0.405v-9.71C17.688,5.731,17.506,5.549,17.283,5.549 M8.786,4.74h2.428v0.809H8.786V4.74z M16.879,15.26H3.122v-4.046h5.665v1.201c0,0.223,0.182,0.404,0.405,0.404h1.618c0.222,0,0.405-0.182,0.405-0.404v-1.201h5.665V15.26z M9.595,9.583h0.81v2.428h-0.81V9.583zM16.879,10.405h-5.665V9.19c0-0.222-0.183-0.405-0.405-0.405H9.191c-0.223,0-0.405,0.183-0.405,0.405v1.215H3.122V6.358h13.757V10.405z"></path>
                                            </svg>
                                            {!! Form::text('poste', $user->poste,['class' => 'form-control  form-control-type-1 ', 'autocomplete'=>'OFF','placeholder'=>trans('user.poste')] ) !!}
                                            
                                        </div>
                                            @error("poste")
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            {!! Form::label('email', trans('user.email'), ['class' => 'control-label']) !!}
                                            <div class="sec_formicon sec-input-icon">
                                            <svg version="1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                            <path d="M21 4H3c-1.103 0-2 .897-2 2v11c0 1.103.897 2 2 2h18c1.103 0 2-.897 2-2V6c0-1.103-.897-2-2-2zm0 1l.159.032L12 12.36 2.841 5.032 3 5h18zm1 12c0 .551-.449 1-1 1H3c-.551 0-1-.449-1-1V6c0-.11.03-.21.063-.309l9.625 7.7a.504.504 0 0 0 .624 0l9.625-7.7A.966.966 0 0 1 22 6v11z"></path>
                                            </svg>
                                            {!! Form::email('email', $user->email,['class' => 'form-control  form-control-type-1 ', 'required' => 'required','autocomplete'=>'OFF','placeholder'=>trans('user.email')] ) !!}
                                            
                                        </div>
                                            @error("email")
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12 ">
                                            <label for="genre" class="genre">{{__("user.sexe")}}</label>
                                        </div>
                                        <div class="col-md-8 mtop10 mbottom15">
                                            @php
                                            $check1=$user->sexe=="H"?"checked":'';
                                            $check2=$user->sexe=="F"?"checked":'';
                                            @endphp
                                            
                                            <label for="sexe1">   {{Form::radio('sexe','H','',['required'=>"required","id"=>"sexe1",$check1])}} {{__("Masculin")}}</label>
                                            <label for="sexe2">   {{Form::radio('sexe','F','',['required'=>"required","id"=>"sexe2",$check2])}} {{__("Féminin")}}</label>
        
                                           
                                        </div>
                                        <div class="col-md-12">
                                            @error("sexe")
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
        
                                    </div>
                                    
                                    <div class="form-group row">
                                        <hr>
                                        <div class="col-md-12">
                                            {!! Form::submit(__('Sauvegarder'), ['class' => 'btn btn-info']) !!}
                                        </div>
                                    </div>
                                </form>

                        
                            </div>

                        </div>
                        
                        
                        
                    </div>
                    <div class="col-xs-12 col-md-4 order-md-2">
                        <div class="alert alert-info text-small" role="alert">
                            <i class="icon-shield"></i>
                            <span>
                                {!!__('user.security_info',['url'=>route('reglement')])!!}
                            </span>
                        </div>
                    </div>
                    <!--end of col-->
                </div>
               <div class="row" id="form-profil">
                   <div class="col-md-12">
                       <hr>
                   </div>
               </div>
               
                <!--end of row-->
            </div>
            <!--end of container-->
        </div>
    </div>

    <script>
    function handleFiles(files) {
      var imageType = /^image\//;
      for (var i = 0; i < files.length; i++) {
      var file = files[i];
      if (!imageType.test(file.type)) {
       // alert({{__("Veuillez sélectionner une image")}});
      }else{
        if(i == 0){
          preview.innerHTML = '';
        }
        var img = document.createElement("img");
        img.classList.add("obj");
        img.file = file;
        preview.appendChild(img); 
        var reader = new FileReader();
        reader.onload = ( function(aImg) { 
        return function(e) { 
        aImg.src = e.target.result; 
      }; 
     })(img);
   
    reader.readAsDataURL(file);
    }
    
    }
   }
    </script>
@endsection