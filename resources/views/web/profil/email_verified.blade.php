@extends('layouts.app')
@section('title')
    {{__('Vérification de mon compte') }} -
@parent
@endsection

@section('nav')
    @include('layouts.partials.simple-nav-dark',['menu'=>"notification"])
    @include('apps.includes.nav.notification')
@endsection
@section('footer')
    @include('layouts.partials.footer-light',['bg'=>''])
@endsection
@section('content')
<style>
    .form-input-code{
        border: 1px solid #dddddd;
        font-size: 35px;
        width: 60px;
        padding: 5px 10px;
    }
    .l-tiret{
        font-size: 35px;
        padding: 5px 10px
    }
</style>
<div class="container minh hmin">
    <div class="row justify-content-center mtop50">
        <div class="col-md-6 col-md-offset-3">
            <div class="card sec_card border_secs  mbottom50">
              
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{route('email_validation')}}">
                        @csrf

                        <div class="form-group row">
                          <div class="col-md-12">
                            <b>{{__("auth.email_v_header")}}</b>
                            <p class="text-xs text-muted ">
                                {!!__("auth.email_v_description",['email'=>Auth::user()->email])!!}
                              </p>
                            <hr>
                          </div>
                            <label for="code" class="col-md-12 col-xs-12 col-form-label text-center text-uppercase mbottom30">
                                 {{__("auth.email_v_label")}} 
                            </label>
                            <div class="col-md-12">
                                @if (count($errors) > 0)
                                <div class="alert alert-danger help-block_ text-danger mbottom30 ">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                
                               
                                <ul class="list-unstyled text-sm bold">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                                </div>
                                @endif
                            </div>
                                
                                
                            <div class="col-md-12 text-center" id="code">
                                <input name="code1" type="text" placeholder="00" maxlength="2" class="form-input-code @error('code1') is-invalid @enderror"  required autocomplete="off" autofocus>
                                <span class="l-tiret">-</span>
                                <input name="code2" type="text" placeholder="00" maxlength="2" class="form-input-code @error('code2') is-invalid @enderror"  required autocomplete="off">
                                <span class="l-tiret">-</span>
                                <input name="code3" type="text" placeholder="00" maxlength="2" class="form-input-code @error('code3') is-invalid @enderror"  required autocomplete="off">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mbottom30 text-center">
                            <div class="col-md-12  col-xs-12 ">
                                <hr>
                                <button type="submit" class="btn btn-md bold btn-xs-block btn-success">
                                    {{ __('Soumettre') }}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
