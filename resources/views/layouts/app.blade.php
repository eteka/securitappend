@include ('layouts.partials.doctype')
    @section('body')
    <body id="app-layout" class="bgsilver cleartop">
    @show
    @section('nav')
        @include('layouts.partials.topnav')
    @show
  <div id="d2u2p2s856sgd">
    @yield('content')
  </div>

@section('jquery')
<script type="text/javascript" src="{{asset('assets/plugins/jquery/jquery-1.12.4.min.js')}}"></script>
@show

@section('bootstrap')
    <script src="{{ asset('storage/assets/plugins/bootstrap/js/bootstrap.min.js')}}" data-turbolinks-eval="false"></script>
    <script>
        $(function(){

            //$("#menu-li-notification").addClass("dblock")
            $('li.drop-menu').on('click', function (e) {
                // do something…
                e.preventDefault();
                var last=".menu";
                //if($(".smenu").hasClass("dblock")){$(".smenu").removeClass('dblock').addClass('hidden');}
                var id=$(this).attr('data-id');
                var menu="#"+id;
                //$(menu).removeClass('hidden').toggleClass("dblock")
                if($(menu).hasClass("dblock")){
                    $(menu).removeClass('dblock');
                }else {
                    $(menu).addClass('dblock');
                    
                }

            });
        })

    </script>
@show

<!--script src="{{ asset('assets/plugins/iclick/instantclick.js')}}"></script>

<script>
  $(function(){
      Turbolinks.setProgressBarDelay(50)
  //pace.start();


 // InstantClick.init(true);
/*
InstantClick.init(50, true);
InstantClick.init('mousedown', true);*/

  })
</script-->
@stack('script')
@stack('dialog')
@yield('plugin')

@section('footer')
@include ('layouts.partials.footer')
@show
