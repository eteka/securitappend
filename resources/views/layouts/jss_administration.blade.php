
<!-- =============== VENDOR SCRIPTS ===============-->
<!-- AUTOSIZE-->
<script src="{{asset('assets/assets/global/plugins/jquery.min.js')}}"></script>
<script src="{{asset('asset/assets/global/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/vendor/autosize/autosize.js')}}"></script>
<script src="{{asset('asset/assets/global/scripts/app.min.js')}}"></script>

{{--<script type="text/javascript" src="{{asset('assets/js/app-admin.js')}}"></script>
<!-- MODERNIZR-->
{{--<script src="{{asset('assets/vendor/modernizr/modernizr.custom.js')}}"></script>--}}
<!-- MATCHMEDIA POLYFILL-->
{{--<script src="{{asset('assets/vendor/matchMedia/matchMedia.js')}}"></script>--}}

<!-- BOOTSTRAP-->

{{--<script src="{{asset('assets/vendor/bootstrap/dist/js/bootstrap.js')}}"></script>--}}
<!-- STORAGE API-->
<script src="{{asset('assets/vendor/jQuery-Storage-API/jquery.storageapi.js')}}"></script>

<script src="{{asset('assets/vendor/screenfull/dist/screenfull.js')}}"></script>

<!-- =============== APP SCRIPTS ===============-->

<script src="{{asset('assets/app/js/app.js')}}"></script>
<script src="{{asset('assets/vendor/pjax/pjax.js')}}"></script>