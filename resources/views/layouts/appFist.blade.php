<!DOCTYPE html>
<html lang="fr">
    <head>
        <title><?php
            if (isset($title)) {
                echo "" . htmlspecialchars($title) . " - Identité médicale Universelle";
            } else {
                echo "IMU | Identité médicale Universelle - KEA Software";
            }
            ?></title>
        <meta charset="utf-8">
        <link rel="SHORTCUT ICON" href="{{ asset('assets/favicon.ico') }}" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="identifier-url" content="" />
        <meta name="title" content=" " />
        <meta name="Description" content="L’Identité Médicale Universelle (IMU) vous permet d'écrire un profil médical qui est accessible à travers l’application mobile en cas d'urgence. Ce profil permet un accès rapide aux informations vitales telles que vos allergies, votre groupage sanguin, les personnes utiles à contacter etc…qui sont essentiels pour les premiers intervenants, les médecins ou le personnel médical qui doivent intervenir. L’application IMU a été créé pour servir les communautés médicales et les populations en offrant des cartes d'identité médicales de qualité qui permettent aux professionnels de la santé de donner un traitement rapide et précis en cas d'urgence. L’application IMU est développée à partir de la société KEA Medicals Pharmaceutics & Technologies, une société de droit Suisse basée à Genève et à but lucratif."/>
        <!-- Style CSS3 -->
        <link rel="stylesheet" href=" {{ asset('assets/plugins/bootstrap/css/bootstrap.css') }}" media="all">
        <link rel="stylesheet" href=" {{ asset('assets/css/main.css') }}" media="all">
        <!-- Javascript -->
        <script src="{{ asset('assets/plugins/jquery/jquery.min.js')}} " ></script>
        <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}" ></script>
        
         <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="notfixed-body">
        <nav class="navbar navbar-default Notnavbar-fixed-top headermenu">
            <div class="container" >
                <div id="page_header">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header col-xs-6">
                        <a class="navbar-brand light pull-left" href="{{ url('/') }}" >
                            
                            <img src="{{ asset('assets/app/kea-logo.png')}}"  alt="KEA">
                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapsenavbar-collapse col-xs-6" id="bs-example-navbar-collapse-1">


                        <ul class="nav navbar-nav navbar-right pull-right">
                     @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Connexion</a></li>
                        
                        <li><a href="{{ url('/register') }}"><button  class="btn rond100 btn-primary btn-sm  rond mtop-3">Créer un compte</button></a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ "Bonjour, ".Auth::user()->prenom }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/profil') }}"><i class="fa fa-btn fa-sign-out"></i>Profil</a></li>
                                <li><a href="{{ url('/imu/user') }}"><i class="fa fa-btn fa-sign-out"></i>Mon compte</a></li>
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                        </ul>
                    </div>
                </div>
                </li>
                            
                     </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container -->
            </div>
        </nav>

        <div class="body">
            
            @yield('content')
        </div>
   
    </body>
</html>
