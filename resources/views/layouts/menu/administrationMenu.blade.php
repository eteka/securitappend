<ul class="nav nav-pills nav-stacked">
    <li ><a href="{{url('/')}}"><span class="icone"><img src="{{asset('assets/app/admin/menu/home.png')}}" ></span><span class="menu_name">Site Web</span></a></li>
    <li class="{{(isset($murl) && $murl=='/')?'active':''}}"><a href="{{url('admin')}}"><span class="icone"><img src="{{asset('assets/app/admin/menu/dashboard.png')}}" ></span><span class="menu_name">Tableau de Bord</span></a></li>
    <li class="{{(isset($murl) && ($murl=='accounts'|| $murl=='account'))?'active':''}}"><a href="{{route('indexAccounts')}}" class="left-tooltip" data-tooltip="Compte IMU" title="Compte IMU"><span class="icone"> <img src="{{asset('assets/app/admin/menu/account.png')}}"  ></span><span class="menu_name">Comptes</span></a></li>
    <li class="{{(isset($murl) && ($murl=='hopitals' || $murl=='hopital'))?'active':''}}"><a href="{{url('admin/hopitals')}}" class="left-tooltip" data-tooltip="Hôpital" title="Ambulance"><span class="icone"> <img src="{{asset('assets/app/admin/menu/clinic.png')}}"  ></span><span class="menu_name">Hopitaux</span></a></li>
<!--<li class="{{(isset($murl) && ($murl=='type-hopitals'))?'active':''}}"><a href="{{url('admin/type-hopitals')}}" class="left-tooltip" data-tooltip="Hôpital" title="Ambulance"><span class="icone"> <img src="{{asset('assets/app/admin/menu/clinic.png')}}"  ></span><span class="menu_name">Hopitaux</span></a></li>-->
    <li class="{{(isset($murl) && ($murl=='pharmacies'))?'active':''}}"><a href="{{url('admin/pharmacies')}}" class="left-tooltip" data-tooltip="Hôpital" title="Pharmacies"><span class="icone"> <img src="{{asset('assets/app/admin/menu/clinic.png')}}"  ></span><span class="menu_name">Pharmarcies</span></a></li>

    <li class="{{(isset($murl) && ($murl=='medecins'|| $murl=='doctor'))?'active':''}}"><a href="{{url('admin/agent/dossier')}}" class="left-tooltip" data-tooltip="Médecin" title="Dossier des agents"><span class="icone"> <img src="{{asset('assets/app/admin/menu/doctor.png')}}"></span><span class="menu_name">Dossiers des agents</span></a></li>

    <li class="{{(isset($murl) && ($murl=='patients'|| $murl=='patient'))?'active':''}}"><a href="{{url('admin/patients')}}" class="left-tooltip" data-tooltip="Patient hospitalisé" title="Patient hospitalisé"><span class="icone"> <img src="{{asset('assets/app/admin/menu/patient.png')}}" ></span><span class="menu_name">Patients</span></a></li>
    <li  class="{{(isset($murl) && ( $murl=='rendez-vous'))?'active':''}}"><a href="{{url('admin/rendez-vous')}}" class="left-tooltip" data-tooltip="Rendez-vous" title="Rendez-vous"><span class="icone"> <img src="{{asset('assets/app/admin/menu/rdv.png')}}"  ></span><span class="menu_name">Rendez-vous</span></a></li>
<!--<li><a href="{{url('treatspatient')}}" class="left-tooltip" data-tooltip="Patient traité" title="Patient traité"><span class="icone"> <img src="{{asset('assets/app/admin/menu/treat_patient.png')}}"  ></span><span class="menu_name">Patient admis</span></a></li>-->

<!--<li class="{{(isset($murl) && ($murl=='nurses' || $murl=='nurse'))?'active':''}}"><a href="{{route('indexNurses')}}" class="left-tooltip" data-tooltip="Infirmier" title="Infirmier"><span class="icone"> <img src="{{asset('assets/app/admin/menu/nurse.png')}}"  ></span><span class="menu_name">Infirmier</span></a></li>-->

<!--<li class="{{(isset($murl) && ($murl=='supportstaffs' || $murl=='supportstaff'))?'active':''}}"><a href="{{route('indexStaffs')}}" class="left-tooltip" data-tooltip="Personnel " title="Personnel "><span class="icone"> <img src="{{asset('assets/app/admin/menu/support.png')}}" ></span><span class="menu_name">Personnels</span></a></li>-->

<!--li class="{{(isset($murl) && ($murl=='pharmacists' || $murl=='pharmacist'))?'active':''}}">
                                <a href="{{route('indexPharmacists')}}" class="left-tooltip" data-tooltip="Pharmaciens" title="Pharmaciens"><span class="icone"> <img src="{{asset('assets/app/admin/menu/pharmacien.png')}}" ></span><span class="menu_name">Pharmaciens</span></a></li-->

    <li class="{{(isset($murl) && ($murl=='labo-staffs' || $murl=='labo-staff'))?'active':''}}">
    <!--<a href="{{route('indexLStaffs')}}" class="left-tooltip" data-tooltip="Personnel de laboratoire" title="Personnel de laboratoire"><span class="icone"> <img src="{{asset('assets/app/admin/menu/laboratoire.png')}}" ></span><span class="menu_name">Personnel de laboratoire</span></a></li>-->

    <li class="{{(isset($murl) && ($murl=='accountants' || $murl=='accountant'))?'active':''}}" >
    <!--<a href="{{route('indexAccountants')}}" class="left-tooltip" data-tooltip="Comptable" title="Comptable"><span class="icone"> <img src="{{asset('assets/app/admin/menu/accountant.png')}}"  ></span><span class="menu_name">Comptable</span></a></li>-->



    <!--<li><a href="{{url('treatment')}}" class="left-tooltip" data-tooltip="Traitement" title="Traitement"><span class="icone"> <img src="{{asset('assets/app/admin/menu/treatment.png')}}"  ></span><span class="menu_name">Traitement</span></a></li>-->

    <!--<li><a href="{{url('prescription')}}" class="left-tooltip" data-tooltip="Ordonnance" title="Ordonnance"><span class="icone"> <img src="{{asset('assets/app/admin/menu/prescription.png')}}" ></span><span class="menu_name">Ordonnance</span></a></li>-->
    <li><a href="{{url('/admin/kea-qr-codes/')}}" class="left-tooltip" data-tooltip="Ordonnance" title="Ordonnance"><span class="icone"> <img src="{{asset('assets/app/admin/menu/qrcode.png')}}" ></span><span class="menu_name">QR Code</span></a></li>

<!--<li><a href="?dashboard=user&amp;page=bedallotment" class="left-tooltip" data-tooltip="Assignation Lit-Infirmiers" title="Assignation Lit-Infirmière"><span class="icone"> <img src="{{asset('assets/app/admin/menu/hospital_bed.png')}}" ></span><span class="menu_name">Assignation Lit</span></a></li>-->

<!--li><a href="?dashboard=user&amp;page=operation" class="left-tooltip" data-tooltip="Liste des opérations" title="Liste des opérations"><span class="icone"> <img src="{{asset('assets/app/admin/menu/operation.png')}}"  ></span><span class="menu_name">Liste des opérations</span></a></li-->

<!--<li><a href="?dashboard=user&amp;page=diagnosis" class="left-tooltip" data-tooltip="Diagnostic" title="Diagnostic"><span class="icone"> <img src="{{asset('assets/app/admin/menu/diagnostic.png')}}"  ></span><span class="menu_name">Diagnostic</span></a></li>-->

<!--li><a href="?dashboard=user&amp;page=bloodbank" class="left-tooltip" data-tooltip="Banque de sang" title="Banque de sang"><span class="icone"> <img src="{{asset('assets/app/admin/menu/blood-bank.png')}}"  ></span><span class="menu_name">Banque de sang</span></a></li-->





<!--li><a href="?dashboard=user&amp;page=event" class="left-tooltip" data-tooltip="Evénement" title="Evénement"><span class="icone"> <img src="{{asset('assets/app/admin/menu/notice.png')}}"  ></span><span class="menu_name">Evénement</span></a></li>

                            <li><a href="?dashboard=user&amp;page=message" class="left-tooltip" data-tooltip="Message" title="Message"><span class="icone"> <img src="{{asset('assets/app/admin/menu/message.png')}}" pagespeed_url_hash="1100635938" ></span><span class="menu_name">Message</span></a></li>

                            <li><a href="?dashboard=user&amp;page=ambulance" class="left-tooltip" data-tooltip="Ambulance" title="Ambulance"><span class="icone"> <img src="{{asset('assets/app/admin/menu/ambulance.png')}}"  ></span><span class="menu_name">Ambulance</span></a></li>
                            <li><a href="?dashboard=user&amp;page=ambulance" class="left-tooltip" data-tooltip="Ambulance" title="Ambulance"><span class="icone"> <img src="{{asset('assets/app/admin/menu/gymnast.png')}}"  ></span><span class="menu_name">Centre de gymnastique</span></a></li>
                            <li><a href="?dashboard=user&amp;page=ambulance" class="left-tooltip" data-tooltip="Ambulance" title="Ambulance"><span class="icone"> <img src="{{asset('assets/app/admin/menu/clinic.png')}}"  ></span><span class="menu_name">Clinique</span></a></li>

                            <li><a href="?dashboard=user&amp;page=report" class="left-tooltip" data-tooltip="Rapport" title="Rapport"><span class="icone"> <img src="{{asset('assets/app/admin/menu/report.png')}}" pagespeed_url_hash="2238844459" ></span><span class="menu_name">Rapport</span></a></li-->

    <li><a href="?dashboard=user&amp;page=account" class="left-tooltip" data-tooltip="Mon compte " title="Mon compte"><span class="icone"> <img src="{{asset('assets/app/admin/menu/setting.png')}}"  ></span><span class="menu_name">Paramètres</span></a></li>


</ul>