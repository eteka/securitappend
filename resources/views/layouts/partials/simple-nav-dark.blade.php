<nav class="navbar navbar-headers  navbar-fixed-top navbar-standard2">
      <div class="container pad0-xs">
          <div class="navbar-headesr pad0-xs col-sm-2 col-md-2 col-lg-4 pad0">

              <!-- Collapsed Hamburger -->
              <button type="button" class="navbar-togglecollapsed hidden" data-toggle="collapse" data-target="#app-navbar-collapse">
                  <span class="sr-only">Menu</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
              @if (!Auth::guest())

              <!--ul class="list-unstyled list-inline text-white hidden visible-xs xs-menu arial">


                  <li><a href="{{ url('userProfil') }}"><i class="fa fa-user"></i>  {{ Auth::user()->prenom }}</a></li>
                  <li>|</li>
                  <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> <span class="hidden">Accueil</span></a></li>
                  <li>|</li>
                  <li><a href="{{ url('/') }}"><i class="fa fa-bell-o"></i> <span class="hidden">Notification</span></a></li>


                  <li>|</li>
                  <li><a href="{{ url('/') }}"><i class="fa fa-sign-out"></i></a></li>
              </ul-->
              <!--ul class="nav navbar-nav navbar-left pad0">

              </ul-->
              @endif
              <!-- Branding Image -->
              <a class="navbar-brand light pull-left" href="{{ url('/') }}" >
                  <img src="{{ asset('storage/assets/app/logo-white.png')}}" class="hidden-xs hidden-sm"  alt="{{config('app.name')}}">
                  <img src="{{ asset('storage/assets/statics/app/logo-xs-sec.png')}}" class="visible-xs visible-sm"  alt="{{config('app.name')}}">

              </a>
              <!--div id="langdiv">
                  <div id="google_translate_element"></div>
              </div-->
          </div>

          @if (!Auth::guest())

          <div class="col-xs-12 col-sm-5 hidden-xs  col-md-5 col-lg-4 " id="menu-l-search">
              <div id="search-input-cover">
                  <form class="navbar-formnavbar-left" method="GET" action="/" role="search">

                      <div class="input-group">
                          <div class="hidden"></div>


                          <input type="text" required="required" name="query" autocomplete="off" value="{{Request::get('query')}}" class="form-control input-sm" id="search-input" placeholder="Rechercher ...">
                          <i id="search-add-btn" class=" input-group-addon  icon-magnifier text-white text-w no-bg"><button type="submit" id="search_btn"><span class="fa fa-search"></span></button></i> 

                      </div>
                  </form>
              </div>
          </div>
          @else
          <div class="visible-lg col-lg-4 col-sm-4 col-md-4">

          </div>
          @endif

          <div class="collapsenavbar-collapse col-xs-9 col-sm-5 col-lg-4" id="app-navbar-collapse">

            <div id="topnavbar-cover">

              <!-- Left Side Of Navbar -->
              <!--ul class="nav navbar-nav">
                  <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Accueil</a></li>
              </ul-->

              <!--ul class="nav navbar-nav navbar-right">

                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </li>
              </ul-->
              <!-- Right Side Of Navbar -->
              <ul id="main_nav" class="nav navbar-nav hidden-x  navbar-right text-right ">
                  <!-- Authentication Links -->
                  @if (Auth::guest())
                  <li><a class="" href="{{ url('/login') }}">{{__("Connexion")}}</a></li>
                  <li><a class="" href="{{ url('/register') }}">{{__("S'inscrire")}}</a></li>
                  @else
                  <!--li><a href="#"><span class="fa  fa-home"></span></a></li>
                  <li><a href="#"><span class="fa  fa-star"></span></a></li-->
                      <li class="hidden-xs"><a title="{{__('Ajouter')}}" href="{{route('create_bien')}}"><span class="sec-nav-icone icone-for-dark">
                      <!--svg class="svg-icon" viewBox="0 0 20 20">
                          <path d="M14.613,10c0,0.23-0.188,0.419-0.419,0.419H10.42v3.774c0,0.23-0.189,0.42-0.42,0.42s-0.419-0.189-0.419-0.42v-3.774H5.806c-0.23,0-0.419-0.189-0.419-0.419s0.189-0.419,0.419-0.419h3.775V5.806c0-0.23,0.189-0.419,0.419-0.419s0.42,0.189,0.42,0.419v3.775h3.774C14.425,9.581,14.613,9.77,14.613,10 M17.969,10c0,4.401-3.567,7.969-7.969,7.969c-4.402,0-7.969-3.567-7.969-7.969c0-4.402,3.567-7.969,7.969-7.969C14.401,2.031,17.969,5.598,17.969,10 M17.13,10c0-3.932-3.198-7.13-7.13-7.13S2.87,6.068,2.87,10c0,3.933,3.198,7.13,7.13,7.13S17.13,13.933,17.13,10"></path>
                      </svg-->
                     <span class="btn btn-success btn-xs bold"><i class="icon-plus"></i> {{__('Ajouter')}}</span>
                  </span></a>
                      </li>
                      <li  title="{{__('Rechercher')}} " class="drop-menu hidden-sm hidden-md hidden-lg" data-id="menu-l-search">
                          <a href="{{route('dashboard')}}"><span class="sec-nav-icone icone-for-dark">

                          <svg class="svg-icon" viewBox="0 0 20 20">
							<path fill="none" d="M18.109,17.776l-3.082-3.081c-0.059-0.059-0.135-0.077-0.211-0.087c1.373-1.38,2.221-3.28,2.221-5.379c0-4.212-3.414-7.626-7.625-7.626c-4.212,0-7.626,3.414-7.626,7.626s3.414,7.627,7.626,7.627c1.918,0,3.665-0.713,5.004-1.882c0.006,0.085,0.033,0.17,0.098,0.234l3.082,3.081c0.143,0.142,0.371,0.142,0.514,0C18.25,18.148,18.25,17.918,18.109,17.776zM9.412,16.13c-3.811,0-6.9-3.089-6.9-6.9c0-3.81,3.089-6.899,6.9-6.899c3.811,0,6.901,3.09,6.901,6.899C16.312,13.041,13.223,16.13,9.412,16.13z"></path>
						</svg>
                      </span>
                          </a>
                      </li>

                      <li title="{{__('Accueil')}} ">
                          <a href="{{route('dashboard')}}"><span class="sec-nav-icone icone-for-dark">
                      <!--svg class="svg-icon" title="{{__('Accueil')}}" viewBox="0 0 20 20">
                          <path fill="none" d="M15.971,7.708l-4.763-4.712c-0.644-0.644-1.769-0.642-2.41-0.002L3.99,7.755C3.98,7.764,3.972,7.773,3.962,7.783C3.511,8.179,3.253,8.74,3.253,9.338v6.07c0,1.146,0.932,2.078,2.078,2.078h9.338c1.146,0,2.078-0.932,2.078-2.078v-6.07c0-0.529-0.205-1.037-0.57-1.421C16.129,7.83,16.058,7.758,15.971,7.708z M15.68,15.408c0,0.559-0.453,1.012-1.011,1.012h-4.318c0.04-0.076,0.096-0.143,0.096-0.232v-3.311c0-0.295-0.239-0.533-0.533-0.533c-0.295,0-0.534,0.238-0.534,0.533v3.311c0,0.09,0.057,0.156,0.096,0.232H5.331c-0.557,0-1.01-0.453-1.01-1.012v-6.07c0-0.305,0.141-0.591,0.386-0.787c0.039-0.03,0.073-0.066,0.1-0.104L9.55,3.75c0.242-0.239,0.665-0.24,0.906,0.002l4.786,4.735c0.024,0.033,0.053,0.063,0.084,0.09c0.228,0.196,0.354,0.466,0.354,0.76V15.408z"></path>
                      </svg-->
                          <svg class="svg-icon" title="{{__('Accueil')}}" viewBox="0 0 20 20">
                          <path d="M18.121,9.88l-7.832-7.836c-0.155-0.158-0.428-0.155-0.584,0L1.842,9.913c-0.262,0.263-0.073,0.705,0.292,0.705h2.069v7.042c0,0.227,0.187,0.414,0.414,0.414h3.725c0.228,0,0.414-0.188,0.414-0.414v-3.313h2.483v3.313c0,0.227,0.187,0.414,0.413,0.414h3.726c0.229,0,0.414-0.188,0.414-0.414v-7.042h2.068h0.004C18.331,10.617,18.389,10.146,18.121,9.88 M14.963,17.245h-2.896v-3.313c0-0.229-0.186-0.415-0.414-0.415H8.342c-0.228,0-0.414,0.187-0.414,0.415v3.313H5.032v-6.628h9.931V17.245z M3.133,9.79l6.864-6.868l6.867,6.868H3.133z"></path>
                      </svg>
                      </span>
                          </a>
                      </li>


                      <li class="dropdown_drop-menu " data-id="menu-li-notification" >

                      <a title="{{__('Notification')}}"  href="{{route('notification')}}">
                              <span class="ibadge ibadge-danger">60</span>
                              <span class="sec-nav-icone icone-for-dark"><svg class="svg-icon" viewBox="0 0 20 20">
				<path d="M14.38,3.467l0.232-0.633c0.086-0.226-0.031-0.477-0.264-0.559c-0.229-0.081-0.48,0.033-0.562,0.262l-0.234,0.631C10.695,2.38,7.648,3.89,6.616,6.689l-1.447,3.93l-2.664,1.227c-0.354,0.166-0.337,0.672,0.035,0.805l4.811,1.729c-0.19,1.119,0.445,2.25,1.561,2.65c1.119,0.402,2.341-0.059,2.923-1.039l4.811,1.73c0,0.002,0.002,0.002,0.002,0.002c0.23,0.082,0.484-0.033,0.568-0.262c0.049-0.129,0.029-0.266-0.041-0.377l-1.219-2.586l1.447-3.932C18.435,7.768,17.085,4.676,14.38,3.467 M9.215,16.211c-0.658-0.234-1.054-0.869-1.014-1.523l2.784,0.998C10.588,16.215,9.871,16.447,9.215,16.211 M16.573,10.27l-1.51,4.1c-0.041,0.107-0.037,0.227,0.012,0.33l0.871,1.844l-4.184-1.506l-3.734-1.342l-4.185-1.504l1.864-0.857c0.104-0.049,0.188-0.139,0.229-0.248l1.51-4.098c0.916-2.487,3.708-3.773,6.222-2.868C16.187,5.024,17.489,7.783,16.573,10.27"></path>
                  </svg></span>

                          </a>

                      </li>
                      <!--ul  class=" user-menu smenu sec_dropdown"  id="menu-li-notification" >
                          
                          <li class="separator">{{__('COMPTE')}}</li>
                          <li class="line-menu"><a href="{{ url('user/setting')}}"><span class="icon-settings"></span> {{__('Paramètre du compte')}}</a></li>
                          <li class="line-menu"><a href="{{ url('user/setting')}}"><span class="icon-info"></span> {{__('Aide et Assistance')}}</a></li>
                          <li class="line-menu"><a href="{{ url('user/setting')}}"><span class="icon-list"></span> {{__('Langue')}}</a></li>
                          <li class="separator">{{__('MANAGEMENT')}}</li>
                      </ul-->
                      <!--li class="dropdown">


                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                          <span class="sec-nav-icone icone-for-dark">
                          <svg version="1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 0C5.383 0 0 5.383 0 12c0 3.18 1.232 6.177 3.47 8.438A11.913 11.913 0 0 0 12 24c3.234 0 6.268-1.269 8.542-3.574A11.924 11.924 0 0 0 24 12c0-6.617-5.383-12-12-12zM4.655 20.17c1.029-.563 2.533-1.111 4.002-1.644.514-.186 1.027-.372 1.52-.559a.5.5 0 0 0 .323-.468V15a.511.511 0 0 0-.335-.472C10.118 14.511 9 14.072 9 12a.48.48 0 0 0-.482-.494c-.047-.032-.138-.218-.138-.506s.091-.474.12-.5A.5.5 0 0 0 9 10c0-.125-.037-.256-.118-.544-.147-.523-.537-1.913-.194-2.366.038-.051.245-.194.714-.1.26.055.518-.11.583-.369.163-.649 1.326-1.121 2.765-1.121s2.602.472 2.765 1.121c.229.917-.178 2.188-.373 2.799-.098.305-.142.443-.142.58a.48.48 0 0 0 .482.494c.046.032.137.218.137.506s-.091.474-.12.5a.5.5 0 0 0-.5.5c0 2.072-1.118 2.511-1.158 2.526A.5.5 0 0 0 13.5 15v2.5a.5.5 0 0 0 .324.468c.543.204 1.086.399 1.614.591 1.51.546 2.943 1.067 3.919 1.599A10.901 10.901 0 0 1 12 23a10.908 10.908 0 0 1-7.345-2.83zm15.453-.739c-1.047-.625-2.583-1.181-4.33-1.812l-1.278-.466v-1.848c.501-.309 1.384-1.108 1.49-2.936.386-.226.63-.727.63-1.37 0-.579-.198-1.043-.52-1.294.243-.757.681-2.144.385-3.327C16.138 4.992 14.256 4.5 12.75 4.5c-1.342 0-2.982.391-3.569 1.456-.707-.042-1.095.273-1.29.53-.635.838-.216 2.368.02 3.211-.329.249-.531.718-.531 1.303 0 .643.244 1.144.63 1.37.106 1.828.989 2.627 1.49 2.936v1.848l-1.184.433c-1.624.589-3.299 1.196-4.413 1.858A10.928 10.928 0 0 1 1 12C1 5.935 5.935 1 12 1s11 4.935 11 11c0 2.774-1.024 5.397-2.892 7.431z"></path></svg>
                      </span>
                          </a>
                      </li-->


                  <!--li><a href="#"><span class="sec-nav-icone icone-for-dark"><svg class="svg-icon" viewBox="0 0 20 20">
				<path d="M14.999,8.543c0,0.229-0.188,0.417-0.416,0.417H5.417C5.187,8.959,5,8.772,5,8.543s0.188-0.417,0.417-0.417h9.167C14.812,8.126,14.999,8.314,14.999,8.543 M12.037,10.213H5.417C5.187,10.213,5,10.4,5,10.63c0,0.229,0.188,0.416,0.417,0.416h6.621c0.229,0,0.416-0.188,0.416-0.416C12.453,10.4,12.266,10.213,12.037,10.213 M14.583,6.046H5.417C5.187,6.046,5,6.233,5,6.463c0,0.229,0.188,0.417,0.417,0.417h9.167c0.229,0,0.416-0.188,0.416-0.417C14.999,6.233,14.812,6.046,14.583,6.046 M17.916,3.542v10c0,0.229-0.188,0.417-0.417,0.417H9.373l-2.829,2.796c-0.117,0.116-0.71,0.297-0.71-0.296v-2.5H2.5c-0.229,0-0.417-0.188-0.417-0.417v-10c0-0.229,0.188-0.417,0.417-0.417h15C17.729,3.126,17.916,3.313,17.916,3.542 M17.083,3.959H2.917v9.167H6.25c0.229,0,0.417,0.187,0.417,0.416v1.919l2.242-2.215c0.079-0.077,0.184-0.12,0.294-0.12h7.881V3.959z"></path>
                  </svg></span></a>
                  </li-->


                      <li  class="dropdown_  drop-menu" id="menu-li-profil" data-id="user-menu">


                          <a href="#" title="{{__("Gestion de mon compte")}}" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <span class="sec-nav-icone icone-for-dark">
                          <svg version="1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 0C5.383 0 0 5.383 0 12c0 3.18 1.232 6.177 3.47 8.438A11.913 11.913 0 0 0 12 24c3.234 0 6.268-1.269 8.542-3.574A11.924 11.924 0 0 0 24 12c0-6.617-5.383-12-12-12zM4.655 20.17c1.029-.563 2.533-1.111 4.002-1.644.514-.186 1.027-.372 1.52-.559a.5.5 0 0 0 .323-.468V15a.511.511 0 0 0-.335-.472C10.118 14.511 9 14.072 9 12a.48.48 0 0 0-.482-.494c-.047-.032-.138-.218-.138-.506s.091-.474.12-.5A.5.5 0 0 0 9 10c0-.125-.037-.256-.118-.544-.147-.523-.537-1.913-.194-2.366.038-.051.245-.194.714-.1.26.055.518-.11.583-.369.163-.649 1.326-1.121 2.765-1.121s2.602.472 2.765 1.121c.229.917-.178 2.188-.373 2.799-.098.305-.142.443-.142.58a.48.48 0 0 0 .482.494c.046.032.137.218.137.506s-.091.474-.12.5a.5.5 0 0 0-.5.5c0 2.072-1.118 2.511-1.158 2.526A.5.5 0 0 0 13.5 15v2.5a.5.5 0 0 0 .324.468c.543.204 1.086.399 1.614.591 1.51.546 2.943 1.067 3.919 1.599A10.901 10.901 0 0 1 12 23a10.908 10.908 0 0 1-7.345-2.83zm15.453-.739c-1.047-.625-2.583-1.181-4.33-1.812l-1.278-.466v-1.848c.501-.309 1.384-1.108 1.49-2.936.386-.226.63-.727.63-1.37 0-.579-.198-1.043-.52-1.294.243-.757.681-2.144.385-3.327C16.138 4.992 14.256 4.5 12.75 4.5c-1.342 0-2.982.391-3.569 1.456-.707-.042-1.095.273-1.29.53-.635.838-.216 2.368.02 3.211-.329.249-.531.718-.531 1.303 0 .643.244 1.144.63 1.37.106 1.828.989 2.627 1.49 2.936v1.848l-1.184.433c-1.624.589-3.299 1.196-4.413 1.858A10.928 10.928 0 0 1 1 12C1 5.935 5.935 1 12 1s11 4.935 11 11c0 2.774-1.024 5.397-2.892 7.431z"></path></svg>
                      </span>
                          </a>

                      </li>


                      <ul  class=" user-menu smenu" aria-labelledby="dLabel"  id='user-menu'>
                          <li >
                              <div>
                                  <div class="col-xs-3">
                                   
                                      <a class="pull-left" tabindex="-1" href="{{Auth::user()->profil()}}"><img alt="" src="{{asset(Auth::user()->avatar())}}"  class="avatar avatar-64 photo" class="img-responsive">
                                      </a>
                                  </div>
                                  <div class="menu-wraps col-xs-9">
                                      <ul class="list-unstyled">
                                          <li class="u-name"><a href="{{Auth::user()->profil()}}"><span class="display-name">{{Auth::user()->fullname()}}</span></a></li>
                                          <li  class="display-poste"><span>{{Auth::user()->poste}}</span></li>
                                          <li id="user-menu-edit-btn"><a class="btn btn-link btn-xs mtop5 mbottom10" href="{{route('edit_profil',Auth::user()->pseudo?Auth::user()->pseudo:Auth::user()->id)}}"><span class="icon-note"></span> {{__('Modifier')}}</a></li>
                                      </ul>
                                  </div>
                              </div>
                          </li>
                          <li class="separator">{{__('COMPTE')}}</li>
                          <li class="line-menu"><a href="{{route('show_edit_parametre',Auth::user()->getPseudo())}}"><span class="icon-settings"></span> {{__('Paramètre du compte')}}</a></li>
                          <li class="line-menu"><a href="{{route('show_edit_password',Auth::user()->getPseudo())}}"><span class="icon-lock"></span> {{__('Changer mot de passe')}}</a></li>
                          <li class="line-menu"><a href="{{ route('lang_settings')}}"><span class="icon-list"></span> {{__('Langue et apparence')}}</a></li>
                          <li class="separator">{{__('MANAGEMENT')}}</li>

                          <li class="line-menu"><a href="{{ route('show_historique',Auth::user()->getPseudo())}}"><span class="icon-lock"></span> {{__('Historique de mes activités')}}</a></li>
                          <!--li class="line-menu"><a href="{{ route('account_security')}}"><span class="icon-lock"></span> {{__('Gestion de la sécurité')}}</a></li-->
                          <li class="line-menu"><a href="{{ route('help_center')}}"><span class="icon-info"></span> {{__('Aide et Assistance')}}</a></li>
                          
                          <!--li class="line-menu"><a href="{{ route('account_business')}}"><span class="icon-plane"></span> {{__('Compte Business')}}</a></li-->
                      <!--li><a href="{{ url('services/qr-code/')}}"><span class="fa fa-qrcode"></span> Mode code </a></li-->
                          <li class="line-menu"><a onclick="event.preventDefault(); document.getElementById('logout-form').submit();" href="{{ url('logout')}}" class="text-muted"><span class="icon-logout"></span> {{__("Se déconnecter")}}</a></li>


                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              {{ csrf_field() }}
                          </form>
                      </ul>

                  @endif
              </ul>
          </div>
            </div>
        </div>
  </nav>

<div class="visible-xs cover-btn-add-xs">
    <a href="{{route('create_bien')}}" class="btn btn-success btn-md btn-foot-add"><i class="icon-plus"></i> Ajouter</a>
</div>
<style>
    .cover-btn-add-xs{
        position: fixed;
        bottom: 0;
        padding: 10px 15px;
        background: #ffffff;
        width: 100%;
        text-align: center;
        border-top: 1px solid #dedede;
        z-index: 10;
    }
    .btn-foot-add{
        width: 100%;
        background-color: #28a745;
        background-image: linear-gradient(-180deg,#34d058,#28a745 100%);
    }
</style>

