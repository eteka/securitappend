<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'nom'   => 'Nom',
    'prenom' => 'Prénom',
    'profil_edit'=>"Paramètres de mon compte",
    'profil_parametre'=>"Paramètre du compte",
    "general"=>"Général",
    "general"=>"Général",
    "password"=>"Mot de passe",
    "parametre"=>"Paramètres",
    "email"=>"Email",
    "ville_name"=>"San Francisco",
    "sexe"=>"Sexe",
    "porfil_id"=>"Identifiant du profil",
    "ville"=>"Ville",
    "pays"=>"Pays",
    "site_web"=>"Site Web",
    "apropos"=>"A propos de moi",
    "profil_public"=>"Profil public",
    ];
