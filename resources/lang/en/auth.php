<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    "register-info"=>"Are you worried about the sudden loss of your property? <br> SecuritAppend is the platform for free backup of information on your property and protects it so as not to lose an extraordinary object in your life.",
    "conditions"=>'By clicking on "Create my account" you accept <a href=":url1">our terms of the contract, the conditions of use</a> and the <a href=":url2">data security policy</a>.',

    "password-info"=>"The password must include at least 8 characters including upper and lower case, numbers and special characters.",

];
