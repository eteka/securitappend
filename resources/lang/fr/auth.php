<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Adresse email ou mot de passe non valide',
    'throttle' => 'Trop de tentatives de connexion. Veuillez essayer de nouveau dans :seconds secondes.',
    "register-info"=>"Êtes vous inquiet de la perte soudaine de vos biens ? <br> SecuritAppend est la plateforme de sauvegarde gratuit des informations sur vos biens et les protège afin de ne pas perdre un objet extraordinaires de votre vie.",
    "conditions"=>'En vous inscrivant, vous acceptez nos  <a href=":url1"> Conditions d’utilisation</a> et notre <a href=":url2">Politique de confidentialité.</a> ',
    "password-info"=>"Votre mot de passe doit inclure au moins 8 caractères comportants des majuscules(A-Z), minuscules(a-z), chiffres (0-9) et caractères spéciaux(!$#%).",
    "email_v_header"=>"Validation de votre compte",
    "email_v_description"=>"Un code de validation de compte à été envoyé à l'adresse <b>:email</b>. Veuillez vous connecter à ce compte email et cliquer sur le lien de validation ou saisir ci-dessous le code qui vous a été envoyé",
    "email_v_label"=>"Saisir le code de vérification",
    "reinitialisation_header"=>"Réinitialisation de mot de passe",
    "reinitialisation_description"=>"Veuillez rensignez l'email que vous avez utilisé lors de la création de votre compte. Un email vous sera ensuite envoyé afin de vous aider à récupérer votre compte.",
];
