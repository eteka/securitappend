<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'nom'   => 'Nom',
    'prenom' => 'Prénom',
    'profil_edit'=>"Paramètres de mon compte",
    'profil_edition'=>"Modification de mon compte",
    'profil_parametre'=>"Paramètre du compte",
    'profil_parametre_edit'=>"Modification des paramètres du compte",
    'password_edit'=>"Changement de mot de passe",
    "general"=>"Général",
    "image_info"=>"Pour de meilleurs résultats, utilisez une image d'au moins 256 pixels sur 256 pixels au format .jpg ou .png",
    "general"=>"Général",
    "select_image"=>"Changer ma photo de profil",
    "password"=>"Mot de passe",
    "parametre"=>"Paramètres",
    "email"=>"Email",
    "ville_name"=>"San Francisco",
    "sexe"=>"Sexe",
    "porfil_id"=>"Identifiant du profil",
    "profil_id_info"=>"Ce lien représente celui utilisé pour accéder à votre profil. Il est public et modifiable une seule fois.",
    "ville"=>"Ville",
    "pays"=>"Pays",
    "security_info"=>"Nous travaivailons à offrir une sécurité et un meilleure expérience à nos utilisateurs. Le point clé de notre partenariat reste la confiance. Consultez notre <a href=':url'> politique de confidentialité</a>",
    "profil_edit_info"=>"Les informations que vous renseignez dans cette section apparaitont sur votre profil public. Un bon profil vous permet de trouver des personnes avec qui vous avez les mêmes intérêts</a>",
    "show_profil"=>"Consulter mon profil",
    "s_en"=>"Your information is always kept encrypted and can never be accessed by third parties.",
    "p_en"=>"This information will appear on your public profile. A detailed public profile helps users with similar professional interests and location to connect with you. ",
    "site_web"=>"Site Web",
    "poste"=>"Occupation actuelle",
    "apropos_info"=>"Décrivez ici vos aspirations et vos valeurs",
    "apropos"=>"A propos de moi",
    "profil_public"=>"Profil public",
    "profil_public_info"=>'Votre profil est par défaut "PUBLIC". Si vous décidez de la rendre "PRIVÉ", il suffit de décocher ce bouton. Ainsi, votre profil, n\'appraitra plus dans les résultats de recherche selon nos <a href=":url">conditions d\'utilisation</a>.',
    "lastpassrword"=>"Ancien mot de passe",
    "passrword"=>"Mot de passe",
    "lastpassword"=>"Ancien mot de passe",
    "newpassword"=>"Nouveau mot de passe",
    "change_password"=>"Changement de mot de passe",
    "password_confirmation"=>"Confirmation du mot de passe",
    "newpassword_info"=>"Après le changement de votre mot de passe, vous serez déconnecté. Vous allez devoir vous connecter à nouveau avec votre nouveau mot de passe.",
    "lastpaswword_incorrect"=>"Votre ancien mot de passe ne correspond pas à celui de votre compte.Veuillez réessayer !",
    "samepaswword_incorrect"=>"Votre nouveau mot de passe doit être différent de l'ancien. Veuillez réessayer !",
    "password_success"=>"Modification de mot de passe effectué avec succès",
    "se_last"=>"Your current password does not matches with the password you provided. Please try again.",
    "account_valide"=>"Validation de compte réussie.",
    "valide_account_header"=>"Compte validé avec succès",
    "valide_account_info"=>"Nous nous éfforçons d'assurer que nos utilisateur sont des personnes réelles et non des robots. Vous pouvez désormais accéder à votre compte en toute sécurité.<br> Vous pouvez accéder à votre profil et complèter vos information dès maintenant.",
];
